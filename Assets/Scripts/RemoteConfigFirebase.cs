using UnityEngine;
using System;
using System.Threading.Tasks;
public class RemoteConfigFirebase
{
    public void InitializeFirebaseRemoteConfig()
    {
        var settings = Firebase.RemoteConfig.FirebaseRemoteConfig.Settings;
      //  settings.IsDeveloperMode = true; // TBD: Remove/activate dev mode
        Firebase.RemoteConfig.FirebaseRemoteConfig.Settings = settings;
        System.Collections.Generic.Dictionary<string, object> defaults = new System.Collections.Generic.Dictionary<string, object>();
        // These are the values that are used if we haven't fetched data from the
        // server
        // yet, or if we ask for values that the server doesn't have:
        defaults.Add(Constants.Firebase.sales_target_level_1, Constants.Firebase.sales_target_level_1_default.ToString());
        defaults.Add(Constants.Firebase.gp_margin_target_level_4, Constants.Firebase.gp_margin_target_level_4_default.ToString());
        defaults.Add(Constants.Firebase.sales_target_level_5, Constants.Firebase.sales_target_level_5_default.ToString());
        defaults.Add(Constants.Firebase.profit_target_level_5, Constants.Firebase.profit_target_level_5_default.ToString());
        defaults.Add(Constants.Firebase.gp_margin_target_level_5, Constants.Firebase.gp_margin_target_level_5_default.ToString());
          
        Firebase.RemoteConfig.FirebaseRemoteConfig.SetDefaults(defaults);
        Debug.Log("RemoteConfig configured and ready!");
    }

    public void FetchData()
    {
        Debug.Log("Fetching data...");
        // FetchAsync only fetches new data if the current data is older than the provided
        // timespan. Otherwise it assumes the data is "recent enough", and does nothing.
        // By default the timespan is 12 hours, and for production apps, this is a good
        // number. For this example though, it's set to a timespan of zero, so that
        // changes in the console will always show up immediately.

        System.Threading.Tasks.Task fetchTask = Firebase.RemoteConfig.FirebaseRemoteConfig.FetchAsync(
        TimeSpan.Zero);
        fetchTask.ContinueWith(FetchComplete);
    }

    void FetchComplete(Task fetchTask)
    {

        if (fetchTask.IsCanceled)
        {
            Debug.Log("Fetch canceled.");
        }
        else if (fetchTask.IsFaulted)
        {
            Debug.Log("Fetch encountered an error.");
        }
        else if (fetchTask.IsCompleted)
        {
            Debug.Log("Fetch completed successfully!");
        }
        else
        {
            Debug.Log("Unknown fetch status");
        }
        var info = Firebase.RemoteConfig.FirebaseRemoteConfig.Info;
        switch (info.LastFetchStatus)
        {
            case Firebase.RemoteConfig.LastFetchStatus.Success:
                Firebase.RemoteConfig.FirebaseRemoteConfig.ActivateFetched();
                Debug.Log(String.Format("Remote data loaded and ready (last fetch time {0}).",
                info.FetchTime));
                //  Debug.Log(info);

                break;
            case Firebase.RemoteConfig.LastFetchStatus.Failure:
                switch (info.LastFetchFailureReason)
                {
                    case Firebase.RemoteConfig.FetchFailureReason.Error:
                        Debug.Log("Fetch failed for unknown reason");
                        break;
                    case Firebase.RemoteConfig.FetchFailureReason.Throttled:
                        Debug.Log("Fetch throttled until " + info.ThrottledEndTime);
                        break;
                }
                break;
            case Firebase.RemoteConfig.LastFetchStatus.Pending:
                Debug.Log("Latest Fetch call still pending.");
                break;
        }
    }

    public void UpdateAppDataFromRemoteConfig()
    {
        Level1.sales_target = Int32.Parse(Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue(Constants.Firebase.sales_target_level_1).StringValue);
      //  Debug.Log("Retrieved value for sales target level 1 firebase is: " + Level1.sales_target);
        Level4.gp_margin = Int32.Parse(Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue(Constants.Firebase.gp_margin_target_level_4).StringValue);
        Level5.sales_target = Int32.Parse(Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue(Constants.Firebase.sales_target_level_5).StringValue); 
        Level5.profit_target = Int32.Parse(Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue(Constants.Firebase.profit_target_level_5).StringValue);
        Level5.gp_margin_target = Int32.Parse(Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue(Constants.Firebase.gp_margin_target_level_5).StringValue);
    }

        public static void OnTokenReceived(object sender, Firebase.Messaging.TokenReceivedEventArgs token)
    {
        UnityEngine.Debug.Log("Received Registration Token: " + token.Token);
    }

    public static void OnMessageReceived(object sender, Firebase.Messaging.MessageReceivedEventArgs e)
    {
        Debug.Log("Just checking onmessagereceived");
        UnityEngine.Debug.Log("New message from: " + e.Message.Notification.Body); 
    }
}