﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Reflection;


public static class SaveGameForResume
{
    public static GameToResumeSaveData savedGame = new GameToResumeSaveData();
   

    public static void Save()
    {

        GameToResumeSaveData thisGameSaveData = new GameToResumeSaveData();

        MainScript mainScript = MainScript.currentMainScript;
        thisGameSaveData.currentGameLevel = mainScript.currentGameLevel;
        thisGameSaveData.gameID = mainScript.gameID;

        thisGameSaveData.bankBalance = mainScript.bankBalance;
        thisGameSaveData.profit = mainScript.profit;
        thisGameSaveData.sales = mainScript.sales;
        thisGameSaveData.costOfSales = mainScript.costOfSales;
        thisGameSaveData.advertising = mainScript.advertising;
        thisGameSaveData.branding = mainScript.branding;
        thisGameSaveData.goodwill = mainScript.goodwill;
        thisGameSaveData.investments = mainScript.investments;
        thisGameSaveData.adLeakage = mainScript.adLeakage;
        thisGameSaveData.elapsedTime = mainScript.elapsedTime;
        thisGameSaveData.minsRemaining = mainScript.minsRemaining;
        thisGameSaveData.oneSecondCount = mainScript.oneSecondCount;

        thisGameSaveData.interest = mainScript.interest;
        thisGameSaveData.interestToUpdate = mainScript.interestToUpdate;

        thisGameSaveData.numberEquipmentUpgradesMade = mainScript.numberEquipmentUpgradesMade;
        thisGameSaveData.numberSuccessfulCostNegotiations = mainScript.numberSuccessfulCostNegotiations;
        thisGameSaveData.counterTankSizeDoubled = mainScript.counterTankSizeDoubled;
        thisGameSaveData.counterTankSizeQuadrupled = mainScript.counterTankSizeQuadrupled;
        thisGameSaveData.counterPumpSizeDoubled = mainScript.counterPumpSizeDoubled;
        thisGameSaveData.counterPumpSizeTripled = mainScript.counterPumpSizeTripled;

        thisGameSaveData.juiceCostToAdd = mainScript.juiceCostToAdd;

        thisGameSaveData.numberQuotesIssued = mainScript.numberQuotesIssued;
        thisGameSaveData.quoteIndexesHistory = mainScript.quoteIndexesHistory;
        thisGameSaveData.pendingAdvertsTotal = mainScript.pendingAdvertsTotal;
        thisGameSaveData.interestPercen = mainScript.interestPercen;
        thisGameSaveData.tankData = mainScript.tankData;
        thisGameSaveData.valveData = mainScript.valveData;
        thisGameSaveData.delayActions = mainScript.delayActions;
        thisGameSaveData.productData = mainScript.productData;
        thisGameSaveData.rawMaterialData = mainScript.rawMaterialData;
        thisGameSaveData.orders = mainScript.orders;
        thisGameSaveData.mainNotificationsToAnimate = mainScript.mainNotificationsToAnimate;
        thisGameSaveData.volJuiceWasted = mainScript.volJuiceWasted;
        thisGameSaveData.volJuiceDelivered = mainScript.volJuiceDelivered;
        thisGameSaveData.juicesConsumedAtRecentMinuteStamps = mainScript.juicesConsumedAtRecentMinuteStamps;
        thisGameSaveData.valvesUsageHistoryAtRecentMinuteStamps = mainScript.valvesUsageHistoryAtRecentMinuteStamps;
        thisGameSaveData.juiceAddedToTanksByIndexHistoryAtRecentMinuteStamps = mainScript.juiceAddedToTanksByIndexHistoryAtRecentMinuteStamps;

        //Remote Config variables
        thisGameSaveData.sales_target_level_1 = Level1.sales_target;
        thisGameSaveData.gp_margin_target_level_4 = Level4.gp_margin;
        thisGameSaveData.sales_target_level_5 = Level5.sales_target;
        thisGameSaveData.profit_target_level_5 = Level5.profit_target;
        thisGameSaveData.gp_margin_target_level_5 = Level5.gp_margin_target;
     
        savedGame = thisGameSaveData;
        BinaryFormatter bf = new BinaryFormatter();
        //Application.persistentDataPath is a string, so if you wanted you can put that into debug.log if you want to know where save games are located
        FileStream file = File.Create(Application.persistentDataPath + Constants.Files.saveGameFilePath); //you can call it anything you want
        bf.Serialize(file, savedGame);
        file.Close();
    }
    public static void Load()
    {
        if (File.Exists(Application.persistentDataPath + Constants.Files.saveGameFilePath))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + Constants.Files.saveGameFilePath, FileMode.Open);
            savedGame = (GameToResumeSaveData)bf.Deserialize(file);
            file.Close();
        }
        else
        {
            savedGame = new GameToResumeSaveData();
        }
    }

    public static void ResumeFromPastGame(MainScript currentMainScript)
    {
     
        MainScript mainScript = MainScript.currentMainScript;
        mainScript.currentGameLevel = savedGame.currentGameLevel;
        mainScript.gameID = savedGame.gameID;

        mainScript.bankBalance = savedGame.bankBalance;
        mainScript.profit = savedGame.profit;
        mainScript.sales = savedGame.sales;
        mainScript.costOfSales = savedGame.costOfSales;
        mainScript.advertising = savedGame.advertising;
        mainScript.branding = savedGame.branding;
        mainScript.goodwill = savedGame.goodwill;
        mainScript.investments = savedGame.investments;
        mainScript.adLeakage = savedGame.adLeakage;
        mainScript.elapsedTime = savedGame.elapsedTime;
        mainScript.minsRemaining = savedGame.minsRemaining;
        mainScript.oneSecondCount = savedGame.oneSecondCount;

        mainScript.numberSuccessfulCostNegotiations = savedGame.numberSuccessfulCostNegotiations;
        mainScript.numberEquipmentUpgradesMade = savedGame.numberEquipmentUpgradesMade;
        mainScript.counterTankSizeDoubled = savedGame.counterTankSizeDoubled;
        mainScript.counterTankSizeQuadrupled = savedGame.counterTankSizeQuadrupled;
        mainScript.counterPumpSizeDoubled = savedGame.counterPumpSizeDoubled;
        mainScript.counterPumpSizeTripled = savedGame.counterPumpSizeTripled;

        mainScript.juiceCostToAdd = savedGame.juiceCostToAdd;

        mainScript.numberQuotesIssued = savedGame.numberQuotesIssued;
        mainScript.quoteIndexesHistory = savedGame.quoteIndexesHistory;
        mainScript.pendingAdvertsTotal = savedGame.pendingAdvertsTotal;

        mainScript.interestPercen = savedGame.interestPercen;
        mainScript.interest = savedGame.interest;
        mainScript.interestToUpdate = savedGame.interestToUpdate;
        mainScript.tankData = savedGame.tankData;
        for (int k = 0; k < mainScript.tankData.Count; k++)
        {
            TankData thisTank = mainScript.tankData[k];
            thisTank.numPipesFromTankAdded = 0;

        }
        mainScript.valveData = savedGame.valveData;
        mainScript.delayActions = savedGame.delayActions;
        mainScript.productData = savedGame.productData;
        mainScript.rawMaterialData = savedGame.rawMaterialData;
        mainScript.orders = savedGame.orders;
        mainScript.mainNotificationsToAnimate = savedGame.mainNotificationsToAnimate;

        mainScript.volJuiceWasted = savedGame.volJuiceWasted;
        mainScript.volJuiceDelivered = savedGame.volJuiceDelivered;
        mainScript.juicesConsumedAtRecentMinuteStamps = savedGame.juicesConsumedAtRecentMinuteStamps;
        mainScript.valvesUsageHistoryAtRecentMinuteStamps = savedGame.valvesUsageHistoryAtRecentMinuteStamps;
        mainScript.juiceAddedToTanksByIndexHistoryAtRecentMinuteStamps = savedGame.juiceAddedToTanksByIndexHistoryAtRecentMinuteStamps;

        //Remote Config variables
        Level1.sales_target = savedGame.sales_target_level_1;
        Level4.gp_margin = savedGame.gp_margin_target_level_4;
        Level5.sales_target = savedGame.sales_target_level_5;
        Level5.profit_target = savedGame.profit_target_level_5;
        Level5.gp_margin_target = savedGame.gp_margin_target_level_5;
       
    }


    public static void ClearSavedGame()
    {
        savedGame = new GameToResumeSaveData();
        string filePath = Application.persistentDataPath + Constants.Files.saveGameFilePath;
        File.Delete(filePath);
    }
}


