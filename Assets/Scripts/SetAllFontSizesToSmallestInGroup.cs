using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;


public class SetAllFontSizesToSmallestInGroup : MonoBehaviour
{
    public Canvas canvas;
    int smallestFontSize;
    bool hasSetAllFontSizeSameSinceBestFitDone = false;
    bool returningFromSmallestFontEnumeratorYield = false;
    bool haveCalculatedSmallestFontSizeAtLeastOnce = false;
    public bool onlyCalculateSmallestSizeOnce = false;
    List<Text> textItems;
    const int fontSizeNotSet = -1;

    void Start()
    {

    }

    void Update()
    {
        if (NeedRecalculateSmallestFont())
        {
            StartCoroutine(SetTextSizesToSmallest());
        }
    }


    bool NeedRecalculateSmallestFont()
    {
        if (textItems != null)
        {
            if (hasSetAllFontSizeSameSinceBestFitDone == false && textItems.Count > 0 && returningFromSmallestFontEnumeratorYield == false && (onlyCalculateSmallestSizeOnce == false || (onlyCalculateSmallestSizeOnce == true && haveCalculatedSmallestFontSizeAtLeastOnce == false)))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }

    public void SetTextItems(List<Text> textItems)
    {
        this.textItems = textItems;
        if ((onlyCalculateSmallestSizeOnce == true && haveCalculatedSmallestFontSizeAtLeastOnce == true))
        {
            SetFontSizeOfTextItemsAsSmallest();
        }
        else
        {
            ResetBestFitDone();
        }
    }

    void ResetBestFitDone()
    {
        hasSetAllFontSizeSameSinceBestFitDone = false;
        returningFromSmallestFontEnumeratorYield = false;
    }

    IEnumerator SetTextSizesToSmallest()
    {
        SetFontSizeOfTextItemsAsLarge();
        returningFromSmallestFontEnumeratorYield = true;
        yield return 0;
        returningFromSmallestFontEnumeratorYield = false;

        smallestFontSize = fontSizeNotSet;

        bool updateResult = UpdateSmallestFontSizeFromTextItems();
        if (updateResult == true)
        {
            SetFontSizeOfTextItemsAsSmallest();
            hasSetAllFontSizeSameSinceBestFitDone = true;
            haveCalculatedSmallestFontSizeAtLeastOnce = true;
        }
        else
        {
            smallestFontSize = fontSizeNotSet;
        }
    }

    bool UpdateSmallestFontSizeFromTextItems()
    {
        foreach (Text item in textItems)
        {
            if (item == null)
            {
                return false;
            }

            bool updateResult = UpdateSmallestFontSizeFromOneTextItem(item);
            if (updateResult == false)
            {
                return false;
            }

        }
        return true;
    }

    bool UpdateSmallestFontSizeFromOneTextItem(Text item)
    {
        int fontSize = item.cachedTextGenerator.fontSizeUsedForBestFit;
      //  Debug.Log("Font size here is: " + fontSize + " for text item: " + item.name + "with parent: " + item.transform.parent.parent.name);

        if (fontSize == 0)
        {
            return false;
        }
        if (smallestFontSize == fontSizeNotSet || fontSize < smallestFontSize)
        {
            smallestFontSize = fontSize;
        }

        return true;
    }

    void SetFontSizeOfTextItemsAsSmallest()
    {
        float multiplier = 1f / canvas.scaleFactor;
        foreach (Text item in textItems)
        {
            item.resizeTextMaxSize = (int)Mathf.Floor(smallestFontSize * multiplier) + 1; // Even if fonts won't all be identical by adding 1, it offers a good balance between readability and sameness in many cases.
        }
    }

    void SetFontSizeOfTextItemsAsLarge()
    {
        float multiplier = 1f / canvas.scaleFactor;
        foreach (Text item in textItems)
        {
            item.resizeTextMaxSize = (int)Mathf.Floor(100 * multiplier);
        }
    }

}