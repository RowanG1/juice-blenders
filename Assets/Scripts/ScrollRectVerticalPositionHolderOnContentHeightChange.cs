using UnityEngine;
using System;
using UnityEngine.UI;
using System.Collections;

public class ScrollRectVerticalPositionHolderOnContentHeightChange: MonoBehaviour {
    public ScrollRect scrollRect;
    float lastVerticalScrollPosition;
    
    IEnumerator SetVerticalPositionScroll()
    {
        // If you set the orderscontent port, the scroll position resets. To avoid this, the position is set in the frame after a change in content port height
        yield return 0;
        scrollRect.verticalNormalizedPosition = lastVerticalScrollPosition;
    }

    public void ContentHeightUpdated() {
        StartCoroutine(SetVerticalPositionScroll());
    }

     void TableScrolledEvent(Vector2 value)
    {
        lastVerticalScrollPosition = scrollRect.verticalNormalizedPosition;
    }

    void Start() {
         scrollRect.onValueChanged.AddListener(TableScrolledEvent);
    }
}