﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class InstrucTextScript : MonoBehaviour {
	public GameObject Canvas;
	public GameObject instrucContent;
	int backButtonWidth;
	int backButtonHeight;
	int headingFontSize;
	int instrucFontSize;
	int operationsImageHeight;
	int tankImageHeight;
	int ordersImageHeight;
	int correctTankImageHeight;

	// Use this for initialization
	void Start () {
		Canvas.SetActive (false);
		Canvas.SetActive (true);
		if (MenuScript.minScreenDimensionmm > MenuScript.screenSizeThreshold) {
			backButtonWidth = 17;
			backButtonHeight = 9;
			headingFontSize = 5;
			instrucFontSize = 5;
			operationsImageHeight = 90;
			tankImageHeight = 70;
			ordersImageHeight = 35;
			correctTankImageHeight = 60;
		} else {
			 backButtonWidth = 12;
			 backButtonHeight = 7;
			 headingFontSize = 3;
			 instrucFontSize = 3;
			 operationsImageHeight = 50;
			 tankImageHeight = 40;
			 ordersImageHeight = 25;
			correctTankImageHeight = 35;
		}

		instrucContent.transform.Find ("FirstText").GetComponent<Text> ().fontSize = instrucFontSize;
		instrucContent.transform.Find ("SecondText").GetComponent<Text> ().fontSize = instrucFontSize;
		instrucContent.transform.Find ("ThirdText").GetComponent<Text> ().fontSize = instrucFontSize;
		instrucContent.transform.Find ("FourthText").GetComponent<Text> ().fontSize = instrucFontSize;
		instrucContent.transform.Find ("FifthText").GetComponent<Text> ().fontSize = instrucFontSize;
		instrucContent.transform.Find ("SixthText").GetComponent<Text> ().fontSize = instrucFontSize;

		instrucContent.transform.Find ("FirstImagePanel").GetComponent<LayoutElement> ().preferredHeight = operationsImageHeight;
		instrucContent.transform.Find ("SecondImagePanel").GetComponent<LayoutElement> ().preferredHeight = tankImageHeight;
		instrucContent.transform.Find ("ThirdImagePanel").GetComponent<LayoutElement> ().preferredHeight = correctTankImageHeight;
		instrucContent.transform.Find ("FourthImagePanel").GetComponent<LayoutElement> ().preferredHeight = ordersImageHeight;

		Canvas.transform.Find ("Heading").GetComponent<Text> ().fontSize = headingFontSize;
		Canvas.transform.Find ("BackButton/Text").GetComponent<Text> ().fontSize = backButtonHeight - 2;
		Canvas.transform.Find ("BackButton").GetComponent<RectTransform> ().sizeDelta = new Vector2 (backButtonWidth, backButtonHeight);




	}
	


	public void backBtnPressed() {
		SceneManager.LoadScene ("InstructionsMenu");
	}
}
