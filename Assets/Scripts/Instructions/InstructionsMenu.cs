﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class InstructionsMenu : MonoBehaviour
{
    int menuButtonWidths;
    int menuButtonHeight;
    int backBtnWidth;
    int backBtnHeight;
    int menuButtonFontSize;
    int backButtonFontSize;

    // Use this for initialization
    void Start()
    {

        if (MenuScript.minScreenDimensionmm > MenuScript.screenSizeThreshold)
        {
            menuButtonWidths = 40;
            menuButtonHeight = 15;
            backBtnWidth = 15;
            backBtnHeight = 10;
            backButtonFontSize = 5;
            menuButtonFontSize = 6;
        }
        else
        {
            menuButtonWidths = 30;
            menuButtonHeight = 10;
            backBtnWidth = 10;
            backBtnHeight = 7;
            backButtonFontSize = 3;
            menuButtonFontSize = 4;
        }

        GameObject.Find("Canvas/InstructionsMenu/InstructionsTextButton").GetComponent<RectTransform>().sizeDelta = new Vector2(menuButtonWidths, menuButtonHeight);
        GameObject.Find("Canvas/InstructionsMenu/InstructionsVideoButton").GetComponent<RectTransform>().sizeDelta = new Vector2(menuButtonWidths, menuButtonHeight);
        GameObject.Find("Canvas/BackButton").GetComponent<RectTransform>().sizeDelta = new Vector2(backBtnWidth, backBtnHeight);

        GameObject.Find("Canvas/InstructionsMenu/InstructionsTextButton/Text").GetComponent<Text>().fontSize = menuButtonFontSize;
        GameObject.Find("Canvas/InstructionsMenu/InstructionsVideoButton/Text").GetComponent<Text>().fontSize = menuButtonFontSize;
        GameObject.Find("Canvas/BackButton/Text").GetComponent<Text>().fontSize = backButtonFontSize;

    }

    // Update is called once per frame


    public void instructionsTextClick()
    {
        Firebase.Analytics.FirebaseAnalytics.LogEvent("text_instructions_clicked", "type", "N/A");
        Common.PlayButtonSound();
        // SceneManager.LoadScene("Instructions");
        Application.OpenURL("https://docs.google.com/document/d/e/2PACX-1vQh8oDoKyEEMc8OIC9lIAhPa2VGiYZ8wm4wmmIlm1FGkdyKyoaKViCrKCyf4h7PORU8Y_-PInC5xwgx/pub");
    }

    public void instructionsVideoClick()
    {
        //SceneManager.LoadScene ("InstructionsVideo");
        Firebase.Analytics.FirebaseAnalytics.LogEvent("text_video_clicked", "type", "N/A");
        Common.PlayButtonSound();
        Application.OpenURL("https://youtu.be/TvlXBxvlJBc");
    }

    public void backBtn()
    {
        Common.PlayButtonSound();
        SceneManager.LoadScene("MainMenu");
    }
}

