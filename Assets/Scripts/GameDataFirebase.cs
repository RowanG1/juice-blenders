using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;
using System.Text.RegularExpressions;
using Firebase;
using Firebase.Auth;
using Firebase.Unity.Editor;
using Firebase.Database;

[System.Serializable]
public class GameDataFirebase
{
    public int currentGameLevel;
    public int levelDurationMins;
    public int elapsedTimeInMin;
    public string bankBalance;
    public int juiceWastePercentage;
    public string customerSatisfactionPercen;
    public string profit;
    public string sales;
    public string costOfSales;
    public int advertising;
    public int branding;
    public string goodwill;
    public string investments;
    public string interest;
    public string gameEndedStatus;
    public string gpMarginPercen;
    public int numberEquipmentUpgradesMade;
    public int numberSuccessfulCostNegotiations;
    public int ordersCount;
    public int ordersAcceptedCount;
    public int ordersDeclinedCount;
    public int ordersLostCount;
    public int counterTankSizeDoubled;
    public int counterTankSizeQuadrupled;
    public int counterPumpSizeDoubled;
    public int counterPumpSizeTripled;

    public GameDataFirebase(MainScript mainScript)
    {
        currentGameLevel = mainScript.currentGameLevel;
        levelDurationMins = Level.GetLevelObj().levelDurationMins;
        elapsedTimeInMin = (int)Mathf.Floor(mainScript.elapsedTime / 60);
        bankBalance = mainScript.bankBalance.ToString();
        juiceWastePercentage = (int)TargetsArea.PercenJuiceWasted(mainScript.volJuiceWasted, mainScript.volJuiceDelivered);
        customerSatisfactionPercen = new BadgesCredits().GetCurrentInGamePercenCustomerService(mainScript.orders).ToString();
        profit = Common.ConvertToTwoDecimal(mainScript.profit).ToString();
        sales =Common.ConvertToTwoDecimal(mainScript.sales).ToString();
        costOfSales = Common.ConvertToTwoDecimal(mainScript.costOfSales).ToString();
        advertising = mainScript.advertising + mainScript.pendingAdvertsTotal;
        branding = mainScript.branding;
        goodwill = mainScript.goodwill.ToString();
        investments = mainScript.investments.ToString();
        interest = mainScript.interest.ToString();
        gameEndedStatus = DataTransferToGameDataFirebase.gameEndedStatus;
        gpMarginPercen = FinanceAreaScript.GPMarginPercen(sales: mainScript.sales, costOfSales: mainScript.costOfSales).ToString();
        numberEquipmentUpgradesMade = mainScript.numberEquipmentUpgradesMade;
        numberSuccessfulCostNegotiations = mainScript.numberSuccessfulCostNegotiations;
        ordersCount = mainScript.orders.Count;
        ordersAcceptedCount = OrderFuncs.OrdersAcceptedCount(mainScript.orders);
        ordersDeclinedCount = OrderFuncs.OrdersDeclinedCount(mainScript.orders);
        ordersLostCount = OrderFuncs.OrdersLostCount(mainScript.orders);
        counterTankSizeDoubled = mainScript.counterTankSizeDoubled;
        counterTankSizeQuadrupled = mainScript.counterTankSizeQuadrupled;
        counterPumpSizeDoubled = mainScript.counterPumpSizeDoubled;
        counterPumpSizeTripled = mainScript.counterPumpSizeTripled;
    }

    public GameDataFirebase()
    {

    }

}

