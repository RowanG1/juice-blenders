public class BadgesEarned
{

    public Badges.BadgeType badgeEarnedCustomerService;
    public Badges.BadgeType badgeEarnedLowWaste;
    public Badges.BadgeType badgeEarnedProfitability;


    public BadgesEarned(BadgesCredits badgesCredits)
    {
        SetBadgesEarned(badgesCredits);
    }


    public void SetBadgesEarned(BadgesCredits badgesCredits)
    {
        badgeEarnedCustomerService = GetBadgeEarnedCustomerService(badgesCredits);
        badgeEarnedLowWaste = GetBadgeEarnedLowWaste(badgesCredits);
        badgeEarnedProfitability = GetBadgeEarnedProfitability(badgesCredits);
    }

    public Badges.BadgeType GetBadgeEarnedOneType(int creditsEarned, BadgeStarCriteriaOneBadgeType criteria)
    {
        if (creditsEarned >= 0 && creditsEarned < criteria.bronze)
        {
            return Badges.BadgeType.None;
        }
        else if (creditsEarned >= criteria.bronze && creditsEarned < criteria.silver)
        {
            return Badges.BadgeType.Bronze;
        }
        else if (creditsEarned >= criteria.silver && creditsEarned < criteria.gold)
        {
            return Badges.BadgeType.Silver;
        }
        else if (creditsEarned >= criteria.gold)
        {
            return Badges.BadgeType.Gold;
        }
        return Badges.BadgeType.None;
    }

    public Badges.BadgeType GetBadgeEarnedCustomerService(BadgesCredits badgesCredits)
    {
        return GetBadgeEarnedOneType(badgesCredits.customerServiceCreditsEarned, Constants.Badges.customerServiceBadgeCriteria);
    }

    public Badges.BadgeType GetBadgeEarnedLowWaste(BadgesCredits badgesCredits)
    {
        return GetBadgeEarnedOneType(badgesCredits.lowWasteCreditsEarned, Constants.Badges.lowWasteBadgeCriteria);
    }

    public Badges.BadgeType GetBadgeEarnedProfitability(BadgesCredits badgesCredits)
    {
        return GetBadgeEarnedOneType(badgesCredits.profitabilityCreditsEarned, Constants.Badges.profitabilityBadgeCriteria);
    }

    public int CreditsNeededForNextOneBadge(int creditsEarned, BadgeStarCriteriaOneBadgeType criteria)
    {
        if (creditsEarned >= 0 && creditsEarned < criteria.bronze)
        {
            return criteria.bronze - creditsEarned;
        }
        else if (creditsEarned >= criteria.bronze && creditsEarned < criteria.silver)
        {
            return criteria.silver - creditsEarned;
        }
        else if (creditsEarned >= criteria.silver && creditsEarned < criteria.gold)
        {
            return criteria.gold - creditsEarned;
        }
        else if (creditsEarned >= criteria.gold)
        {
            return 0;
        }
        return 0;
    }

    public CounterPerBadgeType GetCreditsNeededToGetNextBadges(BadgesCredits badgesCredits)
    {
        int customerServiceCreditsNeeded = CreditsNeededForNextOneBadge(badgesCredits.customerServiceCreditsEarned, Constants.Badges.customerServiceBadgeCriteria);
        int lowWasteCreditsNeeded = CreditsNeededForNextOneBadge(badgesCredits.lowWasteCreditsEarned, Constants.Badges.lowWasteBadgeCriteria);
        int profitabilityCreditsNeeded = CreditsNeededForNextOneBadge(badgesCredits.profitabilityCreditsEarned, Constants.Badges.profitabilityBadgeCriteria);

        return new CounterPerBadgeType(customerServiceCreditsNeeded, lowWasteCreditsNeeded, profitabilityCreditsNeeded);
    }

}