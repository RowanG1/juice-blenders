﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.IO;

public class BadgeStatusArea : MonoBehaviour
{

    public Sprite noStar;
    public Sprite bronzeStar;
    public Sprite silverStar;
    public Sprite goldStar;

    public GameObject customerServiceBadgePanel;
    public GameObject lowWasteBadgePanel;
    public GameObject profitabilityBadgePanel;
    public GameObject boyFace;

    Vector3 originalLocalPositionBoyFace;

    // Use this for initialization
    void Start()
    {

        BadgesEarned badgesEarned = new BadgesEarned(new BadgeCreditsFileHandler().GetBadgeCreditsFromFile());

        setBadgeIcon(customerServiceBadgePanel, badgesEarned.badgeEarnedCustomerService);
        setBadgeIcon(lowWasteBadgePanel, badgesEarned.badgeEarnedLowWaste);
        setBadgeIcon(profitabilityBadgePanel, badgesEarned.badgeEarnedProfitability);

        CounterPerBadgeType creditsNeeded = badgesEarned.GetCreditsNeededToGetNextBadges(new BadgeCreditsFileHandler().GetBadgeCreditsFromFile());

        setNextBadgeText(customerServiceBadgePanel, creditsNeeded.customerService, Badges.NextBadge(badgesEarned.badgeEarnedCustomerService));
        setNextBadgeText(lowWasteBadgePanel, creditsNeeded.lowWaste, Badges.NextBadge(badgesEarned.badgeEarnedLowWaste));
        setNextBadgeText(profitabilityBadgePanel, creditsNeeded.profitability, Badges.NextBadge(badgesEarned.badgeEarnedProfitability));

        setCurrentBadgeText(customerServiceBadgePanel, badgesEarned.badgeEarnedCustomerService);
        setCurrentBadgeText(lowWasteBadgePanel, badgesEarned.badgeEarnedLowWaste);
        setCurrentBadgeText(profitabilityBadgePanel, badgesEarned.badgeEarnedProfitability);

        originalLocalPositionBoyFace = boyFace.GetComponent<RectTransform>().localPosition;
    }

    void Update()
    {
        updateFacePositionAndRotation();
    }

    public void GoHome()
    {
        Common.PlayButtonSound();
        SceneManager.LoadScene("MainMenu");
    }

    public void GoToGame()
    {
        Common.PlayButtonSound();
        SceneManager.LoadScene("Levels");
    }
    void setCurrentBadgeText(GameObject badgePanel, Badges.BadgeType badgeType)
    {
        string badgeText = "None";
        if (badgeType == Badges.BadgeType.Bronze)
        {
            badgeText = Badges.BadgeType.Bronze.ToString();
        }
        else if (badgeType == Badges.BadgeType.Silver)
        {
            badgeText = Badges.BadgeType.Silver.ToString();
        }
        else if (badgeType == Badges.BadgeType.Gold)
        {
            badgeText = Badges.BadgeType.Gold.ToString();
        }

        badgePanel.GetComponent<SingleBadgeStatusUI>().currentBadgeText.GetComponent<Text>().text = badgeText;
    }

    void setNextBadgeText(GameObject badgePanel, int creditsNeeded, Badges.BadgeType nextBadge)
    {
        string nextBadgeText;
        if (nextBadge == Badges.BadgeType.None)
        {
            nextBadgeText = "";
        }
        else
        {
            nextBadgeText = "Credits needed for " + nextBadge.ToString() + " badge: " + creditsNeeded;
        }
        badgePanel.GetComponent<SingleBadgeStatusUI>().nextBadgeText.GetComponent<Text>().text = nextBadgeText;
    }

    void setBadgeIcon(GameObject badgePanel, Badges.BadgeType badgeType)
    {
        if (badgeType == Badges.BadgeType.None)
        {
            badgePanel.GetComponent<SingleBadgeStatusUI>().currentBadgeIcon.GetComponent<Image>().sprite = noStar;
        }
        else if (badgeType == Badges.BadgeType.Bronze)
        {
            badgePanel.GetComponent<SingleBadgeStatusUI>().currentBadgeIcon.GetComponent<Image>().sprite = bronzeStar;
        }
        else if (badgeType == Badges.BadgeType.Silver)
        {
            badgePanel.GetComponent<SingleBadgeStatusUI>().currentBadgeIcon.GetComponent<Image>().sprite = silverStar;
        }
        else if (badgeType == Badges.BadgeType.Gold)
        {
            badgePanel.GetComponent<SingleBadgeStatusUI>().currentBadgeIcon.GetComponent<Image>().sprite = goldStar;
        }
    }

    void updateFacePositionAndRotation()
    {
        boyFace.transform.localEulerAngles = new Vector3(0, 0, Mathf.Sin(2 * Mathf.PI * (0.6f) * Time.realtimeSinceStartup) * 5);
        boyFace.GetComponent<RectTransform>().localPosition = new Vector3(Mathf.Sin(2 * Mathf.PI * (0.4f) * Time.realtimeSinceStartup) * 2 + originalLocalPositionBoyFace.x, originalLocalPositionBoyFace.y, 0);
        // boyFace.GetComponent<RectTransform>().localPosition = new Vector3(0, 0, 0);
    }
}
