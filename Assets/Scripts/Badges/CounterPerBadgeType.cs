    public class CounterPerBadgeType {
        public int customerService;
        public int lowWaste;
        public int profitability;

        public CounterPerBadgeType(int customerService, int lowWaste, int profitability) {
            this.customerService = customerService;
            this. lowWaste = lowWaste;
            this.profitability = profitability;
        }
    }