﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public class Badges
{
    public enum BadgeType : int { None, Bronze, Silver, Gold };

    public static BadgeType NextBadge(BadgeType currentBadge)
    {
        if (currentBadge == BadgeType.None)
        {
            return BadgeType.Bronze;
        } else if (currentBadge == BadgeType.Bronze) {
              return BadgeType.Silver;
        } else if (currentBadge == BadgeType.Silver) {
              return BadgeType.Gold;
        } else {
            return BadgeType.None;
        }
    }

}
