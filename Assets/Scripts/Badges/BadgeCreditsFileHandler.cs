
using UnityEngine;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public class BadgeCreditsFileHandler
{
   
    public BadgesCredits LoadBadgesCredits()
    {
        if (File.Exists(Application.persistentDataPath + Constants.Files.badgesFilePath))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + Constants.Files.badgesFilePath, FileMode.Open);
            BadgesCredits badgesCreditsFileData = (BadgesCredits)bf.Deserialize(file);
            file.Close();
            return badgesCreditsFileData;
        }
        else
        {

            throw new FileNotFoundException("Past badge credits file not found");
        }
    }

    public BadgesCredits GetBadgeCreditsFromFile()
    {
        BadgesCredits badgesCredits;
        try
        {
            badgesCredits = LoadBadgesCredits();
        }
        catch (FileNotFoundException e)
        {
            badgesCredits = new BadgesCredits();
            Debug.Log(e);
        }
        return badgesCredits;
    }

    public void AddCreditsToFileForLevel(MainScript mainScript)
    {
        BadgesCredits badgeCredits = new BadgesCredits();
        int creditForCustomerServiceThisLevel = badgeCredits.GetCreditsForCustomerServiceOnLevel(mainScript.orders);
        int creditsForLowWastageThisLevel = badgeCredits.GetCreditsForLowWastageThisLevel(mainScript.volJuiceWasted, mainScript.volJuiceDelivered);
        int creditsForProfitabilityThisLevel = badgeCredits.GetCreditsForProfitabilityThisLevel(mainScript.profit);
        AddCreditsToFile(creditForCustomerServiceThisLevel, creditsForLowWastageThisLevel, creditsForProfitabilityThisLevel);
    }

    public void AddCreditsToFile(int customerService, int lowWaste, int profit)
    {
        BadgesCredits fromFile = GetBadgeCreditsFromFile();

        fromFile.customerServiceCreditsEarned += customerService;
        fromFile.lowWasteCreditsEarned += lowWaste;
        fromFile.profitabilityCreditsEarned += profit;
        
        BadgesCredits updated = fromFile;
        SaveBadgeCredits(updated);
    }

    public void SaveBadgeCredits(BadgesCredits updated)
    {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + Constants.Files.badgesFilePath); //you can call it anything you want
        bf.Serialize(file, updated);
        file.Close();
    }

        public void ClearBadgesCredits()
    {
        string filePath = Application.persistentDataPath + Constants.Files.badgesFilePath;
        File.Delete(filePath);
    }

}