using UnityEngine;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

[System.Serializable]
public class BadgesCredits
{
    public int customerServiceCreditsEarned;
    public int lowWasteCreditsEarned;
    public int profitabilityCreditsEarned;


    public BadgesCredits()
    {

    }

    public int GetCreditsForCustomerServiceOnLevel(List<Order> orders)
    {
        int currentCustomerServicePercen = GetCurrentInGamePercenCustomerService(orders);
        if (currentCustomerServicePercen >= Constants.Badges.minimumCustomerServicePercenForCredit)
        {
            return 1;
        }
        else
        {
            return 0;
        }
    }

    public int GetCreditsForProfitabilityThisLevel(float profit)
    {
        if (profit > Constants.Badges.minimumProfitForCredit)
        {
            return 1;
        }
        else
        {
            return 0;
        }
    }

    public int GetCreditsForLowWastageThisLevel(float volJuiceWasted, float volJuiceDelivered)
    {
        if (volJuiceWasted < (volJuiceDelivered * Constants.Badges.juiceWastagePercenThreshold / 100))
        {
            return 1;
        }
        else
        {
            return 0;
        }
    }

    public int GetCurrentInGamePercenCustomerService(List<Order> orders)
    {
        int totalMaxPoints = 0;
        int pointsTally = 0;

        foreach (Order order in orders)
        {
            int customerEnquiryMaxPoints = 2;
            totalMaxPoints += customerEnquiryMaxPoints;

            if (order.customerEmotionStatusEnquiry == (int)Common.CustomerEmotionStatus.Happy)
            {
                pointsTally += 2;
            }
            else if (order.customerEmotionStatusEnquiry == Common.CustomerEmotionStatus.NoComment)
            {
                pointsTally += 1;
            }
            else if (order.customerEmotionStatusEnquiry == Common.CustomerEmotionStatus.Sad)
            {
                pointsTally += 0;
            }

            bool deliveryWasPromised = order.accepted;
            if (deliveryWasPromised == true)
            {
                int deliveryMaxServicePoints = 2;
                totalMaxPoints += deliveryMaxServicePoints;

                if (order.customerEmotionStatusDelivery == Common.CustomerEmotionStatus.Happy)
                {
                    pointsTally += 2;
                }
                else if (order.customerEmotionStatusDelivery == Common.CustomerEmotionStatus.NoComment)
                {
                    pointsTally += 1;
                }
                else if (order.customerEmotionStatusDelivery == Common.CustomerEmotionStatus.Sad)
                {
                    pointsTally += 0;
                }
            }

            if (order.lost == true) //Lost due to delivery or neglected enquiry
            {
                pointsTally -= 1;
            }
        }

        if (totalMaxPoints == 0)
        {
            return 100;
        }
        else
        {
            return Mathf.Max(0, (int)((float)pointsTally / totalMaxPoints * 100));
        }
    }

    public void SetCreditsForTransferToLevelPassed(MainScript mainScript)
    {
        DataTransferToLevelPassed.customerServiceBadgeCredit = GetCreditsForCustomerServiceOnLevel(mainScript.orders);
        DataTransferToLevelPassed.lowWasteBadgeCredit = GetCreditsForLowWastageThisLevel(mainScript.volJuiceWasted, mainScript.volJuiceDelivered);
        DataTransferToLevelPassed.profitabilityBadgeCredit = GetCreditsForProfitabilityThisLevel(mainScript.profit);
    }


}