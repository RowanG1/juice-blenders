using System.Collections;
using System.Collections.Generic;
public class BadgeStarCriteriaOneBadgeType {
   public int bronze;
   public int silver;
   public int gold;

    public BadgeStarCriteriaOneBadgeType(int bronze, int silver,  int gold) {
        this.bronze = bronze;
        this.silver = silver;
        this.gold = gold;

    }
}