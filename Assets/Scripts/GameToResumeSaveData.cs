﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


[System.Serializable]
public class GameToResumeSaveData
{
    public int currentGameLevel;
    public string gameID;
    public float bankBalance;
    public float profit;
    public float sales;
    public float costOfSales;
    public int advertising;
    public int branding;
    public float goodwill;
    public float investments;
    public int adLeakage;
    public float interestPercen;
    public float interest;
    public float interestToUpdate;
    public float elapsedTime;
    public int minsRemaining = Constants.Timing.nullGameMinsRemaining;
    public int oneSecondCount;

    public int numberEquipmentUpgradesMade;
    public int numberSuccessfulCostNegotiations;
    public int counterTankSizeDoubled;
    public int counterTankSizeQuadrupled;
    public int counterPumpSizeDoubled;
    public int counterPumpSizeTripled;

    public int numberQuotesIssued;
    public List<int> quoteIndexesHistory = new List<int>();
    public int pendingAdvertsTotal;
    public float juiceCostToAdd;
    public List<TankData> tankData = new List<TankData>();
    public List<Valve> valveData = new List<Valve>();
    public List<DelayAction> delayActions = new List<DelayAction>();
    public List<ProductData> productData = new List<ProductData>();
    public List<RawMaterialData> rawMaterialData = new List<RawMaterialData>();
    public List<Order> orders = new List<Order>();
    public List<MainAnimatedNotification> mainNotificationsToAnimate = new List<MainAnimatedNotification>();
    public float volJuiceWasted;
    public float volJuiceDelivered;
    public Dictionary<int, Dictionary<string, float>> juicesConsumedAtRecentMinuteStamps = new Dictionary<int, Dictionary<string, float>>();
    public Dictionary<int, Dictionary<int, ValveDataStampPeriod>> valvesUsageHistoryAtRecentMinuteStamps = new Dictionary<int, Dictionary<int, ValveDataStampPeriod>>();//Outer key is timestamp, inner key is valveIndex
    public Dictionary<int, Dictionary<int, TankUsageStamp>> juiceAddedToTanksByIndexHistoryAtRecentMinuteStamps = new Dictionary<int, Dictionary<int, TankUsageStamp>>();//Outer key is timestamp, inner key is valveIndex
    
    //Remote config variables. We don't want to resume an old game, with new data from remote config. In future, it could be okay or desired to resume an old game with new remote data.
    public int sales_target_level_5;
    public int sales_target_level_1;
    public int gp_margin_target_level_4;
    public int profit_target_level_5;
    public int gp_margin_target_level_5;

    public GameToResumeSaveData()
    {
    }
}


