using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Reflection;
using Firebase;
using Firebase.Auth;
using Firebase.Unity.Editor;
using Firebase.Database;

public class PupilDataStorage
{
   

    public PupilData LoadFromLocalDevice()
    {
        PupilData pupilData;
        if (File.Exists(Application.persistentDataPath + Constants.Files.pupilDataFilePath))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + Constants.Files.pupilDataFilePath, FileMode.Open);
            pupilData = (PupilData)bf.Deserialize(file);
            file.Close();
            return pupilData;
        }
        else
        {
            return null;
        }
    }

    public void SaveToLocalDevice(PupilData pupilData)
    {
        BinaryFormatter bf = new BinaryFormatter();
        //Application.persistentDataPath is a string, so if you wanted you can put that into debug.log if you want to know where save games are located
        FileStream file = File.Create(Application.persistentDataPath + Constants.Files.pupilDataFilePath); //you can call it anything you want
        bf.Serialize(file, pupilData);
        file.Close();
    }

    public void DeleteLocalFile()
    {
        string filePath = Application.persistentDataPath + Constants.Files.pupilDataFilePath;
        File.Delete(filePath);
    }

    public void UpdatePupilDataRemote(PupilData pupilData, FirebaseWebRequests.ContinueHandlerFirebase continueHandler)
    {
        FirebaseApp.DefaultInstance.SetEditorDatabaseUrl("https://juice-blenders-firebase.firebaseio.com/");

        PupilData purePupilData = new PupilData(); //Seems to be a bug in C#/Unity, where a casted class converted to JSON inherits all members before cast took place. To avoid password being sent to database, a new pupildata object is created, not via pupilformdata casted to pupildata.
        purePupilData.name = pupilData.name;
        purePupilData.surname = pupilData.surname;
        purePupilData.classCode = pupilData.classCode;
        purePupilData.highestLevelPassed = pupilData.highestLevelPassed;
        purePupilData.userID = pupilData.userID;

        string json = JsonUtility.ToJson(purePupilData);

        FirebaseDatabase.DefaultInstance.GetReference(Constants.Firebase.pupils).Child(pupilData.userID).SetRawJsonValueAsync(json).ContinueWith(task =>
        {
            if (task.IsFaulted)
            {
                continueHandler.IsFaulted();
            }
            if (task.IsCompleted)
            {
                continueHandler.IsCompleted(null);
            }
        });
    }

    public void UpdatePupilHighestLevelRemote(int highestLevelPassed)
    {
        FirebaseApp.DefaultInstance.SetEditorDatabaseUrl("https://juice-blenders-firebase.firebaseio.com/");
        Firebase.Auth.FirebaseAuth auth = Firebase.Auth.FirebaseAuth.DefaultInstance;
        var user = auth.CurrentUser;

        if (user != null)
        {
            string userID = user.UserId;
            FirebaseDatabase.DefaultInstance.GetReference(Constants.Firebase.pupils).Child(userID + "/" + Constants.Firebase.highestLevelPassed).SetValueAsync(highestLevelPassed).ContinueWith(task =>
            {
                if (task.IsFaulted)
                {
                    Debug.Log("Error occurred updating highest level passed remotely");
                }
                if (task.IsCompleted)
                {
                    Debug.Log("Updated highest level passed remotely okay");
                }
            });
        }
    }

    public void UnregisterPupilRemote(string userID, FirebaseWebRequests.ContinueHandlerFirebase continueHandler) {
        FirebaseApp.DefaultInstance.SetEditorDatabaseUrl("https://juice-blenders-firebase.firebaseio.com/");
        Firebase.Auth.FirebaseAuth auth = Firebase.Auth.FirebaseAuth.DefaultInstance;
        var user = auth.CurrentUser;

        if (user != null)
        {
            FirebaseDatabase.DefaultInstance.GetReference(Constants.Firebase.pupils).Child(userID).RemoveValueAsync().ContinueWith(task =>
            {
                if (task.IsFaulted)
                {
                    Debug.Log("Error occurred deleting pupil data remotely");
                    continueHandler.IsFaulted();
                }
                if (task.IsCompleted)
                {
                    Debug.Log("Deleted pupil data remotely okay");
                    continueHandler.IsCompleted(null);
                }
            });
        }
    }

}