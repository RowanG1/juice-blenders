﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System;

public class TeachersLoginArea : MonoBehaviour
{
    public GameObject emailInput;
    public GameObject passwordInput;
    public GameObject confirmPasswordInput;
    public GameObject feedbackMessage;
    float lastTimeButtonClickedLinkedToFirebaseCall;

    public class TeacherFormData
    {
        public string email;
        public bool allDataValidInPrinciple;
        public float timeDataCaptured;
        public string password;
    }
    // Use this for initialization
    void Start()
    {
        emailInput.GetComponent<InputField>().lineType = InputField.LineType.MultiLineSubmit;
        passwordInput.GetComponent<InputField>().lineType = InputField.LineType.MultiLineSubmit;
        confirmPasswordInput.GetComponent<InputField>().lineType = InputField.LineType.MultiLineSubmit;

        Text emailFeedbackText = emailInput.transform.Find("Description").GetComponent<Text>();
        InputField inputEmailText = emailInput.GetComponent<InputField>();

        inputEmailText.onValueChanged.AddListener(delegate { FirebaseWebRequests.CheckFirebaseEmailExistsGiveUserFeedback(inputEmailText, emailFeedbackText); });
        feedbackMessage.GetComponent<Text>().text = "";

    }

    // Update is called once per frame
    void Update()
    {

    }
    public void OnHomeBtnClick()
    {
        Common.PlayButtonSound();
        SceneManager.LoadScene("MainMenu");
    }

    public void OnRegisterOrLoginClicked()
    {
        lastTimeButtonClickedLinkedToFirebaseCall = Time.realtimeSinceStartup;
        Common.PlayButtonSound();
        TeacherFormData formData = ValidateFormGiveFeedbackAndGetData();
        formData.timeDataCaptured = lastTimeButtonClickedLinkedToFirebaseCall;
        if (formData.allDataValidInPrinciple == true)
        {
            feedbackMessage.GetComponent<Text>().text = Constants.Firebase.pleaseWait;
            EitherCreateNewUserOrLogin(formData);
        }
    }

    TeacherFormData ValidateFormGiveFeedbackAndGetData()
    {
        ClearAllFormFeedback();

        string email = emailInput.GetComponent<InputField>().text;
        string password = passwordInput.GetComponent<InputField>().text;
        string confirmPassword = confirmPasswordInput.GetComponent<InputField>().text;

        bool formIsValid = true;

        if (!FormValidation.IsValidEmail(email))
        {
            emailInput.transform.Find("Description").GetComponent<Text>().text = Constants.PupilForm.defaultEmailDescription + " <color=red>Invalid email</color>";
            formIsValid = false;
        }
        if (!FormValidation.IsValidPassword(password))
        {
            passwordInput.transform.Find("Description").GetComponent<Text>().text = Constants.PupilForm.defaultPasswordDescription + " <color=red>At least: 6 characters.</color>";
            formIsValid = false;
        }
        if (!FormValidation.passwordAndConfirmPasswordMatches(password: password, confirmPassword: confirmPassword))
        {
            confirmPasswordInput.transform.Find("Description").GetComponent<Text>().text = Constants.PupilForm.defaultConfirmPasswordDescription + " <color=red>Does not match password</color>";
            formIsValid = false;
        }

        TeacherFormData teacherFormData = new TeacherFormData();
        teacherFormData.email = email;
        teacherFormData.password = password;

        teacherFormData.allDataValidInPrinciple = formIsValid;

        return teacherFormData;
    }

    public void ClearAllFormFeedback()
    {
        emailInput.transform.Find("Description").GetComponent<Text>().text = Constants.PupilForm.defaultEmailDescription;
        passwordInput.transform.Find("Description").GetComponent<Text>().text = Constants.PupilForm.defaultPasswordDescription;
        confirmPasswordInput.transform.Find("Description").GetComponent<Text>().text = Constants.PupilForm.defaultConfirmPasswordDescription;
        feedbackMessage.GetComponent<Text>().text = "";
    }

    void ClearAllFormInputs()
    {
        emailInput.GetComponent<InputField>().text = "";
        passwordInput.GetComponent<InputField>().text = "";
        confirmPasswordInput.GetComponent<InputField>().text = "";
    }

    public void ForgotPasswordClicked()
    {
        ClearAllFormFeedback();
        Common.PlayButtonSound();
        Firebase.Auth.FirebaseAuth auth = Firebase.Auth.FirebaseAuth.DefaultInstance;
        var emailAddress = emailInput.GetComponent<InputField>().text;

        if (emailAddress == "")
        {
            emailInput.transform.Find("Description").GetComponent<Text>().text = Constants.PupilForm.defaultEmailDescription + " <color=red>cannot be blank</color>";
            return;
        }

        float timeButtonClickedLinkedToFirebaseCall = Time.realtimeSinceStartup;
        lastTimeButtonClickedLinkedToFirebaseCall = timeButtonClickedLinkedToFirebaseCall;

        feedbackMessage.GetComponent<Text>().text = Constants.Firebase.pleaseWait;
        auth.SendPasswordResetEmailAsync(emailAddress).ContinueWith(task =>
        {
            // Email sent.

            if (timeButtonClickedLinkedToFirebaseCall == lastTimeButtonClickedLinkedToFirebaseCall)
            {
                if (task.IsCompleted)
                {
                    feedbackMessage.GetComponent<Text>().text = "<color=green>Reset password: email sent.</color>";
                    ClearAllFormInputs();
                }

                if (task.IsFaulted)
                {
                    feedbackMessage.GetComponent<Text>().text = "<color=red>Error occurred.</color>";
                }
            }
        });
    }

    public void EitherCreateNewUserOrLogin(TeacherFormData formData)
    {
        Firebase.Auth.FirebaseAuth auth = Firebase.Auth.FirebaseAuth.DefaultInstance;

        auth.CreateUserWithEmailAndPasswordAsync(formData.email, formData.password).ContinueWith(task =>
        {
            if (formData.timeDataCaptured == lastTimeButtonClickedLinkedToFirebaseCall)
            {
                if (task.IsCanceled)
                {
                    Debug.LogError("CreateUserWithEmailAndPasswordAsync was canceled.");
                    return;
                }
                if (task.IsFaulted)
                {
                    //  Debug.LogError("CreateUserWithEmailAndPasswordAsync encountered an error: " + task.Exception);

                    string errorMsg = FirebaseWebRequests.GetErrorMessageFromException(task.Exception.InnerExceptions[0] as Exception);
                    Debug.Log("Error from error msg method is: " + errorMsg);
                    if (errorMsg == Constants.FirebaseAuthErrorCodes.emailAlreadyInUse)
                    {
                        Debug.Log("Trying to log in");
                        LoginWithCredentials(formData); // Possibly user already exists, so try login with credentials
                    }
                    else
                    {
                        feedbackMessage.GetComponent<Text>().text = "<color=red>Error occurred</color>";
                    }
                    return;
                }

                if (task.IsCompleted)
                {
                    // Firebase user has been created.
                    Firebase.Auth.FirebaseUser newUser = task.Result;
                    Debug.LogFormat("Firebase user created successfully: {0} ({1})",
                        newUser.DisplayName, newUser.UserId);

                    return;
                }

                Debug.Log("Unknown issue when creating new user");
            }
        });
    }

    void LoginWithCredentials(TeacherFormData formData)
    {
        Firebase.Auth.FirebaseAuth auth = Firebase.Auth.FirebaseAuth.DefaultInstance;

        auth.SignInWithEmailAndPasswordAsync(formData.email, formData.password).ContinueWith(task =>
        {
            Debug.Log("Task executing in login in task handler");
            if (formData.timeDataCaptured == lastTimeButtonClickedLinkedToFirebaseCall)
            {
                Debug.Log("Executing task decision tree for log in");

                if (task.IsCanceled)
                {
                    Debug.LogError("SignInWithEmailAndPasswordAsync was canceled.");
                    feedbackMessage.GetComponent<Text>().text = "<color=red>Error occurred</color>";
                    return;
                }
                if (task.IsFaulted)
                {
                    Debug.LogError("SignInWithEmailAndPasswordAsync encountered an error: " + task.Exception);
                    string errorMsg = FirebaseWebRequests.GetErrorMessageFromException(task.Exception.InnerExceptions[0] as Exception);
                    if (errorMsg == Constants.FirebaseAuthErrorCodes.wrongPassword)
                    {
                        feedbackMessage.GetComponent<Text>().text = "<color=red>Wrong password</color>";
                    }
                    else
                    {
                        feedbackMessage.GetComponent<Text>().text = "<color=red>Error occurred</color>";
                    }
                    return;
                }

                Firebase.Auth.FirebaseUser user = task.Result;
                Debug.LogFormat("User signed in successfully: {0} ({1})", user.DisplayName, user.UserId);
                feedbackMessage.GetComponent<Text>().text = "<color=green>Login success.</color>";
                StartCoroutine(WaitToShowSuccessAndGoToTeachersAdmin());
            }
        });
    }

    IEnumerator WaitToShowSuccessAndGoToTeachersAdmin()
    {
        yield return new WaitForSeconds(1.2f);
        SceneManager.LoadScene("TeachersClassCodeArea");
        yield return 0;
    }
}
