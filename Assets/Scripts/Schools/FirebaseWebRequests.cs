using UnityEngine;
using System;
using UnityEngine.Networking;

using System.Collections;
using System.Collections.Generic;
using Firebase.Auth;
using UnityEngine.UI;

public class FirebaseWebRequests
{
    public IEnumerator MakeRequest(UnityWebRequest uwr, ContinueHandlerFirebase handler)
    {
        uwr.timeout = 8;
        using (uwr)
        {
        // #if UNITY_ANDROID
        //     yield return uwr.SendWebRequest();
        // #endif

     //   #if UNITY_IOS
            yield return uwr.Send();
     //   #endif

            Debug.Log("Server response body is: ");
            Debug.Log(uwr.downloadHandler.text);
            Debug.Log("Response code is:" + uwr.responseCode);


        // #if UNITY_ANDROID
        //     if (uwr.isNetworkError || uwr.isHttpError)
        // #endif

     //   #if UNITY_IOS
            if (uwr.isError)
      //  #endif

            {
                Debug.Log("Error in MakeRequest func: " + uwr.error);

                if (uwr.downloadHandler.text != "")
                {
                    handler.IsFaulted(uwr.downloadHandler.text);
                }
                else
                {
                    handler.IsFaulted();
                }
            }
            else
            {
                // Show results as text
                Debug.Log("Download handler text is: " + uwr.downloadHandler.text);
                handler.IsCompleted(uwr.downloadHandler.text);
                // Or retrieve results as binary data
            }
        }
    }

    public abstract class ContinueHandlerFirebase
    {

        public abstract void IsFaulted();
        public abstract void IsFaulted(string serverResponseBody);
        public abstract void IsCompleted(string serverResponseBody);

    }

    public static string GetErrorMessageFromException(Exception exception)
    {
        // Debug.Log(exception.ToString());
        Firebase.FirebaseException firebaseEx = exception as Firebase.FirebaseException;
        if (firebaseEx != null)
        {
            var errorCode = (AuthError)firebaseEx.ErrorCode;
            return GetErrorMessageFromCode(errorCode);
        }
        Debug.Log("Whoops. firebase ex was null");
        return exception.ToString();
    }

    private static string GetErrorMessageFromCode(AuthError errorCode)
    {
        var message = "";
        switch (errorCode)
        {
            case AuthError.AccountExistsWithDifferentCredentials:
                message = Constants.FirebaseAuthErrorCodes.accountExists;
                break;
            case AuthError.MissingPassword:
                message = Constants.FirebaseAuthErrorCodes.missingPassword;
                break;
            case AuthError.WeakPassword:
                message = Constants.FirebaseAuthErrorCodes.weakPassword;
                break;
            case AuthError.WrongPassword:
                message = Constants.FirebaseAuthErrorCodes.wrongPassword;
                break;
            case AuthError.EmailAlreadyInUse:
                message = Constants.FirebaseAuthErrorCodes.emailAlreadyInUse;
                break;
            case AuthError.InvalidEmail:
                message = Constants.FirebaseAuthErrorCodes.invalidEmail;
                break;
            case AuthError.MissingEmail:
                message = Constants.FirebaseAuthErrorCodes.missingEmail;
                break;
            default:
                message = Constants.FirebaseAuthErrorCodes.unknownError;
                break;
        }
        Debug.Log("Error message inside get error message func is: " + message);
        return message;
    }

    public static void CheckFirebaseEmailExistsGiveUserFeedback(InputField inputEmailText, Text feedbackText)
    {
        Debug.Log("Email value Changed");
        Firebase.Auth.FirebaseAuth auth;
        auth = Firebase.Auth.FirebaseAuth.DefaultInstance;
        feedbackText.text = Constants.PupilForm.defaultEmailDescription;
        string email = inputEmailText.text;

        if (FormValidation.IsValidEmail(email))
        {

            auth.FetchProvidersForEmailAsync(email).ContinueWith(task =>
            {
                if (task.IsCompleted)
                {
                    foreach (string provider in task.Result)
                    {
                        //  Debug.Log("Provider: " + provider);
                        if (provider == Constants.Firebase.passwordProvider)
                        {
                            Debug.Log("Email already exists");
                            feedbackText.text = Constants.PupilForm.defaultEmailDescription + " <color=blue>User email exists</color>";
                            return;
                        }
                    }

                    feedbackText.text = Constants.PupilForm.defaultEmailDescription + " <color=blue>Email available to create new user</color>";
                }

                if (task.IsFaulted)
                {
                    Debug.Log("Error with fetch email provider.");
                }
            });
        }
        // emailInput.transform.Find("Description").GetComponent<Text>().text = 
    }
}