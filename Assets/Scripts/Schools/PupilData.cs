using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;
using System.Text.RegularExpressions;
using Firebase;
using Firebase.Auth;
using Firebase.Unity.Editor;
using Firebase.Database;

[System.Serializable]
public class PupilData: UserData
{
    public string name;
    public string surname;
    public string classCode;
    public string highestLevelPassed;

}



