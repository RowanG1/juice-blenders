﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;
using System.Text.RegularExpressions;
using Firebase;
using Firebase.Auth;
using Firebase.Unity.Editor;
using Firebase.Messaging;
using Firebase.Database;
using UnityEngine.SceneManagement;


public class PupilArea : MonoBehaviour
{
    public GameObject emailInput;
    public GameObject nameInput;
    public GameObject surnameInput;
    public GameObject passwordInput;
    public GameObject confirmPasswordInput;
    public GameObject feedbackMessage;
    public GameObject logoutBtn;
    Firebase.Auth.FirebaseAuth auth;

    float lastTimUserInputTriggeredFirebaseCall;


    public GameObject classCodeInput;

    public class PupilFormData : PupilData
    {
        public string email;
        public bool allDataValidInPrinciple;
        public bool registerOrRelink;
        public bool unregisterUserRequest;
        public float timeDataCaptured;
        public string password;
    }


    // Use this for initialization
    void Start()
    {
        FirebaseApp.DefaultInstance.SetEditorDatabaseUrl("https://juice-blenders-firebase.firebaseio.com/");
        feedbackMessage.GetComponent<Text>().text = "";

        emailInput.GetComponent<InputField>().lineType = InputField.LineType.MultiLineSubmit;
        nameInput.GetComponent<InputField>().lineType = InputField.LineType.MultiLineSubmit;
        surnameInput.GetComponent<InputField>().lineType = InputField.LineType.MultiLineSubmit;
        passwordInput.GetComponent<InputField>().lineType = InputField.LineType.MultiLineSubmit;
        confirmPasswordInput.GetComponent<InputField>().lineType = InputField.LineType.MultiLineSubmit;
        classCodeInput.GetComponent<InputField>().lineType = InputField.LineType.MultiLineSubmit;

        Text emailFeedbackText = emailInput.transform.Find("Description").GetComponent<Text>();
        InputField inputEmailText = emailInput.GetComponent<InputField>();

        emailInput.GetComponent<InputField>().onValueChanged.AddListener(delegate { FirebaseWebRequests.CheckFirebaseEmailExistsGiveUserFeedback(inputEmailText, emailFeedbackText); });

        auth = Firebase.Auth.FirebaseAuth.DefaultInstance;

        if (auth.CurrentUser == null)
        {
            logoutBtn.SetActive(false);
        }
    }

    public void RegisterOrRelinkBtnClicked()
    {
        Common.PlayButtonSound();
        lastTimUserInputTriggeredFirebaseCall = Time.realtimeSinceStartup;

        PupilFormData formData = GetFormData();
        formData.registerOrRelink = true;
        formData.highestLevelPassed = new LevelsFileHandler().GetMaxLevelPassed().maxLevelPassed.ToString();
        formData.timeDataCaptured = lastTimUserInputTriggeredFirebaseCall;
        if (formData.allDataValidInPrinciple == true)
        {
            feedbackMessage.GetComponent<Text>().text = Constants.Firebase.pleaseWait;
            CheckIfClassCodeExistsThenSignInUserIfSo(formData);
        }
    }

    PupilFormData GetFormData()
    {
        ClearAllFormFeedback();

        string email = emailInput.GetComponent<InputField>().text;
        string password = passwordInput.GetComponent<InputField>().text;
        string classCode = classCodeInput.GetComponent<InputField>().text;
        string name = nameInput.GetComponent<InputField>().text;
        string surname = surnameInput.GetComponent<InputField>().text;
        string confirmPassword = confirmPasswordInput.GetComponent<InputField>().text;

        bool formIsValid = true;

        if (!FormValidation.IsValidEmail(email))
        {
            emailInput.transform.Find("Description").GetComponent<Text>().text = Constants.PupilForm.defaultEmailDescription + " <color=red>Invalid email</color>";
            formIsValid = false;
        }
        if (!FormValidation.IsValidPassword(password))
        {
            passwordInput.transform.Find("Description").GetComponent<Text>().text = Constants.PupilForm.defaultPasswordDescription + " <color=red>At least 6 characters</color>";
            formIsValid = false;
        }
        if (!FormValidation.passwordAndConfirmPasswordMatches(password: password, confirmPassword: confirmPassword))
        {
            confirmPasswordInput.transform.Find("Description").GetComponent<Text>().text = Constants.PupilForm.defaultConfirmPasswordDescription + " <color=red>Does not match password</color>";
            formIsValid = false;
        }
        if (name.Length == 0)
        {
            nameInput.transform.Find("Description").GetComponent<Text>().text = Constants.PupilForm.defaultNameDescription + " <color=red>Name cannot be blank</color>";
            formIsValid = false;
        }
        if (surname.Length == 0)
        {
            surnameInput.transform.Find("Description").GetComponent<Text>().text = Constants.PupilForm.defaultSurnameDescription + " <color=red>Surname cannot be blank</color>";
            formIsValid = false;
        }

        if (classCode.Length == 0)
        {
            classCodeInput.transform.Find("Description").GetComponent<Text>().text = Constants.PupilForm.defaultClassCodeDescription + " <color=red>Class code cannot be blank</color>";
            formIsValid = false;
        }

        PupilFormData pupilFormData = new PupilFormData();
        pupilFormData.email = email;
        pupilFormData.password = password;
        pupilFormData.name = name;
        pupilFormData.surname = surname;
        pupilFormData.classCode = classCode;
        pupilFormData.allDataValidInPrinciple = formIsValid;

        return pupilFormData;
    }

    void ClearAllFormFeedback()
    {
        emailInput.transform.Find("Description").GetComponent<Text>().text = Constants.PupilForm.defaultEmailDescription;
        passwordInput.transform.Find("Description").GetComponent<Text>().text = Constants.PupilForm.defaultPasswordDescription;
        confirmPasswordInput.transform.Find("Description").GetComponent<Text>().text = Constants.PupilForm.defaultConfirmPasswordDescription;
        nameInput.transform.Find("Description").GetComponent<Text>().text = Constants.PupilForm.defaultNameDescription;
        surnameInput.transform.Find("Description").GetComponent<Text>().text = Constants.PupilForm.defaultSurnameDescription;
        classCodeInput.transform.Find("Description").GetComponent<Text>().text = Constants.PupilForm.defaultClassCodeDescription;

        feedbackMessage.GetComponent<Text>().text = "";
    }

    void ClearAllFormData()
    {
        ClearAllFormFeedback();
        ClearAllFormInputs();
    }
    void ClearAllFormInputs()
    {
        emailInput.GetComponent<InputField>().text = "";
        passwordInput.GetComponent<InputField>().text = "";
        confirmPasswordInput.GetComponent<InputField>().text = "";
        nameInput.GetComponent<InputField>().text = "";
        surnameInput.GetComponent<InputField>().text = "";
        classCodeInput.GetComponent<InputField>().text = "";
    }

    void CheckIfClassCodeExistsThenSignInUserIfSo(PupilFormData formData)
    {
        FirebaseDatabase.DefaultInstance.GetReference(Constants.Firebase.classCodes).OrderByChild(Constants.Firebase.classCode).EqualTo(formData.classCode).GetValueAsync().ContinueWith(task =>
        {
            if (task.IsFaulted)
            {
                    // Handle the error...
                    this.feedbackMessage.GetComponent<Text>().text = "<color=red>Error occurred</color>";
                Debug.Log("Task result is: " + task.Result);
            }
            else if (task.IsCompleted)
            {
                DataSnapshot snapshot = task.Result;
                if (snapshot.Value != null)
                {
                    Debug.Log(formData.classCode + " Class code already exists");
                    EitherCreateNewUserOrLoginAndUpdateDetails(formData);
                }
                else
                {
                    Debug.Log(formData.classCode + " Class code does not exist");
                    this.feedbackMessage.GetComponent<Text>().text = "";
                    classCodeInput.transform.Find("Description").GetComponent<Text>().text = Constants.PupilForm.defaultClassCodeDescription + " <color=red>Class code does not exist</color>";
                }
                    // Do something with snapshot...
                }
        });
    }

    public void EitherCreateNewUserOrLoginAndUpdateDetails(PupilFormData pupilFormData)
    {

        auth.CreateUserWithEmailAndPasswordAsync(pupilFormData.email, pupilFormData.password).ContinueWith(task =>
        {
            if (pupilFormData.timeDataCaptured == lastTimUserInputTriggeredFirebaseCall)
            {
                if (task.IsCanceled)
                {
                    Debug.LogError("CreateUserWithEmailAndPasswordAsync was canceled.");
                    return;
                }
                if (task.IsFaulted)
                {
                        //  Debug.LogError("CreateUserWithEmailAndPasswordAsync encountered an error: " + task.Exception);

                        string errorMsg = FirebaseWebRequests.GetErrorMessageFromException(task.Exception.InnerExceptions[0] as Exception);
                    Debug.Log("Error from error msg method is: " + errorMsg);
                    if (errorMsg == Constants.FirebaseAuthErrorCodes.emailAlreadyInUse)
                    {
                        Debug.Log("Trying to log in");
                        LoginWithCredentialsAndUpdateDetails(pupilFormData); // Possibly user already exists, so try login with credentials
                    }
                    else
                    {
                        feedbackMessage.GetComponent<Text>().text = "<color=red>Error occurred</color>";
                    }
                    return;
                }

                if (task.IsCompleted)
                {
                        // Firebase user has been created.
                        Firebase.Auth.FirebaseUser newUser = task.Result;
                    FirebaseMessaging.Subscribe(Constants.Firebase.pupil);
                    Debug.LogFormat("Firebase user created successfully: {0} ({1})",
                        newUser.DisplayName, newUser.UserId);
                    pupilFormData.userID = newUser.UserId;
                    feedbackMessage.GetComponent<Text>().text = "<color=blue>New user created. Please wait...</color>";
                    UpdatePupilDetails(pupilFormData: pupilFormData);
                    return;
                }

                Debug.Log("Unknown issue when creating new user");
            }
        });
    }

    void LoginWithCredentialsAndUpdateDetails(PupilFormData pupilData)
    {
        Firebase.Auth.FirebaseAuth auth = Firebase.Auth.FirebaseAuth.DefaultInstance;

        auth.SignInWithEmailAndPasswordAsync(pupilData.email, pupilData.password).ContinueWith(task =>
        {
            Debug.Log("Task executing in login in task handler");
            if (pupilData.timeDataCaptured == lastTimUserInputTriggeredFirebaseCall)
            {
                Debug.Log("Executing task decision tree for log in");

                if (task.IsCanceled)
                {
                    Debug.LogError("SignInWithEmailAndPasswordAsync was canceled.");
                    feedbackMessage.GetComponent<Text>().text = "<color=red>Error occurred</color>";
                    return;
                }
                if (task.IsFaulted)
                {
                    Debug.LogError("SignInWithEmailAndPasswordAsync encountered an error: " + task.Exception);
                    string errorMsg = FirebaseWebRequests.GetErrorMessageFromException(task.Exception.InnerExceptions[0] as Exception);
                    if (errorMsg == Constants.FirebaseAuthErrorCodes.wrongPassword)
                    {
                        feedbackMessage.GetComponent<Text>().text = "<color=red>Wrong password</color>";
                    }
                    else
                    {
                        feedbackMessage.GetComponent<Text>().text = "<color=red>Error occurred</color>";
                    }
                    return;
                }

                Firebase.Auth.FirebaseUser user = task.Result;
                Debug.LogFormat("User signed in successfully: {0} ({1})", user.DisplayName, user.UserId);
                feedbackMessage.GetComponent<Text>().text = "<color=blue>Login success. Please wait while details update...</color>";
                pupilData.userID = user.UserId;
                UpdatePupilDetails(pupilFormData: pupilData);
            }
        });
    }

    public void UpdatePupilDetails(PupilFormData pupilFormData)
    {

        FirebaseContinueHandlerPupilAdminUpdate firebaseContinueHandlerPupilAdminUpdate = new FirebaseContinueHandlerPupilAdminUpdate(pupilArea: this, pupilFormData: pupilFormData);

        if (pupilFormData.unregisterUserRequest == true)
        {
            new PupilDataStorage().UnregisterPupilRemote(pupilFormData.userID, firebaseContinueHandlerPupilAdminUpdate);
        }
        else
        {
            PupilData pupilData = (PupilData)pupilFormData;
            new PupilDataStorage().UpdatePupilDataRemote(pupilData, firebaseContinueHandlerPupilAdminUpdate);
        }
    }

    class FirebaseContinueHandlerPupilAdminUpdate : FirebaseWebRequests.ContinueHandlerFirebase
    {
        PupilArea pupilArea;
        PupilFormData pupilFormData;
        public FirebaseContinueHandlerPupilAdminUpdate(PupilArea pupilArea, PupilFormData pupilFormData)
        {
            this.pupilArea = pupilArea;
            this.pupilFormData = pupilFormData;
        }
        override public void IsFaulted()
        {
            pupilArea.feedbackMessage.GetComponent<Text>().text = "<color=red>Login success, but failed to update details.</color>";
        }

        override public void IsFaulted(string serverResponseBody)
        {
            pupilArea.feedbackMessage.GetComponent<Text>().text = "<color=red>Login success, but failed to update details.</color>";
        }
        override public void IsCompleted(string serverResponseBody)
        {
            pupilArea.feedbackMessage.GetComponent<Text>().text = "<color=green>Success</color>";
            pupilArea.ClearAllFormInputs();

            if (pupilFormData.registerOrRelink == true)
            {
                //Update local storage
                PupilData pupilData = new PupilData();
                pupilData.name = pupilFormData.name;
                pupilData.classCode = pupilFormData.classCode;
                new PupilDataStorage().SaveToLocalDevice(pupilData);
                pupilArea.logoutBtn.SetActive(true);
            }

            if (pupilFormData.unregisterUserRequest == true)
            {
                new PupilDataStorage().DeleteLocalFile();
                FirebaseMessaging.Unsubscribe(Constants.Firebase.pupil);
                pupilArea.ClearAllFormData();
                Firebase.Auth.FirebaseAuth auth = Firebase.Auth.FirebaseAuth.DefaultInstance;
                pupilArea.feedbackMessage.GetComponent<Text>().text = "<color=green>De-registration complete.</color>";
                auth.SignOut();
                pupilArea.logoutBtn.SetActive(false);
            }

        }
    }


    public void ForgotPasswordClicked()
    {
        ClearAllFormFeedback();
        Common.PlayButtonSound();
        Firebase.Auth.FirebaseAuth auth = Firebase.Auth.FirebaseAuth.DefaultInstance;
        var emailAddress = emailInput.GetComponent<InputField>().text;

        if (emailAddress == "")
        {
            emailInput.transform.Find("Description").GetComponent<Text>().text = Constants.PupilForm.defaultEmailDescription + " <color=red>cannot be blank</color>";
            return;
        }

        float timeButtonClickedLinkedToFirebaseCall = Time.realtimeSinceStartup;
        lastTimUserInputTriggeredFirebaseCall = timeButtonClickedLinkedToFirebaseCall;

        feedbackMessage.GetComponent<Text>().text = Constants.Firebase.pleaseWait;
        auth.SendPasswordResetEmailAsync(emailAddress).ContinueWith(task =>
        {
            // Email sent.
            if (timeButtonClickedLinkedToFirebaseCall == lastTimUserInputTriggeredFirebaseCall)
            {
                if (task.IsCompleted)
                {
                    feedbackMessage.GetComponent<Text>().text = "<color=green>Reset password: email sent.</color>";
                    ClearAllFormInputs();
                }

                if (task.IsFaulted)
                {
                    feedbackMessage.GetComponent<Text>().text = "<color=red>Error occurred.</color>";
                }
            }
        });
    }

    public void UnregisterUser()
    {
        ClearAllFormFeedback();
        Common.PlayButtonSound();
        PupilFormData pupilFormData = new PupilFormData();
        string email = emailInput.GetComponent<InputField>().text;
        string password = passwordInput.GetComponent<InputField>().text;

        pupilFormData.email = email;
        pupilFormData.password = password;
        pupilFormData.unregisterUserRequest = true;

        bool emailOrPasswordBlank = false;

        if (email == "")
        {
            emailInput.transform.Find("Description").GetComponent<Text>().text = Constants.PupilForm.defaultEmailDescription + " <color=red>cannot be blank</color>";
            emailOrPasswordBlank = true;
        }

        if (password == "")
        {
            passwordInput.transform.Find("Description").GetComponent<Text>().text = Constants.PupilForm.defaultPasswordDescription + " <color=red>cannot be blank</color>";
            emailOrPasswordBlank = true;
        }

        if (emailOrPasswordBlank == false)
        {
            feedbackMessage.GetComponent<Text>().text = Constants.Firebase.pleaseWait;
            lastTimUserInputTriggeredFirebaseCall = Time.realtimeSinceStartup;
            pupilFormData.timeDataCaptured = lastTimUserInputTriggeredFirebaseCall;
            LoginWithCredentialsAndUpdateDetails(pupilFormData);
        }
    }

    public void LogoutBtnClicked()
    {
        auth.SignOut();
        SceneManager.LoadScene("MainMenu");
    }

    public void OnHomeBtnClick()
    {
        Common.PlayButtonSound();
        SceneManager.LoadScene("MainMenu");
    }

}

