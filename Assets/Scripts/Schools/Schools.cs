﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Firebase;
using Firebase.Auth;

public class Schools : MonoBehaviour
{
    Firebase.Auth.FirebaseAuth auth;
    // Use this for initialization
    void Start()
    {
        auth = Firebase.Auth.FirebaseAuth.DefaultInstance;
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void OpenTeachers()
    {
        var user = auth.CurrentUser;
        Common.PlayButtonSound();
        if (user != null)
        {
            // User is signed in.
            Debug.Log("User is signed in with id: " + user.UserId);
         	SceneManager.LoadScene("TeachersClassCodeArea");
        }
        else
        {
            // No user is signed in.
            Debug.Log("User is not signed in");
            SceneManager.LoadScene("TeachersLogin");
        }
    }

    public void OpenPupils()
    {
        Common.PlayButtonSound();
        SceneManager.LoadScene("Pupils");
    }

         public void OnHomeBtnClick()
    {
        Common.PlayButtonSound();
        SceneManager.LoadScene("MainMenu");
    }
}
