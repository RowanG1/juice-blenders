﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Linq;

public class PupilsMatchingClassCodeArea : MonoBehaviour
{

    // Use this for initialization
    public Canvas canvas;
    public GameObject title;
    float lastVerticalScrollPosition;
    public GameObject pupilsViewPort;
    public GameObject pupilsContent;
    public GameObject nameSearchInputField;
    public GameObject pupilToDisplayPrefab;
    List<TeachersClassCodeArea.PupilsToDisplayToTeacher> pupilData = new List<TeachersClassCodeArea.PupilsToDisplayToTeacher>();
    List<TeachersClassCodeArea.PupilsToDisplayToTeacher> pupilDataMatchingSearchTerm = new List<TeachersClassCodeArea.PupilsToDisplayToTeacher>();
    float pupilRowHeight = 20;

    public SetAllFontSizesToSmallestInGroup setAllFontSizesToSmallestInGroup;
    public ScrollRectVerticalPositionHolderOnContentHeightChange scrollRectVerticalPositionHolder;


    void Start()
    {

        //  new SetAllFontSizesToSmallestInGroup(canvas);

        // DataTransferToPupilsListMatchingClassCode.pupilData = new List<TeachersClassCodeArea.PupilsToDisplayToTeacher>();
        // TeachersClassCodeArea.PupilsToDisplayToTeacher newPupil = new TeachersClassCodeArea.PupilsToDisplayToTeacher();
        // newPupil.name = "Fred";
        // newPupil.highestLevelPassed = 2;
        // DataTransferToPupilsListMatchingClassCode.pupilData.Add(newPupil);

        // newPupil = new TeachersClassCodeArea.PupilsToDisplayToTeacher();
        // newPupil.name = "Grace";
        // newPupil.highestLevelPassed = 3;
        // DataTransferToPupilsListMatchingClassCode.pupilData.Add(newPupil);

        pupilData = DataTransferToPupilsListMatchingClassCode.pupilData;

        pupilData = pupilData.OrderByDescending(pupil => pupil.highestLevelPassed).ToList();
        pupilDataMatchingSearchTerm = pupilData; // All data is displayed by default

        title.GetComponent<Text>().text = "Pupils for class code: <color=blue>" + DataTransferToPupilsListMatchingClassCode.classCode + "</color>";
        RefreshPupilsDisplayed();

        nameSearchInputField.GetComponent<InputField>().onValueChanged.AddListener(delegate { SearchNamesAndUpdatePupilsDisplayed(); });

    }

    void SearchNamesAndUpdatePupilsDisplayed()
    {
        pupilDataMatchingSearchTerm = new List<TeachersClassCodeArea.PupilsToDisplayToTeacher>();
        string searchTerm = nameSearchInputField.GetComponent<InputField>().text.ToLower();
        Debug.Log("Search term is: " + searchTerm);
        foreach (TeachersClassCodeArea.PupilsToDisplayToTeacher pupil in pupilData)
        {
            Debug.Log("Searching through pupils");
            string name;
            if (pupil.name != null)
            {
                name = pupil.name;
            }
            else
            {
                name = "";
            }

            string surname;
            if (pupil.surname != null)
            {
                surname = pupil.surname;
            }
            else
            {
                surname = "";
            }

            if (name.ToLower().Contains(searchTerm) || surname.ToLower().Contains(searchTerm))
            {
                pupilDataMatchingSearchTerm.Add(pupil);
                Debug.Log("Name or surname contains search term");
            }
        }
        if (!CurrentDisplayedMatchesSearchedPupils())
        {
            Debug.Log("Refreshing table because pupils that match search term differ from displayed table");
            RefreshPupilsDisplayed();
        }
    }

    bool CurrentDisplayedMatchesSearchedPupils()
    {
        if (pupilDataMatchingSearchTerm.Count == pupilsContent.transform.childCount)
        {
            var index = 0;
            foreach (TeachersClassCodeArea.PupilsToDisplayToTeacher pupil in pupilDataMatchingSearchTerm)
            {
                Debug.Log("Pupil name in search list is: " + pupil.name);
                Debug.Log("Pupil surname in search list is: " + pupil.surname);

                string displayedPupilName = pupilsContent.transform.GetChild(index).GetComponent<EachListedPupilMatchingClassCode>().pupilName.GetComponent<Text>().text;
                Debug.Log("Displayed pupil name at same index is:" + displayedPupilName);

                string displayedPupilSurname = pupilsContent.transform.GetChild(index).GetComponent<EachListedPupilMatchingClassCode>().surname.GetComponent<Text>().text;
                Debug.Log("Displayed pupil surname at same index is:" + displayedPupilSurname);

                string name;
                if (pupil.name != null)
                {
                    name = pupil.name;
                }
                else
                {
                    name = "";
                }

                string surname;
                if (pupil.surname != null)
                {
                    surname = pupil.surname;
                }
                else
                {
                    surname = "";
                }

                if (name != displayedPupilName)
                {
                    Debug.Log("Name didn't match");
                    return false;
                }

                if (surname != displayedPupilSurname)
                {
                    Debug.Log("Surname didn't match");
                    Debug.Log("Length one is: " + pupil.surname.Length);
                    Debug.Log("Length other is: " + displayedPupilSurname.Length);
                    return false;
                }
                index++;
            }
        }
        else
        {
            return false;
        }
        return true;
    }


    void RefreshPupilsDisplayed()
    {
        SetPupilsContentPort(pupilDataMatchingSearchTerm.Count);
        DrawPupils();
    }

    void SetPupilsContentPort(int cellDisplayCount)
    {
        pupilsContent.GetComponent<RectTransform>().sizeDelta = new Vector2(0, pupilRowHeight * cellDisplayCount + 2);
        scrollRectVerticalPositionHolder.ContentHeightUpdated();
    }


    void DrawPupils()
    {

        foreach (Transform child in pupilsContent.transform)
        {
            Destroy(child.gameObject);
        }

        List<Text> textItems = new List<Text>();

        for (int k = 0; k < pupilDataMatchingSearchTerm.Count; k++)
        {
            TeachersClassCodeArea.PupilsToDisplayToTeacher thisPupil = pupilDataMatchingSearchTerm[k];

            GameObject pupilItemInst = Instantiate(pupilToDisplayPrefab) as GameObject;
            pupilItemInst.name = "Pupil" + k;
            pupilItemInst.transform.SetParent(pupilsContent.transform, false);

            pupilItemInst.transform.GetComponent<LayoutElement>().minHeight = pupilRowHeight;
            pupilItemInst.transform.GetComponent<LayoutElement>().preferredHeight = pupilRowHeight;

            pupilItemInst.GetComponent<EachListedPupilMatchingClassCode>().pupilName.GetComponent<Text>().text = thisPupil.name;
            pupilItemInst.GetComponent<EachListedPupilMatchingClassCode>().surname.GetComponent<Text>().text = thisPupil.surname;
            pupilItemInst.GetComponent<EachListedPupilMatchingClassCode>().highestLevelPassed.GetComponent<Text>().text = thisPupil.highestLevelPassed.ToString();

            foreach (Text pupilTextItem in pupilItemInst.GetComponentsInChildren<Text>())
            {
                textItems.Add(pupilTextItem);
            }
        }

        // textItems = ((SetAllFontSizesToSmallestInGroup.Interface)this).GetListTextItemsToSetSmallestFont();
        Debug.Log("Length of text items is: " + textItems.Count);
        setAllFontSizesToSmallestInGroup.SetTextItems(textItems);
        // SetAllTableTextToSmallestFontSize();
    }


    // Update is called once per frame
    void Update()
    {

    }

    public void OnHomeBtnClick()
    {
        Common.PlayButtonSound();
        SceneManager.LoadScene("MainMenu");
    }

}
