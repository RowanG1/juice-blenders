﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Firebase;
using Firebase.Unity.Editor;
using Firebase.Messaging;
using Firebase.Database;
using Firebase.Auth;
using UnityEngine.UI;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;


public class TeachersClassCodeArea : MonoBehaviour
{
    public GameObject classCodeText;
    public GameObject feedback;
    public delegate void FirebaseGetClassCodesDelegate(List<string> classcodes);

    Firebase.Auth.FirebaseAuth auth;
    // Use this for initialization
    void Start()
    {
        auth = Firebase.Auth.FirebaseAuth.DefaultInstance;
        FirebaseApp.DefaultInstance.SetEditorDatabaseUrl("https://juice-blenders-firebase.firebaseio.com/");
        classCodeText.GetComponent<InputField>().text = "";
        classCodeText.GetComponent<InputField>().text = "";
        classCodeText.GetComponent<InputField>().lineType = InputField.LineType.MultiLineSubmit;
        feedback.GetComponent<Text>().text = "";

        classCodeText.GetComponent<InputField>().onValueChanged.AddListener(delegate { ClassCodeChanged(); });


    }

    void ClassCodeChanged()
    {
        feedback.GetComponent<Text>().text = "";
    }

    public void GenerateClassCodeBtnClicked()
    {
        //GetExistingClassCodes(GenerateAndDisplayClassCodeAfterCodesInDatabaseReceived);
        feedback.GetComponent<Text>().text = "";
    
        if (auth.CurrentUser != null)
        {
            feedback.GetComponent<Text>().text = Constants.Firebase.pleaseWait;
            CallFirebaseCloudFunctionNewClassCodeGenerate();
            FirebaseMessaging.Subscribe(Constants.Firebase.teacher);
        }
        else
        {
            feedback.GetComponent<Text>().text = "User not logged in";
        }
    }

    public void ViewResultsBtnClicked()
    {
        feedback.GetComponent<Text>().text = "";
    
        string classCode = classCodeText.GetComponent<InputField>().text;

        if (classCode.Length != Constants.Firebase.lengthClasscode) {
            feedback.GetComponent<Text>().text = "<color=red>Class code is not valid</color>";
            return;
        }
        if (auth.CurrentUser != null)
        {
            feedback.GetComponent<Text>().text = Constants.Firebase.pleaseWait;
            CallFirebaseCloudFunctionFetchPupilsWithMatchingClassCode(classCode);
        }
        else
        {
            feedback.GetComponent<Text>().text = "<color=red>User not logged in</color>";
        }
    }

    public class TeacherCreatedByJSONStructure
    {
        public string createdByUserID;
    }
    void CallFirebaseCloudFunctionNewClassCodeGenerate()
    {     
            auth.CurrentUser.TokenAsync(true).ContinueWith(task =>
         {
             if (task.IsFaulted)
             {
                 Debug.LogError("TokenAsync encountered an error: ");
                 feedback.GetComponent<Text>().text = "<color=red>Error occurred. Connected to internet?</color>";
                 return;
             }

             var accessToken = task.Result;
             Debug.Log("Token from client is: " + accessToken);

             UnityWebRequest uwr = new UnityWebRequest("https://us-central1-juice-blenders-firebase.cloudfunctions.net/getNewClassCode", "GET");
             // uwr.uploadHandler = (UploadHandler)new UploadHandlerRaw(bodyRaw);
             uwr.downloadHandler = (DownloadHandler)new DownloadHandlerBuffer();
             uwr.SetRequestHeader("Content-Type", "application/json");
             uwr.SetRequestHeader("Authorization", "Bearer " + accessToken);
             GeneratedClassCodeContinueHandler handler = new GeneratedClassCodeContinueHandler(classCodeText, feedback);
             StartCoroutine(new FirebaseWebRequests().MakeRequest(uwr, handler));
         });
    }

    class GeneratedClassCodeContinueHandler : FirebaseWebRequests.ContinueHandlerFirebase
    {
        GameObject classCodeText;
        GameObject feedback;

        public GeneratedClassCodeContinueHandler(GameObject classCodeText, GameObject feedback)
        {
            this.classCodeText = classCodeText;
            this.feedback = feedback;
        }

        override public void IsFaulted()
        {
            feedback.GetComponent<Text>().text = "<color=red>Error occurred</color>";
        }

        override public void IsFaulted(string serverResponseBody)
        {
            feedback.GetComponent<Text>().text = "<color=red>Error occurred</color>";
        }
        override public void IsCompleted(string serverResponseBody)
        {
            ClassCodeJSONStructure classCodeObject = JsonUtility.FromJson<ClassCodeJSONStructure>(serverResponseBody);
            string classCode = classCodeObject.classCode;
            Debug.Log("Class code in converted json structure: " + classCode);
            UpdateClassCodeTextUI(classCode);
        }

        void UpdateClassCodeTextUI(string text)
        {
            classCodeText.GetComponent<InputField>().text = text;
            feedback.GetComponent<Text>().text = "Record code for future use, and give to students to link their results.";
        }
    }

    [Serializable]
    class ClassCodeJSONStructure
    {
        public string classCode;
    }

    void CallFirebaseCloudFunctionFetchPupilsWithMatchingClassCode(string classCode)
    {
        auth.CurrentUser.TokenAsync(true).ContinueWith(task =>
        {
            if (task.IsFaulted)
            {
                Debug.LogError("TokenAsync encountered an error: " + task.Exception);
                feedback.GetComponent<Text>().text = "<color=red>Error occurred. Connected to internet?</color>";
                return;
            }
            var accessToken = task.Result;
            Debug.Log("Token from client is: " + accessToken);

          //  ClassCodeJSONStructure classCodeJSONStructure = new ClassCodeJSONStructure();
           // classCodeJSONStructure.classCode = classCode;
           // string body = JsonUtility.ToJson(classCodeJSONStructure);
           // byte[] bodyRaw = new System.Text.UTF8Encoding().GetBytes(body);

           string url = "https://us-central1-juice-blenders-firebase.cloudfunctions.net/getPupilsMatchingClassCode";
           string urlParameter = "?" + Constants.Firebase.classCode + "=" + classCode;
           string urlWithParameter = url + urlParameter;

            UnityWebRequest uwr = new UnityWebRequest(urlWithParameter, "GET");
           // uwr.uploadHandler = (UploadHandler)new UploadHandlerRaw(bodyRaw);
            uwr.downloadHandler = (DownloadHandler)new DownloadHandlerBuffer();
            uwr.SetRequestHeader("Content-Type", "application/json");
            uwr.SetRequestHeader("Authorization", "Bearer " + accessToken);
            GetPupilsMatchingClasscodeHandler handler = new GetPupilsMatchingClasscodeHandler(feedback, classCode);
            StartCoroutine(new FirebaseWebRequests().MakeRequest(uwr, handler));
        });
    }

    class GetPupilsMatchingClasscodeHandler : FirebaseWebRequests.ContinueHandlerFirebase
    {
        GameObject feedback;
        string classCode;
        override public void IsFaulted()
        {
            feedback.GetComponent<Text>().text = "Error occurred";
        }

        override public void IsFaulted(string serverResponseBody)
        {
            feedback.GetComponent<Text>().text = "Error occurred";
        }
        public GetPupilsMatchingClasscodeHandler(GameObject feedback, string classCode)
        {
            this.feedback = feedback;
            this.classCode = classCode;
        }
        override public void IsCompleted(string serverResponseBody)
        {
            Debug.Log("Server response body text is:" + serverResponseBody);
            PupilsListMatchingClassCodeServerResponse pupilsMatchingClasscode = JsonUtility.FromJson<PupilsListMatchingClassCodeServerResponse>("{\"result\":" + serverResponseBody + "}");
            var message = pupilsMatchingClasscode.result.message;
            var pupils = pupilsMatchingClasscode.result.pupils;

            if (message != Constants.Firebase.pupilsFoundMatchingClassCode)
            {
                if (message == Constants.Firebase.noPupilsMatchClassCode)
                {
                    feedback.GetComponent<Text>().text = "<color=red>No pupils linked to class code</color>";
                    return;
                }
                if (message == Constants.Firebase.classCodeNotExist)
                {
                    feedback.GetComponent<Text>().text = "<color=red>Class code does not exist</color>";
                    return;
                }
                  if (message == Constants.Firebase.teacherNotLinkedToClassCode)
                {
                    feedback.GetComponent<Text>().text = "<color=red>Teacher not linked to class code</color>";
                    return;
                }
                feedback.GetComponent<Text>().text = message;
                return;
            }

            if (pupils.Count > 0)
            {
                Debug.Log("First pupil in converted json structure, matching classcode: " + pupils[0].name + " passed at highest level: " + pupils[0].highestLevelPassed);
                DataTransferToPupilsListMatchingClassCode.pupilData = pupils;
                DataTransferToPupilsListMatchingClassCode.classCode = classCode;
                SceneManager.LoadScene("DisplayPupilsMatchingClassCode");
            }
        }
    }


    [System.Serializable]
    public class PupilsListMatchingClassCodeServerResponse
    {
        public PupilsListMatchingClassCode result;
    }
    [System.Serializable]
    public class PupilsListMatchingClassCode
    {
        public string message;
        public List<PupilsToDisplayToTeacher> pupils;
    }
    [System.Serializable]
    public class PupilsToDisplayToTeacher
    {
        public string name;
        public string surname;
        public int highestLevelPassed;
    }

    public void OnHomeBtnClick()
    {
        SceneManager.LoadScene("MainMenu");
    }

    public void LogOutBtnClick()
    {
        auth.SignOut();
        SceneManager.LoadScene("MainMenu");
    }
}
