using System.Text.RegularExpressions;
public class FormValidation {
   public static bool IsValidEmail(string email)
    {
        const string matchEmailPattern = @"^(([\w-]+\.)+[\w-]+|([a-zA-Z]{1}|[\w-]{2,}))@"
        + @"((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\."
          + @"([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
        + @"([a-zA-Z]+[\w-]+\.)+[a-zA-Z]{2,4})$";

        if (email != null) return Regex.IsMatch(email, matchEmailPattern);
        else return false;
    }

        public static bool IsValidPassword(string password)
    {
        const string matchPasswordPattern = @"^\S{6,}$"; // 6 or more characters, as per firebase weak password rule. Allows firebase to reset password via console fired email.

        if (password != null) return Regex.IsMatch(password, matchPasswordPattern);
        else return false;
    }

       public static bool passwordAndConfirmPasswordMatches(string password, string confirmPassword)
    {
        if (password == confirmPassword)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}