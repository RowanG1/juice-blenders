using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;
using System.Text.RegularExpressions;
using Firebase;
using Firebase.Auth;
using Firebase.Unity.Editor;
using Firebase.Database;

public class GameDataFirebaseHandler
{
    MainScript mainScript;

    public GameDataFirebaseHandler(MainScript mainScript)
    {
        FirebaseApp.DefaultInstance.SetEditorDatabaseUrl("https://juice-blenders-firebase.firebaseio.com/");
        this.mainScript = mainScript;
    }

    public void SetGameDataRemote()
    {
        string gameID;

        GameDataFirebase gameData = new GameDataFirebase(mainScript);
        string json = JsonUtility.ToJson(gameData);
        if (string.IsNullOrEmpty(mainScript.gameID))
        {
            gameID = FirebaseDatabase.DefaultInstance.GetReference(Constants.Firebase.gameData).Push().Key;
        }
        else
        {
            gameID = mainScript.gameID;
        }


        FirebaseDatabase.DefaultInstance.GetReference(Constants.Firebase.gameData).Child(gameID).SetRawJsonValueAsync(json).ContinueWith(task =>
        {
            if (task.IsFaulted)
            {
                Debug.Log("Error occurred in game data set for firebase");
                Debug.Log(task.ToString());
            }
            if (task.IsCompleted)
            {
                Debug.Log("Completed game data set for firebase");
            }
        });
    }

}

