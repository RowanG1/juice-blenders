﻿using System;

public static class FunctionalAreaNames
{
    public static string procurement = "Procurement";
    public static string equipment = "Equipment";
    public static string marketing = "Marketing";
    public static string operations = "Operations";
    public static string exit = "Exit";
    public static string deliveries = "Delivery";
}
