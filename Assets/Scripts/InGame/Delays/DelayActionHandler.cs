﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DelayActionHandler {
	MainScript mainScript;

	public DelayActionHandler() {
		this.mainScript = MainScript.currentMainScript;
	}

	public void HandleDelayActions(float deltaTime) {
		for (int i = 0; i < mainScript.delayActions.Count; i++) {
			DelayAction thisDelayAction = mainScript.delayActions [i];
			thisDelayAction.delayRemain -= deltaTime;
			if (thisDelayAction.delayRemain < 0) {
				HandleAction (i);
				mainScript.delayActions.RemoveAt (i);
				i--;
			
			}
		}
	}

	public void HandleAction(int actionIndex) {
		DelayAction thisDelayAction = mainScript.delayActions [actionIndex];
		switch (thisDelayAction.actionName) {
		case Constants.Delays.valveOnAfterWaiting:
			Valve thisValve = mainScript.valveData [thisDelayAction.valveIndex];
			thisValve.valveState = (int)Common.ValveState.Open;
			break;
		case Constants.Delays.callTruck:
			int thisTankIndex = thisDelayAction.tankIndex;
			TankData thisTank = mainScript.tankData [thisTankIndex];
			thisTank.tankLevelFrac = Constants.Tanks.tankEmptyLevelFraction;
			Order thisOrder = mainScript.orders [thisDelayAction.orderIndex];
			thisOrder.truckArrived = true;
			thisTank.deliverTruckState = (int)Common.DeliverTruckState.Present;
			mainScript.outletTruckLastTimeFilled[thisTankIndex] = mainScript.oneSecondCount;

			break;
		case Constants.Delays.brandDelay:
			mainScript.HandleBrandSpend (thisDelayAction.brandSpendAmount);
			break;
		case Constants.Delays.adDelay:
			mainScript.HandleAdSpend (thisDelayAction.adSpendAmount);
			break;
		case Constants.Delays.tankUpgrade:
			mainScript.tank_And_Valve_Physical_Updates.UpgradeTank (thisDelayAction.tankIndex, thisDelayAction.tankUpgradeCost, thisDelayAction.tankCapacityExpandFactor, thisDelayAction.tankHeightExpandFactor);
			break;
		case Constants.Delays.pumpUpgrade:
			mainScript.tank_And_Valve_Physical_Updates.PumpUpgrade (thisDelayAction.thisValve, thisDelayAction.lineThick, thisDelayAction.pumpUpgradeValue, thisDelayAction.valveFlowRate);
			break;
		default:
			break;
		}
	}

	public void PumpUpgradeWithDelay(Valve thisValve, int lineThick,int pumpUpgradeValue, float valveFlowRate) {
		int randomTimeToUpgrade = (int) (UnityEngine.Random.Range(0,10)) + 20;
		DelayAction pumpDelayAction = new DelayAction ();

		pumpDelayAction.thisValve = thisValve;
		pumpDelayAction.lineThick = lineThick;
		pumpDelayAction.pumpUpgradeValue = pumpUpgradeValue;
		pumpDelayAction.valveFlowRate = valveFlowRate;
		pumpDelayAction.valveIndex = thisValve.valveIndex;
		pumpDelayAction.delayRemain = randomTimeToUpgrade;

		pumpDelayAction.actionName = Constants.Delays.pumpUpgrade;
		mainScript.delayActions.Add (pumpDelayAction);

		thisValve.upgradeRequested = true;

	}

	public void ToUpgradeTankWithDelay(int tankIndex, float tankUpgradeCost, int tankCapacityExpandFactor, float tankHeightExpandFactor) {
		int randomTimeToUpgrade = (int) UnityEngine.Random.Range(0, 10) + 20;
		DelayAction tankDelayAction = new DelayAction ();

		tankDelayAction.tankIndex = tankIndex;
		tankDelayAction.tankUpgradeCost = tankUpgradeCost;
		tankDelayAction.tankCapacityExpandFactor = tankCapacityExpandFactor;
		tankDelayAction.tankHeightExpandFactor = tankHeightExpandFactor;
		tankDelayAction.delayRemain = randomTimeToUpgrade;

		tankDelayAction.actionName = Constants.Delays.tankUpgrade;
		mainScript.delayActions.Add (tankDelayAction);
		
		mainScript.tankData[tankIndex].upgradeRequested = true;
	}

	public void CancelValveWaitingDelayAction(int valveIndex, List<DelayAction> delayActions) {
		//List <int> deleteActionIndexes = new List <int> ();
		for (int i = 0; i < mainScript.delayActions.Count; i++) {
			if (mainScript.delayActions [i].valveIndex == valveIndex && mainScript.delayActions [i].actionName == Constants.Delays.valveOnAfterWaiting) {
				mainScript.delayActions.RemoveAt (i);
				i--;
			}
		}

	}
}
