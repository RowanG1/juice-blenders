﻿using System;

[System.Serializable] 
	public class DelayAction
	{
	public string actionName;
	public float delayRemain;
	public int valveIndex = -1;
	public int tankIndex = -1;
	public int orderIndex;
	public int brandSpendAmount;
	public int adSpendAmount;

	//Tank Upgrade values
	// See tankIndex above, which is included
	public float tankUpgradeCost; 
	public int tankCapacityExpandFactor; 
	public float tankHeightExpandFactor;

	//Pump delay variables
	public Valve thisValve;
	public int lineThick;
	public int pumpUpgradeValue;
	public float valveFlowRate;
	//

	public DelayAction (string actionName, float delayRemain, int valveIndex)
		{
		this.actionName = actionName;
		this.delayRemain = delayRemain;
		this.valveIndex = valveIndex;
		}
	public DelayAction() {

	}


}

