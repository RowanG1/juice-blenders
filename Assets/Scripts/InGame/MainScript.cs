using UnityEngine;
using System.Collections;
using System.Linq;
using System;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.Audio;



public class MainScript
{


    public static MainScript currentMainScript;

    public int currentGameLevel;
    public string gameID;
    Level thisLevel;

    public GameObject indicatorPanel;
    public GameObject procurementNeededIconPanel;
    public GameObject emptyDeliveryTruckWaitingIconPanel;
    public GameObject lateDeliveryTruckIconPanel;
    public GameObject newOrdersWaitingIconPanel;
    public GameObject brandMarketingNeededIconPanel;
    public GameObject equipmentUpgradeNeededIconPanel;
    public GameObject lowBankBalanceIconPanel;
    public GameObject newAnimatedNotifUI;
    public GameObject timeLeftText;
    public GameObject timeLeftPanel;
    public GameObject customerServicePanel;

    public GameObject operations;
    public GameObject ordersArea;
    public GameObject financeArea;
    public GameObject deliveryArea;
    public GameObject marketingArea;
    public GameObject equipmentArea;
    public GameObject targetsArea;
    public GameObject procurementArea;
    public GameObject exitArea;
    public GameObject tabStripArea;
    public GameObject UICanvas;

    public GameObject drainSound;

    public Material eggMaterial;
    public Material loadingZoneMaterial;
    public Material truckMaterial;

    bool monobehaviourRunning;

    public List<ProductData> productData = new List<ProductData>();
    public List<RawMaterialData> rawMaterialData = new List<RawMaterialData>();
    public Dictionary<string, float> rawMatPrices;
    public List<KeyValuePair<int, float>> toDeliverArraySorted = new List<KeyValuePair<int, float>>();
    public List<TankData> tankData = new List<TankData>();
    public List<Valve> valveData = new List<Valve>();
    public List<Order> orders = new List<Order>();
    public List<DelayAction> delayActions = new List<DelayAction>();
    public List<string> quotations = new List<string>();

    public float bankBalance;
    public float sales;
    public float costOfSales;
    public float volJuiceWasted;
    public float volJuiceDelivered;
    public float profit;
    public int advertising;
    public float goodwill;
    public int pendingAdvertsTotal;
    public int adLeakage;
    public int branding;
    public float investments;
    public float interest;
    public float interestPercen; // Interest rate for 10 minutes, if it were simple interest. However, interest is actually compounded, and calculated every second.
    public int numberEquipmentUpgradesMade;
    public int numberSuccessfulCostNegotiations;
    public int counterTankSizeDoubled;
    public int counterTankSizeQuadrupled;
    public int counterPumpSizeDoubled;
    public int counterPumpSizeTripled;

    public int numberQuotesIssued;
    public List<int> quoteIndexesHistory = new List<int>();

    public float notifLabelCounter;
    public bool newOrdersWaitingIconActive = true;
    public bool emptyDeliveryTruckWaitingIconActive = true;
    public bool delayedDeliveryIconActive = true;
    public bool brandMarketingNeededIconActive = true;
    public bool procurementNeededIconActive = true;
    public bool equipmentUpgradeLackingIconActive = false;
    public bool lowBankBalanceIconActive = false;
    public bool animatedScoreIsBusy = false;
    public MainAnimatedNotification currentAnimatedNotification;
    public List<MainAnimatedNotification> mainNotificationsToAnimate = new List<MainAnimatedNotification>();
    public List<GameObject> lastActiveFlashingIcons = new List<GameObject>();
    public float lastTimeActiveFlashingIconPanelsUpdated;

    public IEnumerator runningHammerCoroutine;

    //Below variables are updated regularly within update functions
    public float interestToUpdate;
    public float juiceCostToAdd;
    public Dictionary<int, Dictionary<string, float>> juicesConsumedAtRecentMinuteStamps = new Dictionary<int, Dictionary<string, float>>();
    public Dictionary<int, Dictionary<int, ValveDataStampPeriod>> valvesUsageHistoryAtRecentMinuteStamps = new Dictionary<int, Dictionary<int, ValveDataStampPeriod>>();//Outer key is timestamp, inner key is valveIndex
    public Dictionary<int, Dictionary<int, TankUsageStamp>> juiceAddedToTanksByIndexHistoryAtRecentMinuteStamps = new Dictionary<int, Dictionary<int, TankUsageStamp>>();//Outer key is timestamp, inner key is valveIndex
    public Dictionary<int, int> outletTruckLastTimeFilled = new Dictionary<int, int>();

    public float elapsedTime;
    public int minsRemaining;
    public int oneSecondCount;
    public float lastTimeSpentAds, lastTimeSpentBrand;


    public float minDimUI;

    public List<GameObject> tankInstances = new List<GameObject>();
    public List<GameObject> valveInstances = new List<GameObject>();
    public List<GameObject> pipeInstances = new List<GameObject>();
    public List<ThisTankChartObjects> tankCharts = new List<ThisTankChartObjects>();

    public UIUpdatesInGame uIUpdatesInGame;
    public Tank_And_Valve_Physical_Updates tank_And_Valve_Physical_Updates;
    public TruckHandling truckHandling;
    public OrderFuncs orderFuncs;
    public Quotations quotationsObj;
    public DelayActionHandler delayActionHandler;


    public MainScript()
    {
        currentMainScript = this;
        tank_And_Valve_Physical_Updates = new Tank_And_Valve_Physical_Updates();
        truckHandling = new TruckHandling();
        orderFuncs = new OrderFuncs();
        orderFuncs.marketingMenuTab = GameObject.Find(Common.tabIconsGameObjectPath + "/Marketing");
        quotationsObj = new Quotations();
        delayActionHandler = new DelayActionHandler();
    }

    public void Awake()
    {

        Firebase.Analytics.FirebaseAnalytics.LogEvent("game_started", "type", "new_or_resumed");

        quotations = quotationsObj.GetQuotes();

        if (DataTransferToGameAtStart.requestedNewGame == true)
        {
            SetupNewGame();
        }
        else
        {
            SaveGameForResume.ResumeFromPastGame(this);
            thisLevel = Level.GetLevelObj();
        }
    }

    public void Start()
    {
        monobehaviourRunning = true;

        uIUpdatesInGame.setInitialUINotifications();
        MenuBtnFuncs.GoToMenu("Operations");
        uIUpdatesInGame.updateTimeText();

        HandleMenuItemsAvailableForLevel();
        uIUpdatesInGame.setTabStripContentDimensions();
        uIUpdatesInGame.ZoomFactoryAtStart();
    }

    void SetupNewGame()
    {
        currentGameLevel = DataTransferToGameAtStart.currentGameLevel;
        bankBalance = Common.startingBankBalance;
        interestPercen = (Mathf.Round(UnityEngine.Random.Range(5f, 8f) * 10)) / 10;
        thisLevel = Level.GetLevelObj();
        minsRemaining = thisLevel.levelDurationMins;
        rawMaterialData = SetupCommodities.CreateRawMatData();
        tankData = PlantSetup.CreateTanks();
        //CreateTanks ();
        PlantSetup.SetTankIndexes(tankData);
        PlantSetup.SetInitialTankLevels(this);
        //CreateProductData();
        productData = SetupCommodities.CreateProductSpecs(rawMaterialData);
        tank_And_Valve_Physical_Updates.RecalcTankParams();
        valveData = PlantSetup.CreateValves(tankData);
        new PlantSetup().SetTankIndexRefsForValves();
    }

    // Update is called once per frame
    void HandleMenuItemsAvailableForLevel()
    {
        foreach (string inactiveFunctionalArea in thisLevel.inactiveFunctionalAreas)
        {
            GameObject.Find(Common.tabIconsGameObjectPath + "/" + inactiveFunctionalArea).gameObject.SetActive(false);
        }
    }

    public void Update()
    {
        //Debug.Log(Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue("sales_target_level_5").StringValue);
        if (bankBalance < Common.minimumBankLimit)
        {
            SaveGameForResume.ClearSavedGame();
            SceneManager.LoadScene("Bankrupt");
        }
        LevelCheckAndActIfPassed();

        rawMatPrices = Common.ConvertRawMatArrayToDictionary(rawMaterialData);
        orderFuncs.CheckStateOrders();
        orderFuncs.UpdateOrdersAtEnquiryStageListAndCustomerSatisfaction();
        toDeliverArraySorted = UpdateToDeliverArray();
        truckHandling.TruckHandleLoop();
        truckHandling.SetDeliveryEmotionStatus();
        elapsedTime += Time.deltaTime;

        tank_And_Valve_Physical_Updates.AllTanksOutSpecCheckAndSet();
        tank_And_Valve_Physical_Updates.ValveAutoShuts();
        PeriodicUpdates();

        tank_And_Valve_Physical_Updates.JuiceTransferValveLoop(Time.deltaTime);

        uIUpdatesInGame.setJuiceGraphics();

        delayActionHandler.HandleDelayActions(Time.deltaTime);
        if (operations.activeSelf == true)
        {
            uIUpdatesInGame.setValveColors();
            uIUpdatesInGame.setTrucksUI();
            uIUpdatesInGame.reDrawTanksIfNecessary();
        }
        if (operations.activeSelf == true)
        {
            SetFlashingIconStatuses();
            uIUpdatesInGame.handleFlashingIcons();
            uIUpdatesInGame.handleFlashingEquipmentUpgradeDots();
            UpdateActiveFlashingIconsArray();

        }
        ManagemainNotificationsToAnimate(mainNotificationsToAnimate, newAnimatedNotifUI);
    }

    void PeriodicUpdates()
    {
        if (elapsedTime > (oneSecondCount + 1))
        {
            if ((oneSecondCount % (10 * 60) == 0))
            {
                quotationsObj.GetAndNotifyRandomQuoteNoRepeatOnLastFiveQuotes();
            }
            oneSecondCount++;

            if (operations.activeSelf == true)
            {
                uIUpdatesInGame.updateChartPositions();

            }

            UpdateRealtimeInterest();

            orderFuncs.OrderGenerateLoop();

            ManageDrainSound();

            if (oneSecondCount % (60 * 2) == 0)
            {
                if (juiceCostToAdd > 2000)
                {
                    mainNotificationsToAnimate.Add(new MainAnimatedNotification("Juice costs", -Mathf.Round(juiceCostToAdd)));
                }
                PayJuiceCosts();
            }

            if (!thisLevel.inactiveFunctionalAreas.Contains(FunctionalAreaNames.marketing))
            {
                MarketingAreaScript.UpdateIndicatorBarValues();
            }
            if (oneSecondCount % (1 * 60) == 0)
            {
                minsRemaining--;
                uIUpdatesInGame.updateTimeText();

                tank_And_Valve_Physical_Updates.TrimNonRecentMinuteStampsForJuicesConsumed();
                tank_And_Valve_Physical_Updates.TrimNonRecentMinuteStampsForTankHistory();
                tank_And_Valve_Physical_Updates.TrimNonRecentMinuteStampsForValveHistory();

                UpdateInterestRatePercentage(); // Each minute, the interest rate is updated.
                UpdateAdDecay();
                DecayedGoodWill();
                BrandLeakage();
            }
            if (minsRemaining <= 0)
            {
                SaveGameForResume.ClearSavedGame();
                SceneManager.LoadScene("GameOver");

                Firebase.Analytics.FirebaseAnalytics.LogEvent("game_timeout_on_level_" + currentGameLevel.ToString(), "type", "N/A");
                DataTransferToGameDataFirebase.gameEndedStatus = Constants.Firebase.levelFailed;
                new GameDataFirebaseHandler(this).SetGameDataRemote();
            }

            if (oneSecondCount % (5 * 60) == 0)
            {
                DataTransferToGameDataFirebase.gameEndedStatus = Constants.Firebase.levelInProgress;
                new GameDataFirebaseHandler(this).SetGameDataRemote();
            }

            if (oneSecondCount % (11 * 60) == 0 && !thisLevel.IsFunctionalAreaInactive(FunctionalAreaNames.procurement))
            {
                InflateJuiceCosts();
                mainNotificationsToAnimate.Add(new MainAnimatedNotification("Juice costs have gone up"));
            }
            if (oneSecondCount % (10 * 60) == 0)
            {
                AllocateInterest(interestToUpdate); // The accumulated interest each second, over 1 minute, is added to customer's bank account.
                mainNotificationsToAnimate.Add(new MainAnimatedNotification("Interest", Mathf.Round(interestToUpdate)));
            }

        }
    }

    void UpdateActiveFlashingIconsArray()
    {
        List<GameObject> currentActiveFlashingIcons = new List<GameObject>();
        foreach (Transform flashingIcon in indicatorPanel.transform)
        {
            if (flashingIcon.gameObject.activeSelf == true)
            {
                currentActiveFlashingIcons.Add(flashingIcon.gameObject);
            }
        }

        if (currentActiveFlashingIcons.All(lastActiveFlashingIcons.Contains))
        {
        }
        else
        {
            lastActiveFlashingIcons = currentActiveFlashingIcons;
            lastTimeActiveFlashingIconPanelsUpdated = elapsedTime;
        }
    }

    public List<KeyValuePair<int, float>> UpdateToDeliverArray()
    {
        List<KeyValuePair<int, float>> toDeliverByOrderAscend = new List<KeyValuePair<int, float>>();
        for (int i = 0; i < orders.Count; i++)
        {
            Order thisOrder = orders[i];
            if (thisOrder.accepted == true && thisOrder.fulfilled == false && thisOrder.lost == false)
            {
                toDeliverByOrderAscend.Add(new KeyValuePair<int, float>(i, thisOrder.orderAcceptedTime));
            }
        }

        List<KeyValuePair<int, float>> toDeliverArraySortedOut;
        (toDeliverArraySortedOut = toDeliverByOrderAscend).Sort(
            delegate (KeyValuePair<int, float> pair1, KeyValuePair<int, float> pair2)
            {
                return pair1.Value.CompareTo(pair2.Value);
            });

        return toDeliverArraySortedOut;
    }

    void LevelCheckAndActIfPassed()
    {
        bool levelHasPassed = CheckLevelPassed();

        if (levelHasPassed == true)
        {

            Firebase.Analytics.FirebaseAnalytics.LogEvent(Firebase.Analytics.FirebaseAnalytics.EventLevelEnd, new Firebase.Analytics.Parameter[] {
                     new Firebase.Analytics.Parameter(
                   "LEVEL_NAME", currentGameLevel.ToString())
                     });

            MaybeUpdateFilesLevelsPassedAndBadgeCredits();
            SaveGameForResume.ClearSavedGame();
            SceneManager.LoadScene("LevelPassed");
            DataTransferToGameDataFirebase.gameEndedStatus = Constants.Firebase.levelPassed;
            new GameDataFirebaseHandler(this).SetGameDataRemote();
        }
    }

    public bool CheckLevelPassed()
    {
        bool levelHasPassed;
        levelHasPassed = thisLevel.CheckLevelPassed();
        return levelHasPassed;
    }

    public void MaybeUpdateFilesLevelsPassedAndBadgeCredits()
    {
        LevelsFileHandler levelsFileHandler = new LevelsFileHandler();

        if (currentGameLevel > new LevelsFileHandler().GetMaxLevelPassed().maxLevelPassed || currentGameLevel == (AllLevels.GetLevelsArray().Count))
        {
            new BadgeCreditsFileHandler().AddCreditsToFileForLevel(this);
            new BadgesCredits().SetCreditsForTransferToLevelPassed(this);
        }

        if (currentGameLevel > new LevelsFileHandler().GetMaxLevelPassed().maxLevelPassed)
        {
            levelsFileHandler.SaveNewHighestLevelPassed(currentGameLevel);
            if (TestStatus.testsAreRunning == false)
            {
                PupilDataStorage pupilDataStorage = new PupilDataStorage();
                pupilDataStorage.UpdatePupilHighestLevelRemote(currentGameLevel);
            }
        }
    }

    public void InflateJuiceCosts()
    {
        for (int i = 0; i < rawMaterialData.Count; i++)
        {
            RawMaterialData thisRawMaterial = rawMaterialData[i];
            float currentPrice = thisRawMaterial.price;
            float newPossiblePrice = Common.ConvertToTwoDecimal(currentPrice * Constants.Juice.costInflationFactorPer10Min);
            if (newPossiblePrice > thisRawMaterial.maxPrice)
            {
                thisRawMaterial.price = thisRawMaterial.maxPrice;
            }
            else
            {
                thisRawMaterial.price = newPossiblePrice;
            }
        }
    }

    public bool JuiceCostExcessive(string juiceName, float costExcessFactor)
    {
        RawMaterialData thisRawMaterial = Common.GetRawMaterialDataFromJuiceName(juiceName, rawMaterialData);
        if (thisRawMaterial.juiceName == juiceName)
        {
            if ((thisRawMaterial.price - thisRawMaterial.minPrice) > ((costExcessFactor - 1) * thisRawMaterial.minPrice))
            {
                return true;
            }
        }

        return false;
    }


    public HashSet<string> JuicesWithExcessCostAndHighConsumption()
    {
        HashSet<string> output = new HashSet<string>();
        Dictionary<string, float> recentTotalConsumptionPerJuice = tank_And_Valve_Physical_Updates.GetRecentTotalConsumptionPerJuice();

        for (int i = 0; i < rawMaterialData.Count; i++)
        {
            RawMaterialData thisRawMaterial = rawMaterialData[i];
            string juiceName = thisRawMaterial.juiceName;
            float recentTotalConsumptionThisJuice = 0;
            if (recentTotalConsumptionPerJuice.ContainsKey(juiceName))
            {
                recentTotalConsumptionThisJuice = recentTotalConsumptionPerJuice[juiceName];
            }
            if (JuiceCostExcessive(juiceName, Common.juiceCostExcessFactor) && tank_And_Valve_Physical_Updates.RecentConsumptionJuiceIsSubstantial(recentTotalConsumptionThisJuice, Common.recentJuiceConsumptionHighThreshold))
            {
                output.Add(juiceName);
            }
        }
        return output;
    }


    public void PayJuiceCosts()
    {
        float roundedJuiceCostToAdd = Common.ConvertToTwoDecimal(juiceCostToAdd);
        profit -= roundedJuiceCostToAdd;
        bankBalance -= roundedJuiceCostToAdd;
        costOfSales = Common.ConvertToTwoDecimal(costOfSales + roundedJuiceCostToAdd);
        juiceCostToAdd = 0;
    }

    public void BrandLeakage()
    {
        for (int w = 0; w < productData.Count; w++)
        {
            ProductData thisProduct = productData[w];
            float newProdPrice = ((float)Mathf.Round(thisProduct.price * (1 - 0.005f) * 100)) / 100;
            if (newProdPrice < thisProduct.priceMin)
            {
                thisProduct.price = thisProduct.priceMin;
            }
            else
            {
                thisProduct.price = newProdPrice;
            }
        }
    }

    public void DecayedGoodWill()
    {
        goodwill = Common.ConvertToTwoDecimal(goodwill * Constants.Timing.decayedGoodwillFactorPerMin);
    }

    public void HandleAdSpend(int adSpend)
    {
        advertising += adSpend;
        pendingAdvertsTotal -= adSpend;
    }

    public void HandleBrandSpend(int brandSpend)
    {
        for (int i = 0; i < productData.Count; i++)
        {
            ProductData thisProduct = productData[i];
            int randomPriceIncreasePercen = (int)(UnityEngine.Random.Range(Common.lowerBoundBrandSpendRandomIncrease, Common.upperBoundBrandSpendRandomIncrease));

            float priceIncrease = Common.ConvertToTwoDecimal((float)randomPriceIncreasePercen / 100 * thisProduct.price * (float)brandSpend * Common.brandInvestRatio);

            //print("Price increase is:" + priceIncrease);
            float newPrice = thisProduct.price + priceIncrease;
            if (newPrice > thisProduct.priceCap)
            {
                thisProduct.price = thisProduct.priceCap;
            }
            else
            {
                thisProduct.price = newPrice;
            }
        }
    }

    public void UpdateAdDecay()
    {
        int adLeakSum = advertising - adLeakage;
        Debug.Assert(adLeakSum >= 0);
        adLeakage += (int)Mathf.Round(adLeakSum * Constants.Timing.adLeakageFactoPerMin);
    }

    bool CheckBrandMarketingNeeded()
    {
        if ((MarketingAreaScript.adLevelPercen < Constants.Marketing.minimumAdLevelPercenForNeglectNotification && (elapsedTime - lastTimeSpentAds > 2 * 60)) || (MarketingAreaScript.brandLevelPercen < Constants.Marketing.minimumBrandLevelPercenForNeglectNotification && (elapsedTime - lastTimeSpentBrand > 2 * 60)))
        {
            return true;
        }
        return false;
    }

    public void AllocateInterest(float interestToUpdate)
    {
        float roundedInterestToUpdate = Common.ConvertToTwoDecimal(interestToUpdate);
        bankBalance += Common.ConvertToTwoDecimal(roundedInterestToUpdate);
        profit += roundedInterestToUpdate;
        interest += roundedInterestToUpdate;
        interestToUpdate = 0;
    }

    public void UpdateInterestRatePercentage()
    {
        float interestRateChange = (UnityEngine.Random.Range(-1, 1 + 1) * 0.4f);

        interestPercen += interestRateChange;
        if (interestPercen >= Common.interestRatePercenMax)
        {
            interestPercen = Common.interestRatePercenMax;
        }
        if (interestPercen <= Common.interestRatePercenMin)
        {
            interestPercen = Common.interestRatePercenMin;
        }
    }

    public void UpdateRealtimeInterest()
    {
        float interest = ((float)Mathf.Round(1000f * bankBalance * interestPercen / 10 / 100 / 60)) / 1000;
        interestToUpdate += interest;
    }

    void ManagemainNotificationsToAnimate(List<MainAnimatedNotification> mainNotificationsToAnimate, GameObject newAnimatedNotifUI)
    {
        if (animatedScoreIsBusy == false && mainNotificationsToAnimate.Count > 0)
        {
            MainAnimatedNotification oldestMainAnimatedNotif = mainNotificationsToAnimate[0];
            uIUpdatesInGame.animateMainNotificationCall(oldestMainAnimatedNotif, newAnimatedNotifUI);
            currentAnimatedNotification = oldestMainAnimatedNotif;
            //StartCoroutine(animateMainNotification(oldestMainAnimatedNotif, newAnimatedNotifUI));
            mainNotificationsToAnimate.RemoveAt(0);
        }
    }

    public void SetFlashingIconActiveState(GameObject iconPanel, bool iconActive)
    {
        if (iconActive == true && iconPanel.activeSelf == false)
        {
            iconPanel.SetActive(true);
        }
        if (iconActive == false && iconPanel.activeSelf == true)
        {
            iconPanel.SetActive(false);
        }
    }

    void SetFlashingIconStatuses()
    {
        if (OrderFuncs.CheckIfNewOrdersPresent() == true)
        {
            newOrdersWaitingIconActive = true;
        }
        else
        {
            newOrdersWaitingIconActive = false;
        }
        if (orderFuncs.CheckIfAnyDeliveriesDelayed(elapsedTime, orders) == true)
        {
            delayedDeliveryIconActive = true;
        }
        else
        {
            delayedDeliveryIconActive = false;
        }
        if (tank_And_Valve_Physical_Updates.CheckIfAnyPresentTruckNotRecentlyFilling() == true)
        {
            emptyDeliveryTruckWaitingIconActive = true;
        }
        else
        {
            emptyDeliveryTruckWaitingIconActive = false;
        }
        if (CheckBrandMarketingNeeded() == true)
        {
            brandMarketingNeededIconActive = true;
        }
        else
        {
            brandMarketingNeededIconActive = false;
        }
        var juices = JuicesWithExcessCostAndHighConsumption();
        if (juices.Count > 0)
        {
            procurementNeededIconActive = true;
        }
        else
        {
            procurementNeededIconActive = false;
        }
        if (tank_And_Valve_Physical_Updates.ValvesOrTanksLackUpgrades())
        {
            equipmentUpgradeLackingIconActive = true;
        }
        else
        {
            equipmentUpgradeLackingIconActive = false;
        }

        if (bankBalance < 25000)
        {
            lowBankBalanceIconActive = true;
        }
        else
        {
            lowBankBalanceIconActive = false;
        }

        SetFlashingIconsInactiveIfDisabledForLevel();

        SetFlashingIconActiveState(newOrdersWaitingIconPanel, newOrdersWaitingIconActive);
        SetFlashingIconActiveState(emptyDeliveryTruckWaitingIconPanel, emptyDeliveryTruckWaitingIconActive);
        SetFlashingIconActiveState(lateDeliveryTruckIconPanel, delayedDeliveryIconActive);
        SetFlashingIconActiveState(brandMarketingNeededIconPanel, brandMarketingNeededIconActive);
        SetFlashingIconActiveState(procurementNeededIconPanel, procurementNeededIconActive);
        SetFlashingIconActiveState(equipmentUpgradeNeededIconPanel, equipmentUpgradeLackingIconActive);
        SetFlashingIconActiveState(lowBankBalanceIconPanel, lowBankBalanceIconActive);

    }

    void SetFlashingIconsInactiveIfDisabledForLevel()
    {
        string[] inactiveFunctionalAreas = thisLevel.inactiveFunctionalAreas;
        foreach (string inactiveFunctionalArea in inactiveFunctionalAreas)
        {
            if (inactiveFunctionalArea == FunctionalAreaNames.equipment)
            {
                equipmentUpgradeLackingIconActive = false;
            }
            if (inactiveFunctionalArea == FunctionalAreaNames.marketing)
            {
                brandMarketingNeededIconActive = false;
            }
            if (inactiveFunctionalArea == FunctionalAreaNames.procurement)
            {
                procurementNeededIconActive = false;
            }
        }

    }

    public void ManageDrainSound()
    {
        if (AnyDrainIsOn() && drainSound.activeSelf == false)
        {
            SetDrainSoundState(true);
        }
        else if (!AnyDrainIsOn() && drainSound.activeSelf == true)
        {
            SetDrainSoundState(false);
        }
    }

    void SetDrainSoundState(bool state)
    {
        AudioSource drainSoundSource = drainSound.GetComponent<AudioSource>();
        if (state)
        {
            drainSound.SetActive(true);
            drainSoundSource.Play();
        }
        else
        {
            drainSound.SetActive(false);
        }
    }

    public void PlaySparkleSound()
    {
        AudioSource sparkleSoundSource = GameScript.currentGameScript.sparkleSoundObj.GetComponent<AudioSource>();
        sparkleSoundSource.Play();
        // if (state) {

        // 	drainSoundSource.Play();
    }

    bool AnyDrainIsOn()
    {
        for (int i = 0; i < valveData.Count; i++)
        {
            Valve thisValve = valveData[i];
            if (thisValve.valveDestination == Constants.Valves.drainCode && thisValve.valveState == (int)Common.ValveState.Open)
            {
                return true;
            }
        }
        return false;
    }



}
