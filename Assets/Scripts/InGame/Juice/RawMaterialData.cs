﻿using System;
using Random = UnityEngine.Random;

[System.Serializable]
public class RawMaterialData
{
    public string juiceName;
    public float price;
    public float minPrice;
    public float maxPrice;



    public RawMaterialData()
    {

    }

    public RawMaterialData(string juiceName, float minPrice)
    {
        this.juiceName = juiceName;
		this.minPrice = minPrice;
        this.price = GetRandomizedInitialJuicePrice(minPrice);
        this.maxPrice = minPrice * Common.juiceMaxCostFactor;
    }

    float GetRandomizedInitialJuicePrice(float minPrice)
    {
        float randomizedPrice = Random.Range(minPrice * Constants.Juice.minimumInitialJuiceCostFactor, minPrice * Common.juiceMaxCostFactor);
        return randomizedPrice;
    }
}