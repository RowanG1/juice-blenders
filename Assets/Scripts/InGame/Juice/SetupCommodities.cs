using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetupCommodities
{

    public static List<RawMaterialData> CreateRawMatData()
    {
        List<RawMaterialData> rawMaterialData = new List<RawMaterialData>();

        RawMaterialData newRawMat = new RawMaterialData(juiceName: Common.JuiceNames.Orange.ToString(), minPrice: Constants.Juice.minPrice);
        rawMaterialData.Add(newRawMat);

        newRawMat = new RawMaterialData(juiceName: Common.JuiceNames.Blueberry.ToString().ToString(), minPrice: Constants.Juice.minPrice);
        rawMaterialData.Add(newRawMat);

        newRawMat = new RawMaterialData(juiceName: Common.JuiceNames.Strawberry.ToString(), minPrice: Constants.Juice.minPrice);
        rawMaterialData.Add(newRawMat);

        newRawMat = new RawMaterialData(juiceName: Common.JuiceNames.Pineapple.ToString(), minPrice: Constants.Juice.minPrice);
        rawMaterialData.Add(newRawMat);

        return rawMaterialData;
    }

    public static List<ProductData> CreateProductSpecs(List<RawMaterialData> rawMaterialData)
    {
        List<ProductData> productData = new List<ProductData>();

        productData.Add(CreateNewProduct(productName: "A/B", juiceSpecs: new List<JuiceSpecs> { Juice(juiceName: Common.JuiceNames.Blueberry.ToString(), targetFrac: 0.2f), Juice(juiceName: Common.JuiceNames.Pineapple.ToString(), targetFrac: 0.8f) }, rawMaterialData: rawMaterialData));
        productData.Add(CreateNewProduct(productName: "C/D", juiceSpecs: new List<JuiceSpecs> { Juice(juiceName: Common.JuiceNames.Strawberry.ToString(), targetFrac: 0.2f), Juice(juiceName: Common.JuiceNames.Orange.ToString(), targetFrac: 0.8f) }, rawMaterialData: rawMaterialData));
        productData.Add(CreateNewProduct(productName: "A/B/C", juiceSpecs: new List<JuiceSpecs> { Juice(juiceName: Common.JuiceNames.Blueberry.ToString(), targetFrac: 0.2f), Juice(juiceName: Common.JuiceNames.Pineapple.ToString(), targetFrac: 0.5f), Juice(juiceName: Common.JuiceNames.Strawberry.ToString(), targetFrac: 0.3f) }, rawMaterialData: rawMaterialData));
        productData.Add(CreateNewProduct(productName: "B/C/D", juiceSpecs: new List<JuiceSpecs> { Juice(juiceName: Common.JuiceNames.Pineapple.ToString(), targetFrac: 0.2f), Juice(juiceName: Common.JuiceNames.Strawberry.ToString(), targetFrac: 0.5f), Juice(juiceName: Common.JuiceNames.Orange.ToString(), targetFrac: 0.3f) }, rawMaterialData: rawMaterialData));
        productData.Add(CreateNewProduct(productName: "A/C/D", juiceSpecs: new List<JuiceSpecs> { Juice(juiceName: Common.JuiceNames.Blueberry.ToString(), targetFrac: 0.2f), Juice(juiceName: Common.JuiceNames.Strawberry.ToString(), targetFrac: 0.3f), Juice(juiceName: Common.JuiceNames.Orange.ToString(), targetFrac: 0.5f) }, rawMaterialData: rawMaterialData));

        return productData;
    }

    static ProductData CreateNewProduct(string productName, List<JuiceSpecs> juiceSpecs, List<RawMaterialData> rawMaterialData)
    {
        ProductData newProduct = new ProductData(productName: productName, juiceSpecs: juiceSpecs);
        CreateOneProductPricing(newProduct, rawMaterialData);
        return newProduct;
    }

    public static float GetPriceOfRawMaterial(string rawMaterialName, List<RawMaterialData> rawMaterialData)
    {
        for (int i = 0; i < rawMaterialData.Count; i++)
        {
            RawMaterialData thisRawMaterial = rawMaterialData[i];
            if (thisRawMaterial.juiceName == rawMaterialName)
            {
                return thisRawMaterial.price;
            }
        }
        return 0;
    }

        public static float GetMaxPriceOfRawMaterial(string rawMaterialName, List<RawMaterialData> rawMaterialData)
    {
        for (int i = 0; i < rawMaterialData.Count; i++)
        {
            RawMaterialData thisRawMaterial = rawMaterialData[i];
            if (thisRawMaterial.juiceName == rawMaterialName)
            {
                return thisRawMaterial.maxPrice;
            }
        }
        return 0;
    }

        public static float GetMinPriceOfRawMaterial(string rawMaterialName, List<RawMaterialData> rawMaterialData)
    {
        for (int i = 0; i < rawMaterialData.Count; i++)
        {
            RawMaterialData thisRawMaterial = rawMaterialData[i];
            if (thisRawMaterial.juiceName == rawMaterialName)
            {
                return thisRawMaterial.minPrice;
            }
        }
        return 0;
    } 

    static void CreateOneProductPricing(ProductData oneProductTechnicalData, List<RawMaterialData> rawMaterialData)
    {
        //	tankLoop:
        ProductData oneProduct = oneProductTechnicalData;
        float minCostPrice = 0;
        for (int i = 0; i < oneProduct.juiceSpecs.Count; i++)
        {
            JuiceSpecs thisJuice = oneProduct.juiceSpecs[i];
            minCostPrice += thisJuice.targetFrac * GetMinPriceOfRawMaterial(thisJuice.juiceName, rawMaterialData);
        }

        float referencePrice = minCostPrice * Constants.JuiceProduct.markupStandard + oneProduct.juiceSpecs.Count * Constants.Juice.valueAddedPerJuice;
        float capPercenAmplitude = Constants.JuiceProduct.capPercenAmplitude;
        oneProduct.priceCap = Common.ConvertToTwoDecimal(referencePrice * ((1.0f + capPercenAmplitude / 100)));
        oneProduct.priceMin = Common.ConvertToTwoDecimal(referencePrice * (1.0f - capPercenAmplitude / 100));
        oneProduct.price = Random.Range(oneProduct.priceMin, oneProduct.priceCap);
  
    }

    static JuiceSpecs Juice(string juiceName, float targetFrac)
    {
        JuiceSpecs juiceSpecs = new JuiceSpecs(juiceName, targetFrac);
        return juiceSpecs;
    }

    public static HashSet<string> ConvertTankNameToJuiceComponentNames(string tankName)
    {
        string[] tankNameSplitArray = tankName.Split('/');
        List<string> juiceNameArray = new List<string>();
        foreach (string letter in tankNameSplitArray)
        {
            juiceNameArray.Add(ConvertTankLetterToJuiceName(letter));
        }

        var tankNameSplitHashSet = new HashSet<string>(juiceNameArray.ToArray());
        return tankNameSplitHashSet;
    }

    public static string ConvertTankLetterToJuiceName(string letter)
    {
        string juiceName = "";
        switch (letter)
        {
            case "A":
                juiceName = Common.JuiceNames.Blueberry.ToString();
                break;
            case "B":
                juiceName = Common.JuiceNames.Pineapple.ToString();
                break;
            case "C":
                juiceName = Common.JuiceNames.Strawberry.ToString();
                break;
            case "D":
                juiceName = Common.JuiceNames.Orange.ToString();
                break;
            default:
                break;
        }
        return juiceName;
    }

}