﻿using System;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class ProductData
{
    public string productName;
    public float price;
    public float priceCap;
    public float priceMin;
    public List<JuiceSpecs> juiceSpecs = new List<JuiceSpecs>();

    public ProductData()
    {
    }

    public ProductData(string productName, float price, float priceCap, float priceMin)
    {
        this.productName = productName;
        this.price = price;
        this.priceCap = priceCap;
        this.priceMin = priceMin;
    }

    public ProductData(string productName, List<JuiceSpecs> juiceSpecs)
    {
        this.productName = productName;
        this.juiceSpecs = juiceSpecs;
    }

}

