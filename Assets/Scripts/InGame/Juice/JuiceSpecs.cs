﻿using System;
using System.Collections;
using System.Collections.Generic;

[System.Serializable] 
	public class JuiceSpecs
{
		public float targetFrac;
		public string juiceName;

		public JuiceSpecs () {
			this.targetFrac = 0;
			this.juiceName = "";
	}

	public JuiceSpecs(string juiceName, float targetFrac) {
			this.targetFrac = targetFrac;
			this.juiceName = juiceName;
	}
}

