using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public static class Common
{
    public enum ValveState : int { Closed = 0, Waiting = 1, Open = 2 };
    public enum DeliverTruckState : int { Absent = 0, Waiting = 1, Present = 2 };
    public enum MainAnimatedNotificationType : int { PlainMessage = 0, Score = 1 };
    public enum JuiceNames : int { Strawberry, Pineapple, Orange, Blueberry };
    public enum CustomerEmotionStatus : int { Happy = 0, NoComment = 1, Sad = 2 };
    public enum CustomerEmotionStatusThresholdInMin : int { Happy = 3, NoComment = 6, Sad = 9 };
    public enum BadgeNames : int { CustomerService = 0, LowWaste = 1, Profitability = 2 };
    public static Color valveOpenColor = new Color(0.0f, 1.0f, 0.0f);
    public static Color valveWaitingColor = new Color(1, 165f / 255, 0.0f);
    public static Color valveClosedColor = new Color(0, 0, 0);
    public static string tabIconsGameObjectPath = "UICanvas/TabStrip/TabScrollOutline/TabScrollPanel/TabIcons";

    //public enum JuiceName: int {Yellow= 0, Orange= 1, Red=2, Blue=3};
    public static int valveWidth = 45;
    public static int tabStripHeight = 10;
    public static int interestRatePercenMin = 5;
    public static int interestRatePercenMax = 8;
    public static float brandInvestRatio = 1.0f / 15000;
    public static int lowerBoundBrandSpendRandomIncrease = 6;
    public static int upperBoundBrandSpendRandomIncrease = 16;
    public static int minimumBankLimit = -5000;
    public static int startingBankBalance = 80000;
    public static float juiceMaxCostFactor = 1.9f;
    public static float juiceCostExcessFactor = 1.4f;
    public static float recentJuiceConsumptionHighThreshold = 3000;
    public static float recentTotalJuiceInputToTankThreshold = 3000;
    public static float recentTotalJuiceTransferredByValveThreshold = 3000;
    public static int recentTimeframeInMinsJuicesConsumed = 8;
    public static int recentTimeframeInMinsTankHistory = 8;
    public static int recentTimeframeInMinsValveHistory = 8;

    public static float ConvertToTwoDecimal(float input)
    {
        return ((float)Mathf.Round(100 * input)) / 100;
    }


    public static Color GetJuiceColor(string juiceName, float alpha)
    {
        Color juiceColor = new Color();
        if (juiceName == Common.JuiceNames.Pineapple.ToString())
        {
            juiceColor = new Color(225f / 225, 238f / 255, 132f / 255, alpha);
        }
        else if (juiceName == Common.JuiceNames.Strawberry.ToString())
        {
            juiceColor = new Color(1, 0, 0, alpha);
        }
        else if (juiceName == Common.JuiceNames.Blueberry.ToString())
        {
            juiceColor = new Color(0, 0, 1, alpha);
        }
        else if (juiceName == Common.JuiceNames.Orange.ToString())
        {
            juiceColor = new Color(1, 165f / 255, 0, alpha);
        }
        return juiceColor;
    }


    public static Color CombineColors(List<Color> colors, List<float> actualFracs)
    {

        float rSum = 0;
        float gSum = 0;
        float bSum = 0;
        for (int i = 0; i < colors.Count; i++)
        {
            Color thisJuiceColor = colors[i];
            rSum += thisJuiceColor.r * actualFracs[i];
            gSum += thisJuiceColor.g * actualFracs[i];
            bSum += thisJuiceColor.b * actualFracs[i];
        }

        return new Color(rSum, gSum, bSum);
    }

    public static void PlayButtonSound()
    {
        AudioSource audio = GameObject.Find("ButtonSound").GetComponent<AudioSource>();
        audio.Play();
    }

    public static Dictionary<string, float> ConvertRawMatArrayToDictionary(List<RawMaterialData> rawMaterialData)
    {
        Dictionary<string, float> rawMatPrices = new Dictionary<string, float>();
        for (int d = 0; d < rawMaterialData.Count; d++)
        {
            RawMaterialData thisRawMaterial = rawMaterialData[d];
            rawMatPrices.Add(thisRawMaterial.juiceName, thisRawMaterial.price);
        }

        return rawMatPrices;
    }

    public static RawMaterialData GetRawMaterialDataFromJuiceName(string juiceName, List<RawMaterialData> rawMaterialData)
    {
        foreach (RawMaterialData thisRawMaterial in rawMaterialData)
        {
            if (thisRawMaterial.juiceName.ToLower() == juiceName.ToLower())
            {
                return thisRawMaterial;
            }
        }
        throw new MissingReferenceException("No raw material found");
    }

}

