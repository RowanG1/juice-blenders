using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;


public class PlantSetup
{

    public static float opTankWidthOrig = 240f;
    public static float opTankHeightOrig = 160f;


    public static List<TankData> CreateTanks()
    {
        List<TankData> tankData = new List<TankData>();

        TankData newTank = new TankData();
        newTank.centerx = 300;
        newTank.centery = 250;
        newTank.tankWidth = opTankWidthOrig;
        newTank.tankHeight = opTankHeightOrig;
        newTank.tankName = "Supply-A";
        newTank.tankCapacity = Constants.Tanks.initialTankCapacity;
        TankJuiceData newJuice = new TankJuiceData();
        newJuice.juiceName = Common.JuiceNames.Blueberry.ToString();
        newJuice.targetFrac = 1.0f;
        newJuice.actualFrac = 1.0f;
        newTank.tankJuiceData.Add(newJuice);

        tankData.Add(newTank);
        // End of new tank

        //Add new tank
        newTank = new TankData();
        newTank.centerx = 600;
        newTank.centery = 250;
        newTank.tankWidth = opTankWidthOrig;
        newTank.tankHeight = opTankHeightOrig;
        newTank.tankName = "Supply-B";
        newTank.tankCapacity = Constants.Tanks.initialTankCapacity;
        newJuice = new TankJuiceData();
        newJuice.juiceName = Common.JuiceNames.Pineapple.ToString();
        newJuice.targetFrac = 1.0f;
        newJuice.actualFrac = 1.0f;
        newTank.tankJuiceData.Add(newJuice);
        tankData.Add(newTank);
        // End of new tank

        //Add new tank
        newTank = new TankData();
        newTank.centerx = 900;
        newTank.centery = 250;
        newTank.tankWidth = opTankWidthOrig;
        newTank.tankHeight = opTankHeightOrig;
        newTank.tankName = "Supply-C";
        newTank.tankCapacity = Constants.Tanks.initialTankCapacity;
        newJuice = new TankJuiceData();
        newJuice.juiceName = Common.JuiceNames.Strawberry.ToString();
        newJuice.targetFrac = 1.0f;
        newJuice.actualFrac = 1.0f;
        newTank.tankJuiceData.Add(newJuice);
        tankData.Add(newTank);
        // End of new tank

        //Add new tank
        newTank = new TankData();
        newTank.centerx = 1200;
        newTank.centery = 250;
        newTank.tankWidth = opTankWidthOrig;
        newTank.tankHeight = opTankHeightOrig;
        newTank.tankName = "Supply-D";
        newTank.tankCapacity = Constants.Tanks.initialTankCapacity;
        newJuice = new TankJuiceData();
        newJuice.juiceName = Common.JuiceNames.Orange.ToString();
        newJuice.targetFrac = 1.0f;
        newJuice.actualFrac = 1.0f;
        newTank.tankJuiceData.Add(newJuice);
        tankData.Add(newTank);
        // End of new tank

        //Add new tank
        newTank = new TankData();
        newTank.centerx = 400;
        newTank.centery = 600;
        newTank.tankWidth = opTankWidthOrig;
        newTank.tankHeight = opTankHeightOrig;
        newTank.tankName = "A";
        newTank.tankCapacity = Constants.Tanks.initialTankCapacity;
        newJuice = new TankJuiceData();
        newJuice.juiceName = Common.JuiceNames.Blueberry.ToString();
        newJuice.targetFrac = 1.0f;
        newJuice.actualFrac = 1.0f;
        newTank.tankJuiceData.Add(newJuice);
        tankData.Add(newTank);
        // End of new tank

        //Add new tank
        newTank = new TankData();
        newTank.centerx = 700;
        newTank.centery = 600;
        newTank.tankWidth = opTankWidthOrig;
        newTank.tankHeight = opTankHeightOrig;
        newTank.tankName = "B";
        newTank.tankCapacity = Constants.Tanks.initialTankCapacity;
        newJuice = new TankJuiceData();
        newJuice.juiceName = Common.JuiceNames.Pineapple.ToString();
        newJuice.targetFrac = 1.0f;
        newJuice.actualFrac = 1.0f;
        newTank.tankJuiceData.Add(newJuice);
        tankData.Add(newTank);
        // End of new tank

        //Add new tank
        newTank = new TankData();
        newTank.centerx = 1000;
        newTank.centery = 600;
        newTank.tankWidth = opTankWidthOrig;
        newTank.tankHeight = opTankHeightOrig;
        newTank.tankName = "C";
        newTank.tankCapacity = Constants.Tanks.initialTankCapacity;
        newJuice = new TankJuiceData();
        newJuice.juiceName = Common.JuiceNames.Strawberry.ToString();
        newJuice.targetFrac = 1.0f;
        newJuice.actualFrac = 1.0f;
        newTank.tankJuiceData.Add(newJuice);
        tankData.Add(newTank);
        // End of new tank

        //Add new tank
        newTank = new TankData();
        newTank.centerx = 1300;
        newTank.centery = 600;
        newTank.tankWidth = opTankWidthOrig;
        newTank.tankHeight = opTankHeightOrig;
        newTank.tankName = "D";
        newTank.tankCapacity = Constants.Tanks.initialTankCapacity;
        newJuice = new TankJuiceData();
        newJuice.juiceName = Common.JuiceNames.Orange.ToString();
        newJuice.targetFrac = 1.0f;
        newJuice.actualFrac = 1.0f;
        newTank.tankJuiceData.Add(newJuice);
        tankData.Add(newTank);
        // End of new tank

        //Add new tank
        newTank = new TankData();
        newTank.centerx = 200;
        newTank.centery = 950;
        newTank.tankWidth = opTankWidthOrig;
        newTank.tankHeight = opTankHeightOrig;
        newTank.tankName = "A/B";
        newTank.tankCapacity = Constants.Tanks.initialTankCapacity;
        float[] temp = GenTwoRandomFracs();
        newJuice = new TankJuiceData();
        newJuice.juiceName = Common.JuiceNames.Blueberry.ToString();
        newJuice.targetFrac = 0.2f;
        newJuice.actualFrac = temp[0];
        newTank.tankJuiceData.Add(newJuice);
        newJuice = new TankJuiceData();
        newJuice.juiceName = Common.JuiceNames.Pineapple.ToString();
        newJuice.targetFrac = 0.8f;
        newJuice.actualFrac = temp[1];
        newTank.tankJuiceData.Add(newJuice);
        tankData.Add(newTank);
        // End of new tank

        //Add new tank
        newTank = new TankData();
        newTank.centerx = 200;
        newTank.centery = 1300;
        newTank.tankWidth = opTankWidthOrig;
        newTank.tankHeight = opTankHeightOrig;
        newTank.tankName = "Outlet-A/B";
        newTank.tankCapacity = Constants.Tanks.outletTankCapacity;
        newJuice = new TankJuiceData();
        newJuice.juiceName = Common.JuiceNames.Blueberry.ToString();
        newJuice.targetFrac = 0.2f;
        newJuice.actualFrac = 0.2f;
        newTank.tankJuiceData.Add(newJuice);
        //	Log.i("hi", "Actual frac for A in Outlet A/B is" + String.valueOf(newJuice.actualFrac));
        newJuice = new TankJuiceData();
        newJuice.juiceName = Common.JuiceNames.Pineapple.ToString();
        newJuice.targetFrac = 0.8f;
        newJuice.actualFrac = 0.8f;
        newTank.tankJuiceData.Add(newJuice);
        //print("Actual frac for B in Outlet A/B is" + (newJuice.actualFrac.ToString()));
        tankData.Add(newTank);
        // End of new tank

        //  C/D and Outlet C/D//
        //Add new tank
        newTank = new TankData();
        newTank.centerx = 1500;
        newTank.centery = 950;
        newTank.tankWidth = opTankWidthOrig;
        newTank.tankHeight = opTankHeightOrig;
        newTank.tankName = "C/D";
        newTank.tankCapacity = Constants.Tanks.initialTankCapacity;
        temp = GenTwoRandomFracs();
        newJuice = new TankJuiceData();
        newJuice.juiceName = Common.JuiceNames.Strawberry.ToString();
        newJuice.targetFrac = 0.2f;
        newJuice.actualFrac = temp[0];
        newTank.tankJuiceData.Add(newJuice);
        newJuice = new TankJuiceData();
        newJuice.juiceName = Common.JuiceNames.Orange.ToString();
        newJuice.targetFrac = 0.8f;
        newJuice.actualFrac = temp[1];
        newTank.tankJuiceData.Add(newJuice);
        tankData.Add(newTank);
        // End of new tank

        //Add new tank
        newTank = new TankData();
        newTank.centerx = 1600;
        newTank.centery = 1300;
        newTank.tankWidth = opTankWidthOrig;
        newTank.tankHeight = opTankHeightOrig;
        newTank.tankName = "Outlet-C/D";
        newTank.tankCapacity = Constants.Tanks.outletTankCapacity;
        newJuice = new TankJuiceData();
        newJuice.juiceName = Common.JuiceNames.Strawberry.ToString();
        newJuice.targetFrac = 0.2f;
        newJuice.actualFrac = 0.2f;
        newTank.tankJuiceData.Add(newJuice);
        newJuice = new TankJuiceData();
        newJuice.juiceName = Common.JuiceNames.Orange.ToString();
        newJuice.targetFrac = 0.8f;
        newJuice.actualFrac = 0.8f;
        newTank.tankJuiceData.Add(newJuice);
        tankData.Add(newTank);
        // End of new tank
        //

        //A/B/A and outlet A/B/C
        //Add new tank
        newTank = new TankData();
        newTank.centerx = 500;
        newTank.centery = 950;
        newTank.tankWidth = opTankWidthOrig;
        newTank.tankHeight = opTankHeightOrig;
        newTank.tankName = "A/B/C";
        newTank.tankCapacity = Constants.Tanks.initialTankCapacity;
        temp = GenThreeRandomFracs();
        //   Log.i("hi","The temp random vals for A/B/C are:" + Arrays.toString(temp));
        newJuice = new TankJuiceData();
        newJuice.juiceName = Common.JuiceNames.Blueberry.ToString();
        newJuice.targetFrac = 0.2f;
        newJuice.actualFrac = temp[0];
        newTank.tankJuiceData.Add(newJuice);
        //print("The  fractions for A in A/B/C is:" + (newJuice.actualFrac.ToString()));
        newJuice = new TankJuiceData();
        newJuice.juiceName = Common.JuiceNames.Pineapple.ToString();
        newJuice.targetFrac = 0.5f;
        newJuice.actualFrac = temp[1];
        newTank.tankJuiceData.Add(newJuice);
        //print("The  fractions for B in A/B/C is:" + (newJuice.actualFrac.ToString()));
        newJuice = new TankJuiceData();
        newJuice.juiceName = Common.JuiceNames.Strawberry.ToString();
        newJuice.targetFrac = 0.3f;
        newJuice.actualFrac = temp[2];
        newTank.tankJuiceData.Add(newJuice);
        //print("The  fractions for C in A/B/C is:" + (newJuice.actualFrac.ToString()));
        tankData.Add(newTank);
        //  Log.i("hi","The three fractions for A/C/D are:" + );
        // End of new tank

        //Add new tank
        newTank = new TankData();
        newTank.centerx = 550;
        newTank.centery = 1300;
        newTank.tankWidth = opTankWidthOrig;
        newTank.tankHeight = opTankHeightOrig;
        newTank.tankName = "Outlet-A/B/C";
        newTank.tankCapacity = Constants.Tanks.outletTankCapacity;
        newJuice = new TankJuiceData();
        newJuice.juiceName = Common.JuiceNames.Blueberry.ToString();
        newJuice.targetFrac = 0.2f;
        newJuice.actualFrac = 0.2f;
        newTank.tankJuiceData.Add(newJuice);
        newJuice = new TankJuiceData();
        newJuice.juiceName = Common.JuiceNames.Pineapple.ToString();
        newJuice.targetFrac = 0.5f;
        newJuice.actualFrac = 0.5f;
        newTank.tankJuiceData.Add(newJuice);
        newJuice = new TankJuiceData();
        newJuice.juiceName = Common.JuiceNames.Strawberry.ToString();
        newJuice.targetFrac = 0.3f;
        newJuice.actualFrac = 0.3f;
        newTank.tankJuiceData.Add(newJuice);
        tankData.Add(newTank);
        // End of new tank
        //

        //B/C/D and outlet B/C/D
        //Add new tank
        newTank = new TankData();
        newTank.centerx = 1180;
        newTank.centery = 950;
        newTank.tankWidth = opTankWidthOrig;
        newTank.tankHeight = opTankHeightOrig;
        newTank.tankName = "B/C/D";
        newTank.tankCapacity = Constants.Tanks.initialTankCapacity;
        temp = GenThreeRandomFracs();
        newJuice = new TankJuiceData();
        newJuice.juiceName = Common.JuiceNames.Pineapple.ToString();
        newJuice.targetFrac = 0.2f;
        newJuice.actualFrac = temp[0];
        newTank.tankJuiceData.Add(newJuice);
        newJuice = new TankJuiceData();
        newJuice.juiceName = Common.JuiceNames.Strawberry.ToString();
        newJuice.targetFrac = 0.5f;
        newJuice.actualFrac = temp[1];
        newTank.tankJuiceData.Add(newJuice);
        newJuice = new TankJuiceData();
        newJuice.juiceName = Common.JuiceNames.Orange.ToString();
        newJuice.targetFrac = 0.3f;
        newJuice.actualFrac = temp[2];
        newTank.tankJuiceData.Add(newJuice);
        tankData.Add(newTank);
        // End of new tank

        //Add new tank
        newTank = new TankData();
        newTank.centerx = 1230;
        newTank.centery = 1300;
        newTank.tankWidth = opTankWidthOrig;
        newTank.tankHeight = opTankHeightOrig;
        newTank.tankName = "Outlet-B/C/D";
        newTank.tankCapacity = Constants.Tanks.outletTankCapacity;
        newJuice = new TankJuiceData();
        newJuice.juiceName = Common.JuiceNames.Pineapple.ToString();
        newJuice.targetFrac = 0.2f;
        newJuice.actualFrac = 0.2f;
        newTank.tankJuiceData.Add(newJuice);
        newJuice = new TankJuiceData();
        newJuice.juiceName = Common.JuiceNames.Strawberry.ToString();
        newJuice.targetFrac = 0.5f;
        newJuice.actualFrac = 0.5f;
        newTank.tankJuiceData.Add(newJuice);
        newJuice = new TankJuiceData();
        newJuice.juiceName = Common.JuiceNames.Orange.ToString();
        newJuice.targetFrac = 0.3f;
        newJuice.actualFrac = 0.3f;
        newTank.tankJuiceData.Add(newJuice);
        tankData.Add(newTank);
        // End of new tank
        //

        //A/C/D and outlet A/C/D
        //Add new tank
        newTank = new TankData();
        newTank.centerx = 820;
        newTank.centery = 950;
        newTank.tankWidth = opTankWidthOrig;
        newTank.tankHeight = opTankHeightOrig;
        newTank.tankName = "A/C/D";
        newTank.tankCapacity = Constants.Tanks.initialTankCapacity;
        temp = GenThreeRandomFracs();
        newJuice = new TankJuiceData();
        newJuice.juiceName = Common.JuiceNames.Blueberry.ToString();
        newJuice.targetFrac = 0.2f;
        newJuice.actualFrac = temp[0];
        newTank.tankJuiceData.Add(newJuice);
        newJuice = new TankJuiceData();
        newJuice.juiceName = Common.JuiceNames.Strawberry.ToString();
        newJuice.targetFrac = 0.3f;
        newJuice.actualFrac = temp[1];
        newTank.tankJuiceData.Add(newJuice);
        newJuice = new TankJuiceData();
        newJuice.juiceName = Common.JuiceNames.Orange.ToString();
        newJuice.targetFrac = 0.5f;
        newJuice.actualFrac = temp[2];
        newTank.tankJuiceData.Add(newJuice);
        tankData.Add(newTank);
        // End of new tank

        //Add new tank
        newTank = new TankData();
        newTank.centerx = 880;
        newTank.centery = 1300;
        newTank.tankWidth = opTankWidthOrig;
        newTank.tankHeight = opTankHeightOrig;
        newTank.tankName = "Outlet-A/C/D";
        newTank.tankCapacity = Constants.Tanks.outletTankCapacity;
        newJuice = new TankJuiceData();
        newJuice.juiceName = Common.JuiceNames.Blueberry.ToString();
        newJuice.targetFrac = 0.2f;
        newJuice.actualFrac = 0.2f;
        newTank.tankJuiceData.Add(newJuice);
        newJuice = new TankJuiceData();
        newJuice.juiceName = Common.JuiceNames.Strawberry.ToString();
        newJuice.targetFrac = 0.3f;
        newJuice.actualFrac = 0.3f;
        newTank.tankJuiceData.Add(newJuice);
        newJuice = new TankJuiceData();
        newJuice.juiceName = Common.JuiceNames.Orange.ToString();
        newJuice.targetFrac = 0.5f;
        newJuice.actualFrac = 0.5f;
        newTank.tankJuiceData.Add(newJuice);
        tankData.Add(newTank);

        return tankData;
    }

    public static List<Valve> CreateValves(List<TankData> tankData)
    {
        List<Valve> valveData = new List<Valve>();
        valveData.Add(CreateValve("C/D", "Outlet-C/D", valveData.Count));
        valveData.Add(CreateValve("A/B", "Outlet-A/B", valveData.Count));
        valveData.Add(CreateValve("A/B/C", "Outlet-A/B/C", valveData.Count));
        valveData.Add(CreateValve("B/C/D", "Outlet-B/C/D", valveData.Count));
        valveData.Add(CreateValve("A/C/D", "Outlet-A/C/D", valveData.Count));
        valveData.Add(CreateValve("Supply-A", "A", valveData.Count));
        valveData.Add(CreateValve("Supply-B", "B", valveData.Count));
        valveData.Add(CreateValve("Supply-C", "C", valveData.Count));
        valveData.Add(CreateValve("Supply-D", "D", valveData.Count));
        valveData.Add(CreateValve("A", "A/B", valveData.Count));
        valveData.Add(CreateValve("B", "A/B", valveData.Count));
        valveData.Add(CreateValve("A", "A/B/C", valveData.Count));
        valveData.Add(CreateValve("B", "A/B/C", valveData.Count));
        valveData.Add(CreateValve("C", "A/B/C", valveData.Count));
        valveData.Add(CreateValve("B", "B/C/D", valveData.Count));
        valveData.Add(CreateValve("D", "B/C/D", valveData.Count));
        valveData.Add(CreateValve("C", "B/C/D", valveData.Count));
        valveData.Add(CreateValve("A", "A/C/D", valveData.Count));
        valveData.Add(CreateValve("C", "A/C/D", valveData.Count));
        valveData.Add(CreateValve("D", "A/C/D", valveData.Count));
        valveData.Add(CreateValve("C", "C/D", valveData.Count));
        valveData.Add(CreateValve("D", "C/D", valveData.Count));
        for (int h = 0; h < tankData.Count; h++)
        {
            TankData thisTank = tankData[h];
            if (!thisTank.tankName.ToLower().Contains(Constants.Tanks.outletCode) && !thisTank.tankName.ToLower().Contains("supply"))
            {
                valveData.Add(CreateValve(thisTank.tankName, "drain", valveData.Count));
            }
        }
        return valveData;
        // CreateValve();
    }

    public static Valve CreateValve(string sourceTank, string destinationTank, int valveIndex)
    {
        Valve newValve = new Valve();
        newValve.tankNameValveAttachedTo = sourceTank;
        newValve.valveDestination = destinationTank;
        newValve.valveState = (int)Common.ValveState.Closed;
        //	newValve.valveWaiting = false;
        newValve.valveFlowRate = Constants.Valves.standardFlowRate; ;
        if (sourceTank.ToLower().Contains(Constants.Tanks.supplyCode))
        {
            newValve.valveFlowRate = Constants.Valves.trucksFlowRate;
        }
        else if (destinationTank.ToLower().Contains(Constants.Tanks.outletCode))
        {
            newValve.valveFlowRate = Constants.Valves.trucksFlowRate;
        }
        else
        {
            newValve.valveFlowRate = Constants.Valves.standardFlowRate;
        }

        newValve.lineThickness = Constants.Lines.initialLineThickness;
        newValve.valveIndex = valveIndex;

        return (newValve);

    }

    public void SetTankIndexRefsForValves()
    {
        foreach (Valve thisValve in MainScript.currentMainScript.valveData)
        {
            LoopTanksForValveIndexRefs(thisValve);
        }
    }

    void LoopTanksForValveIndexRefs(Valve thisValve)
    {
        int indexesSetCount = 0; // Valve has max of two associated indexes
        foreach (TankData thisTank in MainScript.currentMainScript.tankData)
        {

            if (thisValve.tankNameValveAttachedTo == thisTank.tankName)
            {
                thisValve.tankIndexValveAttachedTo = thisTank.tankIndex;
                indexesSetCount++;
            }
            if (thisValve.valveDestination == thisTank.tankName)
            {
                thisValve.valveDestinationTankIndex = thisTank.tankIndex;
                indexesSetCount++;
            }
            if (thisValve.valveDestination == Constants.Valves.drainCode && indexesSetCount == 1)
            {
                return;
            }
            else if (thisValve.valveDestination != Constants.Valves.drainCode && indexesSetCount == 2)
            {
                return;
            }
        }
    }

    public static float[] GenTwoRandomFracs()
    {
        float minimumJuiceFractionAnyJuice = Constants.Tanks.minimumJuiceFractionAnyJuice;
        float random1 = UnityEngine.Random.Range(minimumJuiceFractionAnyJuice, 1 - minimumJuiceFractionAnyJuice); //No juice fraction should be less than 0.2f;
        float random2 = 1 - random1;
        float[] returnArray = { random1, random2 };
        //	print("Random number 1 is" + random1.ToString());
        //	print("Random number 2 is" + random2.ToString());
        return returnArray;
    }

    public static float[] GenThreeRandomFracs()
    {
        float random1;
        float random2;
        float random3;
        float minimumJuiceFractionAnyJuice = Constants.Tanks.minimumJuiceFractionAnyJuice;
        // Want target fraction for each juice to be  > 0.2f, and < 0.8f. Mathematically with all three juices in a 3 juice tank, the maximum target fraction is 0.6f
        float upperBoundfracAllJuices = 1 - 2 * minimumJuiceFractionAnyJuice;
        random1 = UnityEngine.Random.Range(minimumJuiceFractionAnyJuice, upperBoundfracAllJuices);
        random2 = UnityEngine.Random.Range(minimumJuiceFractionAnyJuice, new float[]{upperBoundfracAllJuices, 1 - random1 - minimumJuiceFractionAnyJuice}.Min());
        random3 = 1 - random2 - random1;

        float[] returnArray = { random1, random2, random3 };

        return returnArray;
    }

    public static void SetTankIndexes(List<TankData> tankData)
    {
        for (int i = 0; i < tankData.Count; i++)
        {
            TankData thisTank = tankData[i];
            thisTank.tankIndex = i;
        }
    }

    public static void SetInitialTankLevels(MainScript mainScript)
    {
        for (int i = 0; i < mainScript.tankData.Count; i++)
        {
            TankData thisTank = mainScript.tankData[i];
            string tankName = thisTank.tankName;
            if (tankName.ToLower().Contains(Constants.Tanks.outletCode))
            {
                thisTank.tankLevelFrac = Constants.Tanks.tankEmptyLevelFraction;
            }
            else if (tankName.ToLower().Contains(Constants.Tanks.supplyCode))
            {
                thisTank.tankLevelFrac = Constants.Tanks.tankHalfFull;
            }
            else
            {
                thisTank.tankLevelFrac = GenRandomTankLevel();
            }
        }
    }

    static float GenRandomTankLevel()
    {
        float random = 50;//(float)((Math.random() * 30) + 20);
        return random / 100;
    }

}