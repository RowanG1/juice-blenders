﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ValveDataStampPeriod  {

	public float juiceTransferred;

	public ValveDataStampPeriod() {

	}

	public ValveDataStampPeriod(float juiceTransferred) {
		this.juiceTransferred = juiceTransferred;
	}

}
