﻿using UnityEngine;
using System.Collections;

[System.Serializable] 
	public class Valve
	{
	public string tankNameValveAttachedTo;
	public int valveIndex;
	public int tankIndexValveAttachedTo;
	public int valveDestinationTankIndex = -1;
	public string valveDestination;
	public float valveFlowRate;
	public float centerx;
	public float centery;
	public float lineThickness;
	public int valveState;
	public bool upgradeRequested;
}


