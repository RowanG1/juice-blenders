﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreateEquipment : MonoBehaviour {
    
    GameObject operations;

    List<TankData> tankData;
    List<GameObject> tankInstances;
    List<ThisTankChartObjects> tankCharts;
    public List<GameObject> valveInstances;
    public List<GameObject> pipeInstances;

    public List<Valve> valveData;

    public GameObject smokePrefab;
    public GameObject tankPrefab;
    public GameObject chartAreaPrefab;
    public GameObject fillLabelPrefab;
    public GameObject juiceGush;
    public GameObject valvePrefab;
    public GameObject cylinderPrefab;

    public Material drainMaterial;

	// Use this for initialization
	void Start () {
		tankData = MainScript.currentMainScript.tankData;
        operations = MainScript.currentMainScript.operations;
        tankInstances = MainScript.currentMainScript.tankInstances;
        tankCharts = MainScript.currentMainScript.tankCharts;

        valveData = MainScript.currentMainScript.valveData;
        valveInstances = MainScript.currentMainScript.valveInstances;
        pipeInstances= MainScript.currentMainScript.pipeInstances;

        InstantiateTanks();
        InstantiateValves ();
        CreatePipes ();
	}

	void InstantiateTanks() {
        Material tankBackgroundMaterial = new Material (Shader.Find("Sprites/Default"));
        tankBackgroundMaterial.color = Color.white;

        for (int i = 0; i < tankData.Count; i++) {
            TankData thisTank = tankData [i];
 
            GameObject thisTankInst = Instantiate(tankPrefab, new Vector3(thisTank.centerx, -thisTank.centery,0), Quaternion.identity) as GameObject;
            thisTankInst.transform.parent = operations.transform;
            thisTankInst.transform.Find ("Tank").localScale = new Vector3 (thisTank.tankWidth, thisTank.tankHeight,1);
 
            Transform thisChartArea = thisTankInst.transform.Find ("chartsArea");
            thisChartArea.localPosition = new Vector3 (thisChartArea.localPosition.x, -thisTank.tankHeight/2 + thisChartArea.localScale.y/2 + 5, thisChartArea.localPosition.z);
 
            string tankName = thisTank.tankName;
            if (tankName.ToLower ().Contains (Constants.Tanks.outletCode)) {
                tankName = "Dispatch";
            }
 
            thisTankInst.transform.Find ("Tank/tankText/back").GetComponent<TextMesh> ().text = tankName;
            thisTankInst.transform.Find ("Tank/tankText/front").GetComponent<TextMesh> ().text = tankName;
            thisTankInst.name = "Tank" + tankName;
            tankInstances.Add (thisTankInst);
 
         
            if (thisTank.tankName.ToLower ().Contains (Constants.Tanks.outletCode)) {
            	Transform[] allChildren = thisTankInst.GetComponentsInChildren<Transform>(true);
            	foreach (var child in allChildren) {
            		child.gameObject.tag = "Outlet";
            	}
 
                GameObject newFillLabel = Instantiate (fillLabelPrefab) as GameObject;
                thisTankInst.GetComponent<TankGameObject>().truckOutletFillLabel = newFillLabel;
                newFillLabel.transform.parent = thisTankInst.transform;
                newFillLabel.transform.localPosition = fillLabelPrefab.transform.localPosition;
                newFillLabel.name = "fillLabel";

                GameObject truckSmokeInstance = Instantiate (smokePrefab) as GameObject;
                thisTankInst.GetComponent<TankGameObject>().truckExhaust = truckSmokeInstance;
                truckSmokeInstance.transform.parent = thisTankInst.transform.Find("TruckStatus");
                truckSmokeInstance.transform.localPosition = smokePrefab.transform.localPosition;
                truckSmokeInstance.transform.localScale = smokePrefab.transform.localScale;
                truckSmokeInstance.name = "TruckExhaust";
                truckSmokeInstance.SetActive(false);
            }
            CreateChartsEachTank (tankData[i], tankInstances[i]);
            if (thisTank.tankName.ToLower ().Contains (Constants.Tanks.supplyCode)) {
            	thisTankInst.tag = "Supply";
            	Transform[] allChildren = thisTankInst.GetComponentsInChildren<Transform>(true);
            	foreach (var child in allChildren) {
            		child.gameObject.tag = "Supply";
            	}
                thisTankInst.transform.Find ("Tank/JuiceLevel").gameObject.SetActive (false);
                thisTankInst.transform.Find ("chartsArea").gameObject.SetActive (false);

                GameObject truckSmokeInstance = Instantiate (smokePrefab) as GameObject;
                thisTankInst.GetComponent<TankGameObject>().truckExhaust = truckSmokeInstance;
                truckSmokeInstance.transform.parent = thisTankInst.transform.Find("TruckStatus");
                truckSmokeInstance.transform.localPosition = smokePrefab.transform.localPosition;
                truckSmokeInstance.transform.localScale = smokePrefab.transform.localScale;
                truckSmokeInstance.name = "TruckExhaust";
                truckSmokeInstance.SetActive(false);
            }
 
            if (thisTank.tankName.ToLower ().Contains (Constants.Tanks.outletCode)) {
                thisTankInst.transform.Find ("chartsArea").gameObject.SetActive (false);
                thisTankInst.transform.Find ("Tank/JuiceLevel").gameObject.SetActive (false);
            }
 
            if (!thisTank.tankName.ToLower ().Contains (Constants.Tanks.outletCode) && !thisTank.tankName.ToLower ().Contains (Constants.Tanks.supplyCode)) {
                thisTankInst.transform.Find("Tank").GetComponent<MeshRenderer> ().material =   tankBackgroundMaterial;   
                Destroy (thisTankInst.transform.Find("TruckStatus").gameObject);
            }
        }
    }

	void CreateChartsEachTank(TankData thisTank, GameObject thisTankInst) {
		ThisTankChartObjects thisTankCharts = new ThisTankChartObjects ();
		tankCharts.Add (thisTankCharts);

		for (int k = 0; k < thisTank.tankJuiceData.Count; k++) {
			TankJuiceData thisJuice = thisTank.tankJuiceData [k];
			GameObject newChart = Instantiate (chartAreaPrefab) as GameObject;
			thisTankCharts.Charts.Add(newChart);
			newChart.transform.parent = thisTankInst.transform.Find("chartsArea");
			newChart.transform.localScale = chartAreaPrefab.transform.localScale;
			float newChartWidth = newChart.transform.localScale.x;
			newChart.transform.localPosition = new Vector3 (-0.5f + newChartWidth/2 + 0.1f + newChartWidth * 1.1f * k, 0, -1);
			Color thisActualBarColour = Common.GetJuiceColor(thisJuice.juiceName, 0.9f);
			newChart.transform.Find("actualBar").GetComponent<Renderer>().material.color = thisActualBarColour;
			newChart.transform.Find("targetStrip").GetComponent<Renderer>().material.color = new Color (0/255, 0/255, 0/255, 100f/255);
			newChart.transform.Find("juiceNameText").GetComponent<TextMesh>().text = thisJuice.juiceName;	
	}
	}

    void InstantiateValves() {
        for (int i = 0; i < valveData.Count; i++) {
            Valve thisValve = valveData [i];
            string tankNameValveAttachedTo = thisValve.tankNameValveAttachedTo;
            for (int j = 0; j < tankData.Count; j++) {
                TankData thisTank = tankData [j];
                if (thisTank.tankName == tankNameValveAttachedTo && thisValve.valveDestination != Constants.Valves.drainCode) {
                    GameObject newValveInstance = Instantiate (valvePrefab) as GameObject;
                    newValveInstance.transform.localScale = new Vector3 (Common.valveWidth, Common.valveWidth, 1);
                    newValveInstance.transform.parent = tankInstances [j].transform.Find("Valves");
                    var offsetX = tankData [j].numPipesFromTankAdded;
                    newValveInstance.transform.position = new Vector3 (offsetX * (13f + Common.valveWidth) + thisTank.leftx + Common.valveWidth/2 + 5, -(thisTank.bottomy + Common.valveWidth/2),0);
                    tankData [j].numPipesFromTankAdded++;
                    valveInstances.Add (newValveInstance);
                    newValveInstance.GetComponent<ValveScript> ().valveIndex = i;
                    newValveInstance.GetComponentInChildren<Renderer>().material.color = Common.valveClosedColor;

                }

                if (thisTank.tankName == tankNameValveAttachedTo && thisValve.valveDestination == Constants.Valves.drainCode) {      
                    GameObject newValveInstance = Instantiate (valvePrefab) as GameObject;
                    newValveInstance.name = Constants.Valves.drainCode;
                    newValveInstance.transform.localScale = new Vector3 (Common.valveWidth, Common.valveWidth, 1);
                    newValveInstance.transform.parent = tankInstances [j].transform.Find("Valves");
                    newValveInstance.transform.position = new Vector3 (thisTank.leftx - Common.valveWidth/2, -thisTank.centery, 0);
                    newValveInstance.GetComponent<ValveScript> ().valveIndex = i;
                    newValveInstance.GetComponentInChildren<Renderer>().material.color = Common.valveClosedColor;
                    valveInstances.Add (newValveInstance);
                    //GameObject drainImage = Instantiate(Quad)
                    var drainIm = GameObject.CreatePrimitive(PrimitiveType.Quad);
                    drainIm.GetComponent<MeshRenderer>().material = drainMaterial;
                    drainIm.transform.parent = newValveInstance.transform;
                    drainIm.name = "DrainImage";
                    drainIm.transform.localPosition = new Vector3 (0,0,-1);
                    drainIm.transform.localScale = new Vector3 (1,1,1);

                    GameObject newJuiceDrainGush = Instantiate(juiceGush) as GameObject;
                    tankInstances [j].GetComponent<TankGameObject>().juiceDrainGush = newJuiceDrainGush;
                    newJuiceDrainGush.transform.parent = newValveInstance.transform;
                    newJuiceDrainGush.transform.localScale = juiceGush.transform.localScale;
                    newJuiceDrainGush.transform.localPosition = juiceGush.transform.localPosition;
                    newJuiceDrainGush.name = "JuiceDrainGush";
                    newJuiceDrainGush.SetActive(false);

                    Destroy (drainIm.GetComponent<MeshCollider> ());
                }
        }
    }
    }

    public void CreatePipes() {
        for (int i = 0; i < valveData.Count; i++) {
            Valve thisValve = valveData [i];
            string tankNameValveAttachedTo = thisValve.tankNameValveAttachedTo;
            string valveDestination = thisValve.valveDestination;
            if (valveDestination != Constants.Valves.drainCode) {
                int sourceTankIndex = 0;
                int destinationIndex = 0;

                for (int j = 0; j < tankData.Count;j++) {
                    TankData thisTank = tankData [j];
                    if (thisTank.tankName == tankNameValveAttachedTo) {
                        sourceTankIndex = j;
                    }
                    if (thisTank.tankName == valveDestination) {
                        destinationIndex = j;
                    }
                }
                var valveStart = valveInstances[i].transform.gameObject;
                var valveEnd = tankInstances[destinationIndex].transform.Find("entryNode").gameObject;
                var valveStartPos = valveStart.transform.position;
                var valveEndPos = valveEnd.transform.position;
                valveStartPos += new Vector3 (0, -30f/2,0);
                CreateCylinderBetweenPoints (valveStartPos, valveEndPos,thisValve.lineThickness, sourceTankIndex, destinationIndex, thisValve.valveIndex);
            }
        }
    }

    private void CreateCylinderBetweenPoints(Vector3 start, Vector3 end, float width,int startTankIndex, int destinationTankIndex, int valveIndex ) {
        var offset = end - start;
        var scale = new Vector3(width * 20f/8, offset.magnitude / 2.0f, 1);
        var position = start + (offset / 2.0f);

        GameObject cylinder = Instantiate(cylinderPrefab, position, Quaternion.identity) as GameObject;
        cylinder.name = "Line-" + tankData [startTankIndex].tankName + "-to-" + tankData [destinationTankIndex].tankName;       
        cylinder.transform.parent = operations.transform;
        cylinder.transform.up = offset; // Unity sets the up vector of the local axis to align with the set vector (in this case, the offset DIRECTION)
        cylinder.transform.localScale = scale;
        cylinder.transform.Translate(0, 0, 1, null); // Move along world z co-ord to hide cylinders behind tanks
        cylinder.GetComponent<PipeScript> ().startTankIndex = startTankIndex;
        cylinder.GetComponent<PipeScript> ().destinationTankIndex = destinationTankIndex;
        cylinder.GetComponent<PipeScript> ().valveIndex = valveIndex;
        pipeInstances.Add (cylinder);
}
}
