﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tank_And_Valve_Physical_Updates
{
    MainScript mainScript;
    List<Transform> tankTextFront = new List<Transform>();

    public Tank_And_Valve_Physical_Updates()
    {
        this.mainScript = MainScript.currentMainScript;
    }

    public void RecalcTankParams()
    {
        for (int j = 0; j < mainScript.tankData.Count; j++)
        {
            TankData thisTank = mainScript.tankData[j];
            thisTank.RecalcParams();
        }
    }

    public bool TankIsFull(TankData tankData)
    {
        if (tankData.tankLevelFrac >= Constants.Tanks.tankFullLevelFraction)
        {
            return true;
        }
        else return false;
    }

    public bool TankIsEmpty(TankData tankData)
    {
        if (tankData.tankLevelFrac <= Constants.Tanks.tankEmptyLevelFraction)
        {
            return true;
        }
        else return false;
    }

    public void SwitchOffValveToTank(TankData thisTank)
    {
        foreach (Valve thisvalve in mainScript.valveData)
        {
            if (thisvalve.valveDestination == thisTank.tankName)
            {
                thisvalve.valveState = (int)Common.ValveState.Closed;
            }
        }
    }


    public void EachTankSpecSet(TankData thisTank)
    {
        for (int j = 0; j < thisTank.tankJuiceData.Count; j++)
        {
            TankJuiceData thisJuice = thisTank.tankJuiceData[j];
            float diffActualTargetForJuice = Mathf.Abs(thisJuice.actualFrac - thisJuice.targetFrac);
            if (diffActualTargetForJuice > 0.05f)
            {
                thisTank.inSpec = false;
                return;
            }
        }
        thisTank.inSpec = true;
    }

    public void AllTanksOutSpecCheckAndSet()
    {
        //tankLoop:
        for (int i = 0; i < mainScript.tankData.Count; i++)
        {
            TankData thisTank = mainScript.tankData[i];
            string tankName = thisTank.tankName;
            if (tankName == "A/B" || tankName == "C/D" || tankName == "A/B/C" || tankName == "B/C/D" || tankName == "A/C/D")
            {
                //	juiceLoop:
                EachTankSpecSet(thisTank);

                if (thisTank.inSpec == true && mainScript.uIUpdatesInGame != null)
                {
                    mainScript.uIUpdatesInGame.tankSpecTextColorSet(mainScript.tankInstances[i].GetComponent<TankGameObject>().tankTextFront.transform, new Color(0, 1, 0));
                    //tankInstances [i].transform.Find ("Tank/tankText/front").GetComponent<TextMesh> ().color = new Color (0, 1, 0);
                }
                else if (thisTank.inSpec == false && mainScript.uIUpdatesInGame != null)
                {
                    mainScript.uIUpdatesInGame.tankSpecTextColorSet(mainScript.tankInstances[i].GetComponent<TankGameObject>().tankTextFront.transform, new Color(1, 0, 0));
                    //tankInstances [i].transform.Find ("Tank/tankText/front").GetComponent<TextMesh> ().color = new Color (1, 0, 0);
                }
                if (thisTank.inSpec == false)
                {
                    StopValvesTankOutSpec(thisTank.tankName);
                }
            }
        }
    }

    public void StopValvesTankOutSpec(string tankName)
    {
        for (int k = 0; k < mainScript.valveData.Count; k++)
        {
            Valve thisValve = mainScript.valveData[k];
            if (thisValve.tankNameValveAttachedTo == tankName && thisValve.valveDestination.ToLower().Contains("outlet") && thisValve.valveState == (int)Common.ValveState.Open)
            {
                thisValve.valveState = (int)Common.ValveState.Closed;
                string outOfSpecMsg = "Tank out of spec. Valve closed to truck.";
                if (!MainAnimatedNotification.ExistingNotificationsContainsText(outOfSpecMsg, mainScript.mainNotificationsToAnimate, mainScript.currentAnimatedNotification))
                {
                    mainScript.mainNotificationsToAnimate.Add(new MainAnimatedNotification(outOfSpecMsg));
                }
            }
        }
    }

    public bool CheckIfAnyPresentTruckNotRecentlyFilling()
    {
        for (int i = 0; i < mainScript.tankData.Count; i++)
        {
            TankData thisTank = mainScript.tankData[i];

            if (thisTank.deliverTruckState == (int)Common.DeliverTruckState.Present)
            {
                int lastTimeTruckFilled = 0;
                if (mainScript.outletTruckLastTimeFilled.ContainsKey(i))
                {
                    lastTimeTruckFilled = mainScript.outletTruckLastTimeFilled[i];
                }
                if (mainScript.oneSecondCount - lastTimeTruckFilled > Constants.Trucks.thresholdTimeToNotifyIfDispatchTruckPresentAndNotFilling)
                {
                    return true;
                }
            }
        }
        return false;
    }

    public void ValveAutoShuts()
    {
        //Check to see if tanks are full and empty
        for (int i = 0; i < mainScript.tankData.Count; i++)
        {
            TankData thisTank = mainScript.tankData[i];
            bool thisTankValveChanged2 = false;
            if (TankIsEmpty(thisTank))
            {
                for (int l = 0; l < mainScript.valveData.Count; l++)
                {
                    Valve thisValve = mainScript.valveData[l];
                    if (thisValve.tankNameValveAttachedTo == mainScript.tankData[i].tankName)
                    {
                        if (thisValve.valveState == (int)Common.ValveState.Open)
                        {
                            thisValve.valveState = (int)Common.ValveState.Closed;
                            thisTankValveChanged2 = true;
                        }
                    }
                }
                if (thisTankValveChanged2 == true)
                {

                }
            }

            if (TankIsFull(thisTank))
            {
                bool thisTankValveChanged = false;
                for (int l = 0; l < mainScript.valveData.Count; l++)
                {
                    Valve thisValve = mainScript.valveData[l];
                    if (thisValve.valveDestination == mainScript.tankData[i].tankName)
                    {
                        if (thisValve.valveState == (int)Common.ValveState.Open)
                        {
                            thisValve.valveState = (int)Common.ValveState.Closed;
                            thisTankValveChanged = true;
                        }
                    }
                }
                if (thisTankValveChanged == true)
                {

                }
            }

            if (((thisTank.deliverTruckState == (int)Common.DeliverTruckState.Absent) || (thisTank.deliverTruckState == (int)Common.DeliverTruckState.Waiting)) && thisTank.tankName.ToLower().Contains(Constants.Tanks.outletCode))
            {
                for (int l = 0; l < mainScript.valveData.Count; l++)
                {
                    Valve thisValve = mainScript.valveData[l];
                    if ((thisValve.valveDestination == thisTank.tankName) && thisValve.valveState == (int)Common.ValveState.Open)
                    {
                        thisValve.valveState = (int)Common.ValveState.Closed;
                        string absentTruck = "Collection truck not present. Valve closed.";
                        if (!MainAnimatedNotification.ExistingNotificationsContainsText(absentTruck, mainScript.mainNotificationsToAnimate, mainScript.currentAnimatedNotification))
                        {
                            mainScript.mainNotificationsToAnimate.Add(new MainAnimatedNotification(absentTruck));
                        }
                    }
                }
            }
        }
    }

    public void JuiceTransferValveLoop(float deltaTime)
    {

        for (int i = 0; i < mainScript.valveData.Count; i++)
        {
            Valve thisValve = mainScript.valveData[i];
            if (thisValve.valveState == (int)Common.ValveState.Open)
            {
                Dictionary<string, float> juiceTransferVolumeComponents = new Dictionary<string, float>();
                float juiceVolTransferred = thisValve.valveFlowRate * deltaTime;

                TankData tankValveAttachedTo = mainScript.tankData[thisValve.tankIndexValveAttachedTo];
                TankData destinationTank;

                TransferJuiceFromSourceTank(tankValveAttachedTo, juiceVolTransferred, ref juiceTransferVolumeComponents, deltaTime);

                if (thisValve.valveDestination.ToLower() != (Constants.Valves.drainCode))
                {
                    destinationTank = mainScript.tankData[thisValve.valveDestinationTankIndex];

                    TransferJuiceToDestinationTank(juiceTransferVolumeComponents: juiceTransferVolumeComponents, tank: destinationTank, juiceVolTransferred: juiceVolTransferred);
                    ValveDataStampPeriod newValveUsage = new ValveDataStampPeriod(juiceVolTransferred);
                    AddNewValveHistoryAtMinTimeStamp(newValveUsage, mainScript.oneSecondCount / 60, thisValve.valveIndex);

                    bool tankIsUpgradeableByDefault = !destinationTank.tankName.Contains(Constants.Tanks.outletCode);
                    if (tankIsUpgradeableByDefault)
                    {
                        TankUsageStamp newJuiceAddedToTank = new TankUsageStamp(juiceVolTransferred);
                        AddNewTankUsageStamp(newJuiceAddedToTank, mainScript.oneSecondCount / 60, destinationTank.tankIndex);
                    }

                    if (destinationTank.tankName.ToLower().Contains(Constants.Tanks.outletCode))
                    {
                        AddNewOutletTruckLastTimeFilled(destinationTank.tankIndex, mainScript.oneSecondCount);
                        mainScript.volJuiceDelivered += juiceVolTransferred;
                    }
                }
                else
                {
                    mainScript.volJuiceWasted += juiceVolTransferred;
                }

            }
        }
   
    }

    void TransferJuiceFromSourceTank(TankData tank, float juiceVolTransferred, ref Dictionary<string, float> juiceTransferVolumeComponents, float deltaTime)
    {
        Dictionary<string, float> newJuiceConsumption = new Dictionary<string, float>();

        float tankFillVolume = tank.tankLevelFrac * tank.tankCapacity;
        for (int k = 0; k < tank.tankJuiceData.Count; k++)
        {
            juiceTransferVolumeComponents.Add(tank.tankJuiceData[k].juiceName, tank.tankJuiceData[k].actualFrac * juiceVolTransferred);
        }

        tankFillVolume -= juiceVolTransferred;

        if (tank.tankName.ToLower().Contains(Constants.Tanks.supplyCode))
        {
            string juiceName = tank.tankJuiceData[0].juiceName;
            CostToDrawFromSupplyTruck(juiceName: juiceName, juiceVolumeWithdrawn: juiceVolTransferred);
            AddNewJuiceConsumptionToMinTimeStamp(new Dictionary<string, float>() { { juiceName, juiceVolTransferred } }, mainScript.oneSecondCount / 60);
            // thisTank.tankLevelFrac = 0.5f;
        }
        else
        {
            tank.tankLevelFrac = tankFillVolume / tank.tankCapacity;
        }
        return;
    }

    void TransferJuiceToDestinationTank(Dictionary<string, float> juiceTransferVolumeComponents, TankData tank, float juiceVolTransferred)
    {
        float oldTankVol = tank.tankLevelFrac * (float)tank.tankCapacity;
        float newTankVol = oldTankVol + juiceVolTransferred;
        for (int k = 0; k < tank.tankJuiceData.Count; k++)
        {
            string juiceName = tank.tankJuiceData[k].juiceName;
            float item;
            if (juiceTransferVolumeComponents.TryGetValue(juiceName, out item))
            {
                tank.tankJuiceData[k].actualFrac = (float)(tank.tankJuiceData[k].actualFrac * oldTankVol + juiceTransferVolumeComponents[juiceName]) / newTankVol;
            }
            else
            {
                tank.tankJuiceData[k].actualFrac = tank.tankJuiceData[k].actualFrac * oldTankVol / newTankVol;
            }
        }
        tank.tankLevelFrac = newTankVol / tank.tankCapacity;
    }



    void CostToDrawFromSupplyTruck(string juiceName, float juiceVolumeWithdrawn)
    {
        float costJuicePoured = Common.ConvertToTwoDecimal(juiceVolumeWithdrawn * mainScript.rawMatPrices[juiceName]);
        mainScript.juiceCostToAdd += costJuicePoured;
    }

    public void UpgradeTank(int tankIndex, float tankUpgradeCost, int tankCapacityExpandFactor, float tankHeightExpandFactor)
    {
        TankData thisTank = mainScript.tankData[tankIndex];
        float oldTankCentrey = thisTank.centery;
        float oldTankHeight = thisTank.tankHeight;

        float newTankHeight = thisTank.tankHeight * tankHeightExpandFactor;
        float newTankCentrey = oldTankCentrey + oldTankHeight / 2 - newTankHeight / 2;

        thisTank.tankHeight = newTankHeight;
        thisTank.centery = newTankCentrey;
        thisTank.RecalcParams();

        float currentTankCapacity = thisTank.tankCapacity;
        float currentTankLevelFrac = thisTank.tankLevelFrac;

        float tankFluidVol = currentTankCapacity * currentTankLevelFrac;
        float newTankCapacity = currentTankCapacity * tankCapacityExpandFactor;
        float newTankLevelFrac = tankFluidVol / newTankCapacity;

        thisTank.tankLevelFrac = newTankLevelFrac;
        thisTank.tankCapacity = newTankCapacity;

        mainScript.bankBalance -= tankUpgradeCost;
        mainScript.investments += tankUpgradeCost;

    }

    public void PumpUpgrade(Valve thisValve, int lineThick, int pumpUpgradeValue, float valveFlowRate)
    {
        mainScript.investments += pumpUpgradeValue;
        thisValve.lineThickness = lineThick;
        mainScript.bankBalance -= pumpUpgradeValue;
        thisValve.valveFlowRate = valveFlowRate;
        if (mainScript.uIUpdatesInGame != null)
        {
            mainScript.uIUpdatesInGame.pumpUpgradePipeLoop(thisValve, lineThick);
        }
    }

    public void TrimNonRecentMinuteStampsForJuicesConsumed()
    {
        HashSet<int> minStampKeysToRemove = new HashSet<int>();
        Dictionary<int, Dictionary<string, float>> juicesConsumedAtRecentMinuteStamps = mainScript.juicesConsumedAtRecentMinuteStamps;
        foreach (var juicesConsumedAtRecentMinuteStamp in juicesConsumedAtRecentMinuteStamps)
        {
            if (mainScript.oneSecondCount / 60 - juicesConsumedAtRecentMinuteStamp.Key > Common.recentTimeframeInMinsJuicesConsumed)
            {
                minStampKeysToRemove.Add(juicesConsumedAtRecentMinuteStamp.Key);
            }
        }
        foreach (var keyToRemove in minStampKeysToRemove)
        {
            mainScript.juicesConsumedAtRecentMinuteStamps.Remove(keyToRemove);
        }
    }

    public void TrimNonRecentMinuteStampsForTankHistory()
    {
        HashSet<int> minStampKeysToRemove = new HashSet<int>();
        Dictionary<int, Dictionary<int, TankUsageStamp>> juiceAddedToTanksByIndexHistoryAtRecentMinuteStamps = mainScript.juiceAddedToTanksByIndexHistoryAtRecentMinuteStamps;
        foreach (var recentMinuteStampTankKeyVal in juiceAddedToTanksByIndexHistoryAtRecentMinuteStamps)
        {
            if (mainScript.oneSecondCount / 60 - recentMinuteStampTankKeyVal.Key > Common.recentTimeframeInMinsTankHistory)
            {
                minStampKeysToRemove.Add(recentMinuteStampTankKeyVal.Key);
            }
        }
        foreach (var keyToRemove in minStampKeysToRemove)
        {
            juiceAddedToTanksByIndexHistoryAtRecentMinuteStamps.Remove(keyToRemove);
        }
    }

    public void TrimNonRecentMinuteStampsForValveHistory()
    {
        HashSet<int> minStampKeysToRemove = new HashSet<int>();
        Dictionary<int, Dictionary<int, ValveDataStampPeriod>> valvesUsageHistoryAtRecentMinuteStamps = mainScript.valvesUsageHistoryAtRecentMinuteStamps;
        foreach (var recentMinuteStampValveKeyVal in valvesUsageHistoryAtRecentMinuteStamps)
        {
            if (mainScript.oneSecondCount / 60 - recentMinuteStampValveKeyVal.Key > Common.recentTimeframeInMinsValveHistory)
            {
                minStampKeysToRemove.Add(recentMinuteStampValveKeyVal.Key);
            }
        }
        foreach (var keyToRemove in minStampKeysToRemove)
        {
            valvesUsageHistoryAtRecentMinuteStamps.Remove(keyToRemove);
        }
    }

    public bool RecentConsumptionJuiceIsSubstantial(float juiceConsumption, float consumptionSubstantialThreshold)
    {
        if (juiceConsumption > consumptionSubstantialThreshold)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public Dictionary<string, float> GetRecentTotalConsumptionPerJuice()
    {
        Dictionary<string, float> juiceConsumptions = new Dictionary<string, float>();
        Dictionary<int, Dictionary<string, float>> juicesConsumedAtRecentMinuteStamps = mainScript.juicesConsumedAtRecentMinuteStamps;

        foreach (var timeStampItem in juicesConsumedAtRecentMinuteStamps)
        {
            foreach (var juiceItem in timeStampItem.Value)
            {
                if (!juiceConsumptions.ContainsKey(juiceItem.Key))
                {
                    juiceConsumptions.Add(juiceItem.Key, 0);
                }
                juiceConsumptions[juiceItem.Key] += juiceItem.Value;
            }
        }
        return juiceConsumptions;
    }

    public Dictionary<int, float> GetRecentTotalJuiceTransferredPerValve()
    {
        Dictionary<int, float> juiceTransferredByValve = new Dictionary<int, float>();

        foreach (var valveUsageMinuteStampKeyValue in mainScript.valvesUsageHistoryAtRecentMinuteStamps)
        {
            foreach (var valveUsageKeyValue in valveUsageMinuteStampKeyValue.Value)
            {
                if (!juiceTransferredByValve.ContainsKey(valveUsageKeyValue.Key))
                {
                    juiceTransferredByValve.Add(valveUsageKeyValue.Key, 0);
                }
                juiceTransferredByValve[valveUsageKeyValue.Key] += valveUsageKeyValue.Value.juiceTransferred;
            }
        }
        return juiceTransferredByValve;
    }

    public Dictionary<int, float> GetRecentTotalJuiceIntoTanks()
    {
        Dictionary<int, Dictionary<int, TankUsageStamp>> juiceAddedToTanksByIndexHistoryAtRecentMinuteStamps = mainScript.juiceAddedToTanksByIndexHistoryAtRecentMinuteStamps;
        Dictionary<int, float> totalJuiceInputs = new Dictionary<int, float>();

        foreach (var tankUsageAtStampKeyVal in juiceAddedToTanksByIndexHistoryAtRecentMinuteStamps)
        {
            foreach (var tankItemKeyVal in tankUsageAtStampKeyVal.Value)
            {
                if (!totalJuiceInputs.ContainsKey(tankItemKeyVal.Key))
                {
                    totalJuiceInputs.Add(tankItemKeyVal.Key, 0);
                }
                totalJuiceInputs[tankItemKeyVal.Key] += tankItemKeyVal.Value.totalJuiceInput;
            }
        }
        return totalJuiceInputs;
    }

    public HashSet<int> TanksWithHighJuiceInputByIndex()
    {
        HashSet<int> output = new HashSet<int>();

        Dictionary<int, float> totalJuiceInputsToTanks = GetRecentTotalJuiceIntoTanks();

        foreach (var totalJuiceInputOneTankKeyValuePair in totalJuiceInputsToTanks)
        {
            if (totalJuiceInputOneTankKeyValuePair.Value > Common.recentTotalJuiceInputToTankThreshold)
            {

                output.Add(totalJuiceInputOneTankKeyValuePair.Key);
            }
        }
        return output;
    }

    public HashSet<int> ValvesWithHighJuiceTransferredByIndex()
    {
        HashSet<int> output = new HashSet<int>();

        Dictionary<int, float> totalJuiceTransferredPerValve = GetRecentTotalJuiceTransferredPerValve();

        foreach (var totalJuiceTransferredOneValveKeyValuePair in totalJuiceTransferredPerValve)
        {
            if (totalJuiceTransferredOneValveKeyValuePair.Value > Common.recentTotalJuiceTransferredByValveThreshold)
            {

                output.Add(totalJuiceTransferredOneValveKeyValuePair.Key);
            }
        }
        return output;
    }


    public void AddNewJuiceConsumptionToMinTimeStamp(Dictionary<string, float> newJuiceConsumption, int minTimeStamp)
    {
        Dictionary<int, Dictionary<string, float>> juicesConsumedAtRecentMinuteStamps = mainScript.juicesConsumedAtRecentMinuteStamps;
        foreach (var juiceConsumptionItem in newJuiceConsumption)
        {
            string juiceName = juiceConsumptionItem.Key;
            float juiceVolume = juiceConsumptionItem.Value;
            if (!juicesConsumedAtRecentMinuteStamps.ContainsKey(minTimeStamp))
            {
                juicesConsumedAtRecentMinuteStamps.Add(minTimeStamp, new Dictionary<string, float>());
            }
            if (!juicesConsumedAtRecentMinuteStamps[minTimeStamp].ContainsKey(juiceName))
            {
                juicesConsumedAtRecentMinuteStamps[minTimeStamp][juiceName] = 0;
            }
            juicesConsumedAtRecentMinuteStamps[minTimeStamp][juiceName] += juiceVolume;
        }
    }

    public void AddNewValveHistoryAtMinTimeStamp(ValveDataStampPeriod newValveUsage, int minTimeStamp, int valveIndex)
    {
        Dictionary<int, Dictionary<int, ValveDataStampPeriod>> valvesUsageHistoryAtRecentMinuteStamps = mainScript.valvesUsageHistoryAtRecentMinuteStamps;

        if (!valvesUsageHistoryAtRecentMinuteStamps.ContainsKey(minTimeStamp))
        {
            valvesUsageHistoryAtRecentMinuteStamps.Add(minTimeStamp, new Dictionary<int, ValveDataStampPeriod>());
        }
        if (!valvesUsageHistoryAtRecentMinuteStamps[minTimeStamp].ContainsKey(valveIndex))
        {
            valvesUsageHistoryAtRecentMinuteStamps[minTimeStamp][valveIndex] = new ValveDataStampPeriod();
        }
        valvesUsageHistoryAtRecentMinuteStamps[minTimeStamp][valveIndex].juiceTransferred += newValveUsage.juiceTransferred;
    }

    public void AddNewTankUsageStamp(TankUsageStamp newTankUsage, int minTimeStamp, int tankIndex)
    {
        Dictionary<int, Dictionary<int, TankUsageStamp>> juiceAddedToTanksByIndexHistoryAtRecentMinuteStamps = mainScript.juiceAddedToTanksByIndexHistoryAtRecentMinuteStamps;

        if (!juiceAddedToTanksByIndexHistoryAtRecentMinuteStamps.ContainsKey(minTimeStamp))
        {
            juiceAddedToTanksByIndexHistoryAtRecentMinuteStamps.Add(minTimeStamp, new Dictionary<int, TankUsageStamp>());
        }
        if (!juiceAddedToTanksByIndexHistoryAtRecentMinuteStamps[minTimeStamp].ContainsKey(tankIndex))
        {
            juiceAddedToTanksByIndexHistoryAtRecentMinuteStamps[minTimeStamp][tankIndex] = new TankUsageStamp();
        }
        juiceAddedToTanksByIndexHistoryAtRecentMinuteStamps[minTimeStamp][tankIndex].totalJuiceInput += newTankUsage.totalJuiceInput;
    }

    public void AddNewOutletTruckLastTimeFilled(int tankIndex, int secondTimeStamp)
    {
        Dictionary<int, int> outletTruckLastTimeFilled = mainScript.outletTruckLastTimeFilled;

        if (!outletTruckLastTimeFilled.ContainsKey(tankIndex))
        {
            outletTruckLastTimeFilled.Add(tankIndex, 0);
        }
        outletTruckLastTimeFilled[tankIndex] = secondTimeStamp;
    }

    public bool CheckTankAlreadyUpgraded(int tankIndex)
    {
        TankData thisTank = mainScript.tankData[tankIndex];
        if (thisTank.upgradeRequested == true)
        {
            return true;
        }
        return false;
    }

    public bool CheckValveAlreadyUpgraded(int valveIndex)
    {
        Valve thisValve = mainScript.valveData[valveIndex];
        if (thisValve.upgradeRequested == true)
        {
            return true;
        }
        return false;
    }


    public bool ValvesOrTanksLackUpgrades()
    {
        if (ValvesLackUpgradesByIndex().Count > 0 || TanksLackUpgradesByIndex().Count > 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public HashSet<int> ValvesLackUpgradesByIndex()
    {
        HashSet<int> valvesWithHighJuiceTransfer = ValvesWithHighJuiceTransferredByIndex();
        HashSet<int> output = new HashSet<int>();

        foreach (var valveIndex in valvesWithHighJuiceTransfer)
        {
            if (!CheckValveAlreadyUpgraded(valveIndex))
            {

                output.Add(valveIndex);

            }
        }

        return output;
    }

    public HashSet<int> TanksLackUpgradesByIndex()
    {
        HashSet<int> tanksWithHighJuiceInput = TanksWithHighJuiceInputByIndex();
        HashSet<int> output = new HashSet<int>();
        foreach (var tankIndex in tanksWithHighJuiceInput)
        {
            if (!CheckTankAlreadyUpgraded(tankIndex))
            {
                output.Add(tankIndex);
            }
        }

        return output;
    }


}
