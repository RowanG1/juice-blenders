﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable] 
public class TankData {


	public int tankIndex;
	public string tankName;
	public float tankHeight;
	public float tankWidth;
	public float leftx;
	public float topy;
	public float rightx;
	public float bottomy;
	public float centerx;
	public float centery;
	public float tankLevelFrac;
	public float tankCapacity;
	public float chartBackgroundWidth;
	public float chartBackgroundHeight;
	public List<TankJuiceData> tankJuiceData = new List<TankJuiceData>();

	public bool inSpec;
	public int deliverTruckState;
	public int numPipesFromTankAdded = 0;
	public bool upgradeRequested;

	public void RecalcParams() {
		this.leftx = this.centerx - this.tankWidth/2;
		this.topy = this.centery - this.tankHeight/2;
		this.rightx = this.centerx + this.tankWidth/2;
		this.bottomy = this.centery + this.tankHeight/2;
		this.chartBackgroundWidth = this.tankWidth - 30;
		this.chartBackgroundHeight = this.tankHeight * 0.5f;

	}
}

