﻿using UnityEngine;
using System.Collections;


[System.Serializable] 
public class TankJuiceData {
	public float targetFrac;
	public float actualFrac;
	public string juiceName;

	public TankJuiceData() {
		this.targetFrac = 0;
		this.actualFrac = 0;
		this.juiceName = "";
	}


}


