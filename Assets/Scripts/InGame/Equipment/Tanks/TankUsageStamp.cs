﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class TankUsageStamp {

public float totalJuiceInput;

public TankUsageStamp(float totalJuiceInput) {
	this.totalJuiceInput = totalJuiceInput;
}

public TankUsageStamp() {
	
}

}
