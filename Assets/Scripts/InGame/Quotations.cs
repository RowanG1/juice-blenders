using System.Collections;
using System.Collections.Generic;

public class Quotations {
	MainScript mainScript;
	List<string> quotations = new List<string>();

	public Quotations() {
		this.mainScript = MainScript.currentMainScript;
		CreateQuotations();
	}

	public void CreateQuotations() {
		quotations.Add("Failures- think and never did, or do and never think.");
		quotations.Add("The best way to predict the future is to invent it.");
		quotations.Add("Without customers in mind, you don’t have a business.  You have a hobby.");
		quotations.Add("Better to try and fail, than to fail to try.");
		quotations.Add("The minute you’re satisfied with where you are, you aren’t there anymore.");
		quotations.Add("Luck is what happens when preparation meets opportunity.");
		quotations.Add("The first problem for us all, men and women, is not to learn, but to unlearn.");
		quotations.Add("Not the strongest that survives, nor most intelligent, but the most responsive to change.");
		quotations.Add("Many failures did not realize how close they were to success when they gave up.");
		quotations.Add("I can give you the formula for failure: try to please everybody.");
		quotations.Add("When cost is most important, you’ve already lost");
		quotations.Add("Take the experience first; the cash will come later.");
		quotations.Add("Whether you think you can or whether you think you can’t, you’re right!");
		quotations.Add("Whenever you find yourself on the side of the majority, it is time to pause and reflect.");
		quotations.Add("Genius is 1% inspiration and 99% perspiration.");
		quotations.Add("Capital isn't that important. Experience isn't that important. What is important is ideas.");
		quotations.Add("Your reputation is more important than your paycheck.");
		quotations.Add("If you can’t feed a team with two pizzas, it’s too large.");
		quotations.Add("Make every detail perfect and limit the number of details to perfect.");
		quotations.Add("Nothing works better than just improving your product.");
		quotations.Add("Timing, perseverance, and ten years of trying will make you look like an overnight success.");
		quotations.Add("Anything that is measured and watched, improves.");
		quotations.Add("Data beats emotions.");
		quotations.Add("If you’re doing business solo, you’ll lose out to a team.");
		quotations.Add("The way to get started is to quit talking and begin doing.");
		quotations.Add("Diligence is the mother of good luck.");
		quotations.Add("Always deliver more than expected.");
		quotations.Add("Pay attention to what people need and what has not been done.");
	}

	public List<string> GetQuotes() {
		return quotations;
	}

	public int GetRandomQuoteIndexNoRepeatOnLastFiveQuotes(int quotationsSetCount) {
		while (true)
		{
			int randomQuoteIndex = UnityEngine.Random.Range(0, quotationsSetCount);
			List<int>lastFiveQuoteIndexes = new List<int>();
			if (mainScript.quoteIndexesHistory.Count >= 5) {
				lastFiveQuoteIndexes = mainScript.quoteIndexesHistory.GetRange(mainScript.quoteIndexesHistory.Count-1-4, 5);
			} else 
			{
				lastFiveQuoteIndexes = mainScript.quoteIndexesHistory;
			}
			if (lastFiveQuoteIndexes.Contains(randomQuoteIndex)) {
			continue;
			} else 
			{
				return randomQuoteIndex;
			}
		}
	}

	public void GetAndNotifyRandomQuoteNoRepeatOnLastFiveQuotes() {
		PostNewRandomQuote(GetRandomQuoteIndexNoRepeatOnLastFiveQuotes(quotations.Count));
	}

	public void PostNewRandomQuote(int quoteIndex) {
		mainScript.quoteIndexesHistory.Add(quoteIndex);
		mainScript.mainNotificationsToAnimate.Add(new MainAnimatedNotification("Saying: " + mainScript.quotations[quoteIndex]));
	}
}
