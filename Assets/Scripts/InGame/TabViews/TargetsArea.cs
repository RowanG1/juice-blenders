﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TargetsArea : MonoBehaviour
{
    public Canvas canvas;
    public GameObject targetRowPrefab;
    public GameObject targetsPanel;
    public GameObject targetsContent;
    public GameObject badgeTargetsPanel;
    public GameObject customerServiceCreditText;
    public GameObject lowWasteCreditText;
    public GameObject profitabilityCreditText;
    public Sprite checkMark;
    public Sprite notDoneMark;


    Color noBadgeCreditBckGndColor = new Color(255f / 255, 102f / 255, 102f / 255, 133f / 255);
    Color badgeCreditBckGndColor = new Color(58f / 255, 219f / 255, 166f / 255, 133f / 255);
    MainScript mainScript;
    int sumOfAllRowHeightsInTargetsTable;

    float lastTimeUpdated = 0;

    List<Text> textItems = new List<Text>();
    public SetAllFontSizesToSmallestInGroup setAllFontSizesToSmallestInGroup;


    // Use this for initialization
    void Start()
    {
        int minRowHeightInTargetTable = 10;
        mainScript = MainScript.currentMainScript;
        foreach (Transform child in targetsContent.transform)
        {
            GameObject.Destroy(child.gameObject);
        }

        RectTransform rectTransform = transform.GetComponent<RectTransform>();
        rectTransform.offsetMin = new Vector2(rectTransform.offsetMin.x, Common.tabStripHeight);

        Level thisLevel = Level.GetLevelObj();
        List<LevelSingleCondition> levelConditions = thisLevel.GetLevelConditions();

        int counter = 0;

        textItems.Clear();
        foreach (LevelSingleCondition condition in levelConditions)
        {
            GameObject thistargetRowInst = Instantiate(targetRowPrefab) as GameObject;
            thistargetRowInst.transform.SetParent(targetsContent.transform, false);
            thistargetRowInst.transform.Find("SingleTargetDescriptionPanel/SingleTargetDescription").GetComponent<Text>().text = condition.GetDescription();
            int thisRowPreferredHeight = (minRowHeightInTargetTable - 2) * condition.GetDescription().Length / 30;
            thistargetRowInst.GetComponent<LayoutElement>().preferredHeight = thisRowPreferredHeight;
            thistargetRowInst.GetComponent<LayoutElement>().minHeight = minRowHeightInTargetTable;
            sumOfAllRowHeightsInTargetsTable += Mathf.Max(minRowHeightInTargetTable, thisRowPreferredHeight);
            thistargetRowInst.transform.Find("CurrentValPanel/CurrentValueText").GetComponent<Text>().text = condition.CurrentValue().ToString("#,###");
            thistargetRowInst.transform.Find("CheckedImage").GetComponent<Image>().sprite = notDoneMark;
            thistargetRowInst.name = "Target " + (counter + 1).ToString();

            foreach (Text targetTextItem in thistargetRowInst.GetComponentsInChildren<Text>())
            {
                textItems.Add(targetTextItem);
            }
            counter++;
        }

        setAllFontSizesToSmallestInGroup.SetTextItems(textItems);
        SetTargetContentPort();

    }

    void SetTargetContentPort()
    {
        RectTransform targetsContentRT = targetsContent.GetComponent<RectTransform>();
        targetsContentRT.sizeDelta = new Vector2(0, sumOfAllRowHeightsInTargetsTable + 2);
    }

    // Update is called once per frame

    void Update()
    {

        if (MainScript.currentMainScript.elapsedTime > (lastTimeUpdated + 2.0f))
        {
            Level thisLevel = Level.GetLevelObj();
            List<LevelSingleCondition> levelConditions = thisLevel.GetLevelConditions();

            int counter = 0;
            foreach (Transform child in targetsContent.transform)
            {
                LevelSingleCondition condition = levelConditions[counter];
                if (condition.ConditionMet())
                {
                    child.transform.GetComponent<SingleLevelTargetUI>().checkedImage.GetComponent<Image>().sprite = checkMark;
                }
                else
                {
                    child.transform.GetComponent<SingleLevelTargetUI>().checkedImage.GetComponent<Image>().sprite = notDoneMark;
                }
                float currentValue = condition.CurrentValue();
                if (currentValue == 0)
                {
                    child.transform.GetComponent<SingleLevelTargetUI>().currentValText.GetComponent<Text>().text = "0";
                }
                else
                {
                    child.transform.GetComponent<SingleLevelTargetUI>().currentValText.GetComponent<Text>().text = condition.CurrentValue().ToString("#,###") + condition.GetUnit();
                }
                counter++;
            }

            UpdateBadgeTargets();
            lastTimeUpdated = MainScript.currentMainScript.elapsedTime;
        }
    }

    void UpdateBadgeTargets()
    {
        UpdateCustomerServicePanel();
        UpdateLowJuiceWastePanel();
        UpdateProfitabilityPanel();
    }

    public void UpdateCustomerServicePanel()
    {

        if (new BadgesCredits().GetCreditsForCustomerServiceOnLevel(mainScript.orders) > 0)
        {
            badgeTargetsPanel.transform.Find("CustomerService").GetComponent<Image>().color = badgeCreditBckGndColor;
        }
        else
        {
            badgeTargetsPanel.transform.Find("CustomerService").GetComponent<Image>().color = noBadgeCreditBckGndColor;
        }
        int percentCustomerService = new BadgesCredits().GetCurrentInGamePercenCustomerService(mainScript.orders);
        setCurrentVsTargetText(customerServiceCreditText, percentCustomerService + "%", "Target >= " + Constants.Badges.minimumCustomerServicePercenForCredit + "%");
    }

    public void UpdateLowJuiceWastePanel()
    {
        float percenJuiceWaste = 0;
        string currentJuiceWastage;

        percenJuiceWaste = PercenJuiceWasted(MainScript.currentMainScript.volJuiceWasted, MainScript.currentMainScript.volJuiceDelivered);

        if (percenJuiceWaste < Constants.Badges.juiceWastagePercenThreshold)
        {
            badgeTargetsPanel.transform.Find("LowWaste").GetComponent<Image>().color = badgeCreditBckGndColor;
        }
        else
        {
            badgeTargetsPanel.transform.Find("LowWaste").GetComponent<Image>().color = noBadgeCreditBckGndColor;
        }
        if (percenJuiceWaste > 100)
        {
            currentJuiceWastage = ">100%";
        }
        else
        {
            currentJuiceWastage = (int)percenJuiceWaste + "%";
        }
        setCurrentVsTargetText(lowWasteCreditText, currentJuiceWastage, "Target < " + Constants.Badges.juiceWastagePercenThreshold + "%");
    }

    public static float PercenJuiceWasted(float juiceWasted, float juiceDelivered)
    {
        float percenJuiceWaste = 0;

        if (juiceWasted > 0 && juiceDelivered == 0)
        {
            percenJuiceWaste = 100;
        }
        if (juiceWasted > 0 && juiceDelivered > 0)
        {
            percenJuiceWaste = juiceWasted / juiceDelivered * 100;
        }
        return percenJuiceWaste;
    }

    public void UpdateProfitabilityPanel()
    {
        if (mainScript.profit > 0)
        {
            badgeTargetsPanel.transform.Find("Profit").GetComponent<Image>().color = badgeCreditBckGndColor;
        }
        else
        {
            badgeTargetsPanel.transform.Find("Profit").GetComponent<Image>().color = noBadgeCreditBckGndColor;
        }
        setCurrentVsTargetText(profitabilityCreditText, mainScript.profit.ToString("#,##0"), "Target > " + Constants.Badges.minimumProfitForCredit);
    }

    void setCurrentVsTargetText(GameObject text, string currentText, string targetText)
    {
        text.GetComponent<Text>().text = "<color=blue>" + currentText + "</color>. <color=black>" + targetText + "</color>";
    }
}
