﻿
using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;


public class MarketingAreaScript : MonoBehaviour
{
    public Canvas canvas;
    public GameObject maxMarketingSpendAllowed;
    public GameObject minMarketingSpendAllowed;
    public GameObject marketingSpendSlider;

    public GameObject currentMarketingSpendText;

    float factorAllowableMarketingSpend = 0.4f;

    public GameObject payBtn;
    public GameObject payMessage;

    float lastBankBalance;
    public float lastTimeUpdated = 0;
    public float lastTimeBankingUpdated = 0;

    float lastTimePayMessageUpdated;
    public GameObject adLevelBar;
    public GameObject brandLevelBar;
    public GameObject brandPanel;
    public GameObject adPanel;
    public GameObject brandStrengthText;
    public GameObject adLevelText;

    public GameObject marketingSpendAmountlabel;
    public GameObject marketToggleLabel;
    public GameObject rotatedAdLabelOnToggle;
    public GameObject rotatedBrandLabelOnToggle;
    public GameObject toggleButton;

    public GameObject IndicatorLabel;
    public GameObject pointerBrand;
    public GameObject pointerAds;
    public GameObject pointerAnimated;
    public GameObject pointerToSpend;

    public static float brandLevelPercen;
    public static float adLevelPercen;

    public GameObject needleSound;

    enum PayOptions { Branding, Advertizing };

    int marketingOrAdvertLastToggled = 1;
    bool alreadyStarted = false;

    MainScript mainScript;

    void OnEnable()
    {
        setValuesAssociatedSlider();
        StartCoroutine(AnimateToggleBtnColor());
        if (alreadyStarted == true)
        {
            UpdateIndicatorBarValues();
            UpdateIndicatorBarsUI();
            SetTogglePositionToBrandOrAds();
        }
    }

    void SetTogglePositionToBrandOrAds() {
        float finalValue = 0;
        float pointerAmplitude = Mathf.Abs(pointerBrand.transform.eulerAngles.z);
         switch (marketingOrAdvertLastToggled)
        {
            case (int)PayOptions.Branding:
            finalValue = pointerAmplitude;
            break;
            case (int)PayOptions.Advertizing:
            finalValue = -pointerAmplitude;
            break;
            default:
            break;
        }
            pointerAnimated.transform.eulerAngles = new Vector3(0, 0, finalValue);
    }
    void setValuesAssociatedSlider()
    {
        float allowableSpend = AllowableSpend();
        maxMarketingSpendAllowed.transform.GetComponent<Text>().text = allowableSpend.ToString("#,##0");
        //marketingSpendSlider.transform.GetComponent <Slider> ().value = 0;
        lastBankBalance = MainScript.currentMainScript.bankBalance;
        SetMarketingSlider();
    }
    IEnumerator AnimateToggleBtnColor()
    {
        for (int i = 0; i < 2; i++)
        {
            transform.Find("ToggleButton").GetComponent<Image>().color = new Color(93f / 255f, 205f / 234f, 248f / 255f);
            yield return new WaitForSeconds(0.5f);
            transform.Find("ToggleButton").GetComponent<Image>().color = new Color(51f / 255f, 204f / 255f, 51f / 255f);
            yield return new WaitForSeconds(0.5f);
        }
        transform.Find("ToggleButton").GetComponent<Image>().color = new Color(93f / 255f, 205f / 234f, 248f / 255f);
    }
    float AllowableSpend()
    {
        return Mathf.Round(MainScript.currentMainScript.bankBalance - Common.minimumBankLimit) * factorAllowableMarketingSpend;
    }
    void Start()
    {
        alreadyStarted = true;
        payBtn.GetComponent<Button>().onClick.AddListener(() => OnPayClick());

        ToggleIndicators();
        setRotatedTextDimensionsOnToggle();

        Rect oldHandleRect = marketingSpendSlider.transform.Find("Handle Slide Area/Handle").GetComponent<RectTransform>().rect;
        marketingSpendSlider.transform.Find("Handle Slide Area/Handle").GetComponent<RectTransform>().sizeDelta = new Vector2(oldHandleRect.height, 0);

        RectTransform rectTransform = transform.GetComponent<RectTransform>();
        rectTransform.offsetMin = new Vector2(rectTransform.offsetMin.x, Common.tabStripHeight);

        mainScript = MainScript.currentMainScript;

        marketingSpendSlider.GetComponent<Slider>().onValueChanged.AddListener(delegate { marketSpendValueChangedHandle(); });

        payMessage.GetComponent<Text>().text = "";

        float allowableSpend = AllowableSpend();
        maxMarketingSpendAllowed.transform.GetComponent<Text>().text = allowableSpend.ToString("#,##0");
        SetMarketingSlider();
        UpdateIndicatorBarValues();
        UpdateIndicatorBarsUI();

    }

    void setRotatedTextDimensionsOnToggle()
    {
        rotatedAdLabelOnToggle.GetComponent<RectTransform>().sizeDelta = new Vector2(toggleButton.GetComponent<RectTransform>().rect.height, rotatedAdLabelOnToggle.GetComponent<RectTransform>().rect.height);
        rotatedBrandLabelOnToggle.GetComponent<RectTransform>().sizeDelta = new Vector2(toggleButton.GetComponent<RectTransform>().rect.height, rotatedBrandLabelOnToggle.GetComponent<RectTransform>().rect.height);

    }
    public void ToggleIndicators()
    {
        //int selectedIndex = marketingDropDown.GetComponent<Dropdown> ().value;

        StopAllCoroutines();
        //int newSelection;
        marketingOrAdvertLastToggled = marketingOrAdvertLastToggled == 1 ? 0 : 1;

        switch (marketingOrAdvertLastToggled)
        {
            case (int)PayOptions.Branding:
                brandPanel.transform.GetComponent<Image>().color = new Color(233f / 255f, 154f / 255f, 246f / 255f);
                adPanel.transform.GetComponent<Image>().color = new Color(239f / 255f, 219f / 255f, 242f / 255f);

                marketingSpendAmountlabel.GetComponent<Text>().text = "Brand spend amount";
                break;
            case (int)PayOptions.Advertizing:
                adPanel.transform.GetComponent<Image>().color = new Color(233f / 255f, 154f / 255f, 246f / 255f);
                brandPanel.transform.GetComponent<Image>().color = new Color(239f / 255f, 219f / 255f, 242f / 255f);

                marketingSpendAmountlabel.GetComponent<Text>().text = "Ad spend amount";
                break;
            default:
                break;
        }
        StartCoroutine(AnimateToggle());
        StartCoroutine(FlashArrowToSpend());
        PlayNeedleSound();
    }

    void marketSpendValueChangedHandle()
    {
        SetPayMessage("");
        SetMarketingSlider();
    }
    void SetPayMessage(string message)
    {
        payMessage.GetComponent<Text>().text = message;
        lastTimePayMessageUpdated = MainScript.currentMainScript.elapsedTime;
    }
    IEnumerator AnimateToggle()
    {
        float duration = 1.0f;
        float startValue;
        float finalValue;
        float pointerAmplitude = pointerBrand.transform.eulerAngles.z;
        if (marketingOrAdvertLastToggled == 0)
        {
            finalValue = pointerAmplitude;
        }
        else
        {
            finalValue = -pointerAmplitude;
        }
        startValue = pointerAnimated.transform.eulerAngles.z;
        if (startValue > 180)
        {
            startValue = -(360 - startValue);
        }

        float change = finalValue - startValue;
        float accumulatedTime = 0;
        while (accumulatedTime < duration)
        {
            accumulatedTime += Time.deltaTime;
            float easeOutCurrentVal = EaseOut(accumulatedTime, startValue, change, duration);
            pointerAnimated.transform.eulerAngles = new Vector3(0, 0, easeOutCurrentVal);
            yield return 0;
        }
    }
    float EaseOut(float time, float startValue, float change, float duration)
    {
        time /= duration / 2;
        if (time < 1)
        {
            return change / 2 * time * time + startValue;
        }

        time--;
        return -change / 2 * (time * (time - 2) - 1) + startValue;
    }
    IEnumerator FlashArrowToSpend()
    {
        for (int i = 0; i < 2; i++)
        {
            pointerToSpend.SetActive(true);
            yield return new WaitForSeconds(0.8f);
            pointerToSpend.SetActive(false);
            yield return new WaitForSeconds(0.6f);
        }
        pointerToSpend.SetActive(true);
    }


    void PlayNeedleSound()
    {
        AudioSource audio = needleSound.GetComponent<AudioSource>();
        audio.Play();
    }


    void SetMarketingSlider()
    {
        float marketSliderVal = marketingSpendSlider.transform.GetComponent<Slider>().value;
        float maxMarketSpendFloat = float.Parse(maxMarketingSpendAllowed.transform.GetComponent<Text>().text, System.Globalization.CultureInfo.InvariantCulture);
        float roundedNewMarketSpend = Mathf.Round(marketSliderVal * maxMarketSpendFloat);
        currentMarketingSpendText.transform.GetComponent<Text>().text = roundedNewMarketSpend.ToString("#,##0");
    }
    void UpdateIndicatorBarsUI()
    {
        //  int markIndicBars = this.getResources().getInteger(R.integer.marketIndicBars);
        adLevelBar.GetComponent<RectTransform>().anchorMax = new Vector2(adLevelPercen, 1);
        if (adLevelPercen < 0.33f)
        {
            adLevelBar.GetComponent<Image>().color = Color.red;
        }
        else if (adLevelPercen >= 0.33f && adLevelPercen < 0.66f)
        {
            adLevelBar.GetComponent<Image>().color = new Color(255f / 255, 165f / 255, 0);
        }
        else
        {
            adLevelBar.GetComponent<Image>().color = Color.green;
        }

        if (brandLevelPercen < 0.33f)
        {
            brandLevelBar.GetComponent<Image>().color = Color.red;
        }
        else if (brandLevelPercen >= 0.33f && brandLevelPercen < 0.66f)
        {
            brandLevelBar.GetComponent<Image>().color = new Color(255f / 255, 165f / 255, 0);
        }
        else
        {
            brandLevelBar.GetComponent<Image>().color = Color.green;
        }

        brandLevelBar.GetComponent<RectTransform>().anchorMax = new Vector2(brandLevelPercen, 1);
    }


    void OnPayClick()
    {
        switch (marketingOrAdvertLastToggled)
        {
            case (int)PayOptions.Branding:
          
                BrandPayRequest();
                break;
            case (int)PayOptions.Advertizing:
        
                AdPayRequest();
                break;
            default:
                break;
        }
    }

    void AdPayRequest()
    {
        float adSliderVal = marketingSpendSlider.transform.GetComponent<Slider>().value;

        int currentSpendFloat = (int)float.Parse(currentMarketingSpendText.transform.GetComponent<Text>().text, System.Globalization.CultureInfo.InvariantCulture);

//        print("Ad requested to spend is " + currentSpendFloat);
        if (currentSpendFloat > 0)
        {
            if (currentSpendFloat < (mainScript.bankBalance - Common.minimumBankLimit))
            {

                mainScript.pendingAdvertsTotal += currentSpendFloat;
                mainScript.bankBalance -= currentSpendFloat;
                mainScript.profit -= currentSpendFloat;

                setValuesAssociatedSlider();

                int randomTimeToUpdateCallVolumes = (Random.Range(0, 20) + 10);
                DelayAction adDelayAction = new DelayAction();
                adDelayAction.delayRemain = randomTimeToUpdateCallVolumes;
                adDelayAction.actionName = Constants.Delays.adDelay;
                adDelayAction.adSpendAmount = currentSpendFloat;

                mainScript.lastTimeSpentAds = mainScript.elapsedTime;

                mainScript.delayActions.Add(adDelayAction);

                MakeMarketingSliderZero();
                SetPayMessage("Ad in process");
            }
            else
            {
                SetPayMessage("Sorry, insufficient bank funds");
            }
        }
        else
        {
            SetPayMessage("Value must be greater than zero");
        }
    }

    void BrandPayRequest()
    {

        float brandSliderVal = marketingSpendSlider.transform.GetComponent<Slider>().value;
        int currentSpendFloat = (int)float.Parse(currentMarketingSpendText.transform.GetComponent<Text>().text, System.Globalization.CultureInfo.InvariantCulture);

        print("Brand now spent is " + currentSpendFloat);
        if (currentSpendFloat > 0)
        {
            if (currentSpendFloat < (mainScript.bankBalance - Common.minimumBankLimit))
            {

                mainScript.branding += currentSpendFloat;
                mainScript.bankBalance -= currentSpendFloat;
                mainScript.profit -= currentSpendFloat;

                setValuesAssociatedSlider();

                int randomTimeToUpdatePricing = (int)Random.Range(0, 30) + 30;

                DelayAction brandDelayAction = new DelayAction();
                brandDelayAction.delayRemain = randomTimeToUpdatePricing;
                brandDelayAction.actionName = Constants.Delays.brandDelay;
                brandDelayAction.brandSpendAmount = currentSpendFloat;
                mainScript.delayActions.Add(brandDelayAction);

                mainScript.lastTimeSpentBrand = mainScript.elapsedTime;
                MakeMarketingSliderZero();
                SetPayMessage("Branding in process");
            }
            else
            {
                SetPayMessage("Sorry, insufficient bank funds");
            }
        }
        else
        {
            SetPayMessage("Value must be greater than zero");
        }
    }

    void MakeMarketingSliderZero()
    {
        marketingSpendSlider.GetComponent<Slider>().value = 0;
        SetMarketingSlider();
    }


    public static void UpdateIndicatorBarValues()
    {
        MainScript mainScript = MainScript.currentMainScript;
        float adLevelDeterminant = OrderFuncs.NetAdvertising(advertising: mainScript.advertising,goodwill:mainScript.goodwill, adLeakage:mainScript.adLeakage) / Constants.Orders.OrderDeterminantFactorPerAdSpend + Constants.Orders.minOrderLevel;
        if (adLevelDeterminant > OrderFuncs.newOrdersPercentChanceBaselinePerOrderLoop)
        {
            adLevelDeterminant = OrderFuncs.newOrdersPercentChanceBaselinePerOrderLoop;
        }

        float callVolFrac = (adLevelDeterminant - Constants.Orders.minOrderLevel) / (OrderFuncs.newOrdersPercentChanceBaselinePerOrderLoop - Constants.Orders.minOrderLevel);
        if (callVolFrac < 0.05f)
        {
            callVolFrac = 0.05f;
        }
        adLevelPercen = callVolFrac;
        int countGap = 0;
        float sumPercenGap = 0;
        for (int j = 0; j < mainScript.productData.Count; j++)
        {
            ProductData thisProduct = mainScript.productData[j];
            float thisProductPrice = thisProduct.price;
            float thisProductPriceCap = thisProduct.priceCap;
            float thisProductPriceMin = thisProduct.priceMin;
            sumPercenGap += (thisProductPrice - thisProductPriceMin) / (thisProductPriceCap - thisProductPriceMin) * 100;
            countGap++;
        }
        float aveBrandStrength = (sumPercenGap / (countGap)) / 100;
        //print("Ave brand strength is: " + aveBrandStrength);
        if (aveBrandStrength < 0.05f)
        {
            aveBrandStrength = 0.05f;
        }
        brandLevelPercen = aveBrandStrength;
      //  Debug.Log("Brand level percent is: " + brandLevelPercen);
     //   Debug.Log("Ad level percent is: " + adLevelPercen);
    }

    void Update()
    {

        float bankBalance = mainScript.bankBalance;
        //		int minimumBankBalance = Common.minimumBankLimit;

        if (MainScript.currentMainScript.elapsedTime > (lastTimeUpdated + 1.0f))
        {
            lastTimeUpdated = MainScript.currentMainScript.elapsedTime;
            UpdateIndicatorBarValues();
            UpdateIndicatorBarsUI();

        }

        if (MainScript.currentMainScript.elapsedTime > (lastTimeBankingUpdated + 10.0f))
        {
            if (lastBankBalance != bankBalance)
            {

                setValuesAssociatedSlider();
            }
            lastTimeBankingUpdated = MainScript.currentMainScript.elapsedTime;
        }

        if ((MainScript.currentMainScript.elapsedTime - lastTimePayMessageUpdated) > 2 && payMessage.GetComponent<Text>().text != "")
        {
            SetPayMessage("");
        }

    }



}



