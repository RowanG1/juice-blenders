﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MenuBtnScript : MonoBehaviour
{
    public string btnName;

    // Use this for initialization
    void Start()
    {
        Button thisBut = (transform.gameObject.GetComponent<Button>());
        thisBut.onClick.AddListener(() => OnBtnClick());
        //	MenuBtnFuncs.GoToMenu("Operations");
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void OnBtnClick()
    {
        //tabStripScript.menuStartedBeingTouched = true;
        //		ColorBlock colors;
        MenuBtnFuncs.GoToMenu(this.btnName);
        MainScript.currentMainScript.uIUpdatesInGame.destroyAllEquipmentUpgradeDots();
        Common.PlayButtonSound();
    }


}

public static class MenuBtnFuncs
{

    public static void SetAllUIAreasInActive()
    {
        //print ("Trying to set all aeras inactive");
        MainScript mainScript = MainScript.currentMainScript;
        mainScript.operations.SetActive(false);
        mainScript.ordersArea.SetActive(false);
        mainScript.financeArea.SetActive(false);
        mainScript.deliveryArea.SetActive(false);
        mainScript.marketingArea.SetActive(false);
        mainScript.procurementArea.SetActive(false);
        mainScript.equipmentArea.SetActive(false);
        mainScript.targetsArea.SetActive(false);
        mainScript.timeLeftPanel.SetActive(false);
        mainScript.exitArea.SetActive(false);

        mainScript.SetFlashingIconActiveState(MainScript.currentMainScript.indicatorPanel, false);
        mainScript.uIUpdatesInGame.equipmentUpgradeMsgPanel.SetActive(false);

    }

    public static void SetAllBtnColorDefault()
    {
        Color color = new Color(249f / 255, 223f / 255, 176f / 255, 255f / 255);
        foreach (Transform tabStripBtnTrans in GameObject.Find(Common.tabIconsGameObjectPath).transform)
        {
            SetBtnColor(tabStripBtnTrans.gameObject, color);
        }

        GameObject targetBtn = GameObject.Find("TargetsButton");
        SetBtnColor(targetBtn, color);

    }

    static void SetBtnColor(GameObject button, Color color)
    {
        ColorBlock colors = button.transform.GetComponent<Button>().colors;
        colors.normalColor = color;
        colors.highlightedColor = color;
        colors.pressedColor = color;
        button.GetComponent<Button>().colors = colors;
    }

    public static void ChangeTabTransformColors(Transform thisTabTrans)
    {
        ColorBlock colors = thisTabTrans.GetComponent<Button>().colors;
        thisTabTrans.GetComponent<Button>().colors = ChangeButtonColors(colors);
    }

    public static ColorBlock ChangeButtonColors(ColorBlock colors)
    {
        colors.normalColor = new Color(1, 165f / 255, 0);
        colors.highlightedColor = new Color(1, 165f / 255, 0);
        colors.pressedColor = new Color(1, 165f / 255, 0);
        return colors;
    }

    public static void CommonSwitchingFuncs()
    {
        SetAllUIAreasInActive();
        SetAllBtnColorDefault();
    }

    public static void GoToMenu(string tabName)
    {
        CommonSwitchingFuncs();

        if (tabName != "Operations") {
            MainScript.currentMainScript.uIUpdatesInGame.lastTimeEquipmentDotUpgradesStartedShowing = -1;
        }

        switch (tabName)
        {
            case "Operations":
                //	print ("Operations pressed");
                MainScript.currentMainScript.operations.SetActive(true);
                MainScript.currentMainScript.timeLeftPanel.SetActive(true);
    
                MenuBtnFuncs.ChangeTabTransformColors(GameObject.Find(Common.tabIconsGameObjectPath + "/Operations").transform);
                MainScript.currentMainScript.SetFlashingIconActiveState(MainScript.currentMainScript.indicatorPanel, true);

                break;
            case "Orders":
                //	print ("Order pressed");

                MainScript.currentMainScript.ordersArea.SetActive(true);

                MenuBtnFuncs.ChangeTabTransformColors(GameObject.Find(Common.tabIconsGameObjectPath + "/Orders").transform);
                break;
            case "Deliveries":
                //	print ("Deliveries pressed");
                MainScript.currentMainScript.deliveryArea.SetActive(true);

                MenuBtnFuncs.ChangeTabTransformColors(GameObject.Find(Common.tabIconsGameObjectPath + "/Deliveries").transform);
                break;
            case "Marketing":
                //	print ("Marketing pressed");
                MainScript.currentMainScript.marketingArea.SetActive(true);

                MenuBtnFuncs.ChangeTabTransformColors(GameObject.Find(Common.tabIconsGameObjectPath + "/Marketing").transform);
                break;
            case "Procurement":
                //	print ("Procurement pressed");
                MainScript.currentMainScript.procurementArea.SetActive(true);

                MenuBtnFuncs.ChangeTabTransformColors(GameObject.Find(Common.tabIconsGameObjectPath + "/Procurement").transform);
                break;
            case "Finance":
                //	print ("Finance pressed");
                MainScript.currentMainScript.financeArea.SetActive(true);
                GameObject financeArea = MainScript.currentMainScript.financeArea;
                financeArea.GetComponent<FinanceAreaScript>().UpdateFinance();
                MenuBtnFuncs.ChangeTabTransformColors(GameObject.Find(Common.tabIconsGameObjectPath + "/Finance").transform);
                break;
            case "Equipment":
                //	print ("Equipment pressed");
                MainScript.currentMainScript.equipmentArea.SetActive(true);
                //MainScript.currentMainScript.equipmentArea.GetComponent<EquipmentAreaScript>().indexOfSelectedObjectArrayInOperations = -1;

                MenuBtnFuncs.ChangeTabTransformColors(GameObject.Find(Common.tabIconsGameObjectPath + "/Equipment").transform);
                break;
            case "Targets":
                MainScript.currentMainScript.targetsArea.SetActive(true);
                MenuBtnFuncs.ChangeTabTransformColors(GameObject.Find("TabStrip/TargetsButton").transform);
                MainScript.currentMainScript.timeLeftPanel.SetActive(true);
                break;
            case "Exit":
                //print ("Exit pressed");
                MainScript.currentMainScript.exitArea.SetActive(true);
                MenuBtnFuncs.ChangeTabTransformColors(GameObject.Find(Common.tabIconsGameObjectPath + "/Exit").transform);
                //	SceneManager.LoadScene ("MainMenu");
                break;

        }
    }
}
