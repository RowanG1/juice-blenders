﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using System;
using Random = UnityEngine.Random;

public class ProcurementAreaScript : MonoBehaviour
{
    public Canvas canvas;
    public GameObject procurementPanel;
    public GameObject juiceOptionsPanel;
    public GameObject windowPanel;
    public GameObject redPanel;
    public GameObject redPanelDollarText;
    public GameObject orangePanel;
    public GameObject orangePanelDollarText;
    public GameObject bluePanel;
    public GameObject bluePanelDollarText;
    public GameObject yellowPanel;
    public GameObject yellowPanelDollarText;
    public GameObject starsPrefab;
    public GameObject explainDollar;

    public GameObject maxBidPriceText;
    public GameObject minBidPriceText;
    public GameObject currentBidPriceText;
    public GameObject bidSlider;
    public GameObject bidSlideArea;
    public GameObject bidButton;
    public GameObject messageOnBidClick;
    public GameObject BidDownText;

    Common.JuiceNames lastSelectedJuiceName = Common.JuiceNames.Strawberry;
    RawMaterialData lastSelectedJuiceRawMaterialData;

    float lastBidMessageTime;
    MainScript mainScript;
    List<float> lastPrices = new List<float>();

    System.Random randomSettleSeed = new System.Random();

    void OnEnable()
    {
        StartCoroutine(FlashJuiceSelectWindow());
        StopSparkles(); // In case sparkle particle effect was running while another function area tab was clicked
    }

    IEnumerator FlashJuiceSelectWindow()
    {
        SetJuiceWindowColor(Color.black);
        yield return new WaitForSeconds(0.5f);
        SetJuiceWindowColor(Color.green);
        yield return new WaitForSeconds(0.8f);
        SetJuiceWindowColor(Color.black);
        yield return 0;
    }
    void SetJuiceWindowColor(Color color)
    {
        foreach (Transform windowSide in windowPanel.transform)
        {
            windowSide.GetComponent<Image>().color = color;
        }
    }
    void Start()
    {

        Vector2 oldHandleSlideAreaRect = bidSlider.transform.Find("Handle Slide Area").GetComponent<RectTransform>().sizeDelta;

        Rect oldHandleRect = bidSlider.transform.Find("Handle Slide Area/Handle").GetComponent<RectTransform>().rect;
        bidSlider.transform.Find("Handle Slide Area/Handle").GetComponent<RectTransform>().sizeDelta = new Vector2(oldHandleRect.height, 0);

        RectTransform rectTransform = transform.GetComponent<RectTransform>();
        rectTransform.offsetMin = new Vector2(rectTransform.offsetMin.x, Common.tabStripHeight);
        mainScript = MainScript.currentMainScript;

        for (int j = 0; j < mainScript.rawMaterialData.Count; j++)
        {
            lastPrices.Add(Common.ConvertToTwoDecimal(mainScript.rawMaterialData[j].price));
        }

        messageOnBidClick.GetComponent<Text>().text = "";

        bidSlider.GetComponent<Slider>().onValueChanged.AddListener(delegate
        {
            BidSliderHandler();
        });

        bidButton.GetComponent<Button>().onClick.AddListener(delegate
        {
            BidBtnClickedHandler();
        });

        bidSlider.GetComponent<Slider>().value = 1;
        UpdateSliderValues();
    }


    void Update()
    {
        bool pricesSame = true;
        for (int j = 0; j < mainScript.rawMaterialData.Count; j++)
        {
            if (mainScript.rawMaterialData[j].price != lastPrices[j])
            {
                pricesSame = false;
            }
        }

        if (pricesSame == false)
        {
            ResetLastPrices();
            UpdateSliderValues();
        }

        if ((MainScript.currentMainScript.elapsedTime - lastBidMessageTime) > 2 && messageOnBidClick.GetComponent<Text>().text != "")
        {
            SetBidMessage("");
        }

        SetAllDoubleDollarText();
        SetAllPriceIndicators();
        SetOutlineEffectDistanceForDoubleDollarText();
        
    }
    void ResetLastPrices()
    {
        lastPrices = new List<float>();
        for (int j = 0; j < mainScript.rawMaterialData.Count; j++)
        {
            lastPrices.Add(mainScript.rawMaterialData[j].price);
        }
    }

    void UpdateSliderValues()
    {
        try
        {
            lastSelectedJuiceRawMaterialData = Common.GetRawMaterialDataFromJuiceName(lastSelectedJuiceName.ToString(), mainScript.rawMaterialData);
        }
        catch
        {
            SceneManager.LoadScene("ErrorOccurred");
        }
        currentBidPriceText.GetComponent<Text>().text = lastSelectedJuiceRawMaterialData.price.ToString("#.##");
        maxBidPriceText.GetComponent<Text>().text = lastSelectedJuiceRawMaterialData.price.ToString("#.##");
        minBidPriceText.GetComponent<Text>().text = (lastSelectedJuiceRawMaterialData.price * 0.7f).ToString("#.##");
        bidSlider.GetComponent<Slider>().value = 1;
    }

    public void JuiceSelected(string name)
    {

        bidSlider.GetComponent<Slider>().value = 1;
        messageOnBidClick.GetComponent<Text>().text = "";
        RectTransform windowPanelRT = windowPanel.GetComponent<RectTransform>();
        if (name == Common.JuiceNames.Strawberry.ToString())
        {
            lastSelectedJuiceName = Common.JuiceNames.Strawberry;
            windowPanelRT.anchorMin = new Vector2(0, 0.5f);
            windowPanelRT.anchorMax = new Vector2(0.5f, 1.0f);
        }
        else if (name == Common.JuiceNames.Pineapple.ToString())
        {
            lastSelectedJuiceName = Common.JuiceNames.Pineapple;
            windowPanelRT.anchorMin = new Vector2(0.5f, 0f);
            windowPanelRT.anchorMax = new Vector2(1.0f, 0.5f);
        }
        else if (name == Common.JuiceNames.Orange.ToString())
        {
            lastSelectedJuiceName = Common.JuiceNames.Orange;
            windowPanelRT.anchorMin = new Vector2(0.5f, 0.5f);
            windowPanelRT.anchorMax = new Vector2(1.0f, 1.0f);
        }
        else if (name == Common.JuiceNames.Blueberry.ToString())
        {
            lastSelectedJuiceName = Common.JuiceNames.Blueberry;
            windowPanelRT.anchorMin = new Vector2(0f, 0f);
            windowPanelRT.anchorMax = new Vector2(0.5f, 0.5f);
        }
        else
        {
            SceneManager.LoadScene("ErrorOccurred");
        }

        UpdateSliderValues();
        StopAllCoroutines();
        SetJuiceWindowColor(Color.black);
        StopSparkles();
        StartCoroutine(AnimateValuesAfterJuiceSelected());
    }

    IEnumerator AnimateValuesAfterJuiceSelected()
    {
        bidSlideArea.GetComponent<Image>().color = new Color(0, 0, 0, 0);
        yield return new WaitForSeconds(0.2f);
        bidSlideArea.GetComponent<Image>().color = new Color(0, 1, 0, 0.3f);
        yield return new WaitForSeconds(1);
        bidSlideArea.GetComponent<Image>().color = new Color(0, 0, 0, 0);
    }
    void SetAllDoubleDollarText()
    {
        ClearDoubleDollarTextOnJuicePanels();
        HashSet<string> JuicesWithExcessCostAndHighConsumption = mainScript.JuicesWithExcessCostAndHighConsumption();
        if (JuicesWithExcessCostAndHighConsumption.Count > 0)
        {
            explainDollar.SetActive(true);
        }
        foreach (var juiceName in JuicesWithExcessCostAndHighConsumption)
        {
            if (juiceName == Common.JuiceNames.Strawberry.ToString()) { redPanelDollarText.SetActive(true); continue; }
            if (juiceName == Common.JuiceNames.Blueberry.ToString()) { bluePanelDollarText.SetActive(true); continue; }
            if (juiceName == Common.JuiceNames.Orange.ToString()) { orangePanelDollarText.SetActive(true); continue; }
            if (juiceName == Common.JuiceNames.Pineapple.ToString()) { yellowPanelDollarText.SetActive(true); }
        }
    }

    void SetOutlineEffectDistanceForDoubleDollarText()
    {
        GameObject[] dollarTexts = GameObject.FindGameObjectsWithTag("DollarText");
        foreach (GameObject doubleDollarTextItem in dollarTexts)
        {
            float fontSize = doubleDollarTextItem.GetComponent<Text>().cachedTextGenerator.fontSizeUsedForBestFit;
            float canvasScaleFac = canvas.scaleFactor;
            doubleDollarTextItem.GetComponent<Outline>().effectDistance = new Vector2(fontSize / 15 / canvasScaleFac, -fontSize / 15 / canvasScaleFac);
        }
    }
    void ClearDoubleDollarTextOnJuicePanels()
    {
        GameObject[] dollarTexts = GameObject.FindGameObjectsWithTag("DollarText");

        foreach (GameObject dollarText in dollarTexts)
        {
            dollarText.SetActive(false);
        }

        explainDollar.SetActive(false);
    }

    void SetAllPriceIndicators()
    {
        foreach (Common.JuiceNames juiceName in Enum.GetValues(typeof(Common.JuiceNames)))
        {
            SetPriceLevelIndicator(juiceName);
        }
    }

    void SetPriceLevelIndicator(Common.JuiceNames juiceName)
    {
        string juiceNameStr = juiceName.ToString().ToLower();

        RawMaterialData thisRawMaterialData = new RawMaterialData();
        try
        {
            thisRawMaterialData = Common.GetRawMaterialDataFromJuiceName(juiceName.ToString(), mainScript.rawMaterialData);
        }
        catch (MissingReferenceException e)
        {
            SceneManager.LoadScene("ErrorOccurred");
            Debug.Log(e);
        }
        float normalizedPriceLevel = NormalizePriceLevel(thisRawMaterialData);


        switch (juiceName)
        {
            case Common.JuiceNames.Blueberry:
                SetArrowPosition(bluePanel, normalizedPriceLevel);
                break;
            case Common.JuiceNames.Orange:
                SetArrowPosition(orangePanel, normalizedPriceLevel);
                break;
            case Common.JuiceNames.Pineapple:
                SetArrowPosition(yellowPanel, normalizedPriceLevel);
                break;
            case Common.JuiceNames.Strawberry:
                SetArrowPosition(redPanel, normalizedPriceLevel);
                break;
            default:
                break;
        }
    }

    float NormalizePriceLevel(RawMaterialData thisRawMaterial)
    {
        float priceLevelNormalised = (thisRawMaterial.price - thisRawMaterial.minPrice) / (thisRawMaterial.maxPrice - thisRawMaterial.minPrice);
        return priceLevelNormalised;
    }

    void SetArrowPosition(GameObject juicePanel, float normalizedPriceLevel)
    {
        string subPanelNameForDollarText = "CostLevelPanel/CostLevelIndicator/Arrow";
        RectTransform rectTF = juicePanel.transform.Find(subPanelNameForDollarText).GetComponent<RectTransform>();
        juicePanel.transform.Find(subPanelNameForDollarText).GetComponent<RectTransform>().anchorMin = new Vector2(normalizedPriceLevel, rectTF.anchorMin.y);
        juicePanel.transform.Find(subPanelNameForDollarText).GetComponent<RectTransform>().anchorMax = new Vector2(normalizedPriceLevel, rectTF.anchorMax.y);
    }

    void BidSliderHandler()

    {
        float maxBid = float.Parse(maxBidPriceText.transform.GetComponent<Text>().text, System.Globalization.CultureInfo.InvariantCulture);
        float minBid = float.Parse(minBidPriceText.transform.GetComponent<Text>().text, System.Globalization.CultureInfo.InvariantCulture);
        currentBidPriceText.GetComponent<Text>().text = (Mathf.Round((bidSlider.GetComponent<Slider>().value * (maxBid - minBid) + minBid) * 100) / 100).ToString();
    }

    void BidBtnClickedHandler()
    {
        HandleNegotiation();
    }

    void HandleNegotiation()
    {
        bool bidJuiceStartedExcessPricedAndHighConsumption = false;
        string juiceNameBid = lastSelectedJuiceRawMaterialData.juiceName;
        if (mainScript.JuicesWithExcessCostAndHighConsumption().Contains(juiceNameBid))
        {
            bidJuiceStartedExcessPricedAndHighConsumption = true;
        }

        Text currentBidText = currentBidPriceText.GetComponent<Text>();
        float minVal = lastSelectedJuiceRawMaterialData.minPrice;
        float bidVal = 0f;
        bidVal = float.Parse(currentBidText.text, System.Globalization.CultureInfo.InvariantCulture);
        float currentVal = lastSelectedJuiceRawMaterialData.price;

        if (bidSlider.GetComponent<Slider>().value == 1)
        {
            SetBidMessage("You must bidder lower than current price!");
            return;
        }
        float chanceSettle = ChanceSettle(currentVal, bidVal, minVal);
 
        int randomVal = randomSettleSeed.Next(100);

        if (randomVal < Mathf.Round(chanceSettle * 100))
        {
            float randomMultiplier = (Random.Range(0, 3) - 1) * 0.1f;
            float settleVal = (currentVal - bidVal) * 0.5f * (randomMultiplier + 1) + bidVal; // Bargain roughly settles on halfway between bid and current value.
            if (settleVal < minVal)
            {
                settleVal = minVal;
            }
            settleVal = (float)Mathf.Round(settleVal * 100) / 100;
            if (settleVal != currentVal)
            {
                lastSelectedJuiceRawMaterialData.price = settleVal;
                //	bidHead.setText("Bid- " + getJuicename(pickerSelecVal) + " \n" + "Now $" + String.valueOf(settleVal));
                mainScript.numberSuccessfulCostNegotiations++;
                SetBidMessage("Settle price is: " + (settleVal));
                if ((bidJuiceStartedExcessPricedAndHighConsumption == true) && mainScript.JuicesWithExcessCostAndHighConsumption().Contains(juiceNameBid) == false)
                {
                    StartCoroutine(ShowStars(juiceNameBid));
                    mainScript.PlaySparkleSound();
                }
            }
            else if (settleVal == minVal)
            {
                SetBidMessage("Suppliers say price is rock bottom");
            }
        }
        else
        {
            SetBidMessage("Prices unacceptable to suppliers");
        }
        bidSlider.GetComponent<Slider>().value = 1;
    }

    public static float ChanceSettle(float currentPrice, float bidPrice, float minPrice)
    {
        float chanceSettle = 0;
        float apex_y_OnTwoLineChanceSettleCurve = 0.6f;
        float apex_x_OnTwoLineChanceSettleCurve = 0.3f;
        // For curve of chance to settle vs bid ratio: see https://photos.app.goo.gl/RRrTGKO4KUfsqPOk1
        float bidRatio = (1f - ((currentPrice - bidPrice) / (currentPrice - minPrice))); // 1 is bid = current Price, 0 is bid = minPrice
        if (bidRatio < 0)
        {
            bidRatio = 0;
        }
        if (bidRatio > apex_x_OnTwoLineChanceSettleCurve)
        {
            chanceSettle = apex_y_OnTwoLineChanceSettleCurve + (1f - apex_y_OnTwoLineChanceSettleCurve) / (1f - apex_x_OnTwoLineChanceSettleCurve) * (bidRatio - apex_x_OnTwoLineChanceSettleCurve);
        }
        if (bidRatio <= apex_x_OnTwoLineChanceSettleCurve)
        {
            chanceSettle = apex_y_OnTwoLineChanceSettleCurve / apex_x_OnTwoLineChanceSettleCurve * bidRatio;
        }
        return chanceSettle;
    }

    void SetBidMessage(string message)
    {
        messageOnBidClick.GetComponent<Text>().text = message;
        lastBidMessageTime = MainScript.currentMainScript.elapsedTime;
    }

    IEnumerator ShowStars(string juiceName)
    {
        GameObject starParent = GetStarParent(juiceName);
        GameObject thisStarShowInst = Instantiate(starsPrefab) as GameObject;
        thisStarShowInst.transform.parent = starParent.transform;
        thisStarShowInst.transform.localPosition = starsPrefab.transform.localPosition;
        Vector3 tempGlobPos = starParent.transform.position;
        tempGlobPos.z -= 3f;
        thisStarShowInst.transform.position = tempGlobPos;
        thisStarShowInst.transform.localScale = starsPrefab.transform.localScale;
        yield return new WaitForSeconds(3);
        Destroy(thisStarShowInst);

    }

    GameObject GetStarParent(string juiceName)
    {
        if (juiceName == Common.JuiceNames.Strawberry.ToString()) { return redPanel; }
        if (juiceName == Common.JuiceNames.Blueberry.ToString()) { return bluePanel; }
        if (juiceName == Common.JuiceNames.Orange.ToString()) { return orangePanel; }
        if (juiceName == Common.JuiceNames.Pineapple.ToString()) { return yellowPanel; }
        return null;
    }

    void StopSparkles()
    {
        GameObject[] sparkles = GameObject.FindGameObjectsWithTag("Sparkles");
        foreach (GameObject sparkle in sparkles)
        {
            Destroy(sparkle);
            Debug.Log("Instance destroyed");
        }
    }

}

