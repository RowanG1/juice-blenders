﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ExitArea : MonoBehaviour
{
	Button quitBtn;
	Button submitScoreBtn;
	Button saveGameBtn;
	Text pauseText;

	int quitBtnWidth;
	int buttonHeights;
	int submitScoreBtnWidth;
	int pauseTextFontSize;
	// Use this for initialization
	void Start ()
	{
		if (MenuScript.minScreenDimensionmm > MenuScript.screenSizeThreshold) {
			 quitBtnWidth = 22;
			 buttonHeights = 10;
			submitScoreBtnWidth = 60;
			pauseTextFontSize = 4;
		} else {
			quitBtnWidth = 16;
			buttonHeights = 7;
			submitScoreBtnWidth = 40;
			pauseTextFontSize = 2;
		}
		RectTransform rectTransform = transform.GetComponent<RectTransform> ();
		rectTransform.offsetMin = new Vector2(rectTransform.offsetMin.x, Common.tabStripHeight);

		pauseText = transform.Find ("PauseText").GetComponent<Text>();

		quitBtn = transform.Find ("QuitButton").GetComponent<Button>();
		quitBtn.onClick.AddListener(() => OnQuitBtnClick ());

		saveGameBtn = transform.Find ("SaveGameBtn").GetComponent<Button>();
		saveGameBtn.onClick.AddListener(() => OnSaveGameBtnClick ());

		pauseText.fontSize = pauseTextFontSize;
	}

	void OnQuitBtnClick() {
		SaveGameForResume.ClearSavedGame ();
		DataTransferToGameDataFirebase.gameEndedStatus = Constants.Firebase.levelQuit;
		new GameDataFirebaseHandler(MainScript.currentMainScript).SetGameDataRemote();
		SceneManager.LoadScene ("MainMenu");
	}

	void OnSaveGameBtnClick() {
		SaveGameForResume.Save ();
		SceneManager.LoadScene("MainMenu");

	}

}

