﻿using System;
using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;
using System.Collections;


public class DeliveriesAreaScript : MonoBehaviour
{
    public Canvas canvas;
    int deliveryRowHeight;
    public GameObject deliveryPanel;
    public GameObject deliveryContent;
    public GameObject deliveryViewPort;
    public Sprite noComment;
    public Sprite sad;

    float lastTimeUpdated = 0;
    public GameObject deliveryPreFab;

    float lastVerticalScrollPosition;
    float scheduledPeriodBetweenContentPortUpdates = 1.0f;
    float lastTimeContentPortUpdated;
    int lastDeliveriesToDisplayCount;

    List<Order> lastDisplayedOrders = new List<Order>();

    bool scriptAlreadyStarted = false;

    public SetAllFontSizesToSmallestInGroup setAllFontSizesToSmallestInGroup;
    public ScrollRectVerticalPositionHolderOnContentHeightChange scrollRectVerticalPositionHolder;


    void Awake()
    {
        if (MenuScript.minScreenDimensionmm > MenuScript.screenSizeThreshold)
        {
            deliveryRowHeight = 50;
        }
        else
        {
            deliveryRowHeight = 30;
        }
    }


    void OnEnable()
    {
        if (scriptAlreadyStarted == true)
        {
            RedrawDeliveries();
        }
    }

    void Start()
    {
        scriptAlreadyStarted = true;
        RedrawDeliveries();
        RectTransform rectTransform = deliveryPanel.transform.GetComponent<RectTransform>();
        rectTransform.offsetMin = new Vector2(rectTransform.offsetMin.x, Common.tabStripHeight);

    }


    bool DeliveriesToDisplayChanged()
    {
        if (MainScript.currentMainScript.toDeliverArraySorted.Count == lastDisplayedOrders.Count)
        {
            int counter = 0;
            foreach (KeyValuePair<int, float> KVPairToDeliver in (MainScript.currentMainScript.toDeliverArraySorted))
            {
                int orderIndex = KVPairToDeliver.Key;
                Order thisOrder = MainScript.currentMainScript.orders[orderIndex];
    
                Order lastDisplayedOrderAtSameCounterValue = lastDisplayedOrders[counter];
                int lastDisplayedOrderIndexAtSameCounterValue = lastDisplayedOrders[counter].orderNum-1;

                if (KVPairToDeliver.Key != lastDisplayedOrderIndexAtSameCounterValue || (thisOrder.truckArrived != lastDisplayedOrderAtSameCounterValue.truckArrived))
                {
                    return true;
                }
                counter++;
            }
        }
        else
        {
            return true;
        }
        return false;
    }

    void Update()
    {
        if (MainScript.currentMainScript.elapsedTime > (lastTimeUpdated + 0.7f))
        {
            lastTimeUpdated = MainScript.currentMainScript.elapsedTime;
            if (DeliveriesToDisplayChanged())
            {
                Debug.Log("Deliveries to display has changed");
                RedrawDeliveries();
            }
        }
        if (MainScript.currentMainScript.elapsedTime > scheduledPeriodBetweenContentPortUpdates + lastTimeContentPortUpdated)
        {
            SetDeliveryContentPort(lastDeliveriesToDisplayCount);
            scrollRectVerticalPositionHolder.ContentHeightUpdated();
            lastTimeContentPortUpdated = MainScript.currentMainScript.elapsedTime;
        }

    }

    void RedrawDeliveries()
    {
        List<Text> textItems = new List<Text>();

        foreach (Transform child in deliveryContent.transform)
        {
            Destroy(child.gameObject);
        }

        MainScript mainScript = MainScript.currentMainScript;

        lastDisplayedOrders.Clear();

        for (int k = 0; k < mainScript.toDeliverArraySorted.Count; k++)
        {
            Order thisOrder = mainScript.orders[mainScript.toDeliverArraySorted[k].Key];

            GameObject deliveryItemInst = Instantiate(deliveryPreFab) as GameObject;
            deliveryItemInst.name = "Delivery" + k;
            deliveryItemInst.transform.SetParent(deliveryContent.transform, false);

            deliveryItemInst.transform.GetComponent<LayoutElement>().minHeight = deliveryRowHeight;
            deliveryItemInst.transform.GetComponent<LayoutElement>().preferredHeight = deliveryRowHeight;

            deliveryItemInst.GetComponent<EachDeliveryItemUI>().productNameVal.GetComponent<Text>().text = thisOrder.productName;
            deliveryItemInst.GetComponent<EachDeliveryItemUI>().volVal.GetComponent<Text>().text = thisOrder.volume.ToString("#,###");
            deliveryItemInst.GetComponent<EachDeliveryItemUI>().priceVal.GetComponent<Text>().text = thisOrder.price.ToString("#,###");

            if (thisOrder.truckArrived == true)
            {

            }
            else
            {
                deliveryItemInst.GetComponent<EachDeliveryItemUI>().truckPresent.gameObject.SetActive(false);
            }

            if (thisOrder.customerEmotionStatusDelivery == (int)Common.CustomerEmotionStatus.Happy)
            {

            }
            else if (thisOrder.customerEmotionStatusDelivery == Common.CustomerEmotionStatus.NoComment)
            {
                deliveryItemInst.GetComponent<EachDeliveryItemUI>().customerFeedBackEmoji.GetComponent<Image>().sprite = noComment;
            }
            else
            {
                deliveryItemInst.GetComponent<EachDeliveryItemUI>().customerFeedBackEmoji.GetComponent<Image>().sprite = sad;
            }

            foreach (Text deliveryTextItem in deliveryItemInst.GetComponentsInChildren<Text>())
            {
                textItems.Add(deliveryTextItem);
            }
            setAllFontSizesToSmallestInGroup.SetTextItems(textItems);

            Order newDisplayedOrder = new Order();
            newDisplayedOrder.orderNum = thisOrder.orderNum;
            newDisplayedOrder.truckArrived = thisOrder.truckArrived;
            lastDisplayedOrders.Add(newDisplayedOrder);

        }

        lastDeliveriesToDisplayCount = mainScript.toDeliverArraySorted.Count;
    
    }

    void SetDeliveryContentPort(int cellDisplayCount)
    {
        deliveryContent.GetComponent<RectTransform>().sizeDelta = new Vector2(0, deliveryRowHeight * cellDisplayCount + 2);
    }


}


