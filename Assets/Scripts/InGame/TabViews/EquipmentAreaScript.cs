﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;


public class EquipmentAreaScript : MonoBehaviour
{

    public enum tankSizeOptions : int { Double = 0, Quadruple = 1 };
    public enum pumpSizeOptions : int { Double = 0, Triple = 1 };

    public GameObject equipPanel;
    public GameObject TankCostValueText;
    public GameObject LineCostValueText;
    public GameObject investTankButton;
    public GameObject investLineButton;
    public GameObject tankMessageText;
    public GameObject pumpMessageText;
    public GameObject tankUpgradePanel;
    public GameObject lineUpgradePanel;
    public GameObject tankOptionsPanel;
    public GameObject pumpOptionsPanel;
    public GameObject tankNameText;
    public GameObject pumpNameText;
    public GameObject arrowToTankSize;
    public GameObject arrowToLineSize;
    public GameObject selectTankButton;
    public GameObject selectLineButton;
    public GameObject tankSmearPanel;
    public GameObject valveSmearPanel;

    float lastTimePumpMessageUpdated;
    float lastTimeTankMessageUpdated;

    private IEnumerator selectTankAnimateCoroutine;
    private IEnumerator selectLineAnimateCoroutine;

    MainScript mainScript;

    int lastSelectedUpgradeableTankIndex = Constants.EquipmentUpgrades.lastSelectedUpgradeableItemArrayIndexNone;
    int lastSelectedTankSizeIndex = 0;

    int lastSelectedUpgradeableValveIndex = Constants.EquipmentUpgrades.lastSelectedUpgradeableItemArrayIndexNone;
    int lastSelectedUpgradeableValveSizeIndex = 0;


    bool alreadyStarted = false;


    // Use this for initialization
    void Start()
    {
        alreadyStarted = true;

        SetPumpMessage("");
        SetTankMessage("");

        investTankButton.GetComponent<Button>().onClick.AddListener(() => OnTankInvestBtnClick());
        investLineButton.GetComponent<Button>().onClick.AddListener(() => OnLineInvestBtnClick());
        mainScript = MainScript.currentMainScript;
        TankCostValueText.GetComponent<Text>().text = TankInvestValueForCapacitySelection(0).ToString("#,##0");
        LineCostValueText.GetComponent<Text>().text = PumpInvestValueForCapacitySelection(0).ToString("#,##0");

        RectTransform rectTransform = transform.GetComponent<RectTransform>();
        rectTransform.offsetMin = new Vector2(rectTransform.offsetMin.x, Common.tabStripHeight);

        HandleSelectBtnAnims();
        SetSmearPanelsOnEnterPage();
        SetSelectedTankOrValve();
        SetUITankInteractableBrightness();
        SetUILineInteractableBrightness();
        StartCoroutine(ClearSmearPanels());

        EquipmentTapShort.indexOfSelectedObjectArrayInOperations = Constants.EquipmentUpgrades.lastSelectedUpgradeableItemArrayIndexNone;
    }

    void OnEnable()
    {

        if (alreadyStarted == true)
        {
            HandleSelectBtnAnims();
            SetSmearPanelsOnEnterPage();
            SetSelectedTankOrValve();
            SetUITankInteractableBrightness();
            SetUILineInteractableBrightness();
            StartCoroutine(ClearSmearPanels());
            EquipmentTapShort.indexOfSelectedObjectArrayInOperations = Constants.EquipmentUpgrades.lastSelectedUpgradeableItemArrayIndexNone;
        }
        //			print("tank index selected in on became visible function." + lastSelectedTankIndex);

    }
    void SetTankMessage(string message)
    {
        tankMessageText.GetComponent<Text>().text = message;
        lastTimeTankMessageUpdated = MainScript.currentMainScript.elapsedTime;
    }

    void SetPumpMessage(string message)
    {
        pumpMessageText.GetComponent<Text>().text = message;
        lastTimePumpMessageUpdated = MainScript.currentMainScript.elapsedTime;
    }
    void SetSmearPanelsOnEnterPage()
    {
        if (EquipmentTapShort.indexOfSelectedObjectArrayInOperations == Constants.EquipmentUpgrades.lastSelectedUpgradeableItemArrayIndexNone)
        {
            valveSmearPanel.GetComponent<Image>().color = new Color(1, 1, 1, 0);
            tankSmearPanel.GetComponent<Image>().color = new Color(1, 1, 1, 0);
        }
        else
        {
            if (EquipmentTapShort.selectedObjectTypeInOperations == "tank")
            {
                valveSmearPanel.GetComponent<Image>().color = new Color(1, 1, 1, 0.8f);
                tankSmearPanel.GetComponent<Image>().color = new Color(1, 1, 1, 0);
            }
            else if (EquipmentTapShort.selectedObjectTypeInOperations == "valve")
            {
                tankSmearPanel.GetComponent<Image>().color = new Color(1, 1, 1, 0.8f);
                valveSmearPanel.GetComponent<Image>().color = new Color(1, 1, 1, 0);
            }
        }
    }
    IEnumerator ClearSmearPanels()
    {
        yield return new WaitForSeconds(2);
        int durationFade = 3;
        float accumulatedTime = 0;
        float totalFadeValue = 0.8f * 255;
        while (accumulatedTime < durationFade)
        {
            float accumulatedFade = totalFadeValue * accumulatedTime / durationFade;
            if (tankSmearPanel.GetComponent<Image>().color.a != 0)
            {
                tankSmearPanel.GetComponent<Image>().color = new Color(1, 1, 1, (totalFadeValue - accumulatedFade) / 255);
            }
            if (valveSmearPanel.GetComponent<Image>().color.a != 0)
            {
                valveSmearPanel.GetComponent<Image>().color = new Color(1, 1, 1, (totalFadeValue - accumulatedFade) / 255);
            }
            accumulatedTime += Time.deltaTime;
            yield return 0;
        }
        tankSmearPanel.GetComponent<Image>().color = new Color(1, 1, 1, 0);
        valveSmearPanel.GetComponent<Image>().color = new Color(1, 1, 1, 0);
    }
    void SetSelectedTankOrValve()
    {
        if (EquipmentTapShort.indexOfSelectedObjectArrayInOperations != Constants.EquipmentUpgrades.lastSelectedUpgradeableItemArrayIndexNone)
        {
            lastSelectedUpgradeableTankIndex = Constants.EquipmentUpgrades.lastSelectedUpgradeableItemArrayIndexNone;
            lastSelectedUpgradeableValveIndex = Constants.EquipmentUpgrades.lastSelectedUpgradeableItemArrayIndexNone;
            switch (EquipmentTapShort.selectedObjectTypeInOperations)
            {
                case Constants.Valves.valveCode:

                    for (int k = 0; k < mainScript.valveData.Count; k++)
                    {
                        if (k == EquipmentTapShort.indexOfSelectedObjectArrayInOperations)
                        {
                            string valveRouteName = mainScript.valveData[k].tankNameValveAttachedTo + " to " + mainScript.valveData[k].valveDestination;
                            lastSelectedUpgradeableValveIndex = k;
                            pumpNameText.GetComponent<Text>().text = valveRouteName;
                            StartCoroutine(FlashArrowToSizeOption(arrowToLineSize));
                        }
                    }
                    break;
                case Constants.Tanks.tankCode:


                    for (int k = 0; k < mainScript.tankData.Count; k++)
                    {
                        if (k == EquipmentTapShort.indexOfSelectedObjectArrayInOperations)
                        {
                            string tankName = mainScript.tankData[k].tankName;
                            lastSelectedUpgradeableTankIndex = k;
                            tankNameText.GetComponent<Text>().text = tankName;
                            StartCoroutine(FlashArrowToSizeOption(arrowToTankSize));
                        }
                    }
                    break;
                default:
                    break;
            }
        }
    }


    // Update is called once per frame
    void Update()
    {
        if ((mainScript.elapsedTime - lastTimePumpMessageUpdated) > 2 && pumpMessageText.GetComponent<Text>().text != "")
        {
            SetPumpMessage("");
        }
        if ((mainScript.elapsedTime - lastTimeTankMessageUpdated) > 2 && tankMessageText.GetComponent<Text>().text != "")
        {
            SetTankMessage("");
        }

    }

    void HandleSelectBtnAnims()
    {
        if (EquipmentTapShort.indexOfSelectedObjectArrayInOperations == Constants.EquipmentUpgrades.lastSelectedUpgradeableItemArrayIndexNone)
        {
            if (lastSelectedUpgradeableTankIndex == Constants.EquipmentUpgrades.lastSelectedUpgradeableItemArrayIndexNone && lastSelectedUpgradeableValveIndex == Constants.EquipmentUpgrades.lastSelectedUpgradeableItemArrayIndexNone)
            {
                selectTankAnimateCoroutine = AnimateSelectButton(selectTankButton);
                StartCoroutine(selectTankAnimateCoroutine);
                selectLineAnimateCoroutine = AnimateSelectButton(selectLineButton);
                StartCoroutine(selectLineAnimateCoroutine);
            }
        }
    }
    IEnumerator AnimateSelectButton(GameObject button)
    {
        //print("animating");
        float timeToExpand = 0.7f;
        float expandFactor = 1.1f;
        float timeElapsed = 0;
        float currentSizeFactor = 1f;
        float expandIncrementPerFrame = (expandFactor - 1) / timeToExpand * Time.deltaTime;

        for (int i = 0; i < 2; i++)
        {
            timeElapsed = 0;
            while (timeElapsed < timeToExpand)
            {
                currentSizeFactor += expandIncrementPerFrame;
                button.GetComponent<RectTransform>().localScale = new Vector3(currentSizeFactor, 1, 1);
                timeElapsed += Time.deltaTime;
                yield return 0;
            }
            timeElapsed = 0;
            while (timeElapsed < timeToExpand)
            {
                currentSizeFactor -= expandIncrementPerFrame;
                button.GetComponent<RectTransform>().localScale = new Vector3(currentSizeFactor, 1, 1);
                timeElapsed += Time.deltaTime;
                yield return 0;
            }
        }
    }
    void SetUITankInteractableBrightness()
    {
        if (lastSelectedUpgradeableTankIndex == Constants.EquipmentUpgrades.lastSelectedUpgradeableItemArrayIndexNone)
        {
            tankNameText.GetComponent<Text>().color = new Color(0, 0, 0, 50f / 255);
            investTankButton.transform.GetComponent<Button>().interactable = false;
            tankOptionsPanel.transform.Find("QuadruplePanel").GetComponent<Button>().interactable = false;
            tankOptionsPanel.transform.Find("DoublePanel").GetComponent<Button>().interactable = false;

        }
        else
        {
            tankNameText.GetComponent<Text>().color = new Color(0, 0, 0, 255f / 255);
            investTankButton.transform.GetComponent<Button>().interactable = true;
            tankOptionsPanel.transform.Find("QuadruplePanel").GetComponent<Button>().interactable = true;
            tankOptionsPanel.transform.Find("DoublePanel").GetComponent<Button>().interactable = true;
        }
    }

    void SetUILineInteractableBrightness()
    {

        if (lastSelectedUpgradeableValveIndex == Constants.EquipmentUpgrades.lastSelectedUpgradeableItemArrayIndexNone)
        {
            pumpNameText.GetComponent<Text>().color = new Color(0, 0, 0, 50f / 255);
            investLineButton.transform.GetComponent<Button>().interactable = false;
            pumpOptionsPanel.transform.Find("TriplePanel").GetComponent<Button>().interactable = false;
            pumpOptionsPanel.transform.Find("DoublePanel").GetComponent<Button>().interactable = false;
        }
        else
        {
            pumpNameText.GetComponent<Text>().color = new Color(0, 0, 0, 255f / 255);
            investLineButton.transform.GetComponent<Button>().interactable = true;
            pumpOptionsPanel.transform.Find("TriplePanel").GetComponent<Button>().interactable = true;
            pumpOptionsPanel.transform.Find("DoublePanel").GetComponent<Button>().interactable = true;
        }
    }
    IEnumerator FlashArrowToSizeOption(GameObject arrow)
    {
        for (int i = 0; i < 2; i++)
        {
            arrow.SetActive(true);
            yield return new WaitForSeconds(1);
            arrow.SetActive(false);
            yield return new WaitForSeconds(1);
        }
        arrow.SetActive(true);
    }
    public void SelectTankBtnPressed()
    {
        pumpNameText.GetComponent<Text>().text = "None Selected";
        mainScript.uIUpdatesInGame.addTankUpgradeDotsAndDestroyExistingEquipmentDots();
        mainScript.uIUpdatesInGame.setTextEquipmentUpgradeMsgPanel("Select tank to upgrade");
        MenuBtnFuncs.GoToMenu("Operations");
        ResetTankSelections();

    }

    public void SelectLineBtnPressed()
    {

        tankNameText.GetComponent<Text>().text = "None Selected";
        mainScript.uIUpdatesInGame.addValveUpgradeDotsAndDestroyExistingEquipmentDots();
        mainScript.uIUpdatesInGame.setTextEquipmentUpgradeMsgPanel("Select valve to upgrade");
        MenuBtnFuncs.GoToMenu("Operations");
        ResetLineSelections();
    }
    public void TankUpgradeOptionPressed(string btnName)
    {
        //int indexOfOption = 
        print("Button name is: " + btnName);

        switch (btnName)
        {
            case "double":
                tankOptionsPanel.transform.Find("DoublePanel").GetComponent<Image>().color = new Color(18f / 255, 223f / 255, 87f / 255, 1f);
                tankOptionsPanel.transform.Find("QuadruplePanel").GetComponent<Image>().color = new Color(18f / 255, 223f / 255, 87f / 255, 0.6f);
                lastSelectedTankSizeIndex = 0;

                break;
            case "quadruple":
                tankOptionsPanel.transform.Find("DoublePanel").GetComponent<Image>().color = new Color(18f / 255, 223f / 255, 87f / 255, 0.6f);
                tankOptionsPanel.transform.Find("QuadruplePanel").GetComponent<Image>().color = new Color(18f / 255, 223f / 255, 87f / 255, 1.0f);
                lastSelectedTankSizeIndex = 1;
                break;
            default:
                break;
        }
        TankCostValueText.GetComponent<Text>().text = TankInvestValueForCapacitySelection(lastSelectedTankSizeIndex).ToString("#,##0");

        //TankCostValueText.GetComponent<Text> ().text = tankValueForCapacitySelection (tankSizeOptions [capacityTankDropDown.GetComponent<Dropdown> ().value]).ToString("#,##0");
    }

    public void PumpUpgradeOptionPressed(string btnName)
    {
        //int indexOfOption = 

        switch (btnName)
        {
            case "double":
                pumpOptionsPanel.transform.Find("DoublePanel").GetComponent<Image>().color = new Color(18f / 255, 223f / 255, 87f / 255, 1f);
                pumpOptionsPanel.transform.Find("TriplePanel").GetComponent<Image>().color = new Color(18f / 255, 223f / 255, 87f / 255, 0.6f);
                lastSelectedUpgradeableValveSizeIndex = 0;

                break;
            case "triple":
                pumpOptionsPanel.transform.Find("DoublePanel").GetComponent<Image>().color = new Color(18f / 255, 223f / 255, 87f / 255, 0.6f);
                pumpOptionsPanel.transform.Find("TriplePanel").GetComponent<Image>().color = new Color(18f / 255, 223f / 255, 87f / 255, 1.0f);
                lastSelectedUpgradeableValveSizeIndex = 1;
                break;
            default:
                break;
        }
        //print("Last index in option pressed function is: " + lastSelectedUpgradeableValveIndex);
        LineCostValueText.GetComponent<Text>().text = PumpInvestValueForCapacitySelection(lastSelectedUpgradeableValveSizeIndex).ToString("#,##0");

        //TankCostValueText.GetComponent<Text> ().text = tankValueForCapacitySelection (tankSizeOptions [capacityTankDropDown.GetComponent<Dropdown> ().value]).ToString("#,##0");
    }

    float TankInvestValueForCapacitySelection(int selectedSize)
    {
        float investValue = 0;
        switch (selectedSize)
        {
            case (int)tankSizeOptions.Double:
                investValue = 6000;
                break;
            case (int)tankSizeOptions.Quadruple:
                investValue = 11000;
                break;
            default:
                break;
        }
        return investValue;
    }

    int PumpInvestValueForCapacitySelection(int sizeSelection)
    {
        int investValue = 0;
        switch (sizeSelection)
        {
            case (int)pumpSizeOptions.Double: investValue = 5000; break;
            case (int)pumpSizeOptions.Triple: investValue = 9000; break;
            default: break;
        }
        return investValue;
    }


    void OnTankInvestBtnClick()
    {
        if (lastSelectedUpgradeableTankIndex != Constants.EquipmentUpgrades.lastSelectedUpgradeableItemArrayIndexNone)
        {
            float tankUpgradeCost = TankInvestValueForCapacitySelection(lastSelectedTankSizeIndex);
            int tankIndex = lastSelectedUpgradeableTankIndex;

            float currentTankHeight = mainScript.tankData[tankIndex].tankHeight;
            float upgradedFactor = currentTankHeight / PlantSetup.opTankHeightOrig;

            if (!mainScript.tank_And_Valve_Physical_Updates.CheckTankAlreadyUpgraded(tankIndex))
            {

                switch (lastSelectedTankSizeIndex)
                {
                    case (int)tankSizeOptions.Double:
                        {
                            SetTankMessage("Upgrade in process");
                            mainScript.counterTankSizeDoubled++;
                            mainScript.numberEquipmentUpgradesMade++;
                            mainScript.delayActionHandler.ToUpgradeTankWithDelay(tankIndex, tankUpgradeCost, 2, Constants.Tanks.heightUpgradeFactorDouble);
                        }
                        break;
                    case (int)tankSizeOptions.Quadruple:
                        {
                            SetTankMessage("Upgrade in process");
                            mainScript.numberEquipmentUpgradesMade++;
                            mainScript.counterTankSizeQuadrupled++;
                            mainScript.delayActionHandler.ToUpgradeTankWithDelay(tankIndex, tankUpgradeCost, 4, Constants.Tanks.heightUpgradeFactorQuadruple);
                        }
                        break;
                    default:
                        break;
                }
            }
            else
            {
                SetTankMessage("Tank already upgraded");
            }
            ResetTankSelections();
            if (selectTankAnimateCoroutine != null)
            {
                StopCoroutine(selectTankAnimateCoroutine);
            }
            selectTankAnimateCoroutine = AnimateSelectButton(selectTankButton);
            StartCoroutine(selectTankAnimateCoroutine);
        }
        else
        {
            SetTankMessage("Need to select a Tank");
        }
    }
    void OnLineInvestBtnClick()
    {
        if (lastSelectedUpgradeableValveIndex != Constants.EquipmentUpgrades.lastSelectedUpgradeableItemArrayIndexNone)
        {
            int selectedPumpSizeIndex = lastSelectedUpgradeableValveSizeIndex;

            int selectedValveIndex = lastSelectedUpgradeableValveIndex;

            int pumpUpgradeValue = PumpInvestValueForCapacitySelection(selectedPumpSizeIndex);

            if (!mainScript.tank_And_Valve_Physical_Updates.CheckValveAlreadyUpgraded(selectedValveIndex))
            {
                //print ("Pump selected details are: " + selectedPumpName + "and " + selectedPumpSize);
                Valve thisValve = mainScript.valveData[selectedValveIndex];
                switch (selectedPumpSizeIndex)
                {
                    case (int)pumpSizeOptions.Double:
                            SetPumpMessage("Upgrade in process");
                            mainScript.numberEquipmentUpgradesMade++;
                            mainScript.counterPumpSizeDoubled++;
                            mainScript.delayActionHandler.PumpUpgradeWithDelay(thisValve, Constants.Lines.doubleFlowLineThickness, pumpUpgradeValue, thisValve.valveFlowRate * 2f);
                        break;
                    case (int)pumpSizeOptions.Triple:
                            SetPumpMessage("Upgrade in process");
                            mainScript.numberEquipmentUpgradesMade++;
                            mainScript.counterPumpSizeTripled++;
                            mainScript.delayActionHandler.PumpUpgradeWithDelay(thisValve, Constants.Lines.tripleFlowLineThickness, pumpUpgradeValue, thisValve.valveFlowRate * 3f);  
                        break;
                    default:
                        break;
                }
                ResetLineSelections();
                if (selectLineAnimateCoroutine != null)
                {
                    StopCoroutine(selectLineAnimateCoroutine);
                }
                selectLineAnimateCoroutine = AnimateSelectButton(selectLineButton);
                StartCoroutine(selectLineAnimateCoroutine);
                return;
            }
            else
            {
                SetPumpMessage("Line already upgraded");
            }
        }
        else
        {
            SetPumpMessage("Need to select a valve");
        }
    }
    void ResetTankSelections()
    {
        lastSelectedUpgradeableTankIndex = Constants.EquipmentUpgrades.lastSelectedUpgradeableItemArrayIndexNone;
        lastSelectedTankSizeIndex = 0;
        TankCostValueText.GetComponent<Text>().text = TankInvestValueForCapacitySelection(0).ToString("#,##0");
        tankNameText.GetComponent<Text>().text = "None selected";
        tankOptionsPanel.transform.Find("DoublePanel").GetComponent<Image>().color = new Color(18f / 255, 223f / 255, 87f / 255, 1f);
        tankOptionsPanel.transform.Find("QuadruplePanel").GetComponent<Image>().color = new Color(18f / 255, 223f / 255, 87f / 255, 0.6f);
        SetUITankInteractableBrightness();
    }

    void ResetLineSelections()
    {
        lastSelectedUpgradeableValveIndex = Constants.EquipmentUpgrades.lastSelectedUpgradeableItemArrayIndexNone;
        lastSelectedUpgradeableValveSizeIndex = 0;
        LineCostValueText.GetComponent<Text>().text = PumpInvestValueForCapacitySelection(0).ToString("#,##0");
        pumpNameText.GetComponent<Text>().text = "None selected";
        pumpOptionsPanel.transform.Find("DoublePanel").GetComponent<Image>().color = new Color(18f / 255, 223f / 255, 87f / 255, 1f);
        pumpOptionsPanel.transform.Find("TriplePanel").GetComponent<Image>().color = new Color(18f / 255, 223f / 255, 87f / 255, 0.6f);
        SetUILineInteractableBrightness();
    }
}


