﻿using System;
using UnityEngine;
using UnityEngine.UI;


public class FinanceAreaScript : MonoBehaviour
{

    int financeRowHeight;
    public GameObject financePanel;
    public GameObject financeContent;
    public GameObject financeViewPort;
    float textHeightFactor = 2f;
    int labelWidth;
    int valueWidth;
    int rowCount;
    float lastTimeUpdated = 0;
    bool alreadyStarted;
    MainScript mainScript;

    void Start()
    {

        mainScript = MainScript.currentMainScript;
        alreadyStarted = true;
        if (MenuScript.minScreenDimensionmm > MenuScript.screenSizeThreshold)
        {
            financeRowHeight = 10;
            labelWidth = 40;
            valueWidth = 30;
        }
        else
        {
            financeRowHeight = 7;
            labelWidth = 25;
            valueWidth = 20;
        }
        UpdateFinance();
        HandleRowsToIncludeForLevel();
        RectTransform rectTransform = financePanel.transform.GetComponent<RectTransform>();
        rectTransform.offsetMin = new Vector2(rectTransform.offsetMin.x, Common.tabStripHeight);
        rowCount = 0;
        foreach (Transform child_FinanceRow in financeContent.transform)
        {
            if (child_FinanceRow.gameObject.activeSelf == true)
            {
                child_FinanceRow.GetComponent<RectTransform>().offsetMax = new Vector2(0, -financeRowHeight * rowCount);
                child_FinanceRow.GetComponent<RectTransform>().offsetMin = new Vector2(0, -financeRowHeight * rowCount);
                child_FinanceRow.GetComponent<RectTransform>().sizeDelta = new Vector2(0, financeRowHeight);
                //	child_FinanceRow.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (0, -financeRowHeight * count);

                child_FinanceRow.transform.Find("Name").GetComponent<Text>().fontSize = (int)(((float)financeRowHeight) / textHeightFactor);
                child_FinanceRow.transform.Find("Name").GetComponent<RectTransform>().sizeDelta = new Vector2(labelWidth, financeRowHeight);

                child_FinanceRow.transform.Find("Value").GetComponent<RectTransform>().offsetMin = new Vector2(labelWidth + 4, 0);
                child_FinanceRow.transform.Find("Value").GetComponent<RectTransform>().offsetMax = new Vector2(labelWidth + 4, 0);
                child_FinanceRow.transform.Find("Value").GetComponent<RectTransform>().sizeDelta = new Vector2(valueWidth, financeRowHeight);
                child_FinanceRow.transform.Find("Value").GetComponent<Text>().fontSize = (int)(((float)financeRowHeight) / textHeightFactor);

                rowCount++;
            }
        }
        SetFinanceContentPort(rowCount);

    }

    void HandleRowsToIncludeForLevel()
    {
        Level thisLevel = Level.GetLevelObj();
        foreach (string inactiveFunctionalArea in thisLevel.inactiveFunctionalAreas)
        {
            if (inactiveFunctionalArea == FunctionalAreaNames.marketing)
            {
                financeContent.transform.Find("Advertizing").gameObject.SetActive(false);
                financeContent.transform.Find("Branding").gameObject.SetActive(false);
            }
            if (inactiveFunctionalArea == FunctionalAreaNames.equipment)
            {
                financeContent.transform.Find("Investments").gameObject.SetActive(false);
            }
            if (inactiveFunctionalArea == FunctionalAreaNames.procurement)
            {
                financeContent.transform.Find("CostOfSales").gameObject.SetActive(false);
            }
        }
    }

    void Update()
    {
        if (mainScript.elapsedTime > (lastTimeUpdated + 5.0f))
        {
            UpdateFinance();
            lastTimeUpdated = mainScript.elapsedTime;
        }
    }
    public void UpdateFinance()
    {
        if (alreadyStarted == true)
        {
            financeContent.transform.Find("BankBal").Find("Value").GetComponent<Text>().text = (mainScript.bankBalance).ToString("#,##0");
            financeContent.transform.Find("Sales").Find("Value").GetComponent<Text>().text = mainScript.sales.ToString("#,##0");
            float costofSales = mainScript.costOfSales;
            financeContent.transform.Find("CostOfSales").Find("Value").GetComponent<Text>().text = (((float)Mathf.Round(100 * costofSales)) / 100).ToString("#,##0");
            financeContent.transform.Find("Advertizing").Find("Value").GetComponent<Text>().text = (mainScript.advertising + mainScript.pendingAdvertsTotal).ToString("#,##0");
            financeContent.transform.Find("Branding").Find("Value").GetComponent<Text>().text = mainScript.branding.ToString("#,##0");
            financeContent.transform.Find("Goodwill-service").Find("Value").GetComponent<Text>().text = mainScript.goodwill.ToString("#,##0");
            float profit = mainScript.profit + mainScript.goodwill;
            financeContent.transform.Find("Profit").Find("Value").GetComponent<Text>().text = (((float)Mathf.Round(100 * profit)) / 100).ToString("#,##0");
            financeContent.transform.Find("Investments").Find("Value").GetComponent<Text>().text = mainScript.investments.ToString("#,##0");
            financeContent.transform.Find("Interest").Find("Value").GetComponent<Text>().text = "" + mainScript.interest.ToString("#,##0");
            SetFinanceContentPort(rowCount);
        }
    }

    void SetFinanceContentPort(int cellDisplayCount)
    {

        RectTransform financeViewPortRect = financeViewPort.transform.GetComponent<RectTransform>();
        float heightToAnchorRatio = financeViewPortRect.rect.height / (1);
        float heightNeededForOrder = financeRowHeight * cellDisplayCount;
        float anchorHeightNeeded = heightNeededForOrder / heightToAnchorRatio;
        //        print("Min anchor height needed " + (1 - anchorHeightNeeded));
        financeContent.transform.GetComponent<RectTransform>().anchorMin = new Vector2(0, 1 - anchorHeightNeeded);
    }

    public static float GPMarginPercen(float sales, float costOfSales)
    {
        float gpMargin = 0;
        if (sales > 0)
        {
            gpMargin = (sales - costOfSales) / (sales) * 100;
        }
        return Common.ConvertToTwoDecimal(gpMargin);
    }

}


