﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;



public class OrdersAreaScript : MonoBehaviour
{
    public Canvas canvas;
    public GameObject orderEntryPreFab;
    public GameObject ordersContent;
    public GameObject ordersContentViewPort;
    public Sprite noCommentSprite;
    public Sprite sadSprite;

    public int orderRowHeight;
    public float lastTimeViewRefreshed = 0;
    public float lastTimeOrderBtnClicked;
    float minTimeBetweenBtnClicks = 0.5f;
    float scheduledPeriodBetweenContentPortUpdates = 1.0f;
    float lastTimeContentPortUpdated;

    int lastOrdersToDisplayCount;



    public SetAllFontSizesToSmallestInGroup setAllFontSizesToSmallestInGroup;
    public ScrollRectVerticalPositionHolderOnContentHeightChange scrollRectVerticalPositionHolder;


    void Start()
    {
        if (MenuScript.minScreenDimensionmm > MenuScript.screenSizeThreshold)
        {
            orderRowHeight = 40;
        }
        else
        {
            orderRowHeight = 28;
        }

        //SetOrderContentPort (0);
        RectTransform rectTransform = transform.GetComponent<RectTransform>();
        rectTransform.offsetMin = new Vector2(rectTransform.offsetMin.x, Common.tabStripHeight);
        foreach (Transform child in ordersContent.transform)
        {
            Destroy(child.gameObject);
        }
        SetOrderContentPort(0);
        RefreshDisplayedOrdersIfChanged();

    }

    void Update()
    {

        if ((MainScript.currentMainScript.elapsedTime - lastTimeOrderBtnClicked) > minTimeBetweenBtnClicks)
        {
            RefreshDisplayedOrdersIfChanged();
        }
        if (MainScript.currentMainScript.elapsedTime > scheduledPeriodBetweenContentPortUpdates + lastTimeContentPortUpdated)
        {
            SetOrderContentPort(lastOrdersToDisplayCount);
            lastTimeContentPortUpdated = MainScript.currentMainScript.elapsedTime;
            scrollRectVerticalPositionHolder.ContentHeightUpdated();
        }

    }


    public void RefreshDisplayedOrdersIfChanged()
    {
        if (MainScript.currentMainScript.orderFuncs.OrderToDisplayChanged())
        {
            RedrawOrder();
        }
    }

    public void RedrawOrder()
    {
        List<Text> textItems = new List<Text>();

        foreach (Transform child in ordersContent.transform)
        {
            Destroy(child.gameObject);
        }

        List<Order> ordersAtEnquiryStage = MainScript.currentMainScript.orderFuncs.ordersAtEnquiryStage;

        int orderDisplayCount = 0;
        for (int i = 0; i < ordersAtEnquiryStage.Count; i++)
        {
            Order thisOrder = ordersAtEnquiryStage[i];
            GameObject newOrderInst = Instantiate(orderEntryPreFab) as GameObject;
            newOrderInst.transform.SetParent(ordersContent.transform, false);

            newOrderInst.transform.GetComponent<LayoutElement>().minHeight = orderRowHeight;
            newOrderInst.transform.GetComponent<LayoutElement>().preferredHeight = orderRowHeight;
            //newOrderInst.transform.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (0, -orderRowHeight * orderDisplayCount);

            newOrderInst.transform.GetComponent<EachOrderView>().acceptBtn.GetComponent<OrdersBtnScript>().orderNum = thisOrder.orderNum;
            newOrderInst.transform.GetComponent<EachOrderView>().declineBtn.GetComponent<OrdersBtnScript>().orderNum = thisOrder.orderNum;
            newOrderInst.transform.GetComponent<EachOrderView>().acceptBtn.GetComponent<OrdersBtnScript>().orderInstance = newOrderInst;
            newOrderInst.transform.GetComponent<EachOrderView>().declineBtn.GetComponent<OrdersBtnScript>().orderInstance = newOrderInst;

            newOrderInst.transform.GetComponent<EachOrderView>().productNameVal.GetComponent<Text>().text = thisOrder.productName;
            newOrderInst.transform.GetComponent<EachOrderView>().volumeVal.GetComponent<Text>().text = thisOrder.volume.ToString("#,###");
            newOrderInst.transform.GetComponent<EachOrderView>().priceVal.GetComponent<Text>().text = thisOrder.price.ToString("#,###");

            float GPMargin = thisOrder.GPMargin;
            float tier1 = Constants.Orders.tier1OrderMargin;
            float tier2 = Constants.Orders.tier2OrderMargin;

            if (GPMargin < tier1)
            {
                newOrderInst.transform.GetComponent<EachOrderView>().gpMarginVal.GetComponent<Text>().text = thisOrder.GPMargin.ToString() + "%" + "<color=red>" + " █" + "</color>";
            }
            if (GPMargin >= tier1 && GPMargin < tier2)
            {
                newOrderInst.transform.GetComponent<EachOrderView>().gpMarginVal.GetComponent<Text>().text = thisOrder.GPMargin.ToString() + "%" + "<color=orange>" + " █" + "</color>";
            }

            if (GPMargin >= tier2)
            {
                newOrderInst.transform.GetComponent<EachOrderView>().gpMarginVal.GetComponent<Text>().text = thisOrder.GPMargin.ToString() + "%" + "<color=green>" + " █" + "</color>"; ;
            }

            if (thisOrder.customerEmotionStatusEnquiry == Common.CustomerEmotionStatus.NoComment)
            {
                newOrderInst.transform.GetComponent<EachOrderView>().feedbackEmoji.GetComponent<Image>().sprite = noCommentSprite;
            }

            if (thisOrder.customerEmotionStatusEnquiry == Common.CustomerEmotionStatus.Sad)
            {
                newOrderInst.transform.GetComponent<EachOrderView>().feedbackEmoji.GetComponent<Image>().sprite = sadSprite;
            }

            orderDisplayCount++;

            foreach (Text orderTextItem in newOrderInst.GetComponentsInChildren<Text>())
            {
                    textItems.Add(orderTextItem);
            }
        }
        lastOrdersToDisplayCount = orderDisplayCount;
        MainScript.currentMainScript.orderFuncs.SetLastDisplayedOrders();
        lastTimeViewRefreshed = MainScript.currentMainScript.elapsedTime;

        setAllFontSizesToSmallestInGroup.SetTextItems(textItems);
    }

    void SetOrderContentPort(int cellDisplayCount)
    {
        RectTransform rectTransform = ordersContent.transform.GetComponent<RectTransform>();
        rectTransform.sizeDelta = new Vector2(0, orderRowHeight * cellDisplayCount + 2);

    }

}


