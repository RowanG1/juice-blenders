﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Reflection;
using UnityEngine.UI;

public class GameScript : MonoBehaviour {
	public static GameScript currentGameScript;

	MainScript mainScript;
	public GameObject uiUpdatesInGame;
	public GameObject procurementNeededIconPanel;
	public GameObject indicatorPanel;
	public GameObject emptyDeliveryTruckWaitingIconPanel;
	public GameObject lateDeliveryTruckIconPanel;
	public GameObject newOrdersWaitingIconPanel;
	public GameObject brandMarketingNeededIconPanel;
	public GameObject equipmentUpgradeNeededIconPanel;
	public GameObject lowBankBalanceIconPanel;
    public GameObject timeLeftText;
    public GameObject timeLeftPanel;

 
	public GameObject newAnimatedNotifUI;

	public GameObject operations;
	public GameObject ordersArea;
	public GameObject financeArea;
	public GameObject deliveryArea;
	public GameObject marketingArea;
	public GameObject equipmentArea;
	public GameObject procurementArea;
	public GameObject exitArea;
	public GameObject tabStripArea;
	public GameObject targetsArea;
	public GameObject UICanvas;
	public GameObject sparkleSoundObj;

	public GameObject drainSound;

	public Material eggMaterial;
	public Material loadingZoneMaterial;
	public Material truckMaterial;
	// Use this for initialization
	void Awake() {
		currentGameScript = this;
		mainScript = new MainScript();
		mainScript.uIUpdatesInGame = uiUpdatesInGame.GetComponent<UIUpdatesInGame>();
		mainScript.indicatorPanel = indicatorPanel;
		mainScript.procurementNeededIconPanel = procurementNeededIconPanel;
		mainScript.emptyDeliveryTruckWaitingIconPanel = emptyDeliveryTruckWaitingIconPanel;
		mainScript.lateDeliveryTruckIconPanel = lateDeliveryTruckIconPanel;
		mainScript.newOrdersWaitingIconPanel = newOrdersWaitingIconPanel;
		mainScript.brandMarketingNeededIconPanel = brandMarketingNeededIconPanel;
		mainScript.equipmentUpgradeNeededIconPanel = equipmentUpgradeNeededIconPanel;
		mainScript.lowBankBalanceIconPanel = lowBankBalanceIconPanel;
		mainScript.newAnimatedNotifUI = newAnimatedNotifUI;
        mainScript.timeLeftText = timeLeftText;
        mainScript.timeLeftPanel = timeLeftPanel;

		mainScript.operations = operations;
		mainScript.ordersArea= ordersArea;
		mainScript.financeArea= financeArea;
		mainScript.deliveryArea= deliveryArea;
		mainScript.marketingArea = marketingArea;
		mainScript.targetsArea = targetsArea;
		mainScript.equipmentArea = equipmentArea;
		mainScript.procurementArea = procurementArea;
		mainScript.exitArea= exitArea;
		mainScript.tabStripArea= tabStripArea;
		mainScript.UICanvas = UICanvas;

		mainScript.drainSound= drainSound;

		mainScript.eggMaterial= eggMaterial;
		mainScript.loadingZoneMaterial= loadingZoneMaterial;
		mainScript.truckMaterial= truckMaterial;
			
		mainScript.Awake();

	}

	void Start () {
	
		mainScript.Start();
	}
	
	// Update is called once per frame
	void Update () {
		mainScript.Update();
	}

	   void OnApplicationPause(bool pauseStatus)
    {
         SaveGameForResume.Save ();
    }


}
