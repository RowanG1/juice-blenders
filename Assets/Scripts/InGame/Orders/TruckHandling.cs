﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TruckHandling
{

    MainScript mainScript;


    public TruckHandling()
    {
        this.mainScript = MainScript.currentMainScript;
    }
    // Use this for initialization
    public void TruckHandleLoop()
    {
        for (int u = 0; u < mainScript.tankData.Count; u++)
        {
            TankData thisTank = mainScript.tankData[u];
            if (thisTank.tankName.ToLower().Contains(Constants.Tanks.outletCode))
            {
                NextTruckCallLoopForThisOutletTank(thisTank);
                LoopTruckFull_OrderLost_Or_Penalty(thisTank);
                mainScript.uIUpdatesInGame.updateTruckLabels(thisTank);
            }
        }
    }

    public void NextTruckCallLoopForThisOutletTank(TankData thisTank)
    {

        for (int l = 0; l < mainScript.toDeliverArraySorted.Count; l++)
        {
            int toDeliverArraySortedItemKey = mainScript.toDeliverArraySorted[l].Key;
            Order thisOrder = mainScript.orders[toDeliverArraySortedItemKey];
            if (thisTank.tankName == ("Outlet-" + (thisOrder.productName)) && (thisTank.deliverTruckState == (int)Common.DeliverTruckState.Absent))
            {
                if (thisOrder.truckArrived == false && thisOrder.truckCalled == false && thisOrder.declined == false && thisOrder.lost == false && thisOrder.accepted == true)
                {
                    thisOrder.truckCalled = true;
                    thisTank.deliverTruckState = (int)Common.DeliverTruckState.Waiting;
                    DelayAction truckCallDelayAction = new DelayAction();
                    truckCallDelayAction.actionName = Constants.Delays.callTruck;
                    truckCallDelayAction.delayRemain = UnityEngine.Random.Range(0, 10) + 5;
                    truckCallDelayAction.tankIndex = thisTank.tankIndex;
                    truckCallDelayAction.orderIndex = toDeliverArraySortedItemKey;
                    mainScript.delayActions.Add(truckCallDelayAction);
                    return;
                }

            }
        }
    }

    public void LoopTruckFull_OrderLost_Or_Penalty(TankData thisTank)
    {

        for (int l = 0; l < mainScript.toDeliverArraySorted.Count; l++)
        {

            //}
            Order thisOrder = mainScript.orders[mainScript.toDeliverArraySorted[l].Key];

            if ((thisTank.tankName == ("Outlet-" + (thisOrder.productName))) && thisOrder.truckArrived == true && thisOrder.fulfilled == false && ((mainScript.elapsedTime - thisOrder.orderAcceptedTime) > (int)Common.CustomerEmotionStatusThresholdInMin.Sad * 60) && thisOrder.lost == false)
            {
                //	thisTank.truckPresent = false;
                thisTank.deliverTruckState = (int)Common.DeliverTruckState.Absent;
                thisOrder.lost = true;
                //  MainScript.currentMainScript.badgesPoints.addCustomerServicePointsEarnedForOrderAcceptedOrRejectedOrLost(thisOrder);
                thisTank.tankLevelFrac = Constants.Tanks.tankEmptyLevelFraction;
                float loss = Constants.Orders.goodwillLostDelivery;
                mainScript.goodwill -= loss;
                mainScript.mainNotificationsToAnimate.Add(new MainAnimatedNotification("Order lost- truck waited too long. Goodwill dropped."));
                return;
            }

            if (thisOrder.truckArrived == true && thisOrder.fulfilled == false && (thisTank.tankName == ("Outlet-" + (thisOrder.productName))) && thisOrder.lost == false)
            {
                if (((thisTank.tankLevelFrac - Constants.Tanks.tankEmptyLevelFraction) * thisTank.tankCapacity) >= thisOrder.volume)
                {
                    mainScript.tank_And_Valve_Physical_Updates.SwitchOffValveToTank(thisTank);                    
                    thisTank.tankLevelFrac = Constants.Tanks.tankEmptyLevelFraction;
                    thisOrder.fulfilled = true;
                    //	mainScript.badgesPoints.addDeliveryFulfilledPointsEarned(thisOrder);
                    thisTank.deliverTruckState = (int)Common.DeliverTruckState.Absent;
                    float finalOrderVal = Common.ConvertToTwoDecimal(thisOrder.price - thisOrder.penalty * thisOrder.price);
                    mainScript.profit += finalOrderVal;
                    mainScript.bankBalance += finalOrderVal;
                    mainScript.sales = Common.ConvertToTwoDecimal(mainScript.sales + finalOrderVal);
                   // Debug.Log("New sales value is: " + mainScript.sales);
                    float goodwillGain = Constants.Orders.goodwillGainedFromOrderFulfilled;
                    mainScript.mainNotificationsToAnimate.Add(new MainAnimatedNotification("Truck filled.", Mathf.Round(finalOrderVal)));
                    mainScript.goodwill += goodwillGain;
                    return;
                }
            }
        }
    }

    public void SetDeliveryEmotionStatus()
    {
        for (int k = 0; k < mainScript.toDeliverArraySorted.Count; k++)
        {
            Order thisOrder = mainScript.orders[mainScript.toDeliverArraySorted[k].Key];
            float timeSinceOrderAccepted = mainScript.elapsedTime - thisOrder.orderAcceptedTime;

            if (timeSinceOrderAccepted < (int)Common.CustomerEmotionStatusThresholdInMin.Happy * 60)
            {
                thisOrder.customerEmotionStatusDelivery = (int)Common.CustomerEmotionStatus.Happy;
            }
            else if (timeSinceOrderAccepted < (int)Common.CustomerEmotionStatusThresholdInMin.NoComment * 60)
            {
                thisOrder.customerEmotionStatusDelivery = Common.CustomerEmotionStatus.NoComment;
            }
            else
            {
                thisOrder.customerEmotionStatusDelivery = Common.CustomerEmotionStatus.Sad;
            }
        }
    }

}
