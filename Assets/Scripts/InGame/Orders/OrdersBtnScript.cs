﻿using System;
using UnityEngine;
using UnityEngine.UI;


public class OrdersBtnScript : MonoBehaviour
{
    public string buttonName;
    public int orderNum;
    public GameObject orderInstance;
    public GameObject ordersArea;

    void Start()
    {
        Button thisBut = (transform.gameObject.GetComponent<Button>());
        thisBut.onClick.AddListener(() => OnBtnClick());
    }


    public void OnBtnClick()
    {

        Order thisOrder = MainScript.currentMainScript.orders[orderNum - 1];
        if ((MainScript.currentMainScript.elapsedTime - (MainScript.currentMainScript.ordersArea.GetComponent<OrdersAreaScript>().lastTimeViewRefreshed) > 0.55f))
        {
            //	print ("Button " + butggtonName + " clicked, for order ssedd" + orderNum);
            switch (buttonName)
            {
                case "declined":
                    thisOrder.declined = true;
                     Firebase.Analytics.FirebaseAnalytics.LogEvent("order_btn_accept_clicked", "type", "N/A");
                    break;
                case "accepted":
                    thisOrder.accepted = true;
                    thisOrder.orderAcceptedTime = MainScript.currentMainScript.elapsedTime;
                    Firebase.Analytics.FirebaseAnalytics.LogEvent("order_btn_decline_clicked", "type", "N/A");
                    break;
                default:
                    break;
            }
       
            MainScript.currentMainScript.ordersArea.GetComponent<OrdersAreaScript>().lastTimeOrderBtnClicked = MainScript.currentMainScript.elapsedTime;
            Color myColor = new Color();
            ColorUtility.TryParseHtmlString("#FA8072", out myColor);
            orderInstance.GetComponent<Image>().color = myColor;

        }
    }
}

