﻿using System;

[System.Serializable] 
	public class Order
	{
		public float orderCreated = 0;
		public float orderAcceptedTime = 0;
		public string productName = "";
		public int volume = 0;
		public int orderNum = -1;
		public float price = -1;
		public float GPMargin = -1; //As percentage
		public bool accepted = false;
		public bool declined = false;
		public bool lost = false;
		public bool truckCalled = false;
		public bool truckArrived= false;
		public bool fulfilled = false;
		public float penalty = 0;
		public Common.CustomerEmotionStatus customerEmotionStatusEnquiry= 0;
		public Common.CustomerEmotionStatus customerEmotionStatusDelivery= 0;

		public Order ()
		{
		}
	}


