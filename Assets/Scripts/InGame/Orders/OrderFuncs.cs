using System.Collections.Generic;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using System;


public class OrderFuncs
{
    MainScript mainScript;
    public GameObject marketingMenuTab;

    System.Random randomOrderSeed = new System.Random();
    System.Random randomProductSeed = new System.Random();


    public static float newOrdersPercentChanceBaselinePerOrderLoop = 3.2f;
    public int newOrderPercentChanceMarketingDisabled = 2;

    public List<Order> ordersAtEnquiryStage = new List<Order>();
    public List<Order> lastDisplayedOrders = new List<Order>();


    public OrderFuncs()
    {
        this.mainScript = MainScript.currentMainScript;
    }

    public static float NetAdvertising(float advertising, float goodwill, float adLeakage)
    {
        float netAdvert = advertising + goodwill - adLeakage;
        if (netAdvert < 0)
        {
            netAdvert = 0;
        }

        return netAdvert;
    }

    public void OrderGenerateLoop()
    {
        int randomNum;
        float netAdverts = NetAdvertising(advertising: mainScript.advertising, goodwill: mainScript.goodwill, adLeakage: mainScript.adLeakage);

        randomNum = randomOrderSeed.Next(100);
        float randomCeil1;

        if (mainScript.orders.Count < 2)
        {
            randomCeil1 = newOrdersPercentChanceBaselinePerOrderLoop * 4f;
        }
        else if (Level.GetLevelObj().inactiveFunctionalAreas.Contains(FunctionalAreaNames.marketing))
        {
            randomCeil1 = newOrderPercentChanceMarketingDisabled;
        }
        else
        {
            randomCeil1 = AdvertsToOrdersDeterminant(netAdverts);
            if (randomCeil1 > newOrdersPercentChanceBaselinePerOrderLoop)
            {
                randomCeil1 = newOrdersPercentChanceBaselinePerOrderLoop;
            }
        }
        bool cond1 = (randomNum >= 0 && randomNum <= randomCeil1);
        //  Debug.Log("Random number is: " + randomNum);
        //  Debug.Log("Random ceiling is" + randomCeil1);

        if (cond1)
        {
            Dictionary<string, float> fulfilledBiasProductToOrder = new Dictionary<string, float>();
            Order newOrder = GenOrder(fulfilledBiasProductToOrder, netAdverts);
            mainScript.orders.Add(newOrder);
        }
    }

    public Order GenOrder(Dictionary<string, float> fulfilledBiasProductToOrder, float netAdvert)
    {
        int newOrderNum = 0;
        newOrderNum = mainScript.orders.Count + 1;

        int totalOrdersFulfilled = 0;
        Dictionary<string, int> ordersByProductCount = new Dictionary<string, int>();
        fulfilledBiasProductToOrder.Clear();

        for (int j = 0; j < mainScript.productData.Count; j++)
        {
            ordersByProductCount.Add(mainScript.productData[j].productName, 0);
            fulfilledBiasProductToOrder.Add(mainScript.productData[j].productName, 0);
        }

        for (int u = 0; u < mainScript.orders.Count; u++)
        {
            //print("Order volume is: " + orders[u].volume);
            if (mainScript.orders[u].fulfilled)
            {
                ordersByProductCount[mainScript.orders[u].productName] += 1;
                totalOrdersFulfilled += 1;

            }
        }

        float randomBaseTerm = 1f / mainScript.productData.Count;
        for (int u = 0; u < mainScript.productData.Count; u++)
        {
            //				int item;
            string prodName = mainScript.productData[u].productName;
            // Below serves to create a new order, partly by random from existing product data array, and partly biased to qty of past orders fulfilled.
            // The randomBase term is the portion that is randomized. The other term biases the order so that the ratio of biased orders to random orders is orderBiasToProductsFraction
            //Random portion makes up total of 1f (float). The other portion makes up say 0.6f. A random number is made, from say 0 to (1 + 0.6f). Then we see where the random number lies,to select which product to choose
            // https://photos.app.goo.gl/bwtwjPoNWFEx8BaE2

            int productFulfilledCount = 0;
            productFulfilledCount = ordersByProductCount[prodName];
            if (totalOrdersFulfilled > 0)
            {
                fulfilledBiasProductToOrder[prodName] = randomBaseTerm + Constants.Orders.orderBiasFractionBasedOnDeliveries * (float)productFulfilledCount / totalOrdersFulfilled;
            }
            else
            {
                fulfilledBiasProductToOrder[prodName] = randomBaseTerm + Constants.Orders.orderBiasFractionBasedOnDeliveries / mainScript.productData.Count;
            }
        }
        // Now generate a random number, in range of biased set 
        float randomFloatInFulfilledBias = (float)(randomProductSeed.Next(100 + (int)(100 * Constants.Orders.orderBiasFractionBasedOnDeliveries))) / 100;
        //        Debug.Log("Random product seed is: " + randomFloatInFulfilledBias);

        string productNameToGenerateOrder = FindProductNameInBiasedProductSet(randomFloatInFulfilledBias, fulfilledBiasProductToOrder);

        int productVolumeRandom = Constants.JuiceProduct.minProductVolume + (int)Mathf.Round(UnityEngine.Random.Range(0, Constants.JuiceProduct.rangeProductVolume));

        float productVolumeFactor = (float)netAdvert * Constants.Orders.productVolumeFactorConst + 1;


        if (productVolumeFactor > Constants.Orders.maxProductVolFactor)
        {
            productVolumeFactor = Constants.Orders.maxProductVolFactor;
        }
        float unitPrice = 0;

        for (int v = 0; v < mainScript.productData.Count; v++)
        {
            if (mainScript.productData[v].productName == productNameToGenerateOrder)
            {
                unitPrice = mainScript.productData[v].price;
            }
        }

        int newOrderVolume = (int)Mathf.Round(productVolumeRandom * productVolumeFactor);

        float orderValue = ((float)Mathf.Round(newOrderVolume * unitPrice * 100)) / 100;
        Order newOrder = new Order();
        newOrder.orderCreated = mainScript.elapsedTime;
        newOrder.price = orderValue;
        Debug.Assert(productNameToGenerateOrder != "");

        newOrder.productName = productNameToGenerateOrder; //productRandomKey;
        newOrder.volume = newOrderVolume;
        newOrder.orderNum = newOrderNum;
        newOrder.penalty = 0;

        for (int v = 0; v < mainScript.productData.Count; v++)
        {
            ProductData thisProduct = mainScript.productData[v];
            if (thisProduct.productName == (productNameToGenerateOrder))
            {
                float juiceCost = 0;
                Dictionary<string, float> rawMatPrices = new Dictionary<string, float>();
                for (int j = 0; j < mainScript.rawMaterialData.Count; j++)
                {
                    RawMaterialData thisRawMaterial = mainScript.rawMaterialData[j];
                    rawMatPrices.Add(thisRawMaterial.juiceName, thisRawMaterial.price);
                }
                for (int w = 0; w < thisProduct.juiceSpecs.Count; w++)
                {
                    JuiceSpecs thisJuice = thisProduct.juiceSpecs[w];
                    string juiceName = thisJuice.juiceName;
                    juiceCost += rawMatPrices[juiceName] * thisJuice.targetFrac * newOrderVolume;
                }
                float GPMargin = (orderValue - juiceCost) / orderValue * 100;
                newOrder.GPMargin = ((float)Mathf.Round(GPMargin * 10)) / 10;
            }
        }
        return newOrder;
    }

    public static string FindProductNameInBiasedProductSet(float randomFloatInFulfilledBias, Dictionary<string, float> fulfilledBiasProductToOrder)
    {
        float runningTotal = 0;
        //The running total helps to find out which product type bracket the "randomFloatInFulfilledBias" relates to.
        string productNameToGenerateOrder = "";
        for (int u = 0; u < fulfilledBiasProductToOrder.Count; u++)
        {
            var item = fulfilledBiasProductToOrder.ElementAt(u);
            var itemKey = item.Key;
            var itemValue = item.Value;
            if (randomFloatInFulfilledBias >= runningTotal && randomFloatInFulfilledBias < (runningTotal + itemValue))
            {
                productNameToGenerateOrder = itemKey;
            }
            runningTotal += itemValue;
        }
        return productNameToGenerateOrder;
    }

    public static float AdvertsToOrdersDeterminant(float netAdvert)
    {
        float determinant = (float)(Constants.Orders.minOrderLevel + (((float)netAdvert) / Constants.Orders.OrderDeterminantFactorPerAdSpend));
        Debug.Log("Determinant for orders is: " + determinant);
        return determinant;
    }

    public static bool CheckIfNewOrdersPresent()
    {
        List<Order> orders = MainScript.currentMainScript.orders;
        for (int i = 0; i < orders.Count; i++)
        {
            if (orders[i].declined == false && orders[i].accepted == false && orders[i].lost == false)
            {
                return true;
            }

        }
        return false;
    }

    public bool CheckIfAnyDeliveriesDelayed(float elapsedTime, List<Order> orders)
    {
        for (int i = 0; i < orders.Count; i++)
        {
            if (orders[i].declined == false && orders[i].accepted == true && orders[i].lost == false && (elapsedTime - orders[i].orderAcceptedTime > 8 * 60) && orders[i].fulfilled == false)
            {
                return true;
            }
        }
        return false;
    }

    public void CheckStateOrders()
    {
        for (int k = 0; k < mainScript.orders.Count; k++)
        {
            Order thisOrder = mainScript.orders[k];
            if (((mainScript.elapsedTime - thisOrder.orderCreated) > 3 * 60) && thisOrder.lost == false && thisOrder.accepted == false && thisOrder.declined == false)
            {
                thisOrder.lost = true;
                //  MainScript.currentMainScript.badgesPoints.addCustomerServicePointsEarnedForOrderAcceptedOrRejectedOrLost(thisOrder);
                float goodwillLoss = Constants.Orders.goodWillLostFromNeglectedNewOrder;
                mainScript.goodwill -= goodwillLoss;
                mainScript.mainNotificationsToAnimate.Add(new MainAnimatedNotification("Lost new order. Goodwill dropped."));

            }
            if (thisOrder.accepted == true)
            { // Penalty check to follow
                if (((mainScript.elapsedTime - thisOrder.orderAcceptedTime) > Constants.Timing.penaltyPeriodLateDeliverySecs) && thisOrder.accepted == true && thisOrder.fulfilled == false && thisOrder.lost == false && thisOrder.penalty == 0)
                {
                    float penalty = Constants.Orders.penaltyFromDelayedDelivery;
                    thisOrder.penalty = penalty;
                    string penaltyPercen = ((int)(penalty * 100)) + "%";
                    mainScript.mainNotificationsToAnimate.Add(new MainAnimatedNotification("Late delivery. Penalty:" + penaltyPercen));
                    float goodwillLoss = Constants.Orders.goodWillPenaltyFromDelayedDelivery;
                    mainScript.goodwill -= goodwillLoss;
                }
            }
        }
    }

    public void SetLastDisplayedOrders()
    {
        lastDisplayedOrders = new List<Order>();
        for (int k = 0; k < ordersAtEnquiryStage.Count; k++)
        {
            Order thisOrder = ordersAtEnquiryStage[k];
            if (thisOrder.declined == false && thisOrder.accepted == false && thisOrder.lost == false)
            {
                Order tempOrder = new Order();
                tempOrder.orderNum = thisOrder.orderNum;
                tempOrder.accepted = thisOrder.accepted;
                tempOrder.declined = thisOrder.declined;
                tempOrder.lost = thisOrder.lost;
                tempOrder.customerEmotionStatusEnquiry = thisOrder.customerEmotionStatusEnquiry;
                lastDisplayedOrders.Add(tempOrder);
            }
        }
    }
    public bool OrderToDisplayChanged()
    {
        if (lastDisplayedOrders.Count == ordersAtEnquiryStage.Count)
        {
            if (OrderNumsToDisplayMatchLastDisplayed(ordersAtEnquiryStage, lastDisplayedOrders))
            {
                for (int k = 0; k < ordersAtEnquiryStage.Count; k++)
                {
                    Order thisOrderToDisplay = ordersAtEnquiryStage[k];
                    Order thisLastOrder = lastDisplayedOrders[k];
                    if (thisOrderToDisplay.accepted != thisLastOrder.accepted)
                    {
                        return true;
                    }
                    if (thisOrderToDisplay.declined != thisLastOrder.declined)
                    {
                        return true;
                    }
                    if (thisOrderToDisplay.lost != thisLastOrder.lost)
                    {
                        return true;
                    }
                    if (thisOrderToDisplay.customerEmotionStatusEnquiry != thisLastOrder.customerEmotionStatusEnquiry)
                    {
                        return true;
                    }
                }
            }
            else
            {
                return true;
            }
        }
        else
        {
            return true;
        }
        return false;
    }

    public bool OrderNumsToDisplayMatchLastDisplayed(List<Order> ordersToDisplay, List<Order> lastDisplayedOrders)
    {
        for (int i = 0; i < ordersToDisplay.Count; i++)
        {
            bool thisLoopFound = false;
            Order thisOrder = ordersToDisplay[i];
            for (int j = 0; j < lastDisplayedOrders.Count; j++)
            {
                if (thisOrder.orderNum == lastDisplayedOrders[j].orderNum)
                {
                    thisLoopFound = true;
                }
            }
            if (thisLoopFound == false)
            {
                return false;
            }
        }
        return true;
    }


    public void UpdateOrdersAtEnquiryStageListAndCustomerSatisfaction()
    {
        ordersAtEnquiryStage = GetOrdersAtEnquiryStage();
        SetCustomerEmotionStatus(ordersAtEnquiryStage);
    }
    public List<Order> GetOrdersAtEnquiryStage()
    {
        List<Order> orders = MainScript.currentMainScript.orders;
        ordersAtEnquiryStage = new List<Order>();

        for (int k = 0; k < orders.Count; k++)
        {
            Order thisOrder = orders[k];
            if (thisOrder.declined == false && thisOrder.accepted == false && thisOrder.lost == false)
            {
                ordersAtEnquiryStage.Add(thisOrder);
            }
        }
        return ordersAtEnquiryStage;
    }

    public void SetCustomerEmotionStatus(List<Order> ordersAtEnquiryStage)
    {
        for (int i = 0; i < ordersAtEnquiryStage.Count; i++)
        {
            Order thisOrder = ordersAtEnquiryStage[i];
            float timeSinceOrderAccepted = MainScript.currentMainScript.elapsedTime - thisOrder.orderCreated;
            if (timeSinceOrderAccepted > 1 * 60 && timeSinceOrderAccepted < 2 * 60)
            {
                thisOrder.customerEmotionStatusEnquiry = Common.CustomerEmotionStatus.NoComment;
            }
            if (timeSinceOrderAccepted >= 2 * 60)
            {
                thisOrder.customerEmotionStatusEnquiry = Common.CustomerEmotionStatus.Sad;
            }
        }
    }

    public static int OrdersAcceptedCount(List<Order> orders)
    {
        int acceptedCounter = 0;
        for (int i = 0; i < orders.Count; i++)
        {
            if (orders[i].accepted == true)
            {
                acceptedCounter++;
            }
        }
        return acceptedCounter;
    }

      public static int OrdersDeclinedCount(List<Order> orders)
    {
        int declinedCounter = 0;
        for (int i = 0; i < orders.Count; i++)
        {
            if (orders[i].declined == true)
            {
                declinedCounter++;
            }
        }
        return declinedCounter;
    }

     public static int OrdersLostCount(List<Order> orders)
    {
        int lostCounter = 0;
        for (int i = 0; i < orders.Count; i++)
        {
            if (orders[i].lost == true)
            {
                lostCounter++;
            }
        }
        return lostCounter;
    }
}