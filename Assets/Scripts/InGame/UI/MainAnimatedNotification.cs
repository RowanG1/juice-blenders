using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;

public class MainAnimatedNotification
{
    public string message;
    public float score;
    public int notificationType;

    public MainAnimatedNotification(string message, float score)
    {
        this.message = message;
        this.score = score;
        this.notificationType = (int)Common.MainAnimatedNotificationType.Score;
    }

    public MainAnimatedNotification(string message)
    {
        this.message = message;
        this.notificationType = (int)Common.MainAnimatedNotificationType.PlainMessage;
    }

    public static bool ExistingNotificationsContainsText(string text, List<MainAnimatedNotification> notifications, MainAnimatedNotification currentAnimatedNotification)
    {
        foreach (MainAnimatedNotification notification in notifications)
        {
            if (notification.message.Contains(text))
            {
                return true;
            }
        }
		if (currentAnimatedNotification != null) {
		if (currentAnimatedNotification.message.Contains(text)) {
			return true;
		}
		}
        return false;
    }
}