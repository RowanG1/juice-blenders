﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EquipmentTapShort
{

    MainScript mainScript;
    public static int indexOfSelectedObjectArrayInOperations = -1;
    public static string selectedObjectTypeInOperations;

    public EquipmentTapShort()
    {
        mainScript = MainScript.currentMainScript;
    }

    public void ShortTapCheckAndHandleOnPointerUp()
    {
        //	print("Short valve tap script executing");
        // Check to see if valve is hit

        Vector2 pointPosition = new Vector2();
#if (UNITY_EDITOR || UNITY_STANDALONE_WIN)
        if (Input.GetMouseButtonUp(0))
        {
            Vector3 getMousePos = Input.mousePosition;
            pointPosition = new Vector2(getMousePos.x, getMousePos.y);
        }

#endif
#if (UNITY_ANDROID || UNITY_IPHONE)
        if (Input.touchCount == 1 && Input.GetTouch(0).phase == TouchPhase.Ended)
        {
            pointPosition = Input.GetTouch(0).position;
        }
#endif
        if (pointPosition != Vector2.zero)
        {
            var ray = Camera.main.ScreenPointToRay(pointPosition);
            //print ("Code for raycast entered");
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, 35))
            {
                LoopValveCheck(hit);
                LoopEquipmentUpgradeDot(hit);
            }
        }

    }

    void LoopEquipmentUpgradeDot(RaycastHit hit)
    {
        foreach (GameObject equipmentDot in GameObject.FindGameObjectsWithTag("EquipmentUpgradeDot"))
        {
            if (hit.transform.gameObject == equipmentDot && UIInputHandlerOperations.oneTouchActiveNotMoved == true)
            {
                EquipmentUpgradeDot equipmentUpgradeDot = equipmentDot.GetComponent<EquipmentUpgradeDot>();
                //mainScript.equipmentArea.GetComponent<EquipmentAreaScript>().TankOrValvePressedInOperations(equipmentUpgradeDot.index, equipmentUpgradeDot.equipmentType);
                TankOrValvePressedInOperations(equipmentUpgradeDot.index, equipmentUpgradeDot.equipmentType);

                MenuBtnFuncs.GoToMenu("Equipment");
            }
        }
    }

    void TankOrValvePressedInOperations(int indexOfObjectArray, string selectedObjectType)
    {
        indexOfSelectedObjectArrayInOperations = indexOfObjectArray;
        selectedObjectTypeInOperations = selectedObjectType;


    }

    void LoopValveCheck(RaycastHit hit)
    {

        for (int k = 0; k < mainScript.valveInstances.Count; k++)
        {
            GameObject thisValveInst = mainScript.valveInstances[k];
            //	print("This valve instance " + k);
            if (hit.transform.gameObject == thisValveInst && PanScript.opsDragging == false && UIInputHandlerOperations.oneTouchActiveNotMoved == true && FlashingIconButtonScript.iconStartedBeingTouched == false)
            {
                int valveIndex = thisValveInst.GetComponent<ValveScript>().valveIndex;
                Valve thisValve = mainScript.valveData[valveIndex];
                string tankNameValveAttachedToLowerCse = thisValve.tankNameValveAttachedTo.ToLower();

                if (thisValve.valveState == (int)Common.ValveState.Open)
                {
                    thisValve.valveState = (int)Common.ValveState.Closed;
                }
                else if (thisValve.valveState == (int)Common.ValveState.Waiting)
                {
                    thisValve.valveState = (int)Common.ValveState.Closed;
                    mainScript.delayActionHandler.CancelValveWaitingDelayAction(valveIndex, mainScript.delayActions);
                }
                else // Valve is currently closed
                {
                    if (mainScript.tank_And_Valve_Physical_Updates.TankIsEmpty(mainScript.tankData[thisValve.tankIndexValveAttachedTo]))
                    {
                        string emptyTankNotif = "Tank is empty. Valve not opened.";
                        if (!MainAnimatedNotification.ExistingNotificationsContainsText(emptyTankNotif, mainScript.mainNotificationsToAnimate, mainScript.currentAnimatedNotification))
                        {
                            mainScript.mainNotificationsToAnimate.Add(new MainAnimatedNotification(emptyTankNotif));
                        }
                        return;
                    }
                    if (thisValve.valveDestination != Constants.Valves.drainCode) {
                        if (mainScript.tank_And_Valve_Physical_Updates.TankIsFull(mainScript.tankData[thisValve.valveDestinationTankIndex]))
                        {
                            string fullTankNotif = "Destination tank is full. Valve not opened.";
                            if (!MainAnimatedNotification.ExistingNotificationsContainsText(fullTankNotif, mainScript.mainNotificationsToAnimate, mainScript.currentAnimatedNotification))
                            {
                                mainScript.mainNotificationsToAnimate.Add(new MainAnimatedNotification(fullTankNotif));
                            }
                            return;
                        }
                    }

                    if (tankNameValveAttachedToLowerCse.Contains(Constants.Tanks.supplyCode))
                    {
                        thisValve.valveState = (int)Common.ValveState.Waiting;
                        mainScript.delayActions.Add(new DelayAction(Constants.Delays.valveOnAfterWaiting, UnityEngine.Random.Range(0, 5) + 5, valveIndex));
                    }
                    else
                    {
                        thisValve.valveState = (int)Common.ValveState.Open;

                    }
                }
                return;
            }
        }
    }
}
