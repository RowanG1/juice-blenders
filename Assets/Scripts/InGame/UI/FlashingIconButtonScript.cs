﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;

public class FlashingIconButtonScript : MonoBehaviour {
	public string btnName;
	public static bool iconStartedBeingTouched = false;
	Camera mainCamera;
	// Use this for initialization
	void Start () {
		Button thisBut = (transform.gameObject.GetComponent<Button>());
		thisBut.onClick.AddListener (() => OnBtnClick ());
		mainCamera = Camera.main;
	}
	
	// Update is called once per frame
	void Update () {
		if (EventSystem.current.IsPointerOverGameObject () && Input.GetMouseButtonDown (0)) {
			if (EventSystem.current.currentSelectedGameObject) {
				if (EventSystem.current.currentSelectedGameObject.GetComponent<CanvasRenderer> () && EventSystem.current.currentSelectedGameObject.tag == "FlashingIcon") {
					iconStartedBeingTouched = true;
				//	print ("Flashing icon button down"); // Also Had to give the back strip a separate pointer down event
				}
			}
		}

		if (Input.GetMouseButtonUp (0)) {
			iconStartedBeingTouched = false;
			//menuIsScrolling = false;
		//	print ("Drag strip button up");
		}
	}

	IEnumerator ScrollOperationsRightBriefly() { //Pan the factory to show trucks waiting
		int totalScrollDistance = 1200;
		float totalTime = 2.5f;
		float timeSinceAnimStart = 0f;
		while (timeSinceAnimStart < totalTime) {
			Vector3 tempScrollPosition = mainCamera.transform.localPosition;
			tempScrollPosition.x += totalScrollDistance * Time.deltaTime / totalTime;
			mainCamera.transform.localPosition = tempScrollPosition;
			timeSinceAnimStart += Time.deltaTime;
			yield return 0;
		}
	}

	public void OnBtnClick() {

		UIInputHandlerOperations.oneTouchActiveNotMoved = false;
		iconStartedBeingTouched = false;
		if (MainScript.currentMainScript.elapsedTime - MainScript.currentMainScript.lastTimeActiveFlashingIconPanelsUpdated > 0.65f) {
		switch (this.btnName) {
			case "NewOrder":
				MenuBtnFuncs.GoToMenu("Orders");		
				break;
			case "TruckNeedsFilling":
					float currentCamerazPosition = mainCamera.transform.localPosition.z;
					mainCamera.transform.localPosition = new Vector3(200, -1300, currentCamerazPosition);
					mainCamera.GetComponent<Camera>().orthographicSize = 600;
					StartCoroutine(ScrollOperationsRightBriefly());
				break;
			case "DeliveryDelay":
					MenuBtnFuncs.GoToMenu("Deliveries");
			break;
			case "BrandMarketingNeeded":
					MenuBtnFuncs.GoToMenu("Marketing");
			break;
			case "ProcurementNeeded":
					MenuBtnFuncs.GoToMenu("Procurement");
			break;
			case "EquipmentUpgradeNeeded":
				MainScript.currentMainScript.uIUpdatesInGame.addEquipmentUpgradeDots();
			break;
			case "LowBankBalance":
				MenuBtnFuncs.GoToMenu("Finance");
			break;
			default:
			break;
		}
	}
	}

}
