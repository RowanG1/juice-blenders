﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class BankruptHandler : MonoBehaviour
{

    public void OKClicked()
    {
        Common.PlayButtonSound();
        SceneManager.LoadScene("MainMenu");
    }
}
