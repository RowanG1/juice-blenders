﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;


public class DragCustom : MonoBehaviour, IBeginDragHandler, IEndDragHandler, IDragHandler {
	public static bool menuIsScrolling = false;


	void Start() {

	}
	void Update() {
		if (EventSystem.current.IsPointerOverGameObject () && Input.GetMouseButtonDown (0)) {
			if (EventSystem.current.currentSelectedGameObject) {
				if (EventSystem.current.currentSelectedGameObject.GetComponent<CanvasRenderer> () && EventSystem.current.currentSelectedGameObject.tag == "MenuStrip") {
					TabStripScript.menuStartedBeingTouched = true;
					// Also Had to give the back strip a separate pointer down event
				}
			}
		}

		if (Input.GetMouseButtonUp (0)) {
			TabStripScript.menuStartedBeingTouched = false;
			menuIsScrolling = false;
		//	print ("Drag strip button up");
		}
	}

	#region IBeginDragHandler implementation

	void IBeginDragHandler.OnBeginDrag (PointerEventData eventData)
	{
		//throw new System.NotImplementedException ();
	//	print ("Started Dragging");
		menuIsScrolling = true;
	}

	#endregion

	#region IDragHandler implementation

	void IDragHandler.OnDrag (PointerEventData eventData)
	{
	//	throw new System.NotImplementedException ();
	//	print("Dragging");
	}

	#endregion

	#region IEndDragHandler implementation

	void IEndDragHandler.OnEndDrag (PointerEventData eventData)
	{
		//throw new System.NotImplementedException ();
		print ("Ended Dragging");
		menuIsScrolling = false;
		TabStripScript.menuStartedBeingTouched = false;
	}

	#endregion



	
}
