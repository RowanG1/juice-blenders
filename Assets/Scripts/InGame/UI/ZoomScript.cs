﻿using UnityEngine;


public class ZoomScript : MonoBehaviour
{
	public float perspectiveZoomSpeed = 14f;        // The rate of change of the field of view in perspective mode.
	public float orthoZoomSpeed = 14f;        // The rate of change of the orthographic size in orthographic mode.
	public Camera mainCamera;
	//float origCameraSize;


	void Start() {
		//origCameraSize = mainCamera.orthographicSize;
	}

	void Update()
	{
		// If there are two touches on the device...
		#if (UNITY_EDITOR || UNITY_STANDALONE_WIN)

		float scrollDeltaMag = -Input.GetAxis("Mouse ScrollWheel");

		// Find the difference in the distances between each frame.
	
		// If the camera is orthographic...
		if (mainCamera.orthographic)
		{
			
			// ... change the orthographic size based on the change in distance between the touches.
			mainCamera.orthographicSize += scrollDeltaMag * 60f;

			// Make sure the orthographic size never drops below zero.
			mainCamera.orthographicSize = Mathf.Max(mainCamera.orthographicSize, 80);
			mainCamera.orthographicSize = Mathf.Min(mainCamera.orthographicSize, 670);

		}
		#endif

		#if (UNITY_IPHONE || UNITY_ANDROID)
		if (Input.touchCount == 2)
		{
			print("Zoom seems to be activating");
			// Store both touches.
			Touch touchZero = Input.GetTouch(0);
			Touch touchOne = Input.GetTouch(1);

			// Find the position in the previous frame of each touch.
			Vector2 touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
			Vector2 touchOnePrevPos = touchOne.position - touchOne.deltaPosition;

			// Find the magnitude of the vector (the distance) between the touches in each frame.
			float prevTouchDeltaMag = (touchZeroPrevPos - touchOnePrevPos).magnitude;
			float touchDeltaMag = (touchZero.position - touchOne.position).magnitude;

			// Find the difference in the distances between each frame.
			float deltaMagnitudeDiff = prevTouchDeltaMag - touchDeltaMag;

			// If the camera is orthographic...
			if (mainCamera.orthographic)
			{
				// ... change the orthographic size based on the change in distance between the touches.
				mainCamera.orthographicSize += deltaMagnitudeDiff * orthoZoomSpeed;

				// Make sure the orthographic size never drops below zero.
				mainCamera.orthographicSize = Mathf.Max(mainCamera.orthographicSize, 80);
				mainCamera.orthographicSize = Mathf.Min(mainCamera.orthographicSize, 670);

			}
			else
			{
				// Otherwise change the field of view based on the change in distance between the touches.
				mainCamera.fieldOfView += deltaMagnitudeDiff * perspectiveZoomSpeed;

				// Clamp the field of view to make sure it's between 0 and 180.
				mainCamera.fieldOfView = Mathf.Clamp(mainCamera.fieldOfView, 0.1f, 179.9f);
			}
		}
		#endif
	}
}