﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TabStripScript : MonoBehaviour {
	public static bool menuStartedBeingTouched = false;
	// Use this for initialization
	void Start () {
		Vector2 oldSizeDelta = transform.GetComponent<RectTransform> ().sizeDelta;
		transform.GetComponent<RectTransform> ().sizeDelta = new Vector2(oldSizeDelta.x, Common.tabStripHeight);
		transform.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (0, 0);
		 foreach (Transform tab in GameObject.Find (Common.tabIconsGameObjectPath).transform) {
			tab.GetComponent<LayoutElement>().preferredWidth = Common.tabStripHeight;
		}
		//GameObject.Find ("TargetsButton").GetComponent<RectTransform>().sizeDelta = new Vector2(Common.tabStripHeight, 0);
		//GameObject.Find ("TargetsButton").GetComponent<RectTransform>().anchoredPosition = new Vector2(0, 0);

	}
	

	public void MakeMenuStartedTouchTrue() {
		if (Input.GetMouseButtonDown (0)) {
			menuStartedBeingTouched = true;
			print ("Menu strip backing clicked");
		}
	}

}
