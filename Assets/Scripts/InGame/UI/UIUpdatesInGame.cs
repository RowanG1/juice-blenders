﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIUpdatesInGame : MonoBehaviour
{
    public Canvas canvas;
    public MainScript mainScript;
    public GameObject equipmentUpgradeDotPrefab;
    public List<GameObject> equipmentUpgradeDots;
    public GameObject equipmentUpgradeMsg;
    public GameObject equipmentUpgradeMsgPanel;
    public GameObject indicatorPanel;

    public float lastTimeEquipmentDotUpgradesStartedShowing = -1;
    int equipmentUpgradeMsgDuration = 4;

    float equipmentUpgradeDotCounter;

    public void Start()
    {
        mainScript = MainScript.currentMainScript;
    }

    public void Update()
    {
        handleEquipmentDotUpgradeMsgPanelVisible();
    }

    void handleEquipmentDotUpgradeMsgPanelVisible()
    {
        if ((int)lastTimeEquipmentDotUpgradesStartedShowing != -1)
        {

            if (mainScript.elapsedTime - lastTimeEquipmentDotUpgradesStartedShowing < equipmentUpgradeMsgDuration)
            {
                if ((mainScript.operations.activeSelf == true) && (equipmentUpgradeMsgPanel.activeSelf == false))
                {
                    equipmentUpgradeMsgPanel.SetActive(true);
                }
                float fontSize = equipmentUpgradeMsg.GetComponent<Text>().cachedTextGenerator.fontSizeUsedForBestFit;
                float canvasScaleFac = canvas.scaleFactor;
                equipmentUpgradeMsg.GetComponent<Outline>().effectDistance = new Vector2(fontSize / 18 / canvasScaleFac, -fontSize / 18 / canvasScaleFac);

            }
            if (mainScript.elapsedTime - lastTimeEquipmentDotUpgradesStartedShowing >= equipmentUpgradeMsgDuration)
            {
                equipmentUpgradeMsgPanel.SetActive(false);
            }
            if (mainScript.elapsedTime - lastTimeEquipmentDotUpgradesStartedShowing > 30)
            {
                destroyAllEquipmentUpgradeDots();
                lastTimeEquipmentDotUpgradesStartedShowing = -1;
            }
        }
    }

    public void setTabStripContentDimensions()
    {
        float scrollBarHeight = 1f;
        GameObject tabIcons = GameObject.Find("TabIcons");
        RectTransform rectTransform = tabIcons.GetComponent<RectTransform>();
        // rectTransform.offsetMin = new Vector2(0, 1.5f);
        float tabIconWidth = 9f;
        int activeIconCount = 0;
        foreach (Transform tabIcon in tabIcons.transform)
        {
            tabIcon.GetComponent<LayoutElement>().preferredWidth = 9;
            tabIcon.GetComponent<LayoutElement>().minWidth = 9;
            if (tabIcon.gameObject.activeSelf == true)
            {
                activeIconCount += 1;
            }
        }

        float contentWidth = activeIconCount * (tabIconWidth + tabIcons.GetComponent<HorizontalLayoutGroup>().spacing) + tabIcons.GetComponent<HorizontalLayoutGroup>().padding.left;
        float initialTabIconBottomOffset = rectTransform.offsetMin.y;
        rectTransform.sizeDelta = new Vector2(contentWidth, 0);
        rectTransform.offsetMax = new Vector2(rectTransform.offsetMax.x, 0);
        rectTransform.offsetMin = new Vector2(0, scrollBarHeight);

        RectTransform rectTransformScrollBar = GameObject.Find("TabScrollPanel/ScrollBarPanel").GetComponent<RectTransform>();
        rectTransformScrollBar.sizeDelta = new Vector2(0, scrollBarHeight);

    }

    public void updateTimeText()
    {
        mainScript.timeLeftText.GetComponent<Text>().text = mainScript.minsRemaining + "min left";
    }
    public void setInitialUINotifications()
    {
        float widthUI = mainScript.UICanvas.GetComponent<RectTransform>().rect.width;
        float heightUI = mainScript.UICanvas.GetComponent<RectTransform>().rect.height;
        float minDimUI = Mathf.Min(widthUI, heightUI);
        mainScript.minDimUI = minDimUI;

        Vector2 pos = mainScript.newAnimatedNotifUI.GetComponent<RectTransform>().anchoredPosition;
        pos.y -= minDimUI / 10;
        pos.x -= minDimUI / 20;
        mainScript.newAnimatedNotifUI.GetComponent<RectTransform>().anchoredPosition = pos;
        mainScript.newAnimatedNotifUI.GetComponent<RectTransform>().sizeDelta = new Vector2(minDimUI * 0.7f, minDimUI * 0.7f * 0.25f);

        mainScript.newAnimatedNotifUI.transform.GetChild(0).gameObject.GetComponent<Outline>().effectDistance = new Vector2(minDimUI * 0.7f * 0.25f * 1f / 30, -minDimUI * 0.7f * 0.25f * 1f / 30);
        mainScript.newAnimatedNotifUI.transform.GetChild(1).gameObject.GetComponent<Outline>().effectDistance = new Vector2(minDimUI * 0.7f * 0.25f * 1f / 30, -minDimUI * 0.7f * 0.25f * 1f / 30);

    }

    // Use this for initialization
    public IEnumerator animateMainNotification(MainAnimatedNotification mainAnimatedNotif, GameObject newAnimatedNotifUI)
    {
        if (mainAnimatedNotif.notificationType == (int)Common.MainAnimatedNotificationType.PlainMessage)
        {
            newAnimatedNotifUI.transform.GetChild(0).gameObject.GetComponent<Text>().color = Color.black;
            newAnimatedNotifUI.transform.GetChild(0).gameObject.GetComponent<Text>().text = "" + mainAnimatedNotif.message;
            newAnimatedNotifUI.transform.GetChild(0).gameObject.GetComponent<RectTransform>().anchorMin = new Vector2(0, 0);
            newAnimatedNotifUI.transform.GetChild(1).gameObject.GetComponent<Text>().text = "";
            //yield break;
        }
        mainScript.animatedScoreIsBusy = true;
        float totalAnimDuration = 3f;
        float totalMoveY = MainScript.currentMainScript.minDimUI / 6;

        string directionSymbol = "";
        float currentTime = 0;
        if (mainAnimatedNotif.notificationType == (int)Common.MainAnimatedNotificationType.Score)
        {
            newAnimatedNotifUI.transform.GetChild(0).gameObject.GetComponent<RectTransform>().anchorMin = new Vector2(0, 0.35f);
            newAnimatedNotifUI.transform.GetChild(1).gameObject.GetComponent<Text>().text = "" + mainAnimatedNotif.message;
            if (mainAnimatedNotif.score > 0)
            {
                directionSymbol = "▲";
                newAnimatedNotifUI.transform.GetChild(0).gameObject.GetComponent<Text>().color = Color.green;
            }
            else
            {
                directionSymbol = "▼";
                newAnimatedNotifUI.transform.GetChild(0).gameObject.GetComponent<Text>().color = new Color(179f / 255, 71f / 255, 26f / 255);
            }
            newAnimatedNotifUI.transform.GetChild(0).gameObject.GetComponent<Text>().text = directionSymbol + "$" + Mathf.Abs(mainAnimatedNotif.score);
        }
        newAnimatedNotifUI.transform.GetChild(1).gameObject.GetComponent<Text>().color = Color.black;

        newAnimatedNotifUI.SetActive(true);
        Vector2 origPos = newAnimatedNotifUI.GetComponent<RectTransform>().anchoredPosition;
        while (currentTime < totalAnimDuration)
        {
            Vector2 pos = newAnimatedNotifUI.GetComponent<RectTransform>().anchoredPosition;
            pos.y += totalMoveY / totalAnimDuration * Time.deltaTime;
            //newAnimatedNotifUI.transform.position = pos;
            newAnimatedNotifUI.GetComponent<RectTransform>().anchoredPosition = pos;
            if (currentTime >= totalAnimDuration * 0.6f)
            {
                var tempColorValue = newAnimatedNotifUI.transform.GetChild(0).gameObject.GetComponent<Text>().color;
                tempColorValue.a -= 1 * Time.deltaTime / (totalAnimDuration * 0.4f);
                newAnimatedNotifUI.transform.GetChild(0).gameObject.GetComponent<Text>().color = tempColorValue;

                var tempColorDesc = newAnimatedNotifUI.transform.GetChild(1).gameObject.GetComponent<Text>().color;
                tempColorDesc.a -= 1 * Time.deltaTime / (totalAnimDuration * 0.4f);
                newAnimatedNotifUI.transform.GetChild(1).gameObject.GetComponent<Text>().color = tempColorDesc;

            }
            currentTime += Time.deltaTime;
            yield return 0;
        }
        MainScript.currentMainScript.animatedScoreIsBusy = false;
        newAnimatedNotifUI.SetActive(false);
        newAnimatedNotifUI.GetComponent<RectTransform>().anchoredPosition = origPos;
        MainScript.currentMainScript.currentAnimatedNotification = null;
    }

    public void animateMainNotificationCall(MainAnimatedNotification mainAnimatedNotif, GameObject newAnimatedNotifUI)
    {
        StartCoroutine(animateMainNotification(mainAnimatedNotif, newAnimatedNotifUI));
    }

    public void addEquipmentUpgradeDots()
    {
        destroyAllEquipmentUpgradeDots();
        addValveUpgradeDots();
        addTankUpgradeDots();
        setTextEquipmentUpgradeMsgPanel("Select tank/valve to upgrade");
        equipmentUpgradeMsgPanel.SetActive(true);
        Camera.main.orthographicSize = 670;
    }

    public void setTextEquipmentUpgradeMsgPanel(string text)
    {
        equipmentUpgradeMsg.GetComponent<Text>().text = text;
    }

    public void destroyAllEquipmentUpgradeDots()
    {
        foreach (GameObject equipmentUpgradeDot in equipmentUpgradeDots)
        {
            Destroy(equipmentUpgradeDot);
        }
        equipmentUpgradeDots = new List<GameObject>();
    }

    public void addValveUpgradeDotsAndDestroyExistingEquipmentDots()
    {
        destroyAllEquipmentUpgradeDots();
        addValveUpgradeDots();
        equipmentUpgradeMsgPanel.SetActive(true);
        Camera.main.orthographicSize = 670;
    }

    public void addTankUpgradeDotsAndDestroyExistingEquipmentDots()
    {
        destroyAllEquipmentUpgradeDots();
        addTankUpgradeDots();
        equipmentUpgradeMsgPanel.SetActive(true);
        Camera.main.orthographicSize = 670;
    }

    public void addValveUpgradeDots()
    {
        lastTimeEquipmentDotUpgradesStartedShowing = mainScript.elapsedTime;
        var valvesLackingUpgradesByIndex = mainScript.tank_And_Valve_Physical_Updates.ValvesLackUpgradesByIndex();
        for (int i = 0; i < mainScript.valveData.Count; i++)
        {
            Valve thisValve = mainScript.valveData[i];
            if (thisValve.valveDestination != Constants.Valves.drainCode && !thisValve.upgradeRequested)
            {
                GameObject thisDot = Instantiate(equipmentUpgradeDotPrefab);
                thisDot.transform.parent = mainScript.operations.transform;
                Vector3 valvePosition = mainScript.valveInstances[i].transform.position;
                Vector3 thisDotPosition = new Vector3(valvePosition.x, valvePosition.y - 30, valvePosition.z - 0.6f);
                thisDot.transform.position = thisDotPosition;

                thisDot.GetComponent<EquipmentUpgradeDot>().index = i;
                thisDot.GetComponent<EquipmentUpgradeDot>().equipmentType = "valve";
                equipmentUpgradeDots.Add(thisDot);
            }
        }
    }

    public void addTankUpgradeDots()
    {
        lastTimeEquipmentDotUpgradesStartedShowing = mainScript.elapsedTime;

        var tanksLackingUpgradesByIndex = mainScript.tank_And_Valve_Physical_Updates.TanksLackUpgradesByIndex();

        for (int i = 0; i < mainScript.tankData.Count; i++)
        {
            TankData thisTank = mainScript.tankData[i];
            if (!thisTank.tankName.ToLower().Contains(Constants.Tanks.supplyCode) && !thisTank.tankName.ToLower().Contains(Constants.Tanks.outletCode) && !thisTank.upgradeRequested)
            {
                GameObject thisDot = Instantiate(equipmentUpgradeDotPrefab);
                thisDot.transform.parent = mainScript.operations.transform;
                Vector3 tankPosition = mainScript.tankInstances[i].transform.position;
                Vector3 thisDotPosition = new Vector3(tankPosition.x, tankPosition.y, tankPosition.z - 3.6f);
                thisDot.transform.position = thisDotPosition;

                thisDot.transform.localScale = new Vector3(50, 50, 2);
                thisDot.GetComponent<EquipmentUpgradeDot>().index = i;
                thisDot.GetComponent<EquipmentUpgradeDot>().equipmentType = "tank";
                equipmentUpgradeDots.Add(thisDot);
            }
        }
    }


    public void setJuiceGraphics()
    {
        for (int j = 0; j < mainScript.tankData.Count; j++)
        {
            TankData thisTank = mainScript.tankData[j];
            GameObject thisTankInstance = mainScript.tankInstances[j];
            GameObject juice = thisTankInstance.GetComponent<TankGameObject>().JuiceLevel;
            Transform juiceTransform = juice.transform;
            var newScale = thisTank.tankLevelFrac;
            juiceTransform.localScale = new Vector3(1, newScale, 1);
            juiceTransform.localPosition = new Vector3(0, -0.5f + newScale / 2, -0.1f);

            List<Color> colors = new List<Color>();
            List<float> actualFracs = new List<float>();
            for (int k = 0; k < thisTank.tankJuiceData.Count; k++)
            {
                actualFracs.Add(thisTank.tankJuiceData[k].actualFrac);
                colors.Add(Common.GetJuiceColor(thisTank.tankJuiceData[k].juiceName, 1));
            }

            Color mixedJuiceColor = Common.CombineColors(colors, actualFracs);

            for (int k = 0; k < mainScript.valveData.Count; k++)
            {
                Valve thisValve = mainScript.valveData[k];
                GameObject thisValveInstance = mainScript.valveInstances[k];

                if (thisValve.valveDestination.ToLower() == (Constants.Valves.drainCode))
                {
                    var juiceDrainGush = thisTankInstance.GetComponent<TankGameObject>().juiceDrainGush;
                    if (thisValve.tankNameValveAttachedTo == thisTank.tankName && thisValve.valveState == (int)Common.ValveState.Open)
                    {

                        var main = juiceDrainGush.GetComponent<ParticleSystem>().main;
                        main.startColor = mixedJuiceColor;
                        if (juiceDrainGush.activeSelf == false)
                        {
                            juiceDrainGush.SetActive(true);

                        }
                    }
                    if (thisValve.tankNameValveAttachedTo == thisTank.tankName && thisValve.valveState == (int)Common.ValveState.Closed)
                    {
                        if (juiceDrainGush.activeSelf)
                        {
                            juiceDrainGush.SetActive(false);
                        }
                    }
                }
            }

            Renderer renderer = juice.GetComponentInChildren<Renderer>();
            renderer.material.color = mixedJuiceColor;

            for (int l = 0; l < mainScript.pipeInstances.Count; l++)
            {
                GameObject thisPipe = mainScript.pipeInstances[l];
                if (thisPipe.GetComponent<PipeScript>().startTankIndex == j)
                {
                    Renderer renderer2 = thisPipe.GetComponentInChildren<Renderer>();
                    if (mainScript.valveData[l].valveState == (int)Common.ValveState.Open)
                    {
                        renderer2.material.color = mixedJuiceColor;
                    }
                    else
                    {
                        renderer2.material.color = new Color(0, 0, 0);
                    }
                }
            }
        }

    }


    public void updateChartPositions()
    {
        for (int i = 0; i < mainScript.tankData.Count; i++)
        {
            TankData thisTank = mainScript.tankData[i];
            GameObject thisTankInst = mainScript.tankInstances[i];
            ThisTankChartObjects thisTankCharts = mainScript.tankCharts[i];
            for (int j = 0; j < thisTank.tankJuiceData.Count; j++)
            {
                TankJuiceData thisJuice = thisTank.tankJuiceData[j];
                Vector3 oldChartTargetStripPosition = thisTankCharts.Charts[j].GetComponent<SingleChartUI>().targetStrip.transform.localPosition;
                thisTankCharts.Charts[j].GetComponent<SingleChartUI>().targetStrip.transform.localPosition = new Vector3(oldChartTargetStripPosition.x, -0.5f + thisTank.tankJuiceData[j].targetFrac, oldChartTargetStripPosition.z);

                Vector3 oldChartActualBarPosition = thisTankCharts.Charts[j].GetComponent<SingleChartUI>().actualBar.transform.localPosition;
                Vector3 oldChartActualBarScale = thisTankCharts.Charts[j].GetComponent<SingleChartUI>().actualBar.transform.localScale;
                thisTankCharts.Charts[j].GetComponent<SingleChartUI>().actualBar.transform.localScale = new Vector3(oldChartActualBarScale.x, thisJuice.actualFrac, oldChartActualBarScale.z);

                thisTankCharts.Charts[j].GetComponent<SingleChartUI>().actualBar.transform.localPosition = new Vector3(oldChartActualBarPosition.x, -0.5f + thisJuice.actualFrac / 2, oldChartActualBarPosition.z);
            }
        }

    }

    public void setTrucksUI()
    {
        for (int i = 0; i < mainScript.tankData.Count; i++)
        {
            TankData thisTank = mainScript.tankData[i];
            GameObject thisTankInst = mainScript.tankInstances[i];
            if (thisTank.tankName.ToLower().Contains(Constants.Tanks.outletCode))
            {
                GameObject thisDelivTruckStatusInst = thisTankInst.GetComponent<TankGameObject>().truckStatus;
                if (thisTank.deliverTruckState == (int)Common.DeliverTruckState.Absent && !thisDelivTruckStatusInst.GetComponent<MeshRenderer>().material.name.Contains(mainScript.loadingZoneMaterial.name))
                {
                    thisDelivTruckStatusInst.GetComponent<MeshRenderer>().material = mainScript.loadingZoneMaterial;
                    thisTankInst.GetComponent<TankGameObject>().truckExhaust.SetActive(false);

                }
                if (thisTank.deliverTruckState == (int)Common.DeliverTruckState.Waiting && !thisDelivTruckStatusInst.GetComponent<MeshRenderer>().material.name.Contains(mainScript.eggMaterial.name))
                {
                    thisDelivTruckStatusInst.GetComponent<MeshRenderer>().material = mainScript.eggMaterial;
                }

                if (thisTank.deliverTruckState == (int)Common.DeliverTruckState.Present && !thisDelivTruckStatusInst.GetComponent<MeshRenderer>().material.name.Contains(mainScript.truckMaterial.name))
                {
                    thisDelivTruckStatusInst.GetComponent<MeshRenderer>().material = mainScript.truckMaterial;
                    thisTankInst.GetComponent<TankGameObject>().truckExhaust.SetActive(true);
                }
            }

            if (thisTank.tankName.ToLower().Contains(Constants.Tanks.supplyCode))
            {
                GameObject thisSupplyTruckStatusInst = thisTankInst.GetComponent<TankGameObject>().truckStatus;
                for (int k = 0; k < mainScript.valveData.Count; k++)
                {
                    Valve thisValve = mainScript.valveData[k];
                    if (thisValve.tankNameValveAttachedTo == thisTank.tankName)
                    {
                        if (thisValve.valveState == (int)Common.ValveState.Waiting && !thisSupplyTruckStatusInst.GetComponent<MeshRenderer>().material.name.Contains(mainScript.eggMaterial.name))
                        {
                            thisSupplyTruckStatusInst.GetComponent<MeshRenderer>().material = mainScript.eggMaterial;
                        }
                        if (thisValve.valveState == (int)Common.ValveState.Open && !thisSupplyTruckStatusInst.GetComponent<MeshRenderer>().material.name.Contains(mainScript.truckMaterial.name))
                        {
                            thisSupplyTruckStatusInst.GetComponent<MeshRenderer>().material = mainScript.truckMaterial;
                            thisTankInst.GetComponent<TankGameObject>().truckExhaust.SetActive(true);
                        }
                        if (thisValve.valveState == (int)Common.ValveState.Closed && !thisSupplyTruckStatusInst.GetComponent<MeshRenderer>().material.name.Contains(mainScript.loadingZoneMaterial.name))
                        {
                            thisSupplyTruckStatusInst.GetComponent<MeshRenderer>().material = mainScript.loadingZoneMaterial;
                            thisTankInst.GetComponent<TankGameObject>().truckExhaust.gameObject.SetActive(false);

                        }
                    }
                }
            }
        }
    }

    public void updateTruckLabels(TankData thisTank)
    {
        GameObject fillLabel = MainScript.currentMainScript.tankInstances[thisTank.tankIndex].GetComponent<TankGameObject>().truckOutletFillLabel;
        if (thisTank.deliverTruckState == (int)Common.DeliverTruckState.Present && fillLabel.activeSelf == false)
        {
            fillLabel.SetActive(true);
        }

        if (thisTank.deliverTruckState == (int)Common.DeliverTruckState.Absent && fillLabel.activeSelf == true)
        {
            fillLabel.gameObject.SetActive(false);
        }

        if (thisTank.deliverTruckState == (int)Common.DeliverTruckState.Present)
        {
            float tankFillVolume = Mathf.Floor(((thisTank.tankLevelFrac - Constants.Tanks.tankEmptyLevelFraction) * thisTank.tankCapacity) / 10) * 10;

            float orderVol = 0;
            for (int n = 0; n < MainScript.currentMainScript.toDeliverArraySorted.Count; n++)
            {
                Order thisOrder = MainScript.currentMainScript.orders[MainScript.currentMainScript.toDeliverArraySorted[n].Key];
                if ((thisOrder.truckArrived == true) && thisOrder.fulfilled == false && (thisTank.tankName == "Outlet-" + thisOrder.productName))
                {
                    orderVol = (float)thisOrder.volume;
                    //	print ("Order volume is: " + orderVol);
                }
            }
            string truckLabelText = (int)tankFillVolume + " of " + (int)orderVol + "ℓ";
            fillLabel.GetComponent<TextMesh>().text = truckLabelText;

        }
    }

    public void reDrawTanksIfNecessary()
    {
        for (int i = 0; i < mainScript.tankData.Count; i++)
        {
            TankData thisTank = mainScript.tankData[i];
            GameObject thisTankInstance = mainScript.tankInstances[i];
            if (thisTankInstance.GetComponent<TankGameObject>().tankShell.transform.localScale.y != thisTank.tankHeight)
            {
                thisTankInstance.GetComponent<TankGameObject>().tankShell.transform.localScale = new Vector3(thisTank.tankWidth, thisTank.tankHeight, 1);
                thisTankInstance.transform.position = new Vector3(thisTank.centerx, -thisTank.centery, 0);
                Transform thisChartArea = thisTankInstance.GetComponent<TankGameObject>().chartsArea.transform;
                thisChartArea.localPosition = new Vector3(thisChartArea.localPosition.x, -thisTank.tankHeight / 2 + thisChartArea.localScale.y / 2 + 5, thisChartArea.localPosition.z);
                foreach (Transform thisValve in thisTankInstance.GetComponent<TankGameObject>().valves.transform)
                {
                    if (mainScript.valveData[thisValve.GetComponent<ValveScript>().valveIndex].valveDestination.ToLower() == Constants.Valves.drainCode)
                    {
                        thisValve.localPosition = new Vector3(thisValve.localPosition.x, 0, 0);
                    }
                    else
                    {
                        thisValve.localPosition = new Vector3(thisValve.localPosition.x, -thisTank.tankHeight / 2 - Common.valveWidth / 2, 0);
                    }
                }

            }
        }
    }

    public void pumpUpgradePipeLoop(Valve thisValve, int lineThick)
    {
        for (int j = 0; j < mainScript.pipeInstances.Count; j++)
        {
            GameObject thisPipeInst = mainScript.pipeInstances[j];
            PipeScript thisPipeScript = thisPipeInst.GetComponent<PipeScript>();
            if (thisPipeScript.valveIndex == thisValve.valveIndex)
            {
                thisPipeInst.transform.localScale = new Vector3(lineThick * 20f / 8, thisPipeInst.transform.localScale.y, thisPipeInst.transform.localScale.z);
                return;
            }
        }
    }

    public void setValveColors()
    {
        for (int i = 0; i < mainScript.valveData.Count; i++)
        {
            Valve thisValve = mainScript.valveData[i];
            GameObject thisValveInst = mainScript.valveInstances[i];
            Color thisValveColor = thisValveInst.GetComponentInChildren<Renderer>().material.color;
            if (thisValve.valveState == (int)Common.ValveState.Open && !thisValveColor.Equals(Common.valveOpenColor))
            {
                thisValveInst.GetComponentInChildren<Renderer>().material.color = Common.valveOpenColor;
            }
            else if (thisValve.valveState == (int)Common.ValveState.Waiting && !thisValveColor.Equals(Common.valveWaitingColor))
            {
                thisValveInst.GetComponentInChildren<Renderer>().material.color = Common.valveWaitingColor;
            }
            else if (thisValve.valveState == (int)Common.ValveState.Closed && !thisValveColor.Equals(Common.valveClosedColor))
            {
                thisValveInst.GetComponentInChildren<Renderer>().material.color = Common.valveClosedColor;
            }
        }
    }

    public void handleFlashingIcons()
    {

        if (mainScript.newOrdersWaitingIconActive == true || mainScript.emptyDeliveryTruckWaitingIconActive == true || mainScript.delayedDeliveryIconActive == true || mainScript.brandMarketingNeededIconActive == true || mainScript.procurementNeededIconActive == true)
        {
            mainScript.notifLabelCounter += Time.deltaTime;
            //	print ("notif label counter is: " + notifLabelCounter);

            foreach (Transform flashingIcon in mainScript.indicatorPanel.transform)
            {
                if (mainScript.notifLabelCounter < 8.0f)
                {
                    flashingIcon.GetComponent<Image>().color = Color.green;
                }
                if (mainScript.notifLabelCounter >= 8.0f)
                {
                    flashingIcon.GetComponent<Image>().color = Color.white;
                }
            }

            if (mainScript.notifLabelCounter >= 10.0f)
            {
                mainScript.notifLabelCounter = 0;
            }
        }
    }

    public void handleFlashingEquipmentUpgradeDots()
    {
        equipmentUpgradeDotCounter += Time.deltaTime;

        foreach (GameObject equipmentUpgradeDot in GameObject.FindGameObjectsWithTag("EquipmentUpgradeDot"))
        {
            EquipmentUpgradeDot equipmentUpgradeDotScript = equipmentUpgradeDot.GetComponent<EquipmentUpgradeDot>();
            Color colorForEquipmentDot = getColorForEquipmentUpgradeDot(equipmentUpgradeDotScript);
            if (equipmentUpgradeDotCounter < 1f)
            {
                equipmentUpgradeDot.GetComponent<MeshRenderer>().material.color = colorForEquipmentDot;
            }
            else
            {
                equipmentUpgradeDot.GetComponent<MeshRenderer>().material.color = Color.grey;
            }
            if (equipmentUpgradeDotCounter > 2)
            {
                equipmentUpgradeDotCounter = 0;
            }
        }
    }

    Color getColorForEquipmentUpgradeDot(EquipmentUpgradeDot equipmentUpgradeDotScript)
    {
        var valvesLackingUpgradesByIndex = mainScript.tank_And_Valve_Physical_Updates.ValvesLackUpgradesByIndex();
        if (equipmentUpgradeDotScript.equipmentType == Constants.Valves.valveCode)
        {
            if (valvesLackingUpgradesByIndex.Contains(equipmentUpgradeDotScript.index))
            {
                return Color.red;
            }
        }

        var tanksLackingUpgradesByIndex = mainScript.tank_And_Valve_Physical_Updates.TanksLackUpgradesByIndex();
        if (equipmentUpgradeDotScript.equipmentType == Constants.Tanks.tankCode)
        {
            if (tanksLackingUpgradesByIndex.Contains(equipmentUpgradeDotScript.index))
            {
                return Color.red;
            }
        }
        return new Color(190f / 255, 247f / 255, 216f / 255);
    }

    public void tankSpecTextColorSet(Transform textTransform, Color newColor)
    {
        textTransform.GetComponent<TextMesh>().color = newColor;
    }

    IEnumerator ZoomFactoryAtStartCoRout()
    {
        Camera mainCamera = Camera.main;
        float currentCamerazPosition = mainCamera.transform.localPosition.z;
        mainCamera.transform.localPosition = new Vector3(500, -270, currentCamerazPosition);
        float startingOrthoSize = 670f;
        float finalOrthoSize = 400f;
        mainCamera.GetComponent<Camera>().orthographicSize = startingOrthoSize;
        float currentOrthoSize = startingOrthoSize;
        float totalTimeForAnimation = 3f;
        yield return new WaitForSeconds(1.5f);
        while (currentOrthoSize > finalOrthoSize)
        {
            currentOrthoSize -= (Time.deltaTime / totalTimeForAnimation) * (startingOrthoSize - finalOrthoSize);
          //  Debug.Log("Current orthosize is: " + currentOrthoSize);
            mainCamera.GetComponent<Camera>().orthographicSize = currentOrthoSize;
            yield return 0;
        }
        yield return 0;
    }

    public void ZoomFactoryAtStart()
    {
        StartCoroutine(ZoomFactoryAtStartCoRout());
    }

}
