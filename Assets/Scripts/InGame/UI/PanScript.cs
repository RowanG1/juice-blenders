﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class PanScript : MonoBehaviour
{
	public Camera mainCamera;
	public  float speed;
	public  Canvas menuCanv;

	public  static bool opsDragging;

	float mouseSpeed = 10;
	float origPanSpeed = 7f;

	Vector2 fingerStart, fingerEnd = new Vector2();

	// Use this for initialization
	void Start ()
	{
		speed = origPanSpeed;
	}

	// Update is called once per frame
	public void EachUpdate ()
	{
	//	print("Pan script executing");
		speed = origPanSpeed /900 * mainCamera.orthographicSize;
		//print ("Speed is: " + speed);
		#if (UNITY_EDITOR || UNITY_STANDALONE_WIN)

		if ( Input.GetMouseButton (0 ) && TabStripScript.menuStartedBeingTouched == false && FlashingIconButtonScript.iconStartedBeingTouched == false) {

			float xDelta = Input .GetAxis ("Mouse X") * mouseSpeed;
			float yDelta = -Input .GetAxis ("Mouse Y") * mouseSpeed;
			//     print (xDelta);
			//     print (yDelta);
			if ((xDelta != 0 ) || (yDelta != 0)) {

				opsDragging = true;
				UIInputHandlerOperations.oneTouchActiveNotMoved = false;
				//print("Pan seems to be activating");
				mainCamera.transform.Translate (-xDelta * speed, yDelta * speed, 0);

				Vector3 camPos = mainCamera.transform.localPosition;
				Vector3 camPosOrig = camPos;
				if (camPos.x < 200 )
					camPos.x = 200;
				if (camPos.x > 1600 )
					camPos.x = 1600;
				if (camPos.y > -250 )
					camPos.y = - 250;
				if (camPos.y < -1300 )
					camPos.y = - 1300;
				if (camPos != camPosOrig) {
					mainCamera.transform.localPosition = camPos;
				}
			}
		}

		if ( Input.GetMouseButtonUp(0)) {

			opsDragging = false;
			//oneTouchStartedAndValid = false;
		}

		#endif

		//RectTransformUtility.S
		#if (UNITY_IPHONE || UNITY_ANDROID)

		if (Input.touchCount == 1) {
	
			foreach (Touch touch in Input.touches) {
				if (touch.phase == TouchPhase.Began && DragCustom.menuIsScrolling == false && TabStripScript.menuStartedBeingTouched == false && FlashingIconButtonScript.iconStartedBeingTouched == false) {
					//oneTouchStartedAndValid = true;
					fingerStart = Input.GetTouch(0).position;
					fingerEnd = Input.GetTouch(0).position;
					//print("Touch has began for pan script, with checks of drag strip");
				}

				if (touch.phase == TouchPhase.Moved && DragCustom.menuIsScrolling == false && TabStripScript.menuStartedBeingTouched == false && FlashingIconButtonScript.iconStartedBeingTouched == false) {
					//     opsOneTouchBeganAndValid = false;
					fingerEnd = Input.GetTouch(0).position;
					if (((fingerEnd - fingerStart).magnitude) > Screen.dpi * 0.1f ) {
						opsDragging = true;
						UIInputHandlerOperations.oneTouchActiveNotMoved = false;

					}
					if (opsDragging == true) {
						//     print ("Pan entered move mode");
						Vector2 touchDeltaPosition = Input.GetTouch(0).deltaPosition;
						mainCamera.transform.Translate(-touchDeltaPosition.x * speed, -touchDeltaPosition.y * speed, 0);

						Vector3 camPos = mainCamera.transform.localPosition;
						Vector3 camPosOrig = camPos;
						if (camPos.x < 100)
							camPos.x = 100;
						if (camPos.x > 1600)
							camPos.x = 1600;
						if (camPos.y > -250)
							camPos.y = - 250;
						if (camPos.y < -1400)
							camPos.y = - 1400;
						if (camPos != camPosOrig) {
							mainCamera.transform.localPosition = camPos;
						}
					}
				}
			}
		}
		if (Input.touchCount > 1) {
			opsDragging = false;
			UIInputHandlerOperations.oneTouchActiveNotMoved = false;
		}
		foreach (Touch touch in Input.touches) {
			if (touch.phase == TouchPhase.Began) {
				//print("One of the touches has began in panscript in separate loop, and the touch count is " + Input.touches.Length);
			}

			if (touch.phase == TouchPhase.Ended) {
				opsDragging = false;
				//     print("One of the touches has made onetouchstarted false");
			}
		}

		#endif
		//here

	}
}