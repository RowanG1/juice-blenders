﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class UIInputHandlerOperations : MonoBehaviour {
	public  static bool oneTouchActiveNotMoved;
	EquipmentTapShort equipmentTapShort; 
	// Use this for initialization
	void Start () {
		equipmentTapShort = new EquipmentTapShort();
	}
	
	// Update is called once per frame
	void Update () {

		equipmentTapShort.ShortTapCheckAndHandleOnPointerUp();
		
		#if (UNITY_EDITOR || UNITY_STANDALONE_WIN)
			if ( Input.GetMouseButtonDown (0 )  &&  !EventSystem.current.IsPointerOverGameObject () && TabStripScript.menuStartedBeingTouched == false && FlashingIconButtonScript.iconStartedBeingTouched == false) {
			oneTouchActiveNotMoved = true;
			}
			if ( Input.GetMouseButtonUp(0)) {

			oneTouchActiveNotMoved = false;
			}
		#endif

		#if (UNITY_IPHONE || UNITY_ANDROID)
		if (Input.touchCount == 1) {
			foreach (Touch touch in Input.touches) {
				if (touch.phase == TouchPhase.Began && DragCustom.menuIsScrolling == false && TabStripScript.menuStartedBeingTouched == false && FlashingIconButtonScript.iconStartedBeingTouched == false) {
					oneTouchActiveNotMoved = true;
				}
			}
		}

		foreach (Touch touch in Input.touches) {
			if (touch.phase == TouchPhase.Ended) {
				oneTouchActiveNotMoved = false;
			}
		}
		#endif

		transform.GetComponent<PanScript>().EachUpdate();
	}
}
