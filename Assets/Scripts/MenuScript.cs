﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuScript : MonoBehaviour
{
    public static float minScreenDimensionmm;
    public static float screenSizeThreshold = 100; //mm
    public GameObject newGameBut;
    public GameObject resumeGameBut;
    public GameObject scoresBut;
    public GameObject instrucBut;
    public GameObject buttonLayouts;
    public GameObject canvas;

    RemoteConfigFirebase remoteConfigFirebase;
    List<Text> textItems = new List<Text>();

    public SetAllFontSizesToSmallestInGroup setAllFontSizesToSmallestInGroup;

    // Use this for initialization
    void Awake()
    {
        DontDestroyOnLoad(GameObject.Find("ButtonSound"));
        remoteConfigFirebase = new RemoteConfigFirebase();
        remoteConfigFirebase.InitializeFirebaseRemoteConfig();
    }
    void Start()
    {
//TBD: Remove levels file handler below to set level 4
      //  LevelsFileHandler levelsFileHandler = new LevelsFileHandler();
      //  levelsFileHandler.SaveNewHighestLevelPassed(4);
      
        
        remoteConfigFirebase.FetchData();
        SaveGameForResume.Load();
        if (SaveGameForResume.savedGame.minsRemaining == Constants.Timing.nullGameMinsRemaining)
        {
            resumeGameBut.SetActive(false);
        }

        buttonLayouts.GetComponent<VerticalLayoutGroup>().spacing = 15f / buttonLayouts.transform.childCount;

        textItems.Clear();

        foreach (Text buttonTextItem in buttonLayouts.GetComponentsInChildren<Text>())
        {
            textItems.Add(buttonTextItem);
        }

        setAllFontSizesToSmallestInGroup.SetTextItems(textItems);

        Firebase.Messaging.FirebaseMessaging.TokenReceived += RemoteConfigFirebase.OnTokenReceived;
        Firebase.Messaging.FirebaseMessaging.MessageReceived += RemoteConfigFirebase.OnMessageReceived;

        int highestLevelPassed = new LevelsFileHandler().GetMaxLevelPassed().maxLevelPassed;
        new PupilDataStorage().UpdatePupilHighestLevelRemote(highestLevelPassed);

    }

    // Update is called once per frame

    public void ResumeGame()
    {
        Common.PlayButtonSound();
        DataTransferToGameAtStart.requestedNewGame = false;
        SceneManager.LoadScene("GameScene");
    }

    public void NewGame()
    {
        Common.PlayButtonSound();
        DataTransferToGameAtStart.requestedNewGame = true;
        SaveGameForResume.ClearSavedGame();
        remoteConfigFirebase.UpdateAppDataFromRemoteConfig();
        SceneManager.LoadScene("BadgeStatus");
    }

    public void OpenInstructions()
    {
        Common.PlayButtonSound();
        SceneManager.LoadScene("InstructionsMenu");
    }

    public void OpenComment()
    {
        Common.PlayButtonSound();
        Application.OpenURL("https://docs.google.com/forms/d/e/1FAIpQLSdr9JTJavKgG_QRD2Z31XfRPGMGXleba3s_8w4H9EKTWSoQqw/viewform");
    }

    public void OpenSchools()
    {
        Common.PlayButtonSound();
        SceneManager.LoadScene("Schools");
    }

}
