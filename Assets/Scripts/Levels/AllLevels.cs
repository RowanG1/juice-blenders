using System.Collections;
using System.Collections.Generic;

public class AllLevels {
    public static List<Level> GetLevelsArray() {
        return new List<Level> { new Level1(), new Level2(), new Level3(), new Level4(), new Level5() };
    }

    public AllLevels() {

    }
}