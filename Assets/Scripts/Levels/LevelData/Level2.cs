using System.Collections;
using System.Collections.Generic;

public class Level2 : Level
{
    public Level2() : base()
    {
        levelDurationMins = 18;
        inactiveFunctionalAreas = new string[] { FunctionalAreaNames.procurement, FunctionalAreaNames.equipment };
        levelNumber = 2;
    }

    public override void SetLevels()
    {
        this.levelConditions = new List<LevelSingleCondition>() { new Condition1(), new Condition2() };
    }

    public override bool CheckLevelPassed()
    {
        return DefaultCheckLevelPassed();
    }

    public class Condition1 : LevelSingleCondition
    {

        public Condition1() : base()
        {
            conditionValue = 35000;
            parameterName = "Sales";
        }

        public override float CurrentValue()
        {
            return MainScript.currentMainScript.sales;
        }

    }

    public class Condition2 : LevelSingleCondition
    {

        public Condition2() : base()
        {
            conditionValue = 20000;
            parameterName = "Advert spend";
        }


        public override float CurrentValue()
        {
            return MainScript.currentMainScript.advertising + MainScript.currentMainScript.pendingAdvertsTotal;
        }

    }

}