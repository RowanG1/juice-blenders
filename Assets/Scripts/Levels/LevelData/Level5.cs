﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


public class Level5 : Level
{
public static int sales_target;
public static int profit_target;
public static int gp_margin_target;
    public Level5() : base()
    {
        levelDurationMins = 30;
        inactiveFunctionalAreas = new string[] { };
        levelNumber = 5;
    }

    public override void SetLevels()
    {
        this.levelConditions = new List<LevelSingleCondition>() { new Condition1(), new Condition2(), new Condition3(), new Condition4(), new Condition5(), new Condition6() };
    }

    public override bool CheckLevelPassed()
    {
        return DefaultCheckLevelPassed();
    }

    public class Condition1 : LevelSingleCondition
    {

        public Condition1() : base()
        {
            conditionValue = sales_target;
            parameterName = "Sales";
        }

        public override float CurrentValue()
        {
            return MainScript.currentMainScript.sales;
        }
    }

    public class Condition2 : LevelSingleCondition
    {
        public Condition2() : base()
        {
            conditionValue = 35000;
            parameterName = "Advert spend";
        }

        public override float CurrentValue()
        {
            return MainScript.currentMainScript.advertising + MainScript.currentMainScript.pendingAdvertsTotal;
        }
    }

    public class Condition3 : LevelSingleCondition
    {
        public Condition3() : base()
        {
            conditionValue = 30000;
            parameterName = "Brand spend";
        }

        public override float CurrentValue()
        {
            return MainScript.currentMainScript.branding;
        }

    }

    public class Condition4 : LevelSingleCondition
    {
        public Condition4() : base()
        {
            conditionValue = 4;
            parameterName = "Number of equipment upgrades";
        }


        public override float CurrentValue()
        {
            return MainScript.currentMainScript.numberEquipmentUpgradesMade;
        }
    }

    public class Condition5 : LevelSingleCondition
    {
        public Condition5() : base()
        {
            conditionValue = 10;
            parameterName = "Number of successful juice cost bargains";
        }

        public override float CurrentValue()
        {
            return MainScript.currentMainScript.numberSuccessfulCostNegotiations;
        }
    }

    public class Condition6 : LevelSingleCondition
    {
        public Condition6() : base()
        {
            conditionValue = gp_margin_target;
            parameterName = "Overall gross profit (GP) margin";
            unit = "%";
        }

        public override float CurrentValue()
        {
         return FinanceAreaScript.GPMarginPercen(sales: MainScript.currentMainScript.sales, costOfSales: MainScript.currentMainScript.costOfSales);
        }
    }

	    public class Condition7 : LevelSingleCondition
    {
        public Condition7() : base()
        {
            conditionValue = profit_target;
            parameterName = "Profit";
        }

        public override float CurrentValue()
        {
            return MainScript.currentMainScript.profit;
        }
    }

}