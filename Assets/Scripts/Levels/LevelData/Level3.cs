using System.Collections;
using System.Collections.Generic;

public class Level3 : Level
{
    public Level3() : base()
    {
        levelDurationMins = 22;
        inactiveFunctionalAreas = new string[] {FunctionalAreaNames.equipment };
        levelNumber = 3;
    }

    public override void SetLevels()
    {
        this.levelConditions = new List<LevelSingleCondition>() { new Condition1(), new Condition2(), new Condition3(), new Condition4() };
    }

    public override bool CheckLevelPassed()
    {
        return DefaultCheckLevelPassed();
    }

    public class Condition1 : LevelSingleCondition
    {
        //string description = "Sales greater than 20000";
        public Condition1() : base()
        {
            conditionValue = 50000;
            parameterName = "Sales";
        }

        public override float CurrentValue()
        {
            return MainScript.currentMainScript.sales;
        }

    }

    public class Condition2 : LevelSingleCondition
    {

        public Condition2() : base()
        {
            conditionValue = 25000;
            parameterName = "Advert spend";
        }

        public override float CurrentValue()
        {
            return MainScript.currentMainScript.advertising + MainScript.currentMainScript.pendingAdvertsTotal;
        }
    }

    public class Condition3 : LevelSingleCondition
    {

        public Condition3() : base()
        {
            conditionValue = 18000;
            parameterName = "Brand spend";
        }


        public override float CurrentValue()
        {
            return MainScript.currentMainScript.branding;
        }
    }

    public class Condition4 : LevelSingleCondition
    {

        public Condition4() : base()
        {
            conditionValue = 2;
            parameterName = "Number of successful juice cost bargains";
        }

        public override float CurrentValue()
        {
            return MainScript.currentMainScript.numberSuccessfulCostNegotiations;
        }
    }
}