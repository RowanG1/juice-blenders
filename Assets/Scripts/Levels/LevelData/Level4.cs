using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


public class Level4 : Level
{
public static int gp_margin;
    public Level4() : base()
    {
        levelDurationMins = 30;
        inactiveFunctionalAreas = new string[] { };
        levelNumber = 4;
    }

    public override void SetLevels()
    {
        this.levelConditions = new List<LevelSingleCondition>() { new Condition1(), new Condition2(), new Condition3(), new Condition4(), new Condition5(), new Condition6() };
    }

    public override bool CheckLevelPassed()
    {
        return DefaultCheckLevelPassed();
    }

    public class Condition1 : LevelSingleCondition
    {

        public Condition1() : base()
        {
            conditionValue = 90000;
            parameterName = "Sales";
        }

        public override float CurrentValue()
        {
            return MainScript.currentMainScript.sales;
        }

    }

    public class Condition2 : LevelSingleCondition
    {
        public Condition2() : base()
        {
            conditionValue = 25000;
            parameterName = "Advert spend";
        }

        public override float CurrentValue()
        {
            return MainScript.currentMainScript.advertising + MainScript.currentMainScript.pendingAdvertsTotal;
        }
    }

    public class Condition3 : LevelSingleCondition
    {
        public Condition3() : base()
        {
            conditionValue = 20000;
            parameterName = "Brand spend";
        }

        public override float CurrentValue()
        {
            return MainScript.currentMainScript.branding;
        }

    }

    public class Condition4 : LevelSingleCondition
    {
        public Condition4() : base()
        {
            conditionValue = 2;
            parameterName = "Number of equipment upgrades";
        }


        public override float CurrentValue()
        {
            return MainScript.currentMainScript.numberEquipmentUpgradesMade;
        }
    }

    public class Condition5 : LevelSingleCondition
    {
        public Condition5() : base()
        {
            conditionValue = 5;
            parameterName = "Number of successful juice cost bargains";
        }

        public override float CurrentValue()
        {
            return MainScript.currentMainScript.numberSuccessfulCostNegotiations;
        }
    }

    public class Condition6 : LevelSingleCondition
    {
        public Condition6() : base()
        {
            conditionValue = gp_margin;
            parameterName = "Overall gross profit (GP) margin";
            unit = "%";
        }

        public override float CurrentValue()
        {
           return FinanceAreaScript.GPMarginPercen(sales: MainScript.currentMainScript.sales, costOfSales: MainScript.currentMainScript.costOfSales);
        }
    }

}