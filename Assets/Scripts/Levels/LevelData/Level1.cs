using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level1 : Level
{
    public static int sales_target;
    public Level1() : base()
    {
        levelDurationMins = 18;
        inactiveFunctionalAreas = new string[] {FunctionalAreaNames.procurement, FunctionalAreaNames.equipment, FunctionalAreaNames.marketing };
        levelNumber = 1;
    }

    public override void SetLevels()
    {
        this.levelConditions = new List<LevelSingleCondition>() { new Condition1() };
    }

    public override bool CheckLevelPassed()
    {
        return DefaultCheckLevelPassed();
    }

    public class Condition1 : LevelSingleCondition
    {

        public Condition1() : base()
        {
            conditionValue = sales_target;
            parameterName = "Sales";
        }

        public override float CurrentValue()
        {
            return MainScript.currentMainScript.sales;
        }

    }

}