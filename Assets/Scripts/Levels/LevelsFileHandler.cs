using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public class LevelsFileHandler
{
    public static string highestLevelPassedFilePath = "/highestLevelPassed.gd";

    public bool SaveNewHighestLevelPassed(int currentLevelPassed)
    { // Can only be higher than previous level to return true

        int lastMaxLevelPassedVal;
        MaxLevelPassed lastMaxLevelPassed;
		lastMaxLevelPassed = GetMaxLevelPassed();
		lastMaxLevelPassedVal = lastMaxLevelPassed.maxLevelPassed;

        if (currentLevelPassed > lastMaxLevelPassedVal)
        {
            BinaryFormatter bf = new BinaryFormatter();
            MaxLevelPassed maxLevelPassed = new MaxLevelPassed();
            maxLevelPassed.maxLevelPassed = currentLevelPassed;
            //Application.persistentDataPath is a string, so if you wanted you can put that into debug.log if you want to know where save games are located
            FileStream file = File.Create(Application.persistentDataPath + highestLevelPassedFilePath); //you can call it anything you want
            bf.Serialize(file, maxLevelPassed);
            file.Close();
            return true;
        }
        return false;
    }

    public MaxLevelPassed Load()
    {
        if (File.Exists(Application.persistentDataPath + highestLevelPassedFilePath))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + highestLevelPassedFilePath, FileMode.Open);
            MaxLevelPassed maxLevelPassed = (MaxLevelPassed)bf.Deserialize(file);
            file.Close();
            return maxLevelPassed;
        }
        else
        {
            throw new FileNotFoundException("Max level file not found");
        }

        //return null;
    }

    public MaxLevelPassed GetMaxLevelPassed()
    {
        MaxLevelPassed lastMaxLevelPassed;
        try
        {
            lastMaxLevelPassed = Load();
        }
        catch (FileNotFoundException e)
        {
            Debug.Log(e);
            lastMaxLevelPassed = new MaxLevelPassed();
        }
        return lastMaxLevelPassed;
    }

    public void ClearHighestLevelPassed()
    {
        string filePath = Application.persistentDataPath + highestLevelPassedFilePath;
        File.Delete(filePath);
    }

}