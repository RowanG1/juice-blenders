﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.IO;
using Firebase.Auth;

public class LevelArea : MonoBehaviour
{

    public Sprite lockedPadlock;
    public Sprite unlockedPadlock;
    public GameObject levelBtnPanel;
    public GameObject girlFace;
    public GameObject levelButtonPrefab;
    public GameObject loginInfo;

    Color lockedBckGndColor = new Color(233f / 255, 164f / 255, 177f / 255, 133f / 255);
    Color unlockedBckGndColor = new Color(58f / 255, 219f / 255, 166f / 255, 133f / 255);

    string genericNamePadlockGameObject = "LockImage";

    // Use this for initialization
    void Start()
    {
        MaxLevelPassed maxLevelPassed;
        int maxLevelPassedVal;

        maxLevelPassed = new LevelsFileHandler().GetMaxLevelPassed();
        maxLevelPassedVal = maxLevelPassed.maxLevelPassed;

        foreach (Transform child in levelBtnPanel.transform)
        {
            Destroy(child.gameObject);
        }

        foreach (Level level in AllLevels.GetLevelsArray())
        {
            GameObject levelBtn = Instantiate(levelButtonPrefab) as GameObject;
            levelBtn.transform.SetParent(levelBtnPanel.transform, false);
            levelBtn.name = "Level" + level.levelNumber;
            levelBtn.transform.Find("LevelText").GetComponent<Text>().text = "Level " + level.levelNumber;
            bool levelLocked;
            if (level.levelNumber <= maxLevelPassedVal + 1)
            {
                levelLocked = false;
                levelBtn.transform.Find(genericNamePadlockGameObject).GetComponent<Image>().sprite = unlockedPadlock;
                levelBtn.GetComponent<Image>().color = unlockedBckGndColor;
            }
            else
            {
                levelLocked = true;
                levelBtn.transform.Find(genericNamePadlockGameObject).GetComponent<Image>().sprite = lockedPadlock;
                levelBtn.GetComponent<Image>().color = lockedBckGndColor;
            }
            levelBtn.GetComponent<Button>().onClick.AddListener(() => OnLevelBtnClick(levelLocked, level.levelNumber));
        }

        RemoteConfigFirebase remoteConfigFirebase = new RemoteConfigFirebase();
        remoteConfigFirebase.FetchData();

        Firebase.Auth.FirebaseAuth auth = Firebase.Auth.FirebaseAuth.DefaultInstance;
        if (auth.CurrentUser != null)
        {
            PupilData pupilData = new PupilDataStorage().LoadFromLocalDevice();
            if (pupilData != null)
            {
                loginInfo.GetComponent<Text>().text = "Hi " + pupilData.name + ", you are linked to class: <color=blue>" + pupilData.classCode + "</color>";
            }
            else
            {
                loginInfo.GetComponent<Text>().text = "Not yet linked to a school class";
            }
        } else {
            loginInfo.GetComponent<Text>().text = "You are not logged in for schools.";
        }
    }

    // Update is called once per frame
    public void OnHomeBtnClick()
    {
        Common.PlayButtonSound();
        SceneManager.LoadScene("MainMenu");
    }

    void OnLevelBtnClick(bool levelLocked, int levelNumber)
    {
        if (levelLocked == false)
        {
            Common.PlayButtonSound();
            DataTransferToGameAtStart.currentGameLevel = levelNumber;
            new RemoteConfigFirebase().UpdateAppDataFromRemoteConfig(); // To avoid app delays, whether new data is received in time, or not, we proceed with data we have.
            SceneManager.LoadScene("GameScene");
        }
    }

    void Update()
    {
        UpdateGirlFace();
    }
    void UpdateGirlFace()
    {
        float growthScale2D = Mathf.Sin(2 * Mathf.PI * (0.6f) * Time.realtimeSinceStartup) * 0.03f + 1;
        girlFace.transform.localScale = new Vector3(growthScale2D, growthScale2D, 1);

    }

}
