﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System;

public class LevelPassedArea : MonoBehaviour
{
    public GameObject creditsEarned;
    public GameObject badgeCreditPrefab;
    public Sprite customerService;
    public Sprite lowWaste;
    public Sprite profitability;

    // Use this for initialization
    void Start()
    {
        foreach (Transform child in creditsEarned.transform)
        {
            Destroy(child.gameObject);
        }

        if (DataTransferToLevelPassed.customerServiceBadgeCredit > 0)
        {
            InstantiateBadge(Common.BadgeNames.CustomerService);
        }
        if (DataTransferToLevelPassed.lowWasteBadgeCredit > 0)
        {
            InstantiateBadge(Common.BadgeNames.LowWaste);
        }
        if (DataTransferToLevelPassed.profitabilityBadgeCredit > 0)
        {
            InstantiateBadge(Common.BadgeNames.Profitability);
        }

    }

    void InstantiateBadge(Common.BadgeNames badgeName)
    {
        // (badgeName == Common.BadgeNames.CustomerService)
        GameObject newBadgeInst = Instantiate(badgeCreditPrefab) as GameObject;
        newBadgeInst.transform.SetParent(creditsEarned.transform, false);

        //var badgeNameVal = Enum.GetValue(badgeName.GetType());
        if (badgeName == Common.BadgeNames.CustomerService)
        {
            newBadgeInst.transform.Find("Icon").GetComponent<Image>().sprite = customerService;
        }
        else if (badgeName == Common.BadgeNames.LowWaste)
        {
            newBadgeInst.transform.Find("Icon").GetComponent<Image>().sprite = lowWaste;
        }
        else
        {
            newBadgeInst.transform.Find("Icon").GetComponent<Image>().sprite = profitability;
        }
    }


    public void OKPressed()
    {
        DataTransferToGameAtStart.requestedNewGame = true;
        Common.PlayButtonSound();
        SceneManager.LoadScene("Levels");
    }
}
