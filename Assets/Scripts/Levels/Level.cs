using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

abstract public class Level
{
    protected List<LevelSingleCondition> levelConditions;
    public string[] inactiveFunctionalAreas;
    public int levelDurationMins;
    public int levelNumber;
    abstract public void SetLevels();
    abstract public bool CheckLevelPassed();

    public Level()
    {
        SetLevels();
    }

    public bool DefaultCheckLevelPassed()
    {
        foreach (LevelSingleCondition condition in levelConditions)
        {
            if (!condition.ConditionMet())
            {
                return false;
            }
        }
        return true;
    }

    public List<LevelSingleCondition> GetLevelConditions()
    {
        return levelConditions;
    }

    public static Level GetLevelObj()
    {
        Level thisLevel = null;
        thisLevel = AllLevels.GetLevelsArray()[MainScript.currentMainScript.currentGameLevel - 1];
        return thisLevel;
    }

    public bool IsFunctionalAreaInactive(string function) {
            foreach(string thisFunction in inactiveFunctionalAreas) {
                if (thisFunction == FunctionalAreaNames.procurement) {
                    return true;
                }
            }
            return false;
    }

}