﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

abstract public class LevelSingleCondition
{
    protected string description;
    protected int conditionValue;
    protected string unit = "";
    protected string parameterName;



    abstract public float CurrentValue();


    public LevelSingleCondition()
    {

    }

    public string DefaultDescription()
    {
        return parameterName + " must be greater than or equal to " + conditionValue.ToString("#,###") + unit;
    }

    public void SetDescription(string description)
    {
        this.description = description;
    }

    public string GetDescription()
    {
        if (description != null)
        {
            return description;
        }
        else return DefaultDescription();
    }

    public bool ConditionMet()
    {
        if (CurrentValue() >= conditionValue)
        {
            return true;
        }
        return false;
    }

    public string GetUnit()
    {
        return unit;
    }
}
