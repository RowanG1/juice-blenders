public class Constants
{
    public class Orders
    {
        public const float orderBiasFractionBasedOnDeliveries = 0.4f;
        public const float productVolumeFactorConst = 1f / 2000 * (((float)5) / 100);
        public const float maxProductVolFactor = 2;
        public const int goodWillLostFromNeglectedNewOrder = 3000;
        public const float penaltyFromDelayedDelivery = 0.2f;
        public const float goodWillPenaltyFromDelayedDelivery = 1000;
        public const float goodwillLostDelivery = 5000;
        public const float goodwillGainedFromOrderFulfilled = 1000;
        public const float tier1OrderMargin = 60;
        public const float tier2OrderMargin = 73.5f;
        public const float OrderDeterminantFactorPerAdSpend = 10000;
        public const float minOrderLevel = 0.23f;
    }
    public class Badges
    {
        public static BadgeStarCriteriaOneBadgeType customerServiceBadgeCriteria = new BadgeStarCriteriaOneBadgeType(2, 4, 6);
        public static BadgeStarCriteriaOneBadgeType lowWasteBadgeCriteria = new BadgeStarCriteriaOneBadgeType(2, 4, 6);
        public static BadgeStarCriteriaOneBadgeType profitabilityBadgeCriteria = new BadgeStarCriteriaOneBadgeType(2, 4, 6);
        public const int minimumCustomerServicePercenForCredit = 50;
        public const int minimumProfitForCredit = 0;
        public const float juiceWastagePercenThreshold = 5f;
    }

    public class Tanks
    {
        public const string supplyCode = "supply";
        public const string tankCode = "tank";
        public const string outletCode = "outlet";
        public const float tankFullLevelFraction = 0.9f;
        public const float tankEmptyLevelFraction = 0.1f;
        public const float tankHalfFull = 0.5f;
        public const float initialTankCapacity = 3000f;
        public const float outletTankCapacity = 40000f;
        public const float minimumJuiceFractionAnyJuice = 0.2f;

        public const float heightUpgradeFactorDouble = 1.2f;
        public const float heightUpgradeFactorQuadruple = 1.4f;
    }

    public class Lines
    {
        public const int initialLineThickness = 8;
        public const int doubleFlowLineThickness = 12;
        public const int tripleFlowLineThickness = 16;

    }

    public class Marketing
    {
        public const float minimumAdLevelPercenForNeglectNotification = 0.2f;
        public const float minimumBrandLevelPercenForNeglectNotification = 0.3f;
    }

    public class Trucks
    {
        public const float thresholdTimeToNotifyIfDispatchTruckPresentAndNotFilling = 10;
    }

    public class Juice
    {
        public const float minimumInitialJuiceCostFactor = 1.1f;
        public const float valueAddedPerJuice = 1.5f;
        public const float minPrice = 1.0f;
        public const float costInflationFactorPer10Min = 1.1f;
    }

    public class JuiceProduct
    {
        public const float markupStandard = 1.5f;

        public const float capPercenAmplitude = 20;

        public const int minProductVolume = 1500;
        public const int rangeProductVolume = 3500;
    }

    public class Valves
    {
        public const string drainCode = "drain";
        public const string valveCode = "valve";
        public const float trucksFlowRate = 70;
        public const float toDispatchFlowRate = 70;
        public const float standardFlowRate = 50;
    }

    public class Timing
    {
        public const int nullGameMinsRemaining = -1;
        public const float decayedGoodwillFactorPerMin = 0.95f;
        public const float adLeakageFactoPerMin = 0.055f;
        public const float penaltyPeriodLateDeliverySecs = 6 * 60; //seconds
    }

    public class Files
    {
        public const string saveGameFilePath = "/savedGameForResume.gd";
        public const string badgesFilePath = "/badgesCredits.gd";
        public const string pupilDataFilePath = "/pupilData.gd";
    }

    public class Firebase
    {
        public const string userFeedbackCommentsUrl = "https://juice-blenders-firebase.firebaseio.com";
        public const string user_comment = "user-comment";

        public const string sales_target_level_5 = "sales_target_level_5";
        public const string sales_target_level_1 = "sales_target_level_1";
        public const string gp_margin_target_level_4 = "gp_margin_target_level_4";
        public const string profit_target_level_5 = "profit_target_level_5";
        public const string gp_margin_target_level_5 = "gp_margin_target_level_5";


        public const int sales_target_level_5_default = 120000;
        public const int sales_target_level_1_default = 20000;
        public const int gp_margin_target_level_4_default = 60;
        public const int profit_target_level_5_default = 50000;
        public const int gp_margin_target_level_5_default = 62;

        // Teachers and pupils admin

        public const string classCodes = "classCodes";
        public const string classCode = "classCode";
        public const string userID = "userID";
        public const string gameData = "gameData";
        public const string createdByUserID = "createdByUserID";
        public const string pupils = "pupils";
        public const string pupil = "pupil";
        public const string teacher = "teacher";
        public const string name = "name";
        public const string surname = "surname";
        public const string highestLevelPassed = "highestLevelPassed";
        public const string noPupilsMatchClassCode = "noPupilsMatchClassCode";
        public const string teacherNotLinkedToClassCode = "teacherNotLinkedToClassCode";
        public const string classCodeNotExist = "classCodeNotExist";
        public const string pupilsFoundMatchingClassCode = "PupilsFoundMatchingClassCode";
        public const string pleaseWait = "<color=blue>Please wait...</color>";
        public const string passwordProvider = "password";
        public const int lengthClasscode = 8;
        public const string levelPassed = "levelPassed";
        public const string levelFailed = "levelFailed";
        public const string levelQuit = "levelQuit";
        public const string levelInProgress = "levelInProgress"; 


    }

    public class EquipmentUpgrades
    {
        public const int lastSelectedUpgradeableItemArrayIndexNone = -1;
    }

    public class Delays
    {
        public const string valveOnAfterWaiting = "valveOnAfterWaiting";
        public const string callTruck = "CallTruck";

        public const string brandDelay = "brandDelay";

        public const string adDelay = "adDelay";

        public const string tankUpgrade = "TankUpgrade";

        public const string pumpUpgrade = "PumpUpgrade";

    }

    public class FirebaseAuthErrorCodes
    {
        public const string accountExists = "Account exists with different credentials";

        public const string missingPassword = "Missing password";

        public const string weakPassword = "Weak password";

        public const string wrongPassword = "Wrong password";

        public const string emailAlreadyInUse = "Email already in use";

        public const string invalidEmail = "Invalid email";

        public const string missingEmail = "Missing email";
        public const string unknownError = "Unknown error";
    }

    public class PupilForm
    {
        public const string defaultEmailDescription = "Email:";
        public const string defaultPasswordDescription = "Password:";
        public const string defaultConfirmPasswordDescription = "Confirm Password:";
        public const string defaultNameDescription = "Name:";
        public const string defaultSurnameDescription = "Surname:";
        public const string defaultClassCodeDescription = "Class code:";
    }
}