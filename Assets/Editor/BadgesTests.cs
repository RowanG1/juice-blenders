﻿using UnityEngine;
using UnityEditor;
using UnityEngine.TestTools;
using NUnit.Framework;
using System.Collections;
using System.Collections.Generic;
using System.IO;

public class BadgesTests
{

    [Test]
    public void AddCredits()
    {

        new BadgeCreditsFileHandler().ClearBadgesCredits();

        new BadgeCreditsFileHandler().AddCreditsToFile(1, 1, 1);
   
        BadgesCredits badgesCredits = new BadgeCreditsFileHandler().GetBadgeCreditsFromFile();
        Assert.IsTrue(badgesCredits.customerServiceCreditsEarned == 1);
        Assert.IsTrue(badgesCredits.lowWasteCreditsEarned == 1);
        Assert.IsTrue(badgesCredits.profitabilityCreditsEarned == 1);

        new BadgeCreditsFileHandler().AddCreditsToFile(3, 1, 2);

        badgesCredits = new BadgeCreditsFileHandler().GetBadgeCreditsFromFile();
        Assert.IsTrue(badgesCredits.customerServiceCreditsEarned == 4);
        Assert.IsTrue(badgesCredits.lowWasteCreditsEarned == 2);
        Assert.IsTrue(badgesCredits.profitabilityCreditsEarned == 3);

    }

    [Test]
    public void ClearBadgesCredits()
    {
        new BadgeCreditsFileHandler().ClearBadgesCredits();
    }


    [Test]
    public void SetBadgesCredits()
    {
        new BadgeCreditsFileHandler().AddCreditsToFile(1, 2, 5);
    }

    [Test]
    public void ShowBadgesCredits()
    {
        BadgesCredits badgesCredits = new BadgeCreditsFileHandler().GetBadgeCreditsFromFile();
        Debug.Log("Customer service credits: " + badgesCredits.customerServiceCreditsEarned);
        Debug.Log("Low waste credits: " + badgesCredits.lowWasteCreditsEarned);
        Debug.Log("Profitability credits: " + badgesCredits.profitabilityCreditsEarned);

    }

    [Test]
    public void GetCurrentInGamePercenCustomerService()
    {
        List<Order> orders = new List<Order>();
        MainScript mainScript = new MainScript();
        mainScript.orders = orders;

        orders.Clear();
        Order order1 = new Order();
        order1.customerEmotionStatusEnquiry = Common.CustomerEmotionStatus.NoComment;
        orders.Add(order1);
        Assert.IsTrue(new BadgesCredits().GetCurrentInGamePercenCustomerService(orders) == (int)(1f / 2 * 100));
        //      Next combination
        orders.Clear();
        order1 = new Order();
        order1.customerEmotionStatusEnquiry = Common.CustomerEmotionStatus.Happy;
        order1.accepted = true;
        orders.Add(order1);
        Assert.IsTrue(new BadgesCredits().GetCurrentInGamePercenCustomerService(orders) == (int)(4f / 4 * 100));
        //      Next combination
        orders.Clear();
        order1 = new Order();
        order1.customerEmotionStatusEnquiry = Common.CustomerEmotionStatus.NoComment;
        order1.accepted = true;
        order1.customerEmotionStatusDelivery = Common.CustomerEmotionStatus.NoComment;
        orders.Add(order1);
        Assert.IsTrue(new BadgesCredits().GetCurrentInGamePercenCustomerService(orders) == (int)(2f / 4 * 100));
        //      Next combination
        orders.Clear();
        order1 = new Order();
        orders.Add(order1);
        order1.accepted = true;
        order1.customerEmotionStatusEnquiry = Common.CustomerEmotionStatus.Sad;
        order1.customerEmotionStatusDelivery = Common.CustomerEmotionStatus.Sad;

        Order order2 = new Order();
        orders.Add(order2);
        order2.accepted = false;
        order2.customerEmotionStatusEnquiry = Common.CustomerEmotionStatus.NoComment;

        Assert.IsTrue(new BadgesCredits().GetCurrentInGamePercenCustomerService(orders) == (int)(1f / 6 * 100));
        //      Next combination
        orders.Clear();
        order1 = new Order();
        orders.Add(order1);
        order1.accepted = true;
        order1.lost = true;
        order1.customerEmotionStatusEnquiry = Common.CustomerEmotionStatus.Happy;
        order1.customerEmotionStatusDelivery = Common.CustomerEmotionStatus.Sad;

        order2 = new Order();
        orders.Add(order2);
        order2.accepted = true;
        order2.customerEmotionStatusEnquiry = Common.CustomerEmotionStatus.NoComment;
        order2.customerEmotionStatusDelivery = Common.CustomerEmotionStatus.Happy;

        Assert.IsTrue(new BadgesCredits().GetCurrentInGamePercenCustomerService(orders) == (int)(4f / 8 * 100));

    }

    [Test]
    public void SetCreditsForTransferToLevelPassed()
    {
        MainScript mainScript = new MainScript();
        List<Order> orders = new List<Order>();
        mainScript.orders = orders;

        Order newOrder = new Order();
        newOrder.accepted = true;
        newOrder.customerEmotionStatusEnquiry = Common.CustomerEmotionStatus.Happy;
        newOrder.customerEmotionStatusDelivery = Common.CustomerEmotionStatus.Happy;
        orders.Add(newOrder);

        Order newOrder2 = new Order();
        newOrder2.customerEmotionStatusEnquiry = Common.CustomerEmotionStatus.Happy;
        orders.Add(newOrder2);

        mainScript.profit = 3000;

        mainScript.volJuiceWasted = 800; // Juice waste below 5%
        mainScript.volJuiceDelivered = 11000;

        DataTransferToLevelPassed.customerServiceBadgeCredit = 0;
        DataTransferToLevelPassed.lowWasteBadgeCredit = 0;
        DataTransferToLevelPassed.profitabilityBadgeCredit = 0;

        new BadgesCredits().SetCreditsForTransferToLevelPassed(mainScript);
        Assert.IsTrue(DataTransferToLevelPassed.customerServiceBadgeCredit > 0 && DataTransferToLevelPassed.lowWasteBadgeCredit == 0 && DataTransferToLevelPassed.profitabilityBadgeCredit > 0);

        //Make all customer emotion statuses sad
        newOrder.customerEmotionStatusEnquiry = Common.CustomerEmotionStatus.Sad;
        newOrder.customerEmotionStatusDelivery = Common.CustomerEmotionStatus.Sad;
        newOrder2.customerEmotionStatusEnquiry = Common.CustomerEmotionStatus.Sad;

        mainScript.volJuiceWasted = 200; // Juice waste below 5%

        new BadgesCredits().SetCreditsForTransferToLevelPassed(mainScript);
        Assert.IsTrue(DataTransferToLevelPassed.customerServiceBadgeCredit == 0 && DataTransferToLevelPassed.lowWasteBadgeCredit > 0 && DataTransferToLevelPassed.profitabilityBadgeCredit > 0);


    }

    [Test]
    public void GetBadgeEarnedOneType()
    {
        BadgesCredits badgesCredits = new BadgesCredits();
        badgesCredits.customerServiceCreditsEarned = 2;
        badgesCredits.lowWasteCreditsEarned = 4;
        badgesCredits.profitabilityCreditsEarned = 6;

        BadgesEarned badgesEarned = new BadgesEarned(badgesCredits);

        Badges.BadgeType customerService = badgesEarned.GetBadgeEarnedCustomerService(badgesCredits);
        Badges.BadgeType lowWaste = badgesEarned.GetBadgeEarnedLowWaste(badgesCredits);
        Badges.BadgeType profitability = badgesEarned.GetBadgeEarnedProfitability(badgesCredits);

        Assert.IsTrue(customerService == Badges.BadgeType.Bronze);
        Assert.IsTrue(lowWaste == Badges.BadgeType.Silver);
        Assert.IsTrue(profitability == Badges.BadgeType.Gold);

        badgesCredits.customerServiceCreditsEarned = 0;
        customerService = badgesEarned.GetBadgeEarnedCustomerService(badgesCredits);
        Assert.IsTrue(customerService == Badges.BadgeType.None);

        badgesCredits.profitabilityCreditsEarned = 12;
        profitability = badgesEarned.GetBadgeEarnedProfitability(badgesCredits);
        Assert.IsTrue(profitability == Badges.BadgeType.Gold);

    }

    [Test]
    public void lowWasteBadgeCredits() {
        BadgesCredits badgeCredits = new BadgesCredits();
        int lowWasteCreditEarnedThisLevel = badgeCredits.GetCreditsForLowWastageThisLevel(volJuiceWasted: 5, volJuiceDelivered: 101);
        Assert.IsTrue(lowWasteCreditEarnedThisLevel == 1);


    }

}
