﻿using UnityEngine;
using UnityEditor;
using UnityEngine.TestTools;
using NUnit.Framework;
using System.Collections;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public class PupilDataSavedOnDeviceTests
{

    [Test]
    public void DeleteHighestLevelPassed()
    {
        new PupilDataStorage().DeleteLocalFile();
    }

    [Test]
    public void SavePupilDataOnDevice()
    {
        PupilDataStorage pupilDataStorage = new PupilDataStorage();
        pupilDataStorage.DeleteLocalFile();

        PupilData pupilData = new PupilData();

        string classCode = "Grade8B";
        pupilData.classCode = classCode;
        pupilData.name = "Rowan";
        pupilData.surname = "Gontier";
        pupilData.userID = "5467uu78h7b";
        pupilDataStorage.SaveToLocalDevice(pupilData);

        pupilData = pupilDataStorage.LoadFromLocalDevice();

        Assert.IsTrue(pupilData.classCode == classCode);

    }

    [Test]
    public void LoadPupilData()
    {
        // Use the Assert class to test conditions.
        PupilData pupilData = new PupilDataStorage().LoadFromLocalDevice();
        if (pupilData != null)
        {
            Debug.Log("Class code is: " + pupilData.classCode);
        }
    }

    // A UnityTest behaves like a coroutine in PlayMode
    // and allows you to yield null to skip a frame in EditMode

}
