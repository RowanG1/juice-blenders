﻿using UnityEngine;
using UnityEditor;
using UnityEngine.TestTools;
using NUnit.Framework;
using System.Collections;
using System.Collections.Generic;

public class JuiceAndTankUsage {

[Test]
public void TanksWithHighJuiceInputByIndex() {
	MainScript mainScript = new MainScript();
	Dictionary<int, Dictionary<int, TankUsageStamp>> juiceAddedToTanksByIndexHistoryAtRecentMinuteStamps = new Dictionary<int, Dictionary<int, TankUsageStamp>>();
	juiceAddedToTanksByIndexHistoryAtRecentMinuteStamps.Add(1, new Dictionary<int, TankUsageStamp>() {{0, new TankUsageStamp(100f)}, {1, new TankUsageStamp(5f)}});
	juiceAddedToTanksByIndexHistoryAtRecentMinuteStamps.Add(7, new Dictionary<int, TankUsageStamp>() {{0, new TankUsageStamp(300f)}, {1, new TankUsageStamp(5f)}});
	Common.recentTotalJuiceInputToTankThreshold = 300;

	mainScript.juiceAddedToTanksByIndexHistoryAtRecentMinuteStamps = juiceAddedToTanksByIndexHistoryAtRecentMinuteStamps;
	HashSet<int> TanksWithHighJuiceInputByIndex = mainScript.tank_And_Valve_Physical_Updates.TanksWithHighJuiceInputByIndex();

	Assert.IsTrue(TanksWithHighJuiceInputByIndex.Contains(0) && !TanksWithHighJuiceInputByIndex.Contains(1));
}

[Test]
public void TrimNonRecentMinuteStampsForJuicesConsumed() {
	MainScript mainScript = new MainScript();
	Common.recentTimeframeInMinsJuicesConsumed = 10;
	Dictionary<int, Dictionary<string, float>> juicesConsumedAtRecentMinuteStamps = new Dictionary<int, Dictionary<string, float>>();
	juicesConsumedAtRecentMinuteStamps.Add(1, new Dictionary<string, float>() {{Common.JuiceNames.Blueberry.ToString(), 1f}, {Common.JuiceNames.Strawberry.ToString(), 5}});
	juicesConsumedAtRecentMinuteStamps.Add(2, new Dictionary<string, float>() {{Common.JuiceNames.Blueberry.ToString(), 3}, {Common.JuiceNames.Strawberry.ToString(), 4}});
	juicesConsumedAtRecentMinuteStamps.Add(6, new Dictionary<string, float>() {{Common.JuiceNames.Blueberry.ToString(), 4}, {Common.JuiceNames.Strawberry.ToString(), 7}});
	juicesConsumedAtRecentMinuteStamps.Add(9, new Dictionary<string, float>() {{Common.JuiceNames.Blueberry.ToString(), 5}, {Common.JuiceNames.Strawberry.ToString(), 2}});

	mainScript.juicesConsumedAtRecentMinuteStamps = juicesConsumedAtRecentMinuteStamps;
	mainScript.oneSecondCount = 13 * 60;

	mainScript.tank_And_Valve_Physical_Updates.TrimNonRecentMinuteStampsForJuicesConsumed();

	Assert.IsTrue(!mainScript.juicesConsumedAtRecentMinuteStamps.ContainsKey(1));
	Assert.IsTrue(!mainScript.juicesConsumedAtRecentMinuteStamps.ContainsKey(2));
	Assert.IsTrue(mainScript.juicesConsumedAtRecentMinuteStamps.ContainsKey(6));
	Assert.IsTrue(mainScript.juicesConsumedAtRecentMinuteStamps.ContainsKey(9));
}

[Test]
public void TrimNonRecentMinuteStampsForTankHistory() {

	MainScript mainScript = new MainScript();
	Common.recentTimeframeInMinsTankHistory = 10;
	Dictionary<int, Dictionary<int, TankUsageStamp>> juiceAddedToTanksByIndexHistoryAtRecentMinuteStamps = new Dictionary<int, Dictionary<int, TankUsageStamp>>();
	juiceAddedToTanksByIndexHistoryAtRecentMinuteStamps.Add(1, new Dictionary<int, TankUsageStamp>() {{0, new TankUsageStamp(1f)}, {1, new TankUsageStamp(5f)}});
	juiceAddedToTanksByIndexHistoryAtRecentMinuteStamps.Add(7, new Dictionary<int, TankUsageStamp>() {{0, new TankUsageStamp(1f)}, {1, new TankUsageStamp(5f)}});

	mainScript.juiceAddedToTanksByIndexHistoryAtRecentMinuteStamps = juiceAddedToTanksByIndexHistoryAtRecentMinuteStamps;
	mainScript.oneSecondCount = 13 * 60;

	mainScript.tank_And_Valve_Physical_Updates.TrimNonRecentMinuteStampsForTankHistory();

	Assert.IsTrue(!mainScript.juiceAddedToTanksByIndexHistoryAtRecentMinuteStamps.ContainsKey(1));
	Assert.IsTrue(mainScript.juiceAddedToTanksByIndexHistoryAtRecentMinuteStamps.ContainsKey(7));
	// Assert.IsTrue(mainScript.juicesConsumedAtRecentMinuteStamps.ContainsKey(6));
	// Assert.IsTrue(mainScript.juicesConsumedAtRecentMinuteStamps.ContainsKey(9));
}

[Test]
public void TrimNonRecentMinuteStampsForValveHistory() {

	MainScript mainScript = new MainScript();
	Common.recentTimeframeInMinsValveHistory = 10;
	Dictionary<int, Dictionary<int, ValveDataStampPeriod>> valvesUsageHistoryAtRecentMinuteStamps = new Dictionary<int, Dictionary<int, ValveDataStampPeriod>>();
	valvesUsageHistoryAtRecentMinuteStamps.Add(1, new Dictionary<int, ValveDataStampPeriod>() {{0, new ValveDataStampPeriod()}, {1, new ValveDataStampPeriod()}});
	valvesUsageHistoryAtRecentMinuteStamps.Add(7, new Dictionary<int, ValveDataStampPeriod>() {{0, new ValveDataStampPeriod()}, {1, new ValveDataStampPeriod()}});

	mainScript.valvesUsageHistoryAtRecentMinuteStamps = valvesUsageHistoryAtRecentMinuteStamps;
	mainScript.oneSecondCount = 13 * 60;

	mainScript.tank_And_Valve_Physical_Updates.TrimNonRecentMinuteStampsForValveHistory();

	Assert.IsTrue(!mainScript.valvesUsageHistoryAtRecentMinuteStamps.ContainsKey(1));
	Assert.IsTrue(mainScript.valvesUsageHistoryAtRecentMinuteStamps.ContainsKey(7));
	// Assert.IsTrue(mainScript.juicesConsumedAtRecentMinuteStamps.ContainsKey(6));
	// Assert.IsTrue(mainScript.juicesConsumedAtRecentMinuteStamps.ContainsKey(9));
}

[Test]
public void GetRecentTotalJuiceIntoTanks() {
	MainScript mainScript = new MainScript();

	Dictionary<int, Dictionary<int, TankUsageStamp>> juiceAddedToTanksByIndexHistoryAtRecentMinuteStamps = new Dictionary<int, Dictionary<int, TankUsageStamp>>();

	mainScript.juiceAddedToTanksByIndexHistoryAtRecentMinuteStamps = juiceAddedToTanksByIndexHistoryAtRecentMinuteStamps;
	
	juiceAddedToTanksByIndexHistoryAtRecentMinuteStamps.Add(1, new Dictionary<int, TankUsageStamp>() {{0, new TankUsageStamp(1f)}, {1, new TankUsageStamp(5f)}});
	juiceAddedToTanksByIndexHistoryAtRecentMinuteStamps.Add(2, new Dictionary<int, TankUsageStamp>() {{0, new TankUsageStamp(3f)}, {1, new TankUsageStamp(3f)}});

	mainScript.juiceAddedToTanksByIndexHistoryAtRecentMinuteStamps = juiceAddedToTanksByIndexHistoryAtRecentMinuteStamps;

	Dictionary<int, float> recentTotalJuiceInputs = mainScript.tank_And_Valve_Physical_Updates.GetRecentTotalJuiceIntoTanks();

	Assert.IsTrue(recentTotalJuiceInputs[0] == 4, "Recent total juice input for tank");
}

[Test]
public void GetRecentTotalJuiceTransferredPerValve() {
	MainScript mainScript = new MainScript();
	Dictionary<int, Dictionary<int, ValveDataStampPeriod>> valvesUsageHistoryAtRecentMinuteStamps = new Dictionary<int, Dictionary<int, ValveDataStampPeriod>>();
	valvesUsageHistoryAtRecentMinuteStamps.Add(1, new Dictionary<int, ValveDataStampPeriod> {{0, new ValveDataStampPeriod(300)},{1, new ValveDataStampPeriod(30)}});
	valvesUsageHistoryAtRecentMinuteStamps.Add(9, new Dictionary<int, ValveDataStampPeriod> {{0, new ValveDataStampPeriod(200)},{1, new ValveDataStampPeriod(100)}});
	mainScript.valvesUsageHistoryAtRecentMinuteStamps= valvesUsageHistoryAtRecentMinuteStamps;

	Dictionary<int, float> recentTotalJuiceTransferredByValve = mainScript.tank_And_Valve_Physical_Updates.GetRecentTotalJuiceTransferredPerValve();
	Assert.IsTrue(recentTotalJuiceTransferredByValve[0] == 500 && recentTotalJuiceTransferredByValve[1] == 130);
}

[Test]
public void AddNewValveHistoryAtMinTimeStamp() {
	MainScript mainScript = new MainScript();
	ValveDataStampPeriod newValveUsage = new ValveDataStampPeriod();
	newValveUsage.juiceTransferred += 100;

	int thisTimeStamp = 1;

	int valveIndex = 0;

	mainScript.valvesUsageHistoryAtRecentMinuteStamps = new Dictionary<int, Dictionary<int, ValveDataStampPeriod>>();
	mainScript.tank_And_Valve_Physical_Updates.AddNewValveHistoryAtMinTimeStamp(newValveUsage, thisTimeStamp, valveIndex);

	Assert.IsTrue(mainScript.valvesUsageHistoryAtRecentMinuteStamps[1][valveIndex].juiceTransferred == 100);

	newValveUsage = new ValveDataStampPeriod();
	newValveUsage.juiceTransferred += 200;

	mainScript.tank_And_Valve_Physical_Updates.AddNewValveHistoryAtMinTimeStamp(newValveUsage, thisTimeStamp, valveIndex);

	Assert.IsTrue(mainScript.valvesUsageHistoryAtRecentMinuteStamps[1][valveIndex].juiceTransferred == 300);
}

[Test]
public void AddNewTankUsageStamp() {
	MainScript mainScript = new MainScript();
	TankUsageStamp newTankUsage = new TankUsageStamp();
	newTankUsage.totalJuiceInput += 100;

	int thisTimeStamp = 1;

	int tankIndex = 0;

	mainScript.juiceAddedToTanksByIndexHistoryAtRecentMinuteStamps = new Dictionary<int, Dictionary<int, TankUsageStamp>>();
	mainScript.tank_And_Valve_Physical_Updates.AddNewTankUsageStamp(newTankUsage, thisTimeStamp, tankIndex);
	Assert.IsTrue(mainScript.juiceAddedToTanksByIndexHistoryAtRecentMinuteStamps[1][tankIndex].totalJuiceInput == 100);

	newTankUsage = new TankUsageStamp();
	newTankUsage.totalJuiceInput += 200;

	mainScript.tank_And_Valve_Physical_Updates.AddNewTankUsageStamp(newTankUsage, thisTimeStamp, tankIndex);
	Assert.IsTrue(mainScript.juiceAddedToTanksByIndexHistoryAtRecentMinuteStamps[1][tankIndex].totalJuiceInput == 300);

}

[Test]
public void RecentTotalConsumptionPerJuice() {
	MainScript mainScript = new MainScript();

	Dictionary<int, Dictionary<string, float>> juicesConsumedAtRecentMinuteStamps = new Dictionary<int, Dictionary<string, float>>();
	juicesConsumedAtRecentMinuteStamps.Add(1, new Dictionary<string, float>() {{Common.JuiceNames.Blueberry.ToString(), 1f}, {Common.JuiceNames.Strawberry.ToString(), 5}});
	juicesConsumedAtRecentMinuteStamps.Add(2, new Dictionary<string, float>() {{Common.JuiceNames.Blueberry.ToString(), 3}, {Common.JuiceNames.Strawberry.ToString(), 4}});

	mainScript.juicesConsumedAtRecentMinuteStamps = juicesConsumedAtRecentMinuteStamps;

	Dictionary<string, float> recentConsumptionsPerJuice = mainScript.tank_And_Valve_Physical_Updates.GetRecentTotalConsumptionPerJuice();

	Assert.IsTrue(recentConsumptionsPerJuice[Common.JuiceNames.Blueberry.ToString()] == 4, "Recent juice consumption");
}



[Test]
public void GetValvesWithHighRecentJuiceTransfer() {
	MainScript mainScript = new MainScript();
	Dictionary<int, Dictionary<int, ValveDataStampPeriod>> valvesUsageHistoryAtRecentMinuteStamps = new Dictionary<int, Dictionary<int, ValveDataStampPeriod>>();
	valvesUsageHistoryAtRecentMinuteStamps.Add(1, new Dictionary<int, ValveDataStampPeriod> {{0, new ValveDataStampPeriod(300)},{1, new ValveDataStampPeriod(30)}});
	valvesUsageHistoryAtRecentMinuteStamps.Add(9, new Dictionary<int, ValveDataStampPeriod> {{0, new ValveDataStampPeriod(200)},{1, new ValveDataStampPeriod(100)}});
	mainScript.valvesUsageHistoryAtRecentMinuteStamps= valvesUsageHistoryAtRecentMinuteStamps;

	Common.recentTotalJuiceTransferredByValveThreshold = 400;

	HashSet<int> ValvesWithHighJuiceTransferredByIndex = mainScript.tank_And_Valve_Physical_Updates.ValvesWithHighJuiceTransferredByIndex();
	Assert.IsTrue(ValvesWithHighJuiceTransferredByIndex.Contains(0) && !ValvesWithHighJuiceTransferredByIndex.Contains(1));
}

[Test]
public void AddNewJuiceConsumptionToMinTimeStamp() {
	MainScript mainScript = new MainScript();

	Dictionary<int, Dictionary<string, float>> juicesConsumedAtRecentMinuteStamps = new Dictionary<int, Dictionary<string, float>>();
	juicesConsumedAtRecentMinuteStamps.Add(1, new Dictionary<string, float>() {{Common.JuiceNames.Blueberry.ToString(), 5f}, {Common.JuiceNames.Strawberry.ToString(), 7}});

	mainScript.juicesConsumedAtRecentMinuteStamps = juicesConsumedAtRecentMinuteStamps;

	Dictionary<string, float> newJuiceConsumption = new Dictionary<string, float>() {{Common.JuiceNames.Blueberry.ToString(), 1f}, {Common.JuiceNames.Strawberry.ToString(), 2}, {"Green", 3}};
	int newMinuteStamp = 1;
	mainScript.tank_And_Valve_Physical_Updates.AddNewJuiceConsumptionToMinTimeStamp(newJuiceConsumption, newMinuteStamp);

	Assert.IsTrue(juicesConsumedAtRecentMinuteStamps[1][Common.JuiceNames.Blueberry.ToString()] == 6, "Added juice consumption to existing stamp dictionary failed.");

}

[Test]
public void RecentConsumptionJuiceIsSubstantial() {
	MainScript mainScript = new MainScript();
	// Dictionary<int, Dictionary<string, float>> juicesConsumedAtRecentMinuteStamp = new Dictionary<int, Dictionary<string, float>>();
	// juicesConsumedAtRecentMinuteStamp.Add(1, new Dictionary<string, float>() {{"Blueberry", 15f}});
	// juicesConsumedAtRecentMinuteStamp.Add(2, new Dictionary<string, float>() {{"Blueberry", 3}});

	// mainScript.juicesConsumedAtRecentMinuteStamp = juicesConsumedAtRecentMinuteStamp;

	Assert.IsTrue(mainScript.tank_And_Valve_Physical_Updates.RecentConsumptionJuiceIsSubstantial(7, 5), "Juice consumption substantial test failed");

}
}
