﻿using UnityEngine;
using UnityEditor;
using UnityEngine.TestTools;
using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;


public class JuiceBlendersTests
{
    List<ProductData> productData = new List<ProductData>();
    List<TankData> tankData = new List<TankData>();
    List<RawMaterialData> rawMaterialData = new List<RawMaterialData>();
    Dictionary<string, float> fulfilledBiasProductToOrder;
    List<Order> orders = new List<Order>();

    public JuiceBlendersTests()
    {

    }

    [Test]
    public void JuiceCostExcessive()
    {
        Common.juiceCostExcessFactor = 1.5f;
        MainScript mainScript = new MainScript();
        rawMaterialData = new List<RawMaterialData>();
        RawMaterialData thisRawMaterial = new RawMaterialData(juiceName: Common.JuiceNames.Blueberry.ToString(), minPrice: 1.0f);
        thisRawMaterial.price = 1.6f;
        rawMaterialData.Add(thisRawMaterial);
        mainScript.rawMaterialData = rawMaterialData;
        Assert.IsTrue(mainScript.JuiceCostExcessive(Common.JuiceNames.Blueberry.ToString(), 1.5f), "JuiceCostExcessiveTestFailed");
        Assert.IsFalse(mainScript.JuiceCostExcessive(Common.JuiceNames.Blueberry.ToString(), 1.8f), "JuiceCostExcessiveTestFailed");
    }

    [Test]
    public void ChanceSettle()
    {
        float chanceSettle = 0;
        chanceSettle = ProcurementAreaScript.ChanceSettle(currentPrice: 1.6f, bidPrice: 1.3f, minPrice: 1.1f);
        Assert.IsTrue(chanceSettle >= 0 && chanceSettle <= 1, "Case 1- within 0 and 1 bounds");

        chanceSettle = ProcurementAreaScript.ChanceSettle(currentPrice: 1.6f, bidPrice: 1.3f, minPrice: 1.4f);
        Assert.IsTrue(chanceSettle >= 0 && chanceSettle <= 1, "Case 2- within 0 and 1 bounds");

        chanceSettle = ProcurementAreaScript.ChanceSettle(currentPrice: 1.6f, bidPrice: 1.3f, minPrice: 1.3f);
        Assert.IsTrue(chanceSettle >= 0 && chanceSettle <= 0.1f, "Case 3- chance should be very low");

        chanceSettle = ProcurementAreaScript.ChanceSettle(currentPrice: 1.6f, bidPrice: 1.58f, minPrice: 1.3f);
        Assert.IsTrue(chanceSettle <= 1 && chanceSettle >= 0.9f, "Case 4- chance should be very high");

        chanceSettle = ProcurementAreaScript.ChanceSettle(currentPrice: 1.6f, bidPrice: 1.45f, minPrice: 1.3f);
        Assert.IsTrue(chanceSettle <= 0.9f && chanceSettle >= 0.3f, "Case 4- chance should be fairly high");

    }

    [Test]
    public void JuicesWithExcessCostAndHighConsumption()
    {

        MainScript mainScript = new MainScript();
        Common.recentJuiceConsumptionHighThreshold = 13f;
        Common.juiceCostExcessFactor = 1.4f;

        Dictionary<int, Dictionary<string, float>> juicesConsumedAtRecentMinuteStamps = new Dictionary<int, Dictionary<string, float>>();
        juicesConsumedAtRecentMinuteStamps.Add(1, new Dictionary<string, float>() { { Common.JuiceNames.Blueberry.ToString(), 5f }, { Common.JuiceNames.Strawberry.ToString(), 7 } });
        juicesConsumedAtRecentMinuteStamps.Add(2, new Dictionary<string, float>() { { Common.JuiceNames.Blueberry.ToString(), 4f }, { Common.JuiceNames.Strawberry.ToString(), 5 } });
        juicesConsumedAtRecentMinuteStamps.Add(3, new Dictionary<string, float>() { { Common.JuiceNames.Blueberry.ToString(), 6f }, { Common.JuiceNames.Strawberry.ToString(), 4 } });

        mainScript.juicesConsumedAtRecentMinuteStamps = juicesConsumedAtRecentMinuteStamps;

        rawMaterialData = new List<RawMaterialData>();
        mainScript.rawMaterialData = rawMaterialData;

        RawMaterialData thisRawMaterial = new RawMaterialData();
        thisRawMaterial.minPrice = 0.3f;
        thisRawMaterial.price = 0.9f;
        thisRawMaterial.juiceName = Common.JuiceNames.Blueberry.ToString();
        rawMaterialData.Add(thisRawMaterial);

        thisRawMaterial = new RawMaterialData();
        thisRawMaterial.minPrice = 0.3f;
        thisRawMaterial.price = 0.32f;
        thisRawMaterial.juiceName = Common.JuiceNames.Strawberry.ToString();
        rawMaterialData.Add(thisRawMaterial);

        Assert.IsTrue(mainScript.JuicesWithExcessCostAndHighConsumption().SetEquals(new HashSet<string>() { { Common.JuiceNames.Blueberry.ToString() } }));

        juicesConsumedAtRecentMinuteStamps = new Dictionary<int, Dictionary<string, float>>();
        juicesConsumedAtRecentMinuteStamps.Add(1, new Dictionary<string, float>() { { Common.JuiceNames.Blueberry.ToString(), 3f }, { Common.JuiceNames.Strawberry.ToString(), 7 } });
        juicesConsumedAtRecentMinuteStamps.Add(2, new Dictionary<string, float>() { { Common.JuiceNames.Blueberry.ToString(), 4f }, { Common.JuiceNames.Strawberry.ToString(), 5 } });
        juicesConsumedAtRecentMinuteStamps.Add(3, new Dictionary<string, float>() { { Common.JuiceNames.Blueberry.ToString(), 2f }, { Common.JuiceNames.Strawberry.ToString(), 4 } });

        mainScript.juicesConsumedAtRecentMinuteStamps = juicesConsumedAtRecentMinuteStamps;

        Assert.IsTrue(mainScript.JuicesWithExcessCostAndHighConsumption().SetEquals(new HashSet<string>() { }));
    }


    public bool CheckProductComponentFracsValid(List<ProductData> productDataIn)
    {
        bool checkAllOK = true;
        foreach (ProductData product in productDataIn)
        {

            float targetFracSums = 0;
            foreach (JuiceSpecs juice in product.juiceSpecs)
            {
                targetFracSums += juice.targetFrac;
            }

            if (targetFracSums != 1)
            {
                //Assert.IsFalse(true);
                Debug.Log("Error with target fraction sum specs in test.");
                checkAllOK = false;
            }
        }
        return checkAllOK;
    }

    [Test]
    public void GenTwoRandomFracs()
    {
        bool allChecks = true;
        for (int i = 0; i < 100; i++)
        {
            float[] output = PlantSetup.GenTwoRandomFracs();
            if (output[0] < 0.2f || output[0] > 0.8f)
            {
                allChecks = false;
            }
            if (output[1] < 0.2f || output[1] > 0.8f)
            {
                allChecks = false;
            }
        }
        Assert.IsTrue(allChecks, "Gen two random numbers failed");
    }

    [Test]
    public void getLevelData()
    {
        MainScript mainScript = new MainScript();
        mainScript.sales = 30000;

        List<LevelSingleCondition> levelConditions = new Level1().GetLevelConditions();
        Debug.Log(levelConditions[0].ConditionMet());
        Debug.Log(levelConditions[0].GetDescription());

    }

    [Test]
    public void IsFunctionalAreaInactive()
    {
        Level testLevel = new Level1();
        Assert.IsTrue(testLevel.IsFunctionalAreaInactive(FunctionalAreaNames.procurement));
    }

    [Test]
    public void GenThreeRandomFracs()
    {
        bool allChecks = true;
        for (int i = 0; i < 100; i++)
        {
            Debug.Log("Iteration: " + i);
            float[] output = PlantSetup.GenThreeRandomFracs();
            // Want target fraction for each juice to be  > 0.2f, and < 0.8f. Mathematically with all three juices in a 3 juice tank, the maximum target fraction is 0.6f.
            if (output[0] < 0.2f || output[0] > 0.6f)
            {
                allChecks = false;
            }
            if (output[1] < 0.2f || output[1] > 0.6f)
            {
                allChecks = false;
            }
            if (output[2] < 0.2f || output[2] > 0.6f)
            {
                allChecks = false;
            }
        }
        Assert.IsTrue(allChecks, "Gen two random numbers failed");
    }

    [Test]
    public void eachTankSpecCheck()
    {
        bool allChecks = true;

        TankData newTank = new TankData();

        newTank.tankName = "A/B";
        TankJuiceData newJuice;
        newJuice = new TankJuiceData();
        newJuice.juiceName = Common.JuiceNames.Blueberry.ToString();
        newJuice.targetFrac = 0.2f;
        newJuice.actualFrac = 0.3f;
        newTank.tankJuiceData.Add(newJuice);
        newJuice = new TankJuiceData();
        newJuice.juiceName = Common.JuiceNames.Pineapple.ToString();
        newJuice.targetFrac = 0.8f;
        newJuice.actualFrac = 0.84f;
        newTank.tankJuiceData.Add(newJuice);

        MainScript mainScript = new MainScript();

        Tank_And_Valve_Physical_Updates tank_And_Valve_Physical_Updates = new Tank_And_Valve_Physical_Updates();

        tank_And_Valve_Physical_Updates.EachTankSpecSet(newTank);
        if (newTank.inSpec == true)
        {
            allChecks = false;
        }

        newTank.tankJuiceData[0].actualFrac = 0.23f;
        tank_And_Valve_Physical_Updates.EachTankSpecSet(newTank);
        if (newTank.inSpec == false)
        {
            allChecks = false;
        }

        Assert.IsTrue(allChecks, "tank Spec func failed");
    }

    [Test]
    public void checkConvertTwoDecimal()
    {
        bool allChecks = true;
        float output = Common.ConvertToTwoDecimal(1.234f);
        if (output != 1.23f)
        {
            allChecks = false;
        }

        output = Common.ConvertToTwoDecimal(21.9814f);
        if (output != 21.98f)
        {
            allChecks = false;
        }
        Assert.IsTrue(allChecks, "Problem with convert to two decimal places function.");
    }

    [Test]
    public void NextTruckCallLoopForThisOutletTank()
    {
        bool allChecks = true;
        List<Order> orders = new List<Order>();

        Order newOrder = new Order();
        newOrder.accepted = false;
        newOrder.lost = false;
        newOrder.declined = false;
        newOrder.orderCreated = 10;
        newOrder.orderNum = 0 + 1;
        newOrder.productName = "A/B";
        newOrder.orderAcceptedTime = 22;
        orders.Add(newOrder);

        newOrder = new Order();
        newOrder.accepted = true;
        newOrder.lost = false;
        newOrder.declined = false;
        newOrder.orderCreated = 11;
        newOrder.orderAcceptedTime = 20;
        newOrder.orderNum = 1 + 1;
        newOrder.productName = "A/B";
        orders.Add(newOrder);

        newOrder = new Order();
        newOrder.accepted = true;
        newOrder.lost = false;
        newOrder.declined = false;
        newOrder.orderCreated = 15;
        newOrder.orderAcceptedTime = 17;
        newOrder.orderNum = 2 + 1;
        newOrder.productName = "C/D";
        orders.Add(newOrder);

        newOrder = new Order();
        newOrder.accepted = true;
        newOrder.lost = false;
        newOrder.declined = false;
        newOrder.orderCreated = 18;
        newOrder.orderAcceptedTime = 21;
        newOrder.orderNum = 3 + 1;
        newOrder.productName = "C/D";
        orders.Add(newOrder);

        TankData oneTank = new TankData();
        oneTank.tankName = "Outlet-A/B";

        MainScript mainScript = new MainScript();
        mainScript.orders = orders;

        mainScript.toDeliverArraySorted = mainScript.UpdateToDeliverArray();
        List<DelayAction> delayActions = new List<DelayAction>();
        mainScript.delayActions = delayActions;

        mainScript.truckHandling.NextTruckCallLoopForThisOutletTank(oneTank);
        if (orders[1].truckCalled != true || delayActions[0].orderIndex != 1)
        {
            allChecks = false;
        }

        Debug.Log("Count of delay action in test is:" + delayActions.Count);
        Debug.Log("Action name in test is:" + delayActions[0].actionName);

        Assert.IsTrue(allChecks, "Next truck called failed.");
    }

    [Test]
    public void CheckStateOrders()
    {
        bool allChecks = true;

        Order newOrder = new Order();
        newOrder.accepted = false;
        newOrder.lost = false;
        newOrder.declined = false;
        newOrder.orderCreated = 10;

        List<MainAnimatedNotification> mainNotificationsToAnimate = new List<MainAnimatedNotification>();

        List<Order> orders = new List<Order>();
        orders.Add(newOrder);

        MainScript mainScript = new MainScript();
        mainScript.orders = orders;

        mainScript.elapsedTime = 200;

        mainScript.orderFuncs.CheckStateOrders();
        if (orders[0].lost != true)
        {
            //Debug.Log("Order should have been lost, and is.");
            allChecks = false;
        }

        newOrder = new Order();
        newOrder.accepted = false;
        newOrder.lost = false;
        newOrder.declined = true;
        newOrder.orderCreated = 10;

        orders = new List<Order>();
        mainScript.orders = orders;
        orders.Add(newOrder);

        mainScript.orderFuncs.CheckStateOrders();
        if (orders[0].lost != false)
        {
            allChecks = false;
            Debug.Log("Test A failed");
        }

        newOrder = new Order();
        newOrder.accepted = true;
        newOrder.lost = false;
        newOrder.declined = false;
        newOrder.orderCreated = 10;
        newOrder.orderAcceptedTime = 20;

        orders = new List<Order>();
        orders.Add(newOrder);
        mainScript.orders = orders;

        mainScript.elapsedTime = 630;

        mainScript.orderFuncs.CheckStateOrders();
        if (orders[0].penalty != 0.2f)
        {
            allChecks = false;
            Debug.Log("Test B failed");
        }

        newOrder = new Order();
        newOrder.accepted = true;
        newOrder.lost = true;
        newOrder.declined = false;
        newOrder.orderCreated = 10;
        newOrder.orderAcceptedTime = 20;

        orders = new List<Order>();
        mainScript.orders = orders;
        orders.Add(newOrder);

        mainScript.orderFuncs.CheckStateOrders();
        if (orders[0].penalty != 0f)
        {
            allChecks = false;
            Debug.Log("Test C failed");
        }
        Assert.IsTrue(allChecks, "State of orders and automatic triggers functions failed tests.");
    }

    [Test]
    public void OrderNumsToDisplayMatchLastDisplayed()
    {
        List<Order> lastOrdersDisplayed = new List<Order>();
        List<Order> ordersToDisplay = new List<Order>();

        OrderFuncs orderFuncs = new OrderFuncs();

        Assert.IsTrue(orderFuncs.OrderNumsToDisplayMatchLastDisplayed(ordersToDisplay, lastOrdersDisplayed));

        Order newOrder = new Order();
        newOrder.orderNum = 1;
        lastOrdersDisplayed.Add(newOrder);
        newOrder = new Order();
        newOrder.orderNum = 2;
        lastOrdersDisplayed.Add(newOrder);
        newOrder = new Order();
        newOrder.orderNum = 5;
        lastOrdersDisplayed.Add(newOrder);


        newOrder = new Order();
        newOrder.orderNum = 1;
        ordersToDisplay.Add(newOrder);
        newOrder = new Order();
        newOrder.orderNum = 2;
        ordersToDisplay.Add(newOrder);
        newOrder = new Order();
        newOrder.orderNum = 5;
        ordersToDisplay.Add(newOrder);


        Assert.IsTrue(orderFuncs.OrderNumsToDisplayMatchLastDisplayed(ordersToDisplay, lastOrdersDisplayed));

        newOrder = new Order();
        newOrder.orderNum = 7;
        ordersToDisplay.Add(newOrder);
        Assert.IsFalse(orderFuncs.OrderNumsToDisplayMatchLastDisplayed(ordersToDisplay, lastOrdersDisplayed));

    }

    [Test]
    public void OrderToDisplayChanged()
    {
        List<Order> lastOrdersDisplayed = new List<Order>();
        List<Order> ordersAtEnquiryStage = new List<Order>();

        OrderFuncs orderFuncs = new OrderFuncs();
        orderFuncs.ordersAtEnquiryStage = ordersAtEnquiryStage;
        orderFuncs.lastDisplayedOrders = lastOrdersDisplayed;

        Assert.IsFalse(orderFuncs.OrderToDisplayChanged());

        Order newOrder = new Order();
        newOrder.orderNum = 1;
        ordersAtEnquiryStage.Add(newOrder);
        newOrder = new Order();
        newOrder.orderNum = 2;
        ordersAtEnquiryStage.Add(newOrder);
        newOrder = new Order();
        newOrder.orderNum = 5;
        ordersAtEnquiryStage.Add(newOrder);

        newOrder = new Order();
        newOrder.orderNum = 1;
        ordersAtEnquiryStage.Add(newOrder);
        newOrder = new Order();
        newOrder.orderNum = 2;
        ordersAtEnquiryStage.Add(newOrder);
        newOrder = new Order();
        newOrder.orderNum = 5;
        ordersAtEnquiryStage.Add(newOrder);

        Assert.IsTrue(orderFuncs.OrderToDisplayChanged());
        orderFuncs.SetLastDisplayedOrders();

        ordersAtEnquiryStage[2].accepted = true;
        Assert.IsTrue(orderFuncs.OrderToDisplayChanged());
        orderFuncs.SetLastDisplayedOrders();

        newOrder = new Order();
        newOrder.orderNum = 7;
        ordersAtEnquiryStage.Add(newOrder);

        Assert.IsTrue(orderFuncs.OrderToDisplayChanged());

    }

    [Test]
    public void JuiceTransferValveLoop()
    {
        bool allChecks = true;

        rawMaterialData = StandardTestRawMaterialsCreate();

        List<TankData> tankData = new List<TankData>();
        List<Valve> valveData = new List<Valve>();

        //Source tank
        TankData newTank = new TankData();
        newTank.tankName = "Supply-A";
        newTank.tankLevelFrac = 0.5f;
        newTank.tankCapacity = 1000;
        tankData.Add(newTank); //tank index 0

        TankJuiceData newJuice = new TankJuiceData();
        newJuice.juiceName = Common.JuiceNames.Blueberry.ToString();
        newJuice.targetFrac = 1.0f;
        newJuice.actualFrac = 1.0f;
        newTank.tankJuiceData.Add(newJuice);

        Valve newValve = new Valve();
        newValve.valveState = (int)Common.ValveState.Open;
        newValve.valveDestination = "A/B";
        newValve.tankNameValveAttachedTo = "Supply-A";
        newValve.tankIndexValveAttachedTo = 0;
        newValve.valveDestinationTankIndex = 1;
        newValve.valveFlowRate = 50;
        valveData.Add(newValve);


        //Destination Tank
        newTank = new TankData();
        newTank.tankName = "A/B";
        newTank.tankLevelFrac = 0.5f;
        newTank.tankCapacity = 1000;
        tankData.Add(newTank); // tank index = 1

        newJuice = new TankJuiceData();
        newJuice.juiceName = Common.JuiceNames.Blueberry.ToString();
        newJuice.targetFrac = 0.2f;
        newJuice.actualFrac = 0.3f;
        newTank.tankJuiceData.Add(newJuice);
        newJuice = new TankJuiceData();
        newJuice.juiceName = Common.JuiceNames.Pineapple.ToString();
        newJuice.targetFrac = 0.8f;
        newJuice.actualFrac = 0.7f;
        newTank.tankJuiceData.Add(newJuice);

        float timeDelta = 1;
        float juiceCostToAdd = 0;

        MainScript mainScript = new MainScript();
        mainScript.tankData = tankData;
        mainScript.valveData = valveData;
        mainScript.rawMaterialData = rawMaterialData;
        mainScript.rawMatPrices = Common.ConvertRawMatArrayToDictionary(rawMaterialData);

        mainScript.tank_And_Valve_Physical_Updates.JuiceTransferValveLoop(timeDelta);
        Debug.Log("Cost of juice to transfer" + juiceCostToAdd);
        Debug.Log("The fractions in A/B are: " + tankData[1].tankJuiceData[0].actualFrac + "and " + tankData[1].tankJuiceData[1].actualFrac);

        if (Mathf.Abs(tankData[1].tankJuiceData[0].actualFrac - 0.363f) >= 0.002f)
        {
            allChecks = false;
        }

        tankData.Clear();
        valveData.Clear();

        //	Debug.Log("The new tank data count is: " + tankData.Count + "should be 0.");

        newTank = new TankData();
        newTank.tankName = "A";
        newTank.tankLevelFrac = 0.5f;
        newTank.tankCapacity = 1000;

        newJuice = new TankJuiceData();
        newJuice.juiceName = Common.JuiceNames.Blueberry.ToString();
        newJuice.targetFrac = 1.0f;
        newJuice.actualFrac = 1.0f;
        newTank.tankJuiceData.Add(newJuice);
        tankData.Add(newTank);

        newValve = new Valve();
        newValve.valveState = (int)Common.ValveState.Open;
        newValve.valveDestination = "A/B";
        newValve.tankNameValveAttachedTo = "A";
        newValve.valveDestinationTankIndex = 1;
        newValve.valveFlowRate = 50;
        valveData.Add(newValve);


        //Destination Tank
        newTank = new TankData();
        newTank.tankName = "A/B";
        newTank.tankLevelFrac = 0.5f;
        newTank.tankCapacity = 1000;

        newJuice = new TankJuiceData();
        newJuice.juiceName = Common.JuiceNames.Blueberry.ToString();
        newJuice.targetFrac = 0.2f;
        newJuice.actualFrac = 0.3f;
        newTank.tankJuiceData.Add(newJuice);
        newJuice = new TankJuiceData();
        newJuice.juiceName = Common.JuiceNames.Pineapple.ToString();
        newJuice.targetFrac = 0.8f;
        newJuice.actualFrac = 0.7f;
        newTank.tankJuiceData.Add(newJuice);

        tankData.Add(newTank);

        timeDelta = 1;
        juiceCostToAdd = 0;

        mainScript.tank_And_Valve_Physical_Updates.JuiceTransferValveLoop(timeDelta);

        //	Debug.Log("The new tank level frac is: " + tankData[0].tankLevelFrac);

        if (Mathf.Abs(tankData[0].tankLevelFrac - 0.45f) >= 0.002f)
        {
            allChecks = false;
        }

        Assert.IsTrue(allChecks, "Juice transfer check failed.");

    }

    [Test]
    public void UpdateToDeliverArray()
    {
        bool allChecks = true;

        List<Order> orders = new List<Order>();

        Order newOrder = new Order();
        newOrder.accepted = true;
        newOrder.lost = false;
        newOrder.declined = false;
        newOrder.orderCreated = 10;
        newOrder.orderNum = 1;
        newOrder.orderAcceptedTime = 50;
        orders.Add(newOrder);

        newOrder = new Order();
        newOrder.accepted = true;
        newOrder.lost = false;
        newOrder.declined = false;
        newOrder.orderAcceptedTime = 40;
        newOrder.orderNum = 2;
        orders.Add(newOrder);

        newOrder = new Order();
        newOrder.accepted = true;
        newOrder.lost = false;
        newOrder.declined = false;
        newOrder.orderAcceptedTime = 30;
        newOrder.orderNum = 3;

        orders.Add(newOrder);

        newOrder = new Order();
        newOrder.accepted = true;
        newOrder.lost = true;
        newOrder.declined = false;
        newOrder.orderAcceptedTime = 35;
        newOrder.orderNum = 4;

        orders.Add(newOrder);

        newOrder = new Order();
        newOrder.accepted = true;
        newOrder.lost = false;
        newOrder.declined = false;
        newOrder.orderAcceptedTime = 42;
        newOrder.orderNum = 5;
        newOrder.fulfilled = true;

        orders.Add(newOrder);

        newOrder = new Order();
        newOrder.accepted = true;
        newOrder.lost = false;
        newOrder.declined = false;
        newOrder.orderAcceptedTime = 60;
        newOrder.orderNum = 6;
        newOrder.fulfilled = false;
        newOrder.truckArrived = true;

        orders.Add(newOrder);

        MainScript mainScript = new MainScript();
        mainScript.orders = orders;

        mainScript.toDeliverArraySorted = mainScript.UpdateToDeliverArray();

        if (mainScript.toDeliverArraySorted[0].Key != 3 - 1)
        {
            allChecks = false;
        }

        if (mainScript.toDeliverArraySorted[1].Key != 2 - 1)
        {
            allChecks = false;
        }

        if (mainScript.toDeliverArraySorted[2].Key != 1 - 1)
        {
            allChecks = false;
        }

        if (mainScript.toDeliverArraySorted[3].Key != 6 - 1)
        {
            allChecks = false;
        }

        for (int i = 0; i < mainScript.toDeliverArraySorted.Count; i++)
        {
            Debug.Log(mainScript.toDeliverArraySorted[i].Key);
        }

        Assert.IsTrue(allChecks, "Delivery array sort check failed.");
    }

    [Test]
    public void LoopTruckFull_OrderLost_Or_Penalty()
    {

        bool allChecks = true;

        List<Order> orders = new List<Order>();

        Order newOrder = new Order();
        newOrder.accepted = true;
        newOrder.lost = false;
        newOrder.declined = false;
        newOrder.orderCreated = 10;
        newOrder.orderNum = 1;
        newOrder.orderAcceptedTime = 50;
        orders.Add(newOrder);

        newOrder = new Order();
        newOrder.accepted = true;
        newOrder.lost = false;
        newOrder.declined = false;
        newOrder.orderAcceptedTime = 40;
        newOrder.orderNum = 2;
        orders.Add(newOrder);

        newOrder = new Order();
        newOrder.accepted = true;
        newOrder.lost = false;
        newOrder.declined = false;
        newOrder.orderAcceptedTime = 30;
        newOrder.orderNum = 3;

        orders.Add(newOrder);

        newOrder = new Order();
        newOrder.accepted = true;
        newOrder.lost = true;
        newOrder.declined = false;
        newOrder.orderAcceptedTime = 35;
        newOrder.orderNum = 4;

        orders.Add(newOrder);

        newOrder = new Order();
        newOrder.accepted = true;
        newOrder.lost = false;
        newOrder.declined = false;
        newOrder.orderAcceptedTime = 42;
        newOrder.orderNum = 5;
        newOrder.fulfilled = true;

        orders.Add(newOrder);

        newOrder = new Order();
        newOrder.accepted = true;
        newOrder.lost = false;
        newOrder.declined = false;
        newOrder.orderAcceptedTime = 60;
        newOrder.orderNum = 6;
        newOrder.fulfilled = false;
        newOrder.truckArrived = true;
        newOrder.volume = 300;
        newOrder.productName = "A/B";

        orders.Add(newOrder);

        MainScript mainScript = new MainScript();
        mainScript.orders = orders;

        mainScript.toDeliverArraySorted = mainScript.UpdateToDeliverArray();

        TankData oneTank = new TankData();
        oneTank.tankName = "Outlet-A/B";
        oneTank.tankCapacity = 1000;


        mainScript.goodwill = 10000;
        mainScript.sales = 5000;
        mainScript.profit = 15000;
        mainScript.elapsedTime = 15 * 60 + 70;
        mainScript.bankBalance = 80000;

        mainScript.truckHandling.LoopTruckFull_OrderLost_Or_Penalty(oneTank);
        if (orders[6 - 1].lost != true)
        {
            allChecks = false;
            Debug.Log("Failed at A.");
        }

        orders[6 - 1].lost = false;
        orders[6 - 1].volume = 300;

        orders[6 - 1].price = 11001.23f;
        orders[6 - 1].penalty = 0.2f;
        oneTank.tankLevelFrac = 0.4f;
        orders[6 - 1].truckArrived = true;

        mainScript.elapsedTime = 100;

        mainScript.truckHandling.LoopTruckFull_OrderLost_Or_Penalty(oneTank);
        if (orders[6 - 1].fulfilled != true && oneTank.tankLevelFrac != 0.1f)
        {
            allChecks = false;
            Debug.Log("Failed at B.");
        }

        if (mainScript.profit != 8800.98f + 15000 && mainScript.bankBalance != 80000 + 8800.98f && mainScript.sales != 5000 + 8800.98f && mainScript.goodwill != 11000)
        {
            allChecks = false;
            Debug.Log("Failed at C.");
        }

        Assert.IsTrue(allChecks, "Delivery lost or penalty check failed.");
    }

    [Test]
    public void MarketingLeakage()
    {
        bool allChecks = true;
        int advertising = 1000;
        int adLeakage = 100;

        MainScript mainScript = new MainScript();
        mainScript.advertising = advertising;
        mainScript.adLeakage = adLeakage;

        mainScript.UpdateAdDecay();

        if (mainScript.adLeakage != 150)
        {
            Debug.Log("Marketing leakage is wrong.");
            allChecks = false;
        }

        mainScript.advertising = 10000;
        mainScript.adLeakage = 1205;
        mainScript.UpdateAdDecay();

        if (mainScript.adLeakage != 1689)
        {
            Debug.Log("Marketing leakage is wrong for 10000 and 1205.");
            allChecks = false;
        }

        Assert.IsTrue(allChecks, "Marketing Leakage checks failed.");
    }


    [Test]
    public void GetRawMaterialDataFromJuiceName()
    {
        rawMaterialData = StandardTestRawMaterialsCreate();

        RawMaterialData thisRawMaterialData = Common.GetRawMaterialDataFromJuiceName(Common.JuiceNames.Blueberry.ToString(), rawMaterialData);
        Assert.IsTrue(thisRawMaterialData.price == rawMaterialData[0].price);
    }

    [Test]
    public void InflateJuiceCosts()
    {
        bool allChecks = true;
        rawMaterialData = StandardTestRawMaterialsCreate();

        MainScript mainScript = new MainScript();
        mainScript.rawMaterialData = rawMaterialData;


        Dictionary<string, float> rawMatPrices = Common.ConvertRawMatArrayToDictionary(rawMaterialData);
        float initialPrice = rawMatPrices[Common.JuiceNames.Blueberry.ToString()];

        mainScript.InflateJuiceCosts();

        rawMatPrices = Common.ConvertRawMatArrayToDictionary(rawMaterialData);
        float updatedPrice = rawMatPrices[Common.JuiceNames.Blueberry.ToString()];

        Debug.Log("Initial price of blueberry is: " + initialPrice);
        Debug.Log("new price of blueberry is: " + updatedPrice);

        float maxPrice = Common.GetRawMaterialDataFromJuiceName("Blueberry", rawMaterialData).maxPrice;
        Debug.Log("Max price for Blueberry is: " + maxPrice);

        float expectedPrice = Mathf.Min(initialPrice * Constants.Juice.costInflationFactorPer10Min, maxPrice);
        Debug.Log("Expected price is: " + expectedPrice);
        if (Mathf.Abs(rawMatPrices[Common.JuiceNames.Blueberry.ToString()] - expectedPrice) > 0.01f)
        {
            allChecks = false;
        }
        Assert.IsTrue(allChecks, "inflate juice costs failed.");
    }

    [Test]
    public void HandleDelayActions()
    {
        bool allChecks = true;

        rawMaterialData = StandardTestRawMaterialsCreate();

        productData = SetupCommodities.CreateProductSpecs(rawMaterialData);

        DelayAction newDelayAction;

        List<DelayAction> delayActions = new List<DelayAction>();

        List<TankData> tankData = new List<TankData>();
        List<Valve> valveData = new List<Valve>();
        List<Order> orders = new List<Order>();

        float bankBalance = 0;


        List<GameObject> pipeInstances = new List<GameObject>();

        int brandSpend = 20000;

        for (int i = 0; i < 100; i++)
        {
            productData[0].price = productData[0].priceMin;
            float oldPriceForOneProduct = productData[0].price;

            newDelayAction = new DelayAction();
            newDelayAction.actionName = Constants.Delays.brandDelay;
            newDelayAction.brandSpendAmount = brandSpend;
            newDelayAction.delayRemain = 0;
            delayActions.Add(newDelayAction);

            MainScript mainScript = new MainScript();
            mainScript.delayActions = delayActions;
            mainScript.productData = productData;

            mainScript.delayActionHandler.HandleDelayActions(1f);

            float newPriceForOneProduct = productData[0].price;
            float priceDiffOneProduct = newPriceForOneProduct - oldPriceForOneProduct;
            float lowerBoundPriceIncrease = Common.ConvertToTwoDecimal(Common.brandInvestRatio * brandSpend * (float)Common.lowerBoundBrandSpendRandomIncrease / 100 * oldPriceForOneProduct);
            float upperBoundPriceIncrease = Common.ConvertToTwoDecimal(Common.brandInvestRatio * brandSpend * (float)Common.upperBoundBrandSpendRandomIncrease / 100 * oldPriceForOneProduct);

            if (priceDiffOneProduct >= (lowerBoundPriceIncrease - 0.001f) && priceDiffOneProduct <= (upperBoundPriceIncrease + 0.001f))
            {

            }
            else
            {
                allChecks = false;
                Debug.Log("Brand delay problem");
                Debug.Log("Lower bound price increase is:  " + lowerBoundPriceIncrease);
                Debug.Log("Upper bound price increase is:  " + upperBoundPriceIncrease);
                Debug.Log("Price difference in error price increase is: " + priceDiffOneProduct);
            }
        }

        delayActions = new List<DelayAction>();

        int adSpend = 20000;

        newDelayAction = new DelayAction();
        newDelayAction.actionName = Constants.Delays.adDelay;
        newDelayAction.adSpendAmount = adSpend;
        newDelayAction.delayRemain = 0;

        delayActions.Add(newDelayAction);

        MainScript mainScript2 = new MainScript();
        mainScript2.delayActions = delayActions;
        mainScript2.delayActionHandler.HandleDelayActions(1f);

        if (mainScript2.advertising != adSpend)
        {
            allChecks = false;
            Debug.Log("Ad delay problem");
        }


        delayActions = new List<DelayAction>();
        float tankUpgradeCost = 20000;

        newDelayAction = new DelayAction();
        newDelayAction.actionName = Constants.Delays.tankUpgrade;
        newDelayAction.tankIndex = 0;
        newDelayAction.tankUpgradeCost = tankUpgradeCost;
        newDelayAction.tankCapacityExpandFactor = 2;
        newDelayAction.tankHeightExpandFactor = 1.5f;
        newDelayAction.delayRemain = 0;

        delayActions.Add(newDelayAction);

        tankData = new List<TankData>();
        TankData newTank = new TankData();

        newTank.tankName = "A/B";
        newTank.tankCapacity = 1000;
        newTank.tankLevelFrac = 0.5f;
        newTank.centery = 300;
        newTank.tankHeight = 200;

        tankData.Add(newTank);

        bankBalance = 0;


        mainScript2 = new MainScript();
        mainScript2.delayActions = delayActions;
        mainScript2.investments = 0;

        mainScript2.tankData = tankData;

        mainScript2.delayActionHandler.HandleDelayActions(1f);
        if (mainScript2.bankBalance != -tankUpgradeCost || mainScript2.investments != tankUpgradeCost)
        {
            allChecks = false;
            Debug.Log("Problem updating costs in tank upgrades");
            Debug.Log("Investment value is: " + mainScript2.investments);
        }

        if (tankData[0].tankCapacity != 2000)
        {
            allChecks = false;
            Debug.Log("Problem updating tank capacities in tank upgrades");
        }

        if (tankData[0].tankLevelFrac != 0.25f)
        {
            allChecks = false;
            Debug.Log("Problem updating tank level fractions in tank upgrades");
        }

        delayActions = new List<DelayAction>();
        valveData = new List<Valve>();
        int pumpUpgradeValue = 20000;

        Valve newValve = new Valve();
        newValve.valveFlowRate = 50;
        newValve.valveDestination = "";
        valveData.Add(newValve);

        DelayAction pumpDelayAction = new DelayAction();

        pumpDelayAction.actionName = Constants.Delays.pumpUpgrade;
        pumpDelayAction.thisValve = newValve;
        pumpDelayAction.lineThick = 4;
        pumpDelayAction.pumpUpgradeValue = pumpUpgradeValue;
        pumpDelayAction.valveFlowRate = 100;
        pumpDelayAction.delayRemain = 0;
        delayActions.Add(pumpDelayAction);

        mainScript2 = new MainScript();
        mainScript2.delayActions = delayActions;
        mainScript2.bankBalance = 0;
        mainScript2.investments = 0;

        mainScript2.delayActionHandler.HandleDelayActions(1f);
        if (mainScript2.bankBalance != -pumpUpgradeValue || mainScript2.investments != pumpUpgradeValue)
        {
            allChecks = false;
            Debug.Log("Problem updating costs in pump upgrades");
        }
        Debug.Log("bank bal is: " + bankBalance);
        if (newValve.valveFlowRate != 100f)
        {
            allChecks = false;
            Debug.Log("Problem with valve flow rate check in pump upgrades.");
        }
        Debug.Log("Valve flow rate is: " + newValve.valveFlowRate);

        delayActions = new List<DelayAction>();
        DelayAction truckCallDelayAction = new DelayAction();

        tankData = new List<TankData>();
        newTank = new TankData();
        newTank.tankIndex = 0;
        tankData.Add(newTank);

        Order thisOrder = new Order();
        orders = new List<Order>();
        orders.Add(thisOrder);

        truckCallDelayAction.actionName = Constants.Delays.callTruck;
        truckCallDelayAction.delayRemain = 0;
        truckCallDelayAction.tankIndex = 0;
        truckCallDelayAction.orderIndex = 0;
        delayActions.Add(truckCallDelayAction);

        mainScript2 = new MainScript();
        mainScript2.delayActions = delayActions;
        mainScript2.tankData = tankData;
        mainScript2.orders = orders;

        mainScript2.delayActionHandler.HandleDelayActions(1f);

        if (orders[0].truckArrived != true || tankData[0].deliverTruckState != (int)Common.DeliverTruckState.Present)
        {
            allChecks = false;
            Debug.Log("Problem updating delay call truck action");
        }


        //Now for valveWait test
        delayActions = new List<DelayAction>();
        valveData = new List<Valve>();
        newValve = new Valve();
        valveData.Add(newValve);

        DelayAction valveWaitThenOpen = new DelayAction();
        valveWaitThenOpen.actionName = Constants.Delays.valveOnAfterWaiting;
        valveWaitThenOpen.delayRemain = 0;
        valveWaitThenOpen.valveIndex = 0;
        delayActions = new List<DelayAction>();
        delayActions.Add(valveWaitThenOpen);

        mainScript2 = new MainScript();
        mainScript2.delayActions = delayActions;
        mainScript2.valveData = valveData;

        mainScript2.delayActionHandler.HandleDelayActions(1f);
        if (valveData[0].valveState != (int)Common.ValveState.Open)
        {
            allChecks = false;
            Debug.Log("Problem with test of open supply valve after waiting.");
        }

        delayActions = new List<DelayAction>();
        valveData = new List<Valve>();
        newValve = new Valve();
        newValve.valveState = (int)Common.ValveState.Waiting;
        valveData.Add(newValve);

        valveWaitThenOpen = new DelayAction();
        valveWaitThenOpen.actionName = Constants.Delays.valveOnAfterWaiting;
        valveWaitThenOpen.delayRemain = 2;
        valveWaitThenOpen.valveIndex = 0;
        delayActions = new List<DelayAction>();
        delayActions.Add(valveWaitThenOpen);

        mainScript2.delayActions = delayActions;

        mainScript2.delayActionHandler.CancelValveWaitingDelayAction(0, delayActions);
        if (delayActions.Count != 0)
        {
            allChecks = false;
            Debug.Log("Problem with cancel open of supply valve while waiting");
        }

        Assert.IsTrue(allChecks, "Delayed action tests failed");
    }

    [Test]
    public void PayJuiceCosts()
    {
        bool allChecks = true;

        float profit = 2000;
        float bankBalance = 30000;
        float costOfSales = 4000;
        float juiceCostToAdd = 5000;

        MainScript mainScript = new MainScript();
        mainScript.profit = profit;
        mainScript.bankBalance = bankBalance;
        mainScript.costOfSales = costOfSales;
        mainScript.juiceCostToAdd = juiceCostToAdd;

        mainScript.PayJuiceCosts();
        if (mainScript.profit != -3000 || mainScript.bankBalance != 25000 || mainScript.costOfSales != 9000 || mainScript.juiceCostToAdd != 0)
        {
            Debug.Log("Juice payment function has error");
            allChecks = false;
        }
        Assert.IsTrue(allChecks, "Juice payment check failed.");
    }

    [Test]
    public void UpdateInterestRatePercen()
    {
        bool allChecks = true;
        float interestPercen = 0;
        MainScript mainScript = new MainScript();
        mainScript.interestPercen = interestPercen;

        for (int i = 0; i < 100; i++)
        {
            mainScript.UpdateInterestRatePercentage();
            Debug.Log("Interest rate is: " + interestPercen);
            if (mainScript.interestPercen > Common.interestRatePercenMax || mainScript.interestPercen < (Common.interestRatePercenMin))
            {
                allChecks = false;
            }
        }
        Assert.IsTrue(allChecks, "Interest rates outside of bounds.");

    }

    [Test]
    public void CheckDecayedGoodWill()
    {
        bool allChecks = true;

        MainScript mainScript = new MainScript();
        mainScript.goodwill = 1;

        mainScript.DecayedGoodWill();
        if (mainScript.goodwill != 0.95f)
        {
            allChecks = false;
        }

        mainScript.goodwill = 2000;
        mainScript.DecayedGoodWill();
        if (mainScript.goodwill != 0.95f * 2000)
        {
            allChecks = false;
        }


        Assert.IsTrue(allChecks, "Problem with decayed goodwill function.");
    }

    [Test]
    public void CheckUpdateRealtimeInterest()
    {
        bool allChecks = true;
        float interestToUpdate = 0;

        MainScript mainScript = new MainScript();
        mainScript.bankBalance = 1000;
        mainScript.interestToUpdate = interestToUpdate;
        mainScript.interestPercen = 5;

        mainScript.UpdateRealtimeInterest();

        Debug.Log("Interest to add for one second is: " + interestToUpdate);
        if (Mathf.Abs(mainScript.interestToUpdate - 0.08333f) > 0.001f)
        {
            allChecks = false;
        }
        Assert.IsTrue(allChecks, "Problem with interest function.");
    }

    [Test]
    public void CheckJuiceWastePercen() {
        
        float juiceWaste = 3000;
        float juiceDelivered = 5000;

        float juiceWastePercen = TargetsArea.PercenJuiceWasted(juiceWasted: juiceWaste, juiceDelivered: juiceDelivered);
        Assert.IsTrue(juiceWastePercen == 3000f/5000*100);
//
        juiceWaste = 0;
        juiceDelivered = 4000;

        juiceWastePercen = TargetsArea.PercenJuiceWasted(juiceWasted: juiceWaste, juiceDelivered: juiceDelivered);
        Assert.IsTrue(juiceWastePercen == 0);
//
        juiceWaste = 5000;
        juiceDelivered = 0;

        juiceWastePercen = TargetsArea.PercenJuiceWasted(juiceWasted: juiceWaste, juiceDelivered: juiceDelivered);
        Assert.IsTrue(juiceWastePercen == 100);

    }

    [Test]
    public void CheckTankAndProductDataMatches()
    {

        rawMaterialData = StandardTestRawMaterialsCreate();

        productData = SetupCommodities.CreateProductSpecs(rawMaterialData);
        tankData = PlantSetup.CreateTanks();

        bool allChecks = true;

        foreach (ProductData thisProduct in productData)
        {
            TankData foundTank = tankData.Find(tank => tank.tankName == thisProduct.productName);
            if (foundTank == null)
            {
                allChecks = false;
                Debug.Log("The matching product tank name was not found for product name:" + thisProduct.productName);
            }
            foundTank = tankData.Find(tank => tank.tankName == "Outlet-" + thisProduct.productName);
            if (foundTank == null)
            {
                allChecks = false;
                //Assert.IsTrue(false, "The matching outlet tank name was not found for product name:" + thisProduct.productName);
                Debug.Log("The matching outlet tank name was not found for product name:" + thisProduct.productName);
            }
            //Debug.Log(thisProduct.productName);
        }

        foreach (TankData thisTank in tankData)
        {
            if (thisTank.tankName.Contains("/"))
            {
                ProductData foundProduct;
                if (!thisTank.tankName.Contains("Outlet"))
                {
                    foundProduct = productData.Find(product => product.productName == thisTank.tankName);
                    if (foundProduct == null)
                    {
                        Debug.Log("The matching product name was not found for tank name: " + thisTank.tankName);
                        allChecks = false;
                    }
                }
                else
                {
                    foundProduct = productData.Find(product => "Outlet-" + product.productName == thisTank.tankName);
                    if (foundProduct == null)
                    {
                        allChecks = false;
                        Debug.Log("The matching product name was not found for tank name: " + thisTank.tankName);
                    }
                }
            }
        }
        Assert.IsTrue(allChecks, "Products and tanks data not matching:");
    }

    [Test]
    public void BrandLeakage()
    {
        bool allChecks = true;

        productData = new List<ProductData>();
        productData.Add(new ProductData());
        productData.Add(new ProductData());

        productData[0].priceMin = 1f;
        productData[0].priceCap = 2f;
        productData[0].price = 1.5f;

        productData[1].priceMin = 1f;
        productData[1].priceCap = 2f;
        productData[1].price = 1.1f;

        MainScript mainScript = new MainScript();
        mainScript.productData = productData;

        mainScript.BrandLeakage();

        if (productData[0].price != 1.49f)
        {
            allChecks = false;
        }

        if (productData[1].price != 1.09f)
        {
            allChecks = false;
        }

        Assert.IsTrue(allChecks, "Brand leakage check failed. New prices unexpected.");
    }

    [Test]
    public void CheckColors()
    {
        bool allChecks = true;

        List<Color> colors = new List<Color> { new Color(1, 0, 0), new Color(0, 1, 0) };
        List<float> actualFracs = new List<float> { 0.5f, 0.5f };

        Color color = Common.CombineColors(colors, actualFracs);

        if (color != new Color(0.5f, 0.5f, 0))
        {
            Debug.Log("COLORS MATCH");
            allChecks = false;
        }

        colors = new List<Color> { new Color(0, 0, 0), new Color(0, 0, 1) };
        actualFracs = new List<float> { 0.5f, 0.5f };
        color = Common.CombineColors(colors, actualFracs);

        if (color != new Color(0f, 0f, 0.5f))
        {
            Debug.Log("COLORS Don't MATCH");
            allChecks = false;
        }


        colors = new List<Color> { new Color(0, 0, 0), new Color(0, 0, 0) };
        actualFracs = new List<float> { 0.5f, 0.5f };
        color = Common.CombineColors(colors, actualFracs);

        if (color != new Color(0f, 0f, 0f))
        {
            Debug.Log("COLORS Don't MATCH");
            allChecks = false;
        }

        colors = new List<Color> { new Color(1, 1, 1), new Color(1, 1, 1) };
        actualFracs = new List<float> { 0.5f, 0.5f };
        color = Common.CombineColors(colors, actualFracs);

        if (color != new Color(1f, 1f, 1f))
        {
            Debug.Log("COLORS Don't MATCH");
            allChecks = false;
        }
        Assert.IsTrue(allChecks, "Color algorithm has problem.");

    }


    [Test]
    public void TestProductCreate()
    {
        bool allChecks = true;
        List<ProductData> productData = SetupCommodities.CreateProductSpecs(rawMaterialData);
        for (int i = 0; i < productData.Count; i++)
        {
            ProductData thisProduct = productData[i];
            HashSet<string> productNameSplit = SetupCommodities.ConvertTankNameToJuiceComponentNames(thisProduct.productName);
            HashSet<string> juiceNames = new HashSet<string>();
            foreach (JuiceSpecs juiceSpec in thisProduct.juiceSpecs)
            {
                juiceNames.Add(juiceSpec.juiceName);
            }

            if (juiceNames.SetEquals(productNameSplit))
            {
                Debug.Log("Equal Hash sets for product data");
            }
            else
            {
                Debug.Log("Unequal hash sets for product data");
                allChecks = false;
                //Assert.AreEqual(true, false, "Product component check failed for code setup data- letters and juice names don't match.");
            }

            Debug.Log("Price of product " + thisProduct.productName + " is " + thisProduct.price);
        }

        if (!CheckProductComponentFracsValid(productData))
        {
            allChecks = false;
        }
        else
        {
            Debug.Log("Product component frac valid check okay.");
        }
        Assert.IsTrue(allChecks, "Product component check failed for code setup data.");

        /*HashSet<string> juiceNames = new HashSet<string>();
        juiceNames.Add("Blueberry");
        juiceNames.Add("Pineapple");

        HashSet<string> juiceNamesTwo = new HashSet<string>();
        juiceNamesTwo.Add("Pineapple");
        juiceNamesTwo.Add("Blueberry");
        juiceNamesTwo.Add("Green");

        if (juiceNames.SetEquals(juiceNamesTwo)) {
            Debug.Log("Equal Hash sets");
        } else {
            Debug.Log("Unequal hash sets");
        }*/

        //Debug.Log(SetupCommodities.ConvertTankNameToJuiceComponentNames("A/B/C"));

        /*foreach (string name in SetupCommodities.ConvertTankNameToJuiceComponentNames("A/B/C/D")) {
            Debug.Log("Hash name is: " + name);
            //Debug.Log("Converted Juice name is:" + SetupCommodities.ConvertTankLetterToJuiceName(name));
        }*/

    }

    List<RawMaterialData> StandardTestRawMaterialsCreate()
    {
        rawMaterialData = new List<RawMaterialData>();
        rawMaterialData.Add(new RawMaterialData(juiceName: Common.JuiceNames.Blueberry.ToString(), minPrice: 0.2f));
        rawMaterialData.Add(new RawMaterialData(juiceName: Common.JuiceNames.Pineapple.ToString(), minPrice: 0.2f));
        rawMaterialData.Add(new RawMaterialData(juiceName: Common.JuiceNames.Strawberry.ToString(), minPrice: 0.3f));
        rawMaterialData.Add(new RawMaterialData(juiceName: Common.JuiceNames.Orange.ToString(), minPrice: 0.3f));
        return rawMaterialData;
    }

    [Test]
    public void AllGameVariablesSaved()
    {
        // BindingFlags bindingFlags = BindingFlags.Public |
        //                         BindingFlags.NonPublic |
        //                         BindingFlags.Instance |
        //                         BindingFlags.Static;

        // foreach (FieldInfo field in typeof(MainScript).GetFields(bindingFlags))
        // {
        //     string fieldTypeString = field.FieldType.ToString().ToLower();
        //     if (!fieldTypeString.Contains("gameobject") && !fieldTypeString.Contains("material") && !fieldTypeString.Contains("chartobject") && !fieldTypeString.Contains("ienumerator"))
        //     {
        //         // Debug.Log(field.Name);
        //         // Debug.Log(field.FieldType);
        //         bool thisFieldIsInSaveData = false;
        //         foreach (FieldInfo fieldInSaveData in typeof(GameToResumeSaveData).GetFields(bindingFlags))
        //         {
        //             if (fieldInSaveData.Name == field.Name)
        //             {
        //                 thisFieldIsInSaveData = true;
        //                 //	Debug.Log("Field contained: " + field.Name.ToString());
        //             }
        //         }
        //         if (thisFieldIsInSaveData == false)
        //         {
        //             Debug.Log("Field not contained: " + field.Name.ToString());
        //         }
        //     }
        // }
    }

    [Test]
    public void GenOrders()
    {

        fulfilledBiasProductToOrder = new Dictionary<string, float>();

        bool allChecks = true;
        string failMsg = "";

        productData = new List<ProductData>();
        rawMaterialData = StandardTestRawMaterialsCreate();

        productData.Add(CreateOneProductSpecificValues(productName: "A/B", price: 1.2f, priceCap: 1.5f, priceMin: 0.8f, juiceSpecs: new List<JuiceSpecs> { new JuiceSpecs(Common.JuiceNames.Blueberry.ToString(), 0.2f), new JuiceSpecs(Common.JuiceNames.Pineapple.ToString(), 0.8f) }));
        productData.Add(CreateOneProductSpecificValues("B/C/D", 1.2f, 1.5f, 0.8f, new List<JuiceSpecs> { new JuiceSpecs(Common.JuiceNames.Pineapple.ToString(), 0.1f), new JuiceSpecs(Common.JuiceNames.Strawberry.ToString(), 0.8f), new JuiceSpecs(Common.JuiceNames.Orange.ToString(), 0.1f) }));
        productData.Add(CreateOneProductSpecificValues("C/D", 1.2f, 1.5f, 0.8f, new List<JuiceSpecs> { new JuiceSpecs(Common.JuiceNames.Strawberry.ToString(), 0.8f), new JuiceSpecs(Common.JuiceNames.Orange.ToString(), 0.2f) }));

        if (CheckProductComponentFracsValid(productData) == false)
        {
            allChecks = false;
            failMsg += "Product component check failed for test input data, not code, ";
        }

        orders = new List<Order>();

        MainScript mainScript = new MainScript();
        mainScript.orders = orders;
        mainScript.productData = productData;
        mainScript.rawMaterialData = rawMaterialData;

        Order newGenOrder = mainScript.orderFuncs.GenOrder(fulfilledBiasProductToOrder: fulfilledBiasProductToOrder, netAdvert: 20000);
        if (!(Mathf.Abs(fulfilledBiasProductToOrder["A/B"] - ((1f + Constants.Orders.orderBiasFractionBasedOnDeliveries) / productData.Count)) < 0.001f))
        {
            allChecks = false;
            failMsg += "Zeros orders, ";
        }

        orders.Add(SetOnePastOrder("A/B", false));
        newGenOrder = mainScript.orderFuncs.GenOrder(fulfilledBiasProductToOrder: fulfilledBiasProductToOrder, netAdvert: 20000);
        //Debug.Log("Test value for a/b is: " );
        if (!(Mathf.Abs(fulfilledBiasProductToOrder["A/B"] - ((1f + Constants.Orders.orderBiasFractionBasedOnDeliveries) / productData.Count)) < 0.001f))
        {
            allChecks = false;
            failMsg += "One order not fulfilled, ";
        }

        orders[0].fulfilled = true;

        newGenOrder = mainScript.orderFuncs.GenOrder(fulfilledBiasProductToOrder: fulfilledBiasProductToOrder, netAdvert: 20000);

        if (!(Mathf.Abs(fulfilledBiasProductToOrder["A/B"] - (1.0f / productData.Count + Constants.Orders.orderBiasFractionBasedOnDeliveries)) < 0.001f))
        {
            allChecks = false;
            failMsg += "One order fulfilled, ";
        }

        orders.Add(SetOnePastOrder("C/D", true));

        newGenOrder = mainScript.orderFuncs.GenOrder(fulfilledBiasProductToOrder: fulfilledBiasProductToOrder, netAdvert: 20000);

        if (!(Mathf.Abs(fulfilledBiasProductToOrder["A/B"] - (1.0f / productData.Count + Constants.Orders.orderBiasFractionBasedOnDeliveries / 2)) < 0.001f))
        {
            allChecks = false;
            failMsg += "Two orders fulfilled, ";
        }

        Assert.IsTrue(allChecks, failMsg);

    }

    [Test]
    public void GetRandomQuoteIndexNoRepeatOnLastFiveQuotes()
    {
        bool allChecks = true;

        int totalInQuoteSet = 10;
        List<int> quotationIndexesHistory = new List<int> { 2, 3, 7 };

        MainScript mainScript = new MainScript();
        mainScript.quoteIndexesHistory = quotationIndexesHistory;

        for (int i = 0; i < 100; i++)
        {

            int randomQuoteIndex = mainScript.quotationsObj.GetRandomQuoteIndexNoRepeatOnLastFiveQuotes(totalInQuoteSet);

            if (quotationIndexesHistory.Contains(randomQuoteIndex))
            {
                allChecks = false;
            }

            if (randomQuoteIndex >= 0 && randomQuoteIndex < totalInQuoteSet)
            {

            }
            else
            {
                allChecks = false;
            }

        }

        quotationIndexesHistory = new List<int> { 2, 3, 7, 5, 8 };
        mainScript.quoteIndexesHistory = quotationIndexesHistory;

        for (int i = 0; i < 100; i++)
        {

            int randomQuoteIndex = mainScript.quotationsObj.GetRandomQuoteIndexNoRepeatOnLastFiveQuotes(totalInQuoteSet);

            if (quotationIndexesHistory.Contains(randomQuoteIndex))
            {
                allChecks = false;
            }

            if (randomQuoteIndex >= 0 && randomQuoteIndex < totalInQuoteSet)
            {

            }
            else
            {
                allChecks = false;
            }

        }

        quotationIndexesHistory = new List<int> { 2, 3, 7, 5, 8, 4, 6, 1 };
        mainScript.quoteIndexesHistory = quotationIndexesHistory;

        for (int i = 0; i < 100; i++)
        {

            int randomQuoteIndex = mainScript.quotationsObj.GetRandomQuoteIndexNoRepeatOnLastFiveQuotes(totalInQuoteSet);

            if ((new List<int> { 5, 8, 4, 6, 1 }).Contains(randomQuoteIndex))
            {
                allChecks = false;
            }

            if (randomQuoteIndex >= 0 && randomQuoteIndex < totalInQuoteSet)
            {

            }
            else
            {
                allChecks = false;
            }

        }

        Assert.IsTrue(allChecks, "random quote index get failed.");
    }


    [Test]
    public void CheckFindProductNameInBiasedProductSet()
    {
        // Use the Assert class to test conditions.
        fulfilledBiasProductToOrder = new Dictionary<string, float>() { { "A/B", 0.4f }, { "C/D", 0.3f }, { "A/B/C", 0.4f } };
        bool allChecks = true;
        string failMsg = "";
        if (OrderFuncs.FindProductNameInBiasedProductSet(0.3f, fulfilledBiasProductToOrder) != "A/B")
        {
            allChecks = false;
            failMsg += "A/B, ";
        }

        if (OrderFuncs.FindProductNameInBiasedProductSet(0.6f, fulfilledBiasProductToOrder) != "C/D")
        {
            allChecks = false;
            failMsg += "C/D @ 0.6f, ";
        }

        if (OrderFuncs.FindProductNameInBiasedProductSet(0.7f, fulfilledBiasProductToOrder) != "C/D")
        {
            allChecks = false;
            failMsg += "C/D @ 0.7f, ";
        }

        if (OrderFuncs.FindProductNameInBiasedProductSet(0.8f, fulfilledBiasProductToOrder) != "A/B/C")
        {
            allChecks = false;
            failMsg += "A/B/C @ 0.8f, ";
        }

        if (OrderFuncs.FindProductNameInBiasedProductSet(1.2f, fulfilledBiasProductToOrder) != "")
        {
            allChecks = false;
            failMsg += "Empty String @ 1.2f, ";
        }
        Assert.AreEqual(true, allChecks, failMsg);
    }

    Order SetOnePastOrder(string productName, bool fulfilled)
    {
        Order newOrder = new Order();
        newOrder.productName = productName;
        newOrder.fulfilled = fulfilled;
        //	newOrder.accepted = fulfilled;
        return newOrder;
    }


    ProductData CreateOneProductSpecificValues(string productName, float price, float priceCap, float priceMin, List<JuiceSpecs> juiceSpecs)
    {
        ProductData newProduct = new ProductData(productName, price, priceCap, priceMin);
        newProduct.juiceSpecs = juiceSpecs;
        return newProduct;
    }

    [Test]
    public void SanityCheckOrderLostTimeGreaterPenaltyTime()
    {
        Debug.Log("Period for orders emotion status more than sad" + (int)Common.CustomerEmotionStatusThresholdInMin.Sad);
        Debug.Log("Period for penalty late delivery" + Constants.Timing.penaltyPeriodLateDeliverySecs);
        Assert.IsTrue((int)Common.CustomerEmotionStatusThresholdInMin.Sad * 60 > Constants.Timing.penaltyPeriodLateDeliverySecs);

    }

}
