﻿using UnityEngine;
using UnityEditor;
using UnityEngine.TestTools;
using NUnit.Framework;
using System.Collections;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public class LevelsFileHandlerTests
{
	int newHighestLevelPassed = 3;
    [Test]
    public void DeleteHighestLevelPassed()
    {
        LevelsFileHandler levelsFileHandler = new LevelsFileHandler();
        levelsFileHandler.ClearHighestLevelPassed();
    }

    [Test]
    public void SaveNewHighestLevel()
    {
        LevelsFileHandler levelsFileHandler = new LevelsFileHandler();
        levelsFileHandler.ClearHighestLevelPassed();

        if (!levelsFileHandler.SaveNewHighestLevelPassed(newHighestLevelPassed))
        {
            Assert.Fail("No new data saved. Could be that new passed level is not higher than previous, or new level not higher than 0.");
        }
    }

    [Test]
    public void LoadHighestLevelPassed()
    {
        // Use the Assert class to test conditions.
		SetHighestLevelPassed(newHighestLevelPassed);
        LevelsFileHandler levelPassedHandler = new LevelsFileHandler();
        levelPassedHandler.GetMaxLevelPassed();
		Assert.IsTrue(levelPassedHandler.GetMaxLevelPassed().maxLevelPassed == newHighestLevelPassed);
    }

	[Test]
	public void MaybeUpdateFilesLevelsPassedAndBadgeCredits() {
		TestStatus.testsAreRunning = true;
        MainScript mainScript = new MainScript();
		int newCurrentGameLevelPassed = 1;
		mainScript.currentGameLevel = newCurrentGameLevelPassed;
		new LevelsFileHandler().ClearHighestLevelPassed();
		mainScript.MaybeUpdateFilesLevelsPassedAndBadgeCredits();
		Assert.IsTrue(new LevelsFileHandler().GetMaxLevelPassed().maxLevelPassed == newCurrentGameLevelPassed);

		newCurrentGameLevelPassed = AllLevels.GetLevelsArray().Count;
		SetHighestLevelPassed(AllLevels.GetLevelsArray().Count);
		mainScript.MaybeUpdateFilesLevelsPassedAndBadgeCredits();
		Assert.IsTrue(new LevelsFileHandler().GetMaxLevelPassed().maxLevelPassed == newCurrentGameLevelPassed);

	}

    public void SetHighestLevelPassed(int levelPassed)
    { 
            BinaryFormatter bf = new BinaryFormatter();
            MaxLevelPassed maxLevelPassed = new MaxLevelPassed();
            maxLevelPassed.maxLevelPassed = levelPassed;
    
            FileStream file = File.Create(Application.persistentDataPath + LevelsFileHandler.highestLevelPassedFilePath);
            bf.Serialize(file, maxLevelPassed);
            file.Close();
    }

    // A UnityTest behaves like a coroutine in PlayMode
    // and allows you to yield null to skip a frame in EditMode

}
