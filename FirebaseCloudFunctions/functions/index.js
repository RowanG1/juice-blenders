const constants = require("./src/constants.js");
var url = require('url');
const functions = require('firebase-functions');
const admin = require('firebase-admin');
const classCodeFuncs = require('./src/classcodes');

admin.initializeApp();

exports.getNewClassCode = functions.https.onRequest((request, response) => {
  console.log("Server log:");
  if (request.headers.authorization && request.headers.authorization.startsWith('Bearer ')) {
    var idToken;
    var teacherID;
    idToken = request.headers.authorization.split('Bearer ')[1];

    admin.auth().verifyIdToken(idToken).then((decodedIdToken) => {
      teacherID = decodedIdToken.uid;
      return classCodeFuncs.getExistingClassCodes();
    }).then(classCodes => {
      console.log("Classcodes are: " + classCodes);
      var newClassCode = classCodeFuncs.generateRandomClassCodeNotInDatabase(classCodes);
      console.log("New class code is: " + newClassCode);
      var newClassCodeRecord = {};
      newClassCodeRecord[constants.classCode] = newClassCode;
      newClassCodeRecord[constants.createdByUserID] = teacherID;
      admin.database().ref(constants.classCodes).push(newClassCodeRecord);
      var classCodeObj = {};
      classCodeObj[constants.classCode] = newClassCode;
      var json = JSON.stringify(classCodeObj);
      response.send(json);
      // response.send("Hello from firebase");
      return null;
    }).catch(error => {
      console.error('Error while verifying Firebase ID token:', error);
      if (error.errorInfo) {
        console.error('Error info code is:', error.errorInfo.code);
        if (error.errorInfo.code === constants.authArgumentError) {
          console.log("Sending 403 unauthorized message to client");
          response.status(403).send('Unauthorized');
        }
      }

    });
  } else {
    console.log("No authorization token found");
    response.status(403).send('No authorization bearer');
  }
}
);

exports.getPupilsMatchingClassCode = functions.https.onRequest((request, response) => {
  var teacherID;
  var url_parts = url.parse(request.url, true);
  var query = url_parts.query;

  var classCode = query.classCode;
  console.log("Class code is: ");
  console.log(classCode);
  if (request.headers.authorization && request.headers.authorization.startsWith('Bearer ')) {
    var idToken;
    idToken = request.headers.authorization.split('Bearer ')[1];

    admin.auth().verifyIdToken(idToken).then((decodedIdToken) => {
      teacherID = decodedIdToken.uid;//request.body[constants.teacherID];
      return classCodeFuncs.getCreatorOfClassCode(classCode);
    }).then(createdByUserID => {
      if (createdByUserID === constants.classCodeNotExist) {
        response.send({ "message": constants.classCodeNotExist, "pupils": [] });
        throw new Error('Class code does not exist');
      }
      if (createdByUserID !== teacherID) {
        response.send({ "message": constants.teacherNotLinkedToClassCode, "pupils": [] });
        throw new Error('Teacher not linked to class code');
      }
      console.log("Class code created by user ID: " + createdByUserID);
      return classCodeFuncs.getPupilsMatchingClassCode(classCode);
    }).then(pupils => {
      if (pupils.length === 0) {
        response.send({ "message": constants.noPupilsMatchClassCode, "pupils": [] });
        throw new Error("No pupils found that match class code given by teacher");
      }
      console.log("First pupil name from get students function is: " + pupils[0].name);
      response.send({ "message": constants.pupilsFoundMatchingClassCode, "pupils": pupils });
      return;
    }).catch((error) => {
      console.error('Error while verifying Firebase ID token:', error);
      response.status(403).send('Unauthorized');
    });
  } else {
    console.log("No authorization token found");
    response.status(403).send('No authorization bearer');
  }

});

exports.gameCreated = functions.database.ref(constants.gameData + '/{gameID}').onCreate((snap, context) => {
  // Get an object representing the document
  // e.g. {'name': 'Marie', 'age': 66}

  var timestamp = admin.database.ServerValue.TIMESTAMP;
  
  console.log("Executing gameCreated function");
  return snap.ref.child('createdTimeStamp').set(timestamp);
});









