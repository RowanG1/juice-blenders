module.exports = Object.freeze({
    classCodes: "classCodes",
    classCode: "classCode",
    gameData: "gameData",
    createdByUserID: "createdByUserID",
    pupils: "pupils",
    name: "name",
    surname: "surname",
    highestLevelPassed: "highestLevelPassed",
    noPupilsMatchClassCode: "noPupilsMatchClassCode",
    teacherNotLinkedToClassCode: "teacherNotLinkedToClassCode",
    classCodeNotExist: "classCodeNotExist",
    userNotAuthorized: "userNotAuthorized",
    teacherID: "teacherID",
    pupilsFoundMatchingClassCode: "PupilsFoundMatchingClassCode",
    authArgumentError: "auth/argument-error",
    lengthClassCode: 8
});