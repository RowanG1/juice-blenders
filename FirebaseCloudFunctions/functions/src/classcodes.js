const admin = require('firebase-admin');
const constants = require('./constants.js');
const functions = require('firebase-functions');

module.exports = {
  generateRandomClassCode: function () {
    var alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    var builtCode = "";
    for (var i = 0; i < constants.lengthClassCode; i++) {
      var randomIndex = Math.floor(Math.random() * alphabet.length);
      builtCode += alphabet[randomIndex];
    }
    console.log("Generated random code : " + builtCode);
    return builtCode;
  },
  checkIfCodeIsInList: function (code, codeList) {
    if (codeList.includes(code)) {
      return true;
    }
    return false;
  },
  generateRandomClassCodeNotInDatabase: function (classCodesInDatabase) {
    var generatedCodeIsInDatabase = false;
    var newClassCode;
    do {
      newClassCode = this.generateRandomClassCode();
      generatedCodeIsInDatabase = this.checkIfCodeIsInList(newClassCode, classCodesInDatabase);
    } while (generatedCodeIsInDatabase === true);
    return newClassCode;
  },
  getCreatorOfClassCode: function (classCode) {
    return admin.database().ref(constants.classCodes).orderByChild(constants.classCode).equalTo(classCode).once("value").then(snapshot => {
      console.log("Creator of class codes snapshot: " + snapshot.val());
      if (snapshot.val() === null) {
        console.log("No class code matches class code submitted by teacher!");
        return constants.classCodeNotExist;
      }
      var keys = Object.keys(snapshot.val());
      if (keys.length === 1) {
        var key = keys[0];
        var createdByUserID = snapshot.val()[key][constants.createdByUserID];
       // if createdByUserID ===  firebase.auth.currentUser.uid;
        return createdByUserID;
      } else return constants.classCodeNotExist;
    });
  },
  getExistingClassCodes: function () {
    var output = [];

    return admin.database().ref(constants.classCodes).once('value').then(dataSnapshot => {
      // handle read data.
      console.log("Class codes received");
      dataSnapshot.forEach(snapItem => {
        console.log("For snapshot key: " + snapItem.key);
        var classCode = snapItem.child(constants.classCode).val();
        console.log("snapshot classcode value is: " + classCode);
        console.log("snapshot createdByUserID value is: " + snapItem.child(constants.createdByUserID).val());

        output.push(classCode);
      });
      return output;
    }).catch(error => {
      console.error(error);
      // response.error(500);
    });
  },
  getPupilsMatchingClassCode: function (classCode) {
    return admin.database().ref(constants.pupils).orderByChild(constants.classCode).equalTo(classCode).once("value").then(snapshot => {
      var output = [];

      snapshot.forEach(snapItem => {
        var pupilName = snapItem.val().name;
        var highestLevelPassed = snapItem.val().highestLevelPassed;
        var surname = snapItem.val().surname;

        var pupilToAdd = {};
        pupilToAdd[constants.name] = pupilName;
        pupilToAdd.highestLevelPassed = highestLevelPassed;
        pupilToAdd[constants.surname] = surname;

        output.push(pupilToAdd);
      });
      return output;
    });
  }
}