describe("classcodes", function () {
  var classCodeFuncs = require('./../../functions/src/classcodes.js');

  describe("GenerateRandomClassCode", function () {
    it("should generate non blank random code", function () {
      expect(!classCodeFuncs.generateRandomClassCode()).toBeFalsy();
      //demonstrates use of custom matcher
    });
  });

  describe("CheckIfCodeInList", function () {
    it("should confirm is code is in list", function () {
      var codeList = ["what12","screen4"];
      expect(classCodeFuncs.checkIfCodeIsInList("what12", codeList)).toBe(true);
      //demonstrates use of custom matcher
    });
  }); 

});
